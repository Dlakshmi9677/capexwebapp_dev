using CapexApp.commonconfig;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.ServiceFabric.Services.Runtime;
using System;
using System.Diagnostics;
using System.Diagnostics.Tracing;
using System.Threading;
using System.Threading.Tasks;

namespace CapexApp
{
    internal static class Program
    {
        private static String INSTRUMENTATIONKEY = "InstrumentationKey";

        /// <summary>
        /// This is the entry point of the service host process.
        /// </summary>
        private static void Main()
        {
            try
            {
                // The ServiceManifest.XML file defines one or more service type names.
                // Registering a service maps a service type name to a .NET type.
                // When Service Fabric creates an instance of this service type,
                // an instance of the class is created in this host process.


                ServiceEventListener listener = new ServiceEventListener("CapexApp");
                listener.EnableEvents(ServiceEventSource.Current, EventLevel.LogAlways, EventKeywords.All);

                ServiceRuntime.RegisterServiceAsync("CapexAppType",
                     context =>
                     {
                         TelemetryConfiguration.Active.InstrumentationKey = CommonConfiguration.GetConfig(context, INSTRUMENTATIONKEY);
                         return new global::CapexApp.CapexApp(context); 
                     });
                ServiceEventSource.Current.ServiceTypeRegistered(Process.GetCurrentProcess().Id, typeof(CapexApp).Name);

                // Prevents this host process from terminating so services keeps running. 
                Thread.Sleep(Timeout.Infinite);
            }
            catch (Exception e)
            {
                ServiceEventSource.Current.ServiceHostInitializationFailed(e.ToString());
                throw;
            }
        }
    }
}
