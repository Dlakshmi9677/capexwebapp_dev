using CapexApp.Models.ValidateJson;
using ICommonInterfaces;
using Microsoft.ServiceFabric.Services.Remoting.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapexApp.Models.Authorization
{
  /// <summary>
  /// This class is used to authenticate user
  /// </summary>
  public class AuthenticateUser
  {
    /// <summary>
    /// Authguard proxy refrence
    /// </summary>
    public IAuthGuard remoteservice;

    /// <summary>
    /// Authenticating user by proxy call to authguard ValidateToke
    /// </summary>
    /// <param name="token">Acess Token</param>
    /// <param name="tenant">Optional value, name of the tenant</param>
    /// <returns></returns>
    public async Task<bool> IsAuthorizedUser(string token, string tenant, string applicationName)
    {
      bool isAuthented = false;
      this.remoteservice = ServiceProxy.Create<IAuthGuard>(new Uri("fabric:/AuthguardApp/AuthguardApi"), listenerName: "Authguard");

      var response = await this.remoteservice.ValidateToken(token, "",applicationName);
      if (response != "" && response != null && response != "null")
      {
        if (CustomValdiateJson.IsValidJson(response))
        {
          var parsedObject = JObject.Parse(response);
          var isValid = parsedObject["valid"].ToString();
          if (isValid.ToLower() == "true")
          {
            isAuthented = true;
          }
        }
      }
      return await Task.FromResult(isAuthented);
    }
  }
}
