using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapexApp.Models.Authorization
{
  public class AuthenticationResult
  {
    public Exception Exception { get; set; }
    public object Data { get; set; }
  }
}
