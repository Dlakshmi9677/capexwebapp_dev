using CapexApp.Models.ValidateJson;
using ICommonInterfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.ServiceFabric.Services.Remoting.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CapexApp.Models.Authorization
{
  /// <summary>
  /// Common Authentication class to filter Controller Attribute Tagged as [AuthActionFilter] on top of Rest Api Methods
  /// </summary>
  public class AuthActionFilter : ActionFilterAttribute, IAsyncActionFilter
  {
    //private readonly string _parameterName;
    //public AuthActionFilter(string parameterName)
    //{
    //  _parameterName = parameterName;
    //}
   
    public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    { 
      bool isAuthenticated = false;
      int times = 3;
      int attempts = 0;
      var token = context.HttpContext.Request.Cookies["token"];
      var applicationName = context.HttpContext.Request.Cookies["dname_cookie"].Split('_')[1];
      if (token == null||token=="")
      {
        var host = context.HttpContext.Request.Host.Host.ToString();
        //this.CancellationContext(context);
        if (host != "localhost")
        {
          do
          {
            attempts++;
            token = context.HttpContext.Request.Cookies["token"];
          } while (attempts <= times);

          this.CancellationContext(context);
        }
        else
        {
          await next();
          return;
        }
      }
      else
      {
        AuthenticateUser authenticateUser = new AuthenticateUser();
        isAuthenticated = await authenticateUser.IsAuthorizedUser(token,"",applicationName);
        if (isAuthenticated)
        {
          var resultContext = await next();
          return;
        }
        else
        {
          this.CancellationContext(context);
        }
      }
    }
    public override void OnActionExecuted(ActionExecutedContext filterContext)
    {
      //Http401UnAuthorizedResult.HttpUnAuthorizedUser("OnActionExecuted", filterContext.RouteData,filterContext);
    }

    public override void OnResultExecuting(ResultExecutingContext filterContext)
    {
      //Http401UnAuthorizedResult.HttpUnAuthorizedUser("OnResultExecuting", filterContext.RouteData,filterContext);
    }

    public override void OnResultExecuted(ResultExecutedContext filterContext)
    {
      //Http401UnAuthorizedResult.HttpUnAuthorizedUser("OnResultExecuted", filterContext.RouteData,filterContext);
    }

    private void CancellationContext(dynamic context)
    {
      var testResult = new AuthenticationResult();
      testResult.Exception = new Exception("You don't have permission to access these resources");
      testResult.Data = "You don't have permission to access these resources:";
      testResult.Exception.Data.Add("Data", "Unauthorized");
      testResult.Exception.Data.Add("status", 401);
      context.Result = new AuthenticationActionResult(testResult);

      //context.Result= new Http401UnAuthorizedResult(401);
      //Http401UnAuthorizedResult.HttpUnAuthorizedUser("OnActionExecuting", context.RouteData, context);
    }
  }
}
