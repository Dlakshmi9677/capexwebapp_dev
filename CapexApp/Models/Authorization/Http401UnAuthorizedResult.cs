using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace CapexApp.Models.Authorization
{
  public class Http401UnAuthorizedResult : ActionResult
  {
    /// <summary>
    /// Http Authorization status code
    /// </summary>
    private readonly int _StatusCode;

    public Http401UnAuthorizedResult(int statusCode)
    {
      _StatusCode = statusCode;
    }

    /// <summary>
    /// On Action Method cancellation,short-circuit the Action and send response to client
    /// </summary>
    /// <param name="context"></param>
    /// <returns>Return Result of the context to client</returns>
    public async override Task ExecuteResultAsync(ActionContext context)
    {
      // Set the response code to _StatusCode.
      context.HttpContext.Response.StatusCode = _StatusCode;
    }

    /// <summary>
    /// Unauthorized Message to Client
    /// </summary>
    /// <param name="methodName"></param>
    /// <param name="routeData"></param>
    /// <param name="filterContext"></param>
    public static void HttpUnAuthorizedUser(string methodName, RouteData routeData, dynamic filterContext)
    {
      var controllerName = routeData.Values["controller"];
      var actionName = routeData.Values["action"];
      var message = String.Format("{0} controller:{1} action:{2}", methodName, controllerName, actionName);
      Debug.WriteLine(message, "Action Filter Log");

      filterContext.Result = new RedirectResult("~/Home/UnAuthorizedPage");
      filterContext.HttpContext.Response.StatusCode = 401;

      //filterContext.Result = new ViewResult
      //{
      //  ViewName = "~/Views/Home/UnauthorizedPage.cshtml"
      //};
    }
  }
}
