using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapexApp.Models.ValidateJson
{
  /// <summary>
  /// This is used to validate Json Object.
  /// </summary>
  public class CustomValdiateJson
  {
    /// <summary>
    /// To Check whether passed string is a valid json or not
    /// </summary>
    /// <param name="strInput">Take either JsonArray String Or JsonObject String or String value as input argument</param>
    /// <returns></returns>
    public static bool IsValidJson(string strInput)
    {
      strInput = strInput.Trim();
      if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
          (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
      {
        try
        {
          var obj = JToken.Parse(strInput);
          return true;
        }
        catch (JsonReaderException jex)
        {
          //Exception in parsing json
          Console.WriteLine(jex.Message);
          return false;
        }
        catch (Exception ex) //some other exception
        {
          Console.WriteLine(ex.ToString());
          return false;
        }
      }
      else
      {
        return false;
      }
    }
  }
}
