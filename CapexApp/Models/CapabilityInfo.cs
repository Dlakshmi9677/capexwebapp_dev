﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapexApp.Models
{
    public class CapabilityInfo
    {
        public String CapabilityId;
        public String Name;
        public List<RoleInfo> Role;
    }
}
