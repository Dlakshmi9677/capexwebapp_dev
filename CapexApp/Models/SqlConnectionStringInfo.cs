﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapexApp.Models
{
    public class SqlConnectionStringInfo
    {
        public string TenantId { get; set; }
        public string TenantName { get; set; }
        public string ConnectionStringType { get; set; }
    }
}
