using CapexApp.commonconfig;
using CapexApp.Ignite;
using CapexApp.Models;
using ICommonInterfaces;
using ICommonInterfaces.Model;
using Microsoft.ApplicationInsights;
using Microsoft.ServiceFabric.Services.Remoting.Client;
using Models.Ignite;
using Models.common.FDC;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Fabric;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ICommonInterfaces.KeyvaultService;

namespace CapexApp.Util
{
  public class IgniteClient
  {

    public static string igniteip = null;
    private readonly IgniteThinClientService _igniteThinClientService = null;
    public KeyvaultServiceConnectionStringApi keyvaultServiceConnectionStringApi;

    public IgniteClient()
    {
      //igniteip = CommonConfiguration.GetConfiguration("Ignite");
      _igniteThinClientService = new IgniteThinClientService(CommonConfiguration.GetConfiguration("endpoint"));
      keyvaultServiceConnectionStringApi = new KeyvaultServiceConnectionStringApi();

    }

    public async Task<string> GetTenantIDAsync(string tenantName,string applicationName)
    {
      string tenantId = null;
      TelemetryClient tc = new TelemetryClient();
      //try
      //{
      //  tc.TrackEvent("==> inside GetTenantID()");
      //  string cacheName = "TenantDetails";
      //  string url = igniteip + "/Tenantservice/read?key=" + tenantName + "&cacheName=" + cacheName;
      //  tc.TrackEvent("==> inside GetTenantID() ... url :"+ url);
      //  tenantId = IgniteCustomRestApi(url);
      //}
      //catch (Exception ex)
      //{
      //  tc.TrackEvent("==> inside GetTenantID() exception block... tenantName :" + tenantName);
      //  var authguardService = ServiceProxy.Create<IAuthGuard>(new Uri("fabric:/AuthguardApp/AuthguardApi"), listenerName: "Authguard");
      //  tenantId = await authguardService.GetTenantId(tenantName);
      //}
      var msgInfo = new JObject();
      msgInfo.Add("Operation", "GetTenantId");
      msgInfo.Add("tenant", tenantName);
      msgInfo.Add("ApplicationName", applicationName);

      var client = ServiceProxy.Create<ICommunication>(new Uri("fabric:/KeyVaultServiceApp/KeyVaultServiceApi"), listenerName: "Remoting Listener");
      tenantId = await client.OnRouteMessageaAsync(JsonConvert.SerializeObject(msgInfo));
      // tenantId = "C091E548-B45A-49B4-B8EC-2CB5E27C7AF6"; // for testing purposes, I have hardcoded "V2RDEMO" tenant Id.
      return tenantId;
    }

    public async Task<string> GetLockDateFromField(string EntityId, string TenantId, string TenantName)
    {
      string fieldData = null;
      try
      {
        var igniteTableName = FDCEntityTypesUtil.GetIgniteTableName("Field", "Config");
        string query = "select _DATALOCKDATE from " + igniteTableName + " where EntityId='" + EntityId + "' order by _ProductionDay desc LIMIT 1";
        var data = await this._igniteThinClientService.QueryConfigRecordAllAsync(TenantId, query, "", false);
        if (!string.IsNullOrEmpty(data) && data != "[]")
        {
          DateTime date;
          var jobj = JArray.Parse(data);
          date = Convert.ToDateTime(jobj[0][0].ToString());
          var DataLockDate = date.ToString("MM/dd/yyyy");
          var annonymusObj = new
          {
            DataLockDate = DataLockDate,
          };
          fieldData = JsonConvert.SerializeObject(annonymusObj);
        }
      }
      catch (Exception ex)
      {
        throw;
      }
      return fieldData;
    }
    public async Task<string> GetLicenseeDataFromThinClient(string EntityId, string EntityType, string TenantId, string CapabilityId)
    {
      string licenseeData = null;
      try
      {
        var igniteTableName = FDCEntityTypesUtil.GetIgniteTableName(EntityType, "Config");
        string query = "select Licensee,LicenseeId from " + igniteTableName + " where EntityId='" + EntityId + "' order by _ProductionDay desc LIMIT 1";
        var data = await this._igniteThinClientService.QueryConfigRecordAsync(TenantId + "_" + CapabilityId, query, "", false);
        if (!string.IsNullOrEmpty(data) && data != "[]")
        {
          licenseeData = data;
        }
      }
      catch (Exception ex)
      {
        throw;
      }
      return licenseeData;
    }
    public async Task<bool> CheckForFDCLocationProductionDayData(string TenantId, string EntityId, string EntityType, string TenantName)
    {
      bool IsDataExists = false;
      try
      {
        var igniteTableName = FDCEntityTypesUtil.GetIgniteTableName(EntityType, "Prod");
        string thinClientQuery = "select count(*) from " + igniteTableName + " where EntityId ='" + EntityId + "'";
        string productionData = await this._igniteThinClientService.QueryConfigRecordAllAsync(TenantId, thinClientQuery, "", false);//IgniteCustomRestApi(readurl);
        if (productionData != null && productionData != "null" && productionData != "[]")
        {
          IsDataExists = true;
        }
      }
      catch (Exception ex)
      {
        throw;
      }
      return IsDataExists;
    }
    public async Task<DateTime> GetTimeZoneForParticularEntity(string EntityId, string EntityType, string TenantId, string TenantName)
    {
      DateTime TodayProductionDay = new DateTime();
      try
      {
        var IgniteQuery = "";
        var DataBaseQuery = "";
        var TableName = "FDC" + EntityType + "ConfigRecords";
        var DistrictTableName = "FDCDistrictConfigRecords";
        switch (EntityType)
        {
          case "District":
            IgniteQuery = "select Top(1) TimeZone from \"" + TenantId + "_" + TableName + "\" where EntityId ='" + EntityId + "'";
            DataBaseQuery = "select TimeZone from " + TableName + " where EntityId ='" + EntityId + "'";
            break;
          default:
            IgniteQuery = "select Top(1) \"" + TenantId + "_" + DistrictTableName + "\".TimeZone from \"" + TenantId + "_" + DistrictTableName + "\", \"" + TenantId + "_" + TableName + "\" where \"" + TenantId + "_" + TableName + "\".EntityId ='" + EntityId + "' and \"" + TenantId + "_" + TableName + "\".DistrictId=\"" + TenantId + "_" + DistrictTableName + "\".EntityId";
            DataBaseQuery = "select Top(1) " + DistrictTableName + ".TimeZone from " + DistrictTableName + "," + TableName + " where " + TableName + ".EntityId ='" + EntityId + "' and " + TableName + ".DistrictId=" + DistrictTableName + ".EntityId";
            break;
        }
        string readurl = String.Format("{0}{1}?cacheName={2}&query={3}", igniteip, "/service/query", TenantId + "_" + TableName, IgniteQuery);

        string adminData = IgniteCustomRestApi(readurl);
        string timeZone = "";
        if (string.IsNullOrEmpty(adminData) || adminData == "[]")
        {
          string applicationName = (CommonConfiguration.GetConfiguration("ApplicationName") + CommonConfiguration.GetConfiguration("ApplicationPostFix")).ToLower(); //(energy+app)
          string connectionStringType = CommonConfiguration.GetConfiguration("ConnectionStringType");

          string althingConnectionString = await keyvaultServiceConnectionStringApi.GetConnectionStringFromKeyVault(TenantId, applicationName, connectionStringType);
          using (SqlConnection conn = new SqlConnection(althingConnectionString))
          {
            using (SqlCommand cmd = new SqlCommand(DataBaseQuery, conn))
            {
              conn.Open();
              using (SqlDataReader reader = cmd.ExecuteReader())
              {

                while (reader.Read())
                {
                  timeZone = reader["TimeZone"].ToString();
                }
              }
            }
          }
        }
        else
        {
          var jarrayflattenData = JArray.Parse(adminData);
          if (jarrayflattenData.Count > 0)
          {
            timeZone = jarrayflattenData[0].ToString();
          }
        }
        if (timeZone != null && timeZone != "")
        {
          int start = timeZone.IndexOf("GMT");
          int end = timeZone.IndexOf(")");
          start = start + 4;
          string timeDifferenceToServer = timeZone.Substring(start, end - start);
          DateTime entityDateTime = DateTime.Parse(timeDifferenceToServer);
          DateTime serverDateTime = DateTime.UtcNow;
          DateTime sDateTime = DateTime.Parse("0:00");
          TimeSpan timeDiff = TimeSpan.Zero;
          char sign = timeZone[start - 1];
          if (sign == '+')
          {
            timeDiff = entityDateTime - sDateTime;
          }
          else
          {
            timeDiff = sDateTime - entityDateTime;
          }
          DateTime entityTime = serverDateTime.AddMilliseconds(timeDiff.TotalMilliseconds);
          DateTime entityProductiondateTime = new DateTime(entityTime.Year, entityTime.Month, entityTime.Day, 8, 0, 0, 0);

          if (entityTime < entityProductiondateTime)
          {
            TodayProductionDay = entityProductiondateTime.AddDays(-2);
          }
          else
          {
            TodayProductionDay = entityProductiondateTime.AddDays(-1);
          }
        }
      }
      catch (Exception ex)
      {
        throw;
      }
      return TodayProductionDay;
    }

    public async Task<JObject> GetUserIdAndAdminPermission(string username, string tenantName, string tenantId,string applicationName)
    {
      JObject userDetails = null;
      try
      {

        userDetails = await this._igniteThinClientService.GetUserDetailsAsync(tenantId, username);
        if (userDetails == null)
        {
          var authguardService = ServiceProxy.Create<IAuthGuard>(new Uri("fabric:/AuthguardApp/AuthguardApi"), listenerName: "Authguard");
          string result = await authguardService.GetUserId(username, tenantName, applicationName);
          userDetails = JObject.Parse(result);

        }
      }
      catch (Exception ex)
      {
        var authguardService = ServiceProxy.Create<IAuthGuard>(new Uri("fabric:/AuthguardApp/AuthguardApi"), listenerName: "Authguard");
        string result = await authguardService.GetUserId(username, tenantName,applicationName);
        userDetails = JObject.Parse(result);

      }

      return userDetails;
    }

    public JObject GetUserId(string userEmail, string tenantName, string tenantId)
    {
      string UserId = null;
      JObject userDetails = new JObject();
      try
      {

        string cacheName = "TenantDetails";

        string userIdurl = igniteip + "/Tenantservice/userId?email=" + userEmail + "&cacheName=" + cacheName + "&tenantName=" + tenantName;

        UserId = IgniteCustomRestApi(userIdurl);
        string userJsonUrl = igniteip + "/service/entities?key=" + UserId.Trim() + "&cacheName=" + tenantId + "&type=Permissions";
        string userPermissions = IgniteCustomRestApi(userJsonUrl);

        userDetails["UserId"] = UserId.Trim();
        userDetails["TenantId"] = tenantId;
        userDetails["IsAdmin"] = false;
        userDetails["CapabilityAdminList"] = new JArray();
        userDetails["Capabilities"] = new JArray();

        if (userPermissions != null && userPermissions != "" && userPermissions != "null")
        {
          JObject userJobj = JObject.Parse(userPermissions);

          if (userJobj["Roles"] != null)
          {
            bool isAdmin = userJobj["Roles"]["isAdmin"] != null ? userJobj["Roles"]["isAdmin"].ToObject<Boolean>() : false;
            var capAdminList = userJobj["Roles"]["CapabilityAdminList"] != null ? (JArray)userJobj["Roles"]["CapabilityAdminList"] : new JArray();
            var capabilitiesArray = userJobj["Roles"]["Capabilities"] != null ? (JArray)userJobj["Roles"]["Capabilities"] : new JArray();

            userDetails["IsAdmin"] = isAdmin;
            userDetails["CapabilityAdminList"] = capAdminList;
            userDetails["Capabilities"] = capabilitiesArray;

          }
        }

      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);

      }

      return userDetails;
    }
    /// <summary>
    ///
    /// </summary>
    /// <param name="url"></param>
    /// <returns>result</returns>
    public string IgniteCustomRestApi(string url)
    {
      string result = "";
      TelemetryClient tc = new TelemetryClient();

      HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
      req.Method = "GET";
      HttpWebResponse GETResponse;
      try
      {
        using (GETResponse = (HttpWebResponse)req.GetResponse())
        {
          var responseValue = string.Empty;
          if (GETResponse.StatusCode != HttpStatusCode.OK)
          {
            var message = String.Format("Request failed. Received HTTP {0}", GETResponse.StatusCode);
            throw new ApplicationException(message);
          }
          Stream getResponseStream = GETResponse.GetResponseStream();
          if (getResponseStream != null)
          {
            using (var sr = new StreamReader(getResponseStream))
            {
              //read the response
              result = sr.ReadToEnd();
            }
          }
        }
      }
      catch (Exception ex)
      {
        tc.TrackTrace("IgniteCustomRestApi catch" + ex.Message);
        tc.TrackException(ex);
        tc.Context.Device.OperatingSystem = Environment.OSVersion.ToString();
        throw;
      }
      return result;
    }

    public string GetSearchItems(string term, String tenantName)
    {
      string result = "";
      String url = igniteip + "/service/search?encodedTerm=" + term + "*" + "&cacheName=" + tenantName;
      HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
      req.Method = "GET";
      HttpWebResponse GETResponse;
      try
      {
        using (GETResponse = (HttpWebResponse)req.GetResponse())
        {
          var responseValue = string.Empty;
          if (GETResponse.StatusCode != HttpStatusCode.OK)
          {
            var message = String.Format("Request failed. Received HTTP {0}", GETResponse.StatusCode);
            throw new ApplicationException(message);
          }
          Stream getResponseStream = GETResponse.GetResponseStream();
          if (getResponseStream != null)
          {
            using (var sr = new StreamReader(getResponseStream))
            {
              //read the response
              result = sr.ReadToEnd();
            }
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);

      }
      return result;
    }

    public async Task<string> GetUserDetailsAsync(string tenantName, string userEmail,string applicationName)
    {
      string result = null;
      string tenantId = null;
      TelemetryClient tc = new TelemetryClient();
      try
      {
        tc.TrackEvent("==> inside GetUserDetailsAsync()");
        tenantId = await GetTenantIDAsync(tenantName,applicationName);
        var userDetailObject = await GetUserIdAndAdminPermission(userEmail, tenantName, tenantId,applicationName);
        //var userDetailObject = JObject.Parse(userDetails);
        string userId = userDetailObject["userId"].ToString();

        if (!string.IsNullOrEmpty(userId))
        {

          JObject userProperties = new JObject();
          userProperties["UserId"] = userId.Trim();
          userProperties["TenantId"] = tenantId;
          //if (userDetailObject["isAdmin"].ToString() != "")
          //{
          //  userProperties["IsAdmin"] = userDetailObject["isAdmin"].ToObject<Boolean>();
          //}
          //else
          //{
          //  throw new Exception("IsAdmin is coming blank string from Ignite GetUserIdAndAdminPermission(),can't convert into boolean,String was not recognized as a valid Boolean");
          //}
          userProperties["CapabilityAdminList"] = new JArray();
          userProperties["Capabilities"] = new JArray();

          //get capabilities for this user  call authorization service
          MessageWrapper message = new MessageWrapper();
          message.EntityID = "usercapabilities";
          message.UserID = userId;
          message.TenantID = tenantId;
          var client = ServiceProxy.Create<ICommunication>(new Uri("fabric:/AuthorizationApp/AuthorizationApi"), listenerName: "Authorization");
          result = await client.ReadAsync(message);
          JObject capabilities = JObject.Parse(result);
          userProperties["Capabilities"] = capabilities["capabilities"];
          userProperties["AdminCapabilities"] = capabilities["admincapabilities"];
          userProperties["SecurityGroups"] = capabilities["securitygroups"] != null ? capabilities["securitygroups"] : new JArray();
          result = userProperties.ToString(Formatting.None);
        }
        else
        {
          tc.TrackException(new Exception("USER ID IS NULL/EMPTY. Email=" + userEmail + ". TenantName= " + tenantName + " .Tenant Id=" + tenantId));
        }
      }
      catch (Exception ex)
      {
        var prop = new Dictionary<string, string>();
        prop["tenantId"] = tenantId;
        prop["tenantName"] = tenantName;
        prop["Email"] = userEmail;
        tc.TrackException(ex, prop);
        throw ex;
      }
      return result;
    }


    public string GetCapabilitiesList(string entityId, string tenantId)
    {
      string result = "";
      String url = igniteip + "/service/capabilities?key=" + entityId + "&cacheName=" + tenantId;
      HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
      req.Method = "GET";
      HttpWebResponse GETResponse;
      try
      {
        using (GETResponse = (HttpWebResponse)req.GetResponse())
        {
          var responseValue = string.Empty;
          if (GETResponse.StatusCode != HttpStatusCode.OK)
          {
            var message = String.Format("Request failed. Received HTTP {0}", GETResponse.StatusCode);
            throw new ApplicationException(message);
          }
          Stream getResponseStream = GETResponse.GetResponseStream();
          if (getResponseStream != null)
          {
            using (var sr = new StreamReader(getResponseStream))
            {
              //read the response
              result = sr.ReadToEnd();
            }
          }
        }
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);

      }
      return result;
    }

    /// <summary>
    /// Get the capabilities wich are subscribed to the tenant  and user level UserDetails
    /// </summary>
    /// <param name="tenantName"></param>
    /// <param name="userName"></param>
    /// <returns></returns>
    public async Task<string> GetUserDetails(string tenantName, string userEmail,string applicationName)
    {
      string cacheName = "Capabilities";
      string tenantId = GetTenantIDAsync(tenantName,applicationName).ToString();
      var userDetailObject = await GetUserIdAndAdminPermission(userEmail, tenantName, tenantId,applicationName);
      //var userDetailObject = JObject.Parse(userDetails);
      string userId = userDetailObject["userId"].ToString();

      JObject userProperties = new JObject();
      userProperties["UserId"] = userId.Trim();
      userProperties["TenantId"] = tenantId;
      // userProperties["IsAdmin"] = userDetailObject["isAdmin"].ToObject<Boolean>();
      userProperties["CapabilityAdminList"] = new JArray();
      userProperties["Capabilities"] = new JArray();

      //JObject userDetails = GetUserProperties(userEmail, tenantName, tenantId);
      string result = "";

      string query = "select capabilityid, name, status from capabilities where tenantid = '" + tenantId + "'";
      string RoleQueryUrl = string.Format("{0}{1}?cacheName={2}&query={3}", igniteip, "/service/query", cacheName, query);

      HttpWebRequest req = (HttpWebRequest)WebRequest.Create(RoleQueryUrl);
      req.Method = "GET";
      HttpWebResponse GETResponse;

      List<JObject> capabilitiesSchemaList = new List<JObject>();

      try
      {
        using (GETResponse = (HttpWebResponse)req.GetResponse())
        {
          var responseValue = string.Empty;
          if (GETResponse.StatusCode != HttpStatusCode.OK)
          {
            var message = String.Format("Request failed. Received HTTP {0}", GETResponse.StatusCode);
            throw new ApplicationException(message);
          }
          Stream getResponseStream = GETResponse.GetResponseStream();
          if (getResponseStream != null)
          {
            using (var sr = new StreamReader(getResponseStream))
            {
              //read the response
              result = sr.ReadToEnd();
              JArray capabilitiesAsJson = JArray.Parse(result);
              foreach (var sGroup in capabilitiesAsJson)
              {
                dynamic jsonObject = new JObject();
                jsonObject.Name = sGroup[1].ToString();
                jsonObject.Status = sGroup[2].ToString();
                jsonObject.CapabilityId = sGroup[0].ToString();
                capabilitiesSchemaList.Add(jsonObject);
              }

            }
          }
        }

        List<JObject> tenantCapabilitiesList = new List<JObject>();
        List<JObject> userCapabilitiesNameList = null;
        JObject dmsNameIfON = null;

        //capabilities schema of the tenant
        capabilitiesSchemaList.ForEach(capObj =>
        {
          //check the fabric status 
          if (capObj["Status"].ToString() == "ON" || capObj["Status"].ToString() == "MARKETING")
          {
            var capabilityObj = new JObject();
            capabilityObj["Name"] = capObj["Name"].ToString();
            capabilityObj["Status"] = capObj["Status"].ToString();
            capabilityObj["CapabilityId"] = capObj["CapabilityId"].ToString();

            tenantCapabilitiesList.Add(capabilityObj);
            if (capObj["Name"].ToString() == "Document Management System")
            {
              dmsNameIfON = new JObject();
              dmsNameIfON["Name"] = capObj["Name"].ToString();
              dmsNameIfON["Status"] = capObj["Status"].ToString();
              dmsNameIfON["CapabilityId"] = capObj["CapabilityId"].ToString();

            }
          }
        });

        if (!userProperties["IsAdmin"].ToObject<Boolean>())
        {
          List<string> userAdminCapabilities = ((JArray)userProperties["CapabilityAdminList"]).ToObject<List<string>>();
          List<string> usergeneralCapabilities = ((JArray)userProperties["Capabilities"]).ToObject<List<string>>();
          List<string> userGenAdmincapabilities = userAdminCapabilities.Union(usergeneralCapabilities).ToList<string>();

          //GET Only tenant subscribed ,for safety purpose
          userCapabilitiesNameList = tenantCapabilitiesList.Where(fObj => userGenAdmincapabilities.Any(uFabricName => fObj["Name"].ToString() == uFabricName)).ToList();
          if (dmsNameIfON != null)
          {
            userCapabilitiesNameList.Add(dmsNameIfON);
          }
          userProperties["Capabilities"] = JArray.FromObject(userCapabilitiesNameList);
        }
        else
        {
          userProperties["Capabilities"] = JArray.FromObject(tenantCapabilitiesList);
        }
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
      result = userProperties.ToString(Formatting.None);
      return result;
    }

    public string GetUserCapabilitiesRolePermissions(string userId, string tenantId, string capabilityId)
    {

      UserPermissions permissions = new UserPermissions { Read = false, Write = false, Delete = false, AddParticipants = false, RemoveParticipants = false };
      string url = igniteip + "/service/entities?key=" + userId + "&cacheName=" + tenantId + "&type=Permissions";
      string result = IgniteCustomRestApi(url);
      List<string> roleIdList = new List<string>();
      if (result != "null" && result != null)
      {
        var permissionObj = JObject.Parse(result);
        JArray sGroupsjarray = (JArray)permissionObj["SecurityGroups"];
        var rolesJObject = permissionObj["Roles"];
        List<SecurityGroups> sGroups = sGroupsjarray.ToObject<List<SecurityGroups>>();
        var groupId = "";
        foreach (var sGroup in sGroups)
        {
          groupId = groupId + "'" + sGroup.GroupId + "',";
        }
        if (groupId != "")
        {
          groupId = groupId.TrimEnd(',').Trim();
          var query = "select EntityInfoJson from Entity where EntityType ='SecurityGroup' and EntityId in(" + groupId + ")";
          var groupUrl = igniteip + "/service/query?cacheName=" + tenantId + "&query=" + query;
          var resultSet = IgniteCustomRestApi(groupUrl);
          if (resultSet != "[]" || resultSet != "")
          {
            List<List<string>> myDeserializedObjList =
                                  JsonConvert.DeserializeObject<List<List<string>>>(resultSet);
            foreach (var innerList in myDeserializedObjList)
            {
              if (innerList.Count == 1)
              {
                var data = innerList.ElementAt(0);
                var objJson = JObject.Parse(data.ToString());
                if (objJson["Details"] != null)
                {
                  var objDetails = objJson["Details"];
                  var obj = JObject.Parse(objDetails.ToString());
                  JObject capRole = (JObject)obj["Capability_Role"];
                  if (capRole[capabilityId] != null)
                  {
                    //all roles in that capability
                    JArray roleArray = (JArray)capRole[capabilityId];
                    roleIdList = roleArray.ToObject<List<string>>();
                    foreach (var roleId in roleIdList)
                    {
                      var readUrl = igniteip + "/service/entities?&key=" + roleId + "&cacheName=" + tenantId + "&type=Details";
                      var roleResult = IgniteCustomRestApi(readUrl);
                      if (roleResult != null && roleResult != "null" && roleResult != "")
                      {
                        JObject roleObj = JObject.Parse(roleResult);
                        permissions.Read = roleObj["Read"]["Value"].ToObject<bool>();
                        permissions.Write = roleObj["Write"]["Value"].ToObject<bool>();
                        permissions.Delete = roleObj["Write"]["Value"].ToObject<bool>();
                        permissions.AddParticipants = roleObj["AddParticipants"]["Value"].ToObject<bool>();
                        permissions.RemoveParticipants = roleObj["RemoveParticipants"]["Value"].ToObject<bool>();

                      }
                    }

                  }

                }
              }
            }

          }

        }
        if (rolesJObject != null)//checking for individual users also in role instead of Sgroups only
        {
          var rolesJarray = rolesJObject[capabilityId] != null ? (JArray)rolesJObject[capabilityId] : null;
          if (rolesJarray != null)
          {
            List<string> roleList = rolesJarray.ToObject<List<string>>();
            roleList.ForEach(roleId =>
            {
              var readUrl = igniteip + "/service/entities?&key=" + roleId + "&cacheName=" + tenantId + "&type=Details";
              var roleResult = IgniteCustomRestApi(readUrl);
              JObject roleObj = JObject.Parse(roleResult);
              permissions.Read = roleObj["Read"]["Value"].ToObject<bool>();
              permissions.Write = roleObj["Write"]["Value"].ToObject<bool>();
              permissions.Delete = roleObj["Write"]["Value"].ToObject<bool>();
            });
          }


        }

      }
      // permissions.Write = true;
      // permissions.Delete = true;
      return JsonConvert.SerializeObject(permissions);
    }
    /// <summary>
    /// This method will check the users in the entityjson,and is user exists ,then fetch the capabilities roles for that user 
    /// </summary>
    /// <param name="userId">current login user userId</param>
    /// <param name="tenantId">TenantId for that login user</param>
    /// <param name="entityId">EntityId which user performing operations</param>
    /// <returns>if users not found return "[]" otherwise {"capId1":{"write":boolean,"Read":boolean,"Delete":boolean},"capId2":{"write":boolean,"Read":boolean,"Delete":boolean}}</returns>
    public string GetUserEntityPermissions(string userId, string tenantId, string entityId)
    {
      try
      {
        UserPermissions permissions = new UserPermissions { Read = false, Write = false, Delete = false };
        string url = igniteip + "/service/entities?key=" + entityId + "&cacheName=" + tenantId + "&type=Users";
        string EntityUsersAsJsonString = IgniteCustomRestApi(url);
        if (EntityUsersAsJsonString != "null" && EntityUsersAsJsonString != null)
        {
          JObject entityUsers = JObject.Parse(EntityUsersAsJsonString);
          var capabilityRoles = entityUsers[userId] != null ? entityUsers[userId].ToString() : "[]";
          if (capabilityRoles != "[]")
          {
            JObject capabilityRolesJobj = JObject.Parse(capabilityRoles);
            Dictionary<string, List<string>> capRoleDictionary = capabilityRolesJobj.ToObject<Dictionary<string, List<string>>>();
            foreach (var entry in capRoleDictionary)
            {
              JArray roleArray = (JArray)capabilityRolesJobj[entry.Key];

              List<string> roleIdList = roleArray.ToObject<List<string>>();
              roleIdList.ForEach(roleId =>
              {

                var readUrl = igniteip + "/service/entities?&key=" + roleId + "&cacheName=" + tenantId + "&type=Details";
                var roleResult = IgniteCustomRestApi(readUrl);
                if (roleResult != "null")
                {
                  JObject roleObj = JObject.Parse(roleResult);
                  permissions.Read = roleObj["Read"]["Value"].ToObject<bool>();
                  permissions.Write = roleObj["Write"]["Value"].ToObject<bool>();
                  permissions.Delete = roleObj["Write"]["Value"].ToObject<bool>();
                }

              });
              capabilityRolesJobj[entry.Key] = JObject.FromObject(permissions);
            }

            return capabilityRolesJobj.ToString();
          }
          else
            return "[]";
        }
        else
        {
          return "[]";
        }
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
    }
    /// <summary>
    /// 
    /// 
    /// </summary>
    /// <param name="userId">current login user userId</param>
    /// <param name="tenantId">TenantId for that login user</param>
    /// <param name="entityId">EntityId which user performing operations</param>
    /// <returns></returns>
    public string GetEntityPermissions(string userId, string tenantId, string entityId)
    {
      try
      {
        UserPermissions permissions = new UserPermissions { Read = false, Write = false, Delete = false };
        string url = igniteip + "/service/entities?key=" + entityId + "&cacheName=" + tenantId + "&type=Permissions";
        string EntityPermissions = IgniteCustomRestApi(url);
        if (EntityPermissions != "null" && EntityPermissions != null)
        {
          JObject EntityPermissionsJobj = JObject.Parse(EntityPermissions);
          JArray usersJarray = (JArray)EntityPermissionsJobj["Users"];
          List<string> usersList = usersJarray.ToObject<List<string>>();
          if (usersList.Contains(userId))
          {
            return GetUserPermissions(userId, tenantId);
          }

          else
          {
            return "[]";
          }
        }
        else
        {
          return "[]";
        }
      }

      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
    }
    public string GetUserPermissions(string userId, string tenantId)
    {

      UserPermissions permissions = new UserPermissions { Read = false, Write = false, Delete = false };
      string url = igniteip + "/service/entities?key=" + userId + "&cacheName=" + tenantId + "&type=Permissions";
      string result = IgniteCustomRestApi(url);
      List<string> roleIdList = new List<string>();
      Dictionary<string, UserPermissions> capPermissionsDic = new Dictionary<string, UserPermissions>();
      if (result != "null" && result != null)
      {
        var permissionObj = JObject.Parse(result);
        JArray sGroupsjarray = (JArray)permissionObj["SecurityGroups"];

        List<SecurityGroups> sGroups = sGroupsjarray.ToObject<List<SecurityGroups>>();
        var groupId = "";
        foreach (var sGroup in sGroups)
        {
          groupId = groupId + "'" + sGroup.GroupId + "',";
        }
        if (groupId != "")
        {
          groupId = groupId.TrimEnd(',').Trim();
          var query = "select EntityInfoJson from Entity where EntityType ='SecurityGroup' and EntityId in(" + groupId + ")";
          var groupUrl = igniteip + "/service/query?cacheName=" + tenantId + "&query=" + query;
          var resultSet = IgniteCustomRestApi(groupUrl);
          if (resultSet != "[]" || resultSet != "")
          {
            List<List<string>> myDeserializedObjList =
                                  JsonConvert.DeserializeObject<List<List<string>>>(resultSet);
            foreach (var innerList in myDeserializedObjList)
            {
              if (innerList.Count == 1)
              {
                var data = innerList.ElementAt(0);
                var objJson = JObject.Parse(data.ToString());
                if (objJson["Details"] != null)
                {
                  var objDetails = objJson["Details"];
                  var obj = JObject.Parse(objDetails.ToString());
                  JObject capRoleJobj = (JObject)obj["Capability_Role"];
                  Dictionary<string, List<string>> capRoleDic = capRoleJobj.ToObject<Dictionary<string, List<string>>>();
                  foreach (var caproleElement in capRoleDic)
                  {
                    List<string> roleArrayList = caproleElement.Value;
                    foreach (var roleId in roleArrayList)
                    {
                      var readUrl = igniteip + "/service/entities?&key=" + roleId + "&cacheName=" + tenantId + "&type=Details";
                      var roleResult = IgniteCustomRestApi(readUrl);
                      JObject roleObj = JObject.Parse(roleResult);
                      permissions.Read = roleObj["Read"]["Value"].ToObject<bool>();
                      permissions.Write = roleObj["Write"]["Value"].ToObject<bool>();
                      permissions.Delete = roleObj["Write"]["Value"].ToObject<bool>();
                    }
                    capPermissionsDic.Add(caproleElement.Key, permissions);

                  }

                }
              }
            }


          }
          if (permissionObj["Roles"] != null)
          {
            capPermissionsDic.ToList().ForEach(x => GetUserIndividualRoles((JObject)permissionObj["Roles"], igniteip, tenantId).Add(x.Key, x.Value));
          }
        }
        else
        {
          //for roles
          if (permissionObj["Roles"] != null)
          {
            capPermissionsDic = GetUserIndividualRoles((JObject)permissionObj["Roles"], igniteip, tenantId);
          }
        }

      }
      return JsonConvert.SerializeObject(capPermissionsDic);
    }

    private Dictionary<string, UserPermissions> GetUserIndividualRoles(JObject permissionObj, string ingiteIp, string tenantId)
    {
      UserPermissions permissions = new UserPermissions { Read = false, Write = false, Delete = false };
      Dictionary<string, List<string>> userIndividualcapRoleDic = permissionObj.ToObject<Dictionary<string, List<string>>>();
      Dictionary<string, UserPermissions> userPermissionsDic = new Dictionary<string, UserPermissions>();
      foreach (var entry in userIndividualcapRoleDic)
      {
        entry.Value.ForEach(roleId =>
        {
          var readUrl = ingiteIp + "/service/entities?&key=" + roleId + "&cacheName=" + tenantId + "&type=Details";
          var roleResult = IgniteCustomRestApi(readUrl);
          JObject roleObj = JObject.Parse(roleResult);
          permissions.Read = roleObj["Read"]["Value"].ToObject<bool>();
          permissions.Write = roleObj["Write"]["Value"].ToObject<bool>();
          permissions.Delete = roleObj["Write"]["Value"].ToObject<bool>();

        });
        userPermissionsDic.Add(entry.Key, permissions);
      }
      return userPermissionsDic;

    }

    public string GetEntityType(String Cachename, String EntityId)
    {
      String EntityType = null;
      try
      {
        var url = igniteip + "/service/entities?&key=" + EntityId + "&cacheName=" + Cachename + "&type=Details";
        EntityType = IgniteCustomRestApi(url);
      }
      catch (Exception ex)
      {
        throw new Exception(ex.Message);
      }
      return EntityType;
    }
    /// <summary>
    /// GetEntityCapabilities:This method is used to get the  capabiklities and roles available for that entity and returns the json object as string like{"capId1":["roleid1","roleId2"]}
    /// </summary>
    /// <param name="entityId">EntityId when double click on entity</param>
    /// <param name="tenantId">TenantId of userLogin</param>
    /// <returns>{"capId1":["roleid1","roleId2"]} as json string</returns>
    public string GetEntityCapabilities(string entityId, string tenantId)
    {
      Dictionary<string, List<string>> uiCapaDic = new Dictionary<string, List<string>>();

      //getting user json
      string entityUrl = igniteip + "/service/entities?key=" + entityId + "&cacheName=" + tenantId + "&type=Users";
      var entityUserString = IgniteCustomRestApi(entityUrl);
      if (entityUserString == "null" || entityUserString == "" || entityUserString == null)
      {
        return JsonConvert.SerializeObject(uiCapaDic);
      }
      else
      {
        JObject entityUsersObj = JObject.Parse(entityUserString);
        Dictionary<string, JObject> usersDic = entityUsersObj.ToObject<Dictionary<string, JObject>>();


        foreach (var entry in usersDic)
        {
          Dictionary<string, List<string>> capaDic = entry.Value.ToObject<Dictionary<string, List<string>>>();
          foreach (var capEntry in capaDic)
          {
            if (uiCapaDic.ContainsKey(capEntry.Key))
            {
              List<string> entitiesRoleList = capEntry.Value;
              List<string> uiEntityRoleList = uiCapaDic[capEntry.Key];
              //list of role ids that doesn't exist in the uiEntityRoleList
              List<string> notExistingRoleList = entitiesRoleList.Except(uiEntityRoleList).ToList();
              if (notExistingRoleList.Count != 0)
              {
                uiEntityRoleList.AddRange(notExistingRoleList);
                uiCapaDic[capEntry.Key] = uiEntityRoleList;
              }

            }
            else
            {
              uiCapaDic.Add(capEntry.Key, capEntry.Value);
            }
          }

        }
        string uiCapaDicAsString = JsonConvert.SerializeObject(uiCapaDic);
        return uiCapaDicAsString;
      }

    }
    /// <summary>
    /// Get the Respective Fabric Capabilities for Tenant Based
    /// </summary>
    /// <param name="fabric"></param>
    /// <param name="tenantId"></param>
    /// <returns></returns>
    public string GetFabricCapabilities(string fabric, string tenantId)
    {
      string cacheName = "TenantFormSchema";
      string column = "CapabilitiesSchema";
      string url = String.Format("{0}/infoService/read?key={1}&cacheName={2}&column={3}", igniteip, tenantId, cacheName, column);
      string result = IgniteCustomRestApi(url);
      var objDetails = "";
      string capabilitiesArrayAsString = "[]";

      List<JObject> capabilitiesList = new List<JObject>();
      if (result != null && result != "null" && result != "")
      {
        Dictionary<string, List<JObject>> dic = new Dictionary<string, List<JObject>>();
        var capObjJson = JObject.Parse(result.ToString());
        if (capObjJson["Details"] != null)
        {
          objDetails = capObjJson["Details"].ToString();
          List<JObject> fabricList = JsonConvert.DeserializeObject<List<JObject>>(objDetails);
          foreach (var fbric in fabricList)
          {
            string fabricName = fbric["fabricName"] != null ? fbric["fabricName"].ToString() : "";
            if (string.Equals(fabricName, fabric, StringComparison.OrdinalIgnoreCase))
            {
              JArray capabilitiesJarray = (JArray)fbric["capabilities"];
              capabilitiesArrayAsString = JsonConvert.SerializeObject(capabilitiesJarray);

            }
            else if (fabricName.StartsWith("People"))
            {
              JArray capabilitiesJarray = (JArray)fbric["capabilities"];
              capabilitiesArrayAsString = JsonConvert.SerializeObject(capabilitiesJarray);
            }
          }

        }
      }
      else
      {
        throw new HttpRequestException("response from ignite got null  or empty for the url:" + url);
      }
      return capabilitiesArrayAsString;

    }
    private string QuerytoGetCapabilities(string tenantId, string fabric)
    {
      string cacheName = "TenantFormSchema";
      string column = "CapabilitiesSchema";
      string url = String.Format("{0}/infoService/read?key={1}&cacheName={2}&column={3}", igniteip, tenantId, cacheName, column);
      string result = IgniteCustomRestApi(url);
      var objDetails = "";
      string capabilitiesArrayAsString = "[]";

      List<JObject> capabilitiesList = new List<JObject>();
      if (result != null && result != "null" && result != "")
      {
        Dictionary<string, List<JObject>> dic = new Dictionary<string, List<JObject>>();
        var capObjJson = JObject.Parse(result.ToString());
        if (capObjJson["Details"] != null)
        {
          objDetails = capObjJson["Details"].ToString();
          List<JObject> fabricList = JsonConvert.DeserializeObject<List<JObject>>(objDetails);
          foreach (var fbric in fabricList)
          {
            string fabricName = fbric["fabricName"] != null ? fbric["fabricName"].ToString() : "";
            if (string.Equals(fabricName, fabric, StringComparison.OrdinalIgnoreCase))
            {
              JArray capabilitiesJarray = (JArray)fbric["capabilities"];
              capabilitiesArrayAsString = JsonConvert.SerializeObject(capabilitiesJarray);

            }
          }

        }
      }
      else
      {
        throw new HttpRequestException("response from ignite got null or empty for the url:" + url);
      }
      return capabilitiesArrayAsString;
    }
    public string GetCapabilitiesSchema(string tenantID)
    {
      string tenantId = tenantID;
      string cacheName = "TenantFormSchema";
      string column = "CapabilitiesSchema";

      string url = String.Format("{0}/infoService/read?key={1}&cacheName={2}&column={3}", igniteip, tenantId, cacheName, column);
      string result = IgniteCustomRestApi(url);
      var jsonObject = JObject.Parse(result);

      var data = jsonObject["Details"];
      JObject ObjectToreturn = new JObject();
      foreach (var value in data)
      {
        var key = value["fabricName"];
        ObjectToreturn.Add(key.ToString(), new JArray());

        var values = value["capabilities"];
        foreach (var c in values)
        {
          var Temp = (JArray)ObjectToreturn.GetValue(key.ToString());
          Temp.Add(c["name"]);
        }
      }
      return ObjectToreturn.ToString();
    }

    public async Task<string> GetAdminCredetial(string TenantID, string tenantName)
    {
      string result = null;
      try
      {

        string thinClientQuery = "select DetailsSchema from TenantFormsSchema where TenantId ='" + TenantID + "'";
        string AdminInfoJson = await this._igniteThinClientService.QueryConfigRecordAllAsync("TenantFormsSchema", thinClientQuery, null, false);
        if (!string.IsNullOrEmpty(AdminInfoJson) && AdminInfoJson != "[]")
        {
          JArray obj = JArray.Parse(AdminInfoJson);
          var tempobj = JObject.Parse(obj[0][0].ToString());
          var username = tempobj["ZoomdataAdminUser"] != null ? (tempobj["ZoomdataAdminUser"]["Value"] != null ? tempobj["ZoomdataAdminUser"]["Value"].ToString() : tempobj["ZoomdataAdminUser"].ToString()) : null;
          result = username;
        }
      }
      catch (Exception e)
      {
        throw new Exception(e.Message);
      }
      return await Task.FromResult(result);
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="Tenantid"></param>
    /// <param name="column"></param>
    /// <returns></returns>
    public async Task<string> ReadTenantAsync(string Tenantid, string column)
    {
      List<string> list = new List<string>();

      string result1 = "";
      string result2 = "";
      string result = "";

      //schema
      string cacheName = "TenantFormSchema";
      string entityId = Tenantid;
      string url1 = igniteip + "/Tenantservice/tenantschema/get?cacheName=TenantDetails&fabric=NewTenant&entityType=NewTenant";
      result1 = IgniteCustomRestApi(url1);
      list.Add(result1);

      //model
      //key :entityId  value:cacheName here cacheName is tenantId
      if (column == "DetailsSchema" || column == "SecuritySchema" || column == "CapabilitiesSchema")
      {
        string url2 = igniteip + "/infoService/read?key=" + entityId + "&cacheName=" + cacheName + "&column=" + column;
        result2 = IgniteCustomRestApi(url2);
        list.Add(result2);
      }

      result = JsonConvert.SerializeObject(list);
      return result;
    }

  }
}
