using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CapexApp.Models.RestClient
{
  public class RestClient
  {
    // for https issue
    public static bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
    {
      return true;
    }

    public static async Task<String> PostHttp(string url, string postData, string contenttype)
    {
      try
      {

        // for https
        ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);

        var request = (HttpWebRequest)WebRequest.Create(url);
        var data = Encoding.ASCII.GetBytes(postData);
        request.Method = "POST";
        request.ContentType = contenttype;
        request.ContentLength = data.Length;
        using (var stream = request.GetRequestStream())
        {
          stream.Write(data, 0, data.Length);
        }
        var response = (HttpWebResponse)request.GetResponse();

        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

        return await Task.FromResult(responseString);
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }
  }
}
