﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapexApp.Models
{
    public class SecurityGroups
    {
        public string GroupId { get; set; }
        public string GroupName { get; set; }
    }
}
