using Microsoft.ApplicationInsights;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CapexApp
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      var aiOptions = new Microsoft.ApplicationInsights.AspNetCore.Extensions.ApplicationInsightsServiceOptions
      {
        EnableQuickPulseMetricStream = true
      };
      services.AddMvc();
      services.AddApplicationInsightsTelemetry(aiOptions);
      services.AddCors(option =>
      {
        option.AddPolicy("AllowSpecificOrigin", policy => policy.WithOrigins("*"));
        option.AddPolicy("AllowGetMethod", policy => policy.WithMethods("GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
      });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
     // app.UsePathBase("/construction");
      TelemetryClient tc = new TelemetryClient();
      tc.TrackTrace("Environment: " + env);

      if (env.IsDevelopment())
      {
        tc.TrackTrace("Development");
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseDeveloperExceptionPage();
      }
      app.UsePathBase("/constructionapp");
      app.UseStaticFiles();
      app.UseCors(select => select.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
      
      app.UseMvc(routes =>
      {
        routes.MapRoute(
                  name: "default",
                  template: "{controller=Home}/{action=Index}/{id?}");

        routes.MapSpaFallbackRoute(
                  name: "spa-fallback",
                  defaults: new { controller = "Home", action = "Index" });
      });

      //app.Use((context, next) =>
      //{
      //  context.Request.PathBase = new PathString("/Application1");
      //  return next.Invoke();
      //});
    }
  }
}
