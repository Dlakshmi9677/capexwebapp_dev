using System;
using System.Collections.Generic;
using System.Fabric;
using System.Linq;
using System.Threading.Tasks;

namespace CapexApp.commonconfig
{
  public class CommonConfiguration
  {
    public static string GetConfig(ServiceContext context, string key)
    {
      var configurationPackage = context.CodePackageActivationContext.GetConfigurationPackageObject("Config");
      var connectionStringParameter = configurationPackage.Settings.Sections["ServiceConfig"].Parameters[key];
      return connectionStringParameter.Value;
    }

    public static string GetConfiguration(string type)
    {
      var activationContext = FabricRuntime.GetActivationContext();
      var configurationPackage = activationContext.GetConfigurationPackageObject("Config");
      var connectionParameter = configurationPackage.Settings.Sections["ServiceConfig"].Parameters[type];
      return connectionParameter.Value;
    }
  }
}
