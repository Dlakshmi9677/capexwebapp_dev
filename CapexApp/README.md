# Visur Dev GuidLines Introduction (https://visur.io/)

Visur project with Angular 8(+) and Electron (Typescript + SASS + Hot Reload) for creating web and Desktop  applications.

Currently runs with:

- Angular v8.3.6
- npm v6.11.3
- node v12.11.0
- Electron v8.2.0
- Electron Builder v20.26.0

With this Application, you can :
- Run your app in a local VSCode for dev environment with angular Cli
- Run your app in a local development environment with Electron & Hot reload
- Run your app in a production environment
- Package your app into an executable file for Windows.

## Getting Started

Clone this repository locally :

``` bash
git clone https://ram-biz@bitbucket.org/VisurTechnologies/capexapp.git
```

cd (Change Directory) to path where package.json exist  :

``` bash
cd  CapexApp
```

Install dependencies with npm :

``` bash
npm install
```

Please use `npm` as dependencies manager.


If you want to generate Angular components with Angular-cli , you **MUST** install `@angular/cli` in npm global context.  
Please follow [Angular-cli documentation](https://github.com/angular/angular-cli) if you had installed a previous version of `angular-cli`.

``` bash
npm install -g @angular/cli
```

#### Run App As Electron
* [Readme file For Desktop](https://bitbucket.org/VisurTechnologies/capexapp/src/development/CapexApp/DESKTOP-README.md)


## Included Commands

|Command|Description|
|--|--|
|`npm run start`| Execute the app in the VScode |
|`npm run dev:build`| Build the app for development. Your built files are in the /wwwroot folder. |
|`npm run prod:build`| Build the app for production with Angular aot. Your built files are in the /wwwroot folder. |
|`npm run electron:local`| Builds your Angular  application and start electron
|`npm run electron:ng:serve`| Builds your Angular application and serve electron in localhost:4200/ |
|`npm run test`| Run angular karma unit test cases |
|`npm run test:coverage`| Run angular karma unit test cases to get testCases coverage  |
|`npm run compodoc`| Create Angular code documentation using compodoc |

**Your application is optimised. Only /wwwroot folder and node dependencies are included in the executable.**

## Application external files Location

Keep all your images, js ,css and any external resource files in below mintioned path folder
**capexapp/CapexApp/ClientApp/appResourceFiles**
At build time all these files will be placed in the final wwwroot folder



## Browser mode

Maybe you want to execute the application in the browser with hot reload ? You can do it with `npm run electron:ng:serve`.  
Note that you can't use Electron or NodeJS native libraries in this case. Please check `providers/electron.service.ts` to watch how conditional import of electron/Native libraries is done.

## Application Performance:

**1.Out of DOM references:**

  example: storing the DOM element in JSON,so here two reference one for DOM tree node and one for the newly created JSON object.
  
**2.Out of Service/Components reference:**

 example: don't store components or services instance into global service or components variables JSON

```
  instances ={
    appService:this
  }
```
  

**3.Don't create multiple address refrence for the variable:**
  
**Before:**

```
  appInitializationExpandDefault={header:false,leftPan:true,rightPan:false};  
  appInitializationExpandDefault={header:true,leftPan:true,rightPan:false};   
```

**After:**
  
```  
  appInitializationExpandDefault.header=true;//inside same slot assigning value
```
  

**4.Prioritize access to local variables:**

-declared global and using inside method like(no need global variables)
  
** Before**
   
```
 colors=[]; 
 getColors(){
  this.colors = getColorsFromApi();
  sendColorToService(this.colors);
 }
```

** After**
   
```
    getColors(){
    let colors = getColorsFromApi();
    sendColorToService(colors);
   }
```

**5.Global variables(try to avoid declaring global variables as much as possible):**

  1.-Always declare with data type such as string, boolean, Array, etc, etc.
   e.g:

```
      isLeftTree:boolean=false;
```

-don't do:

 isLeftTree;//undefined and compiler will not understand how much memory needs to allocate.

2. Don't use any type as the data type for string or boolean variable types etc.
  

**6. Declare variable at proper places(not mandatory but design matter).**

-at the global declaration,
  -before constructor,
  -based on beside already exists types.
  
**7.Export from global constants instead of declaring a global variable inside services and components for the constants.**
 
 e.g:https://stackoverflow.com/questions/36158848/how-can-i-declare-a-global-variable-in-angular-2-typescript
  
**8. Accessing dom element multiple times inside method, better to reference dom element to local variable and access throughout the method same variable.**
e.g:

** Before:**

```
   domObject<HtmlElement>;
     accessDom(){
        domObject 
        domObject 
        domObject 
     }
```
** After:**
     
```
     accessDom(){
        let domElement = domObject; 
        domElement
        domElement
     }
```

**9.State CRUD operation:**

 -Instead of creating duplicate method,state service import/Injecting into multiple service or components.
  access commonservice methods from services or components.

**10.avoid using the timer and set interval instead make use of ngAfterViewInit**


**11. After usage clean the global stored data:**

   -from components and methods inside ngOnDestory or resetting value of the variable.
   
**12. Try to avoid using condition check with operators like this.**

** Before:**
   
```   
   isConfigForm:boolean=false;  
   if(!isConfigForm){
   }
```

** After:**

```
   isConfigForm:boolean=true;
   if(isConfigForm){
   }
```

**13. Use Observable share() operator, For the same computation at multiple places subscription.**
   
**14. Properly use let and var keyword.**
** Compiler Error**
   
```   
   console.log(num1); 

   let num1:number = 10 ;
```

** OK, Output: undefined **
   
```
   console.log(num2); 

   var num2:number = 10 ;
```

   
**Some useful reference link**
--------------------------
-https://daveceddia.com/react-redux-immutability-guide/

-https://nodesource.com/blog/improve-javascript-performance

-https://blog.sessionstack.com/how-javascript-works-memory-management-how-to-handle-4-common-memory-leaks-3f28b94cfbec

-https://netbasal.com/optimizing-the-performance-of-your-angular-application-f222f1c16354

-https://medium.com/faun/44-quick-tips-to-fine-tune-angular-performance-9f5768f5d945



