using Apache.Ignite.Core;
using Apache.Ignite.Core.Cache.Query;
using Apache.Ignite.Core.Client;
using Apache.Ignite.Core.Client.Cache;
using Apache.Ignite.Core.Cache.Configuration;
using Models.Ignite;
using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using ICommonInterfaces.Model;
using System.Linq;
namespace CapexApp.Ignite
{
  public class IgniteThinClientService
  {
    private readonly IgniteClientConfiguration _igniteClientConfiguration;
    public IgniteThinClientService(string endPoint)
    {
      
            var endpoints = new List<string>();
            if (endPoint.Contains(","))
            {
                endpoints = endPoint.Split(',').ToList();
            }
            else
            {
                endpoints.Add(endPoint);
            }

      _igniteClientConfiguration = new IgniteClientConfiguration
      {
                Endpoints = endpoints,
        SocketTimeout = TimeSpan.FromSeconds(60)

      };
    }
    public async Task<JObject> GetUserDetailsAsync(string tenantId, string email)
    {
      string userId = null;
      string isAdmin = null;
      JObject resultJobj = null;
      string cacheName = tenantId + "_UserDetails";
      cacheName = GetCacheNameWithUpperCase(cacheName);
      string query = "select UserId , isAdmin  from UserDetails where email= '" + email + "'";
      using (IIgniteClient client = Ignition.StartClient(this._igniteClientConfiguration))
      {
        //get cache config 
        var cache = client.GetCache<string, Object>(cacheName);
        var listResult = cache.Query(new SqlFieldsQuery(query)).GetAll();
        foreach (var row in listResult)
        {
          userId = row[0] as string;
          isAdmin = row[1] as string;

        }
      }
      if (userId != null)
      {
        resultJobj = JObject.FromObject(new
        {
          userId = userId,
          isAdmin = isAdmin
        });
      }
      return resultJobj;
    }

    /// <summary>
    /// Method To Read A Record Form A Specified Cache Based On Key
    /// </summary>
    /// <param name="cacheName">ignite cache Name where LoginSessionCache objects store</param>
    /// <param name="key">cache key : LoginSessionCache key</param>
    /// <returns>LoginSessionCache record</returns>
    public async Task<LoginSessionCache> ReadRecordAsync(string cacheName, string key)
    {
      LoginSessionCache sessionClient = null;

      try
      {
        cacheName = GetCacheNameWithUpperCase(cacheName);
        using (IIgniteClient client = Ignition.StartClient(this._igniteClientConfiguration))
        {
          //get cache config 
          var cacheConfig = await GetOrCreateWebSessionCacheConfig(cacheName);
          var cache = client.GetOrCreateCache<string, LoginSessionCache>(cacheConfig);

          try
          {
            sessionClient = cache.Get(key);
          }
          catch (Exception ex)
          {
            if (ex.Message == "The given key was not present in the cache.")
            {
              return sessionClient;
            }

            throw;
          }
        }
      }
      catch (Exception ex)
      {
        throw;
      }

      return sessionClient;
    }
    public async Task<string> QueryConfigRecordAsync(string cacheName, string query, string productionday, bool dateisQueryField = false)
    {
      string listResult = null;
      cacheName = GetCacheNameWithUpperCase(cacheName);

      using (IIgniteClient client = Ignition.StartClient(this._igniteClientConfiguration))
      {
        var cache = client.GetCache<object, ICustomCacheStore>(cacheName);
        SqlFieldsQuery sqlQuery = null;

        if (dateisQueryField && productionday != null)
        {
          string[] split = productionday.Split('-');
          var dateObj = new DateTime(Convert.ToInt32(split[0]), Convert.ToInt32(split[1]), Convert.ToInt32(split[2]), 0, 0, 0, DateTimeKind.Utc);

          sqlQuery = new SqlFieldsQuery(query, dateObj);
          sqlQuery.Lazy = true;
        }
        else
        {
          sqlQuery = new SqlFieldsQuery(query);
          sqlQuery.Lazy = true;

        }

        var queryCursor = cache.Query(sqlQuery);
        List<JObject> listRecords = new List<JObject>();
        foreach (var rcursor in queryCursor)
        {
          JObject jobj = new JObject();
          String fieldName = null;
          for (int i = 0; i < rcursor.Count; i++)
          {

            fieldName = queryCursor.FieldNames[i];
            if (rcursor[i] != null && rcursor[i].GetType().Equals(typeof(Boolean)))
            {
              Boolean fieldValue = (Boolean)rcursor[i];
              jobj.Add(fieldName, fieldValue);
            }
            else if (rcursor[i] != null && rcursor[i].GetType().Equals(typeof(int)))
            {
              int fieldValue = (int)rcursor[i];
              jobj.Add(fieldName, fieldValue);
            }
            else if (rcursor[i] != null && (rcursor[i].GetType().Equals(typeof(float)) || rcursor[i].GetType().Equals(typeof(Double))))
            {
              double fieldValue = (double)rcursor[i];
              jobj.Add(fieldName, fieldValue);
            }
            else if (rcursor[i] != null && rcursor[i].GetType().Equals(typeof(DateTime)))
            {
              DateTime time = (DateTime)rcursor[i];
              jobj.Add(fieldName, time.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            else if (rcursor[i] != null && rcursor[i].GetType().Name == "List`1")
            {

              jobj.Add(fieldName, JArray.FromObject(rcursor[i]));
            }
            else
            {

              string fieldValue = rcursor[i] != null ? rcursor[i].ToString() : null;
              jobj.Add(fieldName, fieldValue);
            }

          }
          listRecords.Add(jobj);

        }
        listResult = JsonConvert.SerializeObject(listRecords);
      }
      return listResult;
    }

    public async Task<string> QueryConfigRecordAllAsync(string cacheName, string query, string productionday, bool dateisQueryField = false)
    {
      string listResult = null;
      cacheName = GetCacheNameWithUpperCase(cacheName);

      using (IIgniteClient client = Ignition.StartClient(this._igniteClientConfiguration))
      {
        var cache = client.GetCache<object, ICustomCacheStore>(cacheName);
        SqlFieldsQuery sqlQuery = null;
        if (productionday != null && dateisQueryField)
        {
          string[] split = productionday.Split('-');
          var dateObj = new DateTime(Convert.ToInt32(split[0]), Convert.ToInt32(split[1]), Convert.ToInt32(split[2]), 8, 0, 0, DateTimeKind.Utc);
          if (dateisQueryField)
          {
            sqlQuery = new SqlFieldsQuery(query, dateObj);
          }
        }
        else
        {
          sqlQuery = new SqlFieldsQuery(query);
        }
        var queryCursor = cache.Query(sqlQuery).GetAll();
        listResult = JsonConvert.SerializeObject(queryCursor);
      }
      return listResult;
    }

    private static string GetCacheName(MessageWrapper message)
    {
      return (message.TenantID + "_" + message.CapabilityId).ToUpper();
    }

    private static string GetCacheNameWithUpperCase(string cacheName)
    {
      return cacheName.ToUpper();
    }


    /// <summary>
    /// Method To Create and return  Cache Cocnfiguration
    /// </summary>
    /// <param name="cacheName"></param>
    /// <returns></returns>
    public async Task<CacheClientConfiguration> GetOrCreateWebSessionCacheConfig(string cacheName)
    {
      var cacheConfig = new CacheClientConfiguration
      {
        CacheMode = Apache.Ignite.Core.Cache.Configuration.CacheMode.Partitioned,
        Name = cacheName,
        QueryEntities = new[]
          {
                new QueryEntity(typeof(string), typeof(LoginSessionCache))
          }
      };

      return cacheConfig;
    }
    public async Task<FormSchema> GetFormSchemaByNameAsync(string formName, string tenantId)
    {
      FormSchema formSchema = null;
      string cacheName = tenantId + "_FormSchema";
      cacheName = GetCacheNameWithUpperCase(cacheName);
      try
      {
        using (IIgniteClient client = Ignition.StartClient(this._igniteClientConfiguration))
        {
          try
          {
            //get cache config 
            var cache = client.GetCache<string, FormSchema>(cacheName);
            var formSchema1 = await cache.GetAsync(formName);
            formSchema = formSchema1;
          }
          catch (Exception ex)
          {

          }
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
      return formSchema;
    }

    public async Task<string> GetDashboardinfo(string tenantId , string entityid)
    {
      string EntityInfoJson = null;

      string cacheName = tenantId;
      cacheName = GetCacheNameWithUpperCase(cacheName);

      string query = "select EntityInfoJson  from EntitiesTableInfo where EntityId= '" + entityid + "'";
      using (IIgniteClient client = Ignition.StartClient(this._igniteClientConfiguration))
      {
        //get cache config 
        var cache = client.GetCache<string, Object>(cacheName);
        var listResult = cache.Query(new SqlFieldsQuery(query)).GetAll();
        foreach (var row in listResult)
        {
          EntityInfoJson = JObject.Parse( row[0].ToString())["Details"].ToString();
         
        }
      }
      return EntityInfoJson;
    }

    /// <summary>
    /// Query Ignite Records using fields
    /// </summary>
    /// <param name="cacheName"></param> e.g.TenantDetails,TenantFormsSchema
    /// <param name="query"></param>Select TenantId from CommonTenantInfo;
    /// <returns></returns>
    public async Task<string> QueryIgniteByFieldAsync(string cacheName, string query)
    {
      JArray jArray = new JArray();
      string listResult = "";
      try
      {
        cacheName = GetCacheNameWithUpperCase(cacheName);

        using (var client = Ignition.StartClient(this._igniteClientConfiguration))
        {
          var cache = client.GetCache<object, object>(cacheName);
          SqlFieldsQuery sqlQuery = new SqlFieldsQuery(query);

          var queryCursor = cache.Query(sqlQuery).GetAll();

          foreach (var cursorResult in queryCursor)
          {
            listResult = cursorResult[0].ToString();
          }
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
      return listResult;
    }
  }
}
