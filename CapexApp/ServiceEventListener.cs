using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapexApp
{
    internal class ServiceEventListener : EventListener
    {
        private string fileName;
        private string filepath = Path.GetTempPath();

        public ServiceEventListener(string appName)
        {
            this.fileName = appName + DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".log";
        }

        /// <summary>
        /// We override this method to get a callback on every event we subscribed to with EnableEvents
        /// </summary>
        /// <param name="eventData">The event arguments that describe the event.</param>
        protected override void OnEventWritten(EventWrittenEventArgs eventData)
        {
            using (StreamWriter writer = new StreamWriter(new FileStream(filepath + fileName, FileMode.Append)))
            {
                // report all event information
                writer.Write(Write(eventData.Task.ToString(),
                eventData.EventName,
                eventData.EventId.ToString(),
                eventData.Level));

                if (eventData.Message != null)
                {

                    writer.WriteLine(string.Format(CultureInfo.InvariantCulture, eventData.Message, eventData.Payload.ToArray()));
                }
            }
        }

        private static string Write(string taskName, string eventName, string id, EventLevel level)
        {
            StringBuilder output = new StringBuilder();

            DateTime now = DateTime.UtcNow;
            output.Append(now.ToString("yyyy/MM/dd-HH:mm:ss.fff", CultureInfo.InvariantCulture));
            output.Append(',');
            output.Append(ConvertLevelToString(level));
            output.Append(',');
            output.Append(taskName);

            if (!string.IsNullOrEmpty(eventName))
            {
                output.Append('.');
                output.Append(eventName);
            }

            if (!string.IsNullOrEmpty(id))
            {
                output.Append('@');
                output.Append(id);
            }

            output.Append(',');
            return output.ToString();
        }

        private static string ConvertLevelToString(EventLevel level)
        {
            switch (level)
            {
                case EventLevel.Informational:
                    return "Info";
                default:
                    return level.ToString();
            }
        }
    }
}
