function hideShow(event) {
  switch (event) {
    case "onmouseover":
      document.getElementById("live_chat_support_solid_grey").style.display = "none";
      document.getElementById("live_chat_support_solid_white").style.display = "block";
      break;

    case "onmouseout":
      document.getElementById("live_chat_support_solid_grey").style.display = "block";
      document.getElementById("live_chat_support_solid_white").style.display = "none";
      break;
  }
}

function storeCookie(key, value) {
  var d = new Date();
  d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
  document.cookie = key + "=" + value + ";" + "expires=" + d.toUTCString() + ";path=/";
  localStorage.setItem(key, value);
  console.log(key + ": " + value);
}

function getDeviceDetail() {
  /*var client = new ClientJS();

  var fingerprint = "Unknown";
  if (client.isWindows) fingerprint = client.getOS() + client.getOSVersion();
  else if (client.isUbuntu) fingerprint = "Ubuntu";
  else if (client.isSafari) fingerprint = "Safari";
  else if (client.isMac) fingerprint = "Mac";
  else if (client.isIphone) fingerprint = "Iphone";
  else if (client.isMobile) {
    if (client.isMobileAndroid) fingerprint = "Android";
    else if (client.isMobileBlackBerry) fingerprint = "BlackBerry";
    else if (client.isMobileIOS) fingerprint = "IOS";
  }*/

  $(function () {
    $.getJSON("https://api.ipify.org?format=jsonp&callback=?", function (json) {

      var cookieValue = getCookie("deviceDetail");
      var loc = json.ip;
      var submitPasswordDisabled = false;

      var deviceDetail = document.getElementById("deviceDetail");
      if (deviceDetail !== null) {
        deviceDetail.value = loc;
      }

      var submitPassword = document.getElementById("submitPassword");
      if (submitPassword !== null) {
        submitPasswordDisabled = false;
      }

      if (loc !== cookieValue) {
        if (submitPassword !== null) {
          submitPasswordDisabled = true;
        }
        if ((json.ip == "" || json.ip == null)) {
          console.error("Error in deviceDetail. " + loc);
          if (submitPassword !== null) {
            submitPasswordDisabled = true;
          }
        }
        else {
          storeCookie("deviceDetail", loc);
          if (deviceDetail !== null) {
            deviceDetail.value = loc;
          }
          if (submitPassword !== null) {
            submitPasswordDisabled = false;
          }
        }
      }

      var userName = document.getElementById("un");
      if (submitPassword !== null && (userName != null || window.location.pathname !== '/Home/ChangePassword')) {
        submitPassword.disabled = submitPasswordDisabled;

        var verifying_device = document.getElementById("blinking-text");
        if (verifying_device !== null) {
          verifying_device.innerHTML = '';
        }
      }
    })
      .fail(function (err) {
        var msg = "Either your browser or AdBlock extension is not allowing the confirmation of your IP address. Please change settings and try again.";

        var errorMessage = document.getElementById("ip-error-message");
        if (errorMessage !== null) {
          errorMessage.innerHTML = msg;
        } else {
          console.error(err);
        }

        //var verifying_device = document.getElementById("blinking-text");
        //if (verifying_device !== null) {
        //  verifying_device.innerHTML = '';
        //}

      })
  });
}

function DesktopDeviceDetail() {
  var client = new ClientJS();

  var fingerprint = "Unknown";
  if (client.isWindows) fingerprint = client.getOS() + client.getOSVersion();
  else if (client.isUbuntu) fingerprint = "Ubuntu";
  else if (client.isSafari) fingerprint = "Safari";
  else if (client.isMac) fingerprint = "Mac";
  else if (client.isIphone) fingerprint = "Iphone";
  else if (client.isMobile) {
    if (client.isMobileAndroid) fingerprint = "Android";
    else if (client.isMobileBlackBerry) fingerprint = "BlackBerry";
    else if (client.isMobileIOS) fingerprint = "IOS";
  }

  return fingerprint;
}

function getDeviceDetailFromLS() {
  document.getElementById("deviceDetail").value = localStorage.getItem("deviceDetail");
  console.log("Device detail: " + document.getElementById("deviceDetail").value)
}

function progressBar() {
  checkToken("token");
  var page_load_time = new Date().getTime() - performance.timing.navigationStart;
  var perfData = window.performance.timing; // The PerformanceTiming interface
  var EstimatedTime = -(perfData.loadEventEnd - perfData.navigationStart); // Calculated Estimated Time of Page Load which returns negative value.
  var time = (EstimatedTime / 1000) % 60 * 100;
  var width = time / 100;

  var elem = document.getElementById("myBar");
  if (elem !== null) {
    var id = setInterval(frame, 1);
    elem.style.width = width + '%';

    function frame() {
      if (width >= 100)
        clearInterval(id);
      else {
        width++;
        elem.style.width = width + '%';
        //elem.innerHTML = "Loading in progress... " + width * 1 + '%';
      }
    }
  }

  console.log("One moment please ... initializing application.")
  //console.log("User-perceived page loading time: " + page_load_time + "\nEstimatedTime: " + EstimatedTime + "\ntime: " + time + "\npercentage: " + width);
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function checkToken(cname) {
  var cookieValue = getCookie(cname);
  if (cookieValue == "") {
    document.getElementById("myProgress").style.display = 'none';
    window.location.reload();
  }
}

function isUserLogin(cname) {
  var cookieValue = getCookie(cname);
  console.log("please wait... the browser cache memory is refreshing. " + cname + ": " + cookieValue);
  if (cookieValue != "") {
    window.location.reload();
  }
}

function getDomainAndUserName() {
  if (window.location.search != "")
    document.getElementById("url").value = window.location.search.split('?')[1];

  document.getElementById("isChangePassword").value = "true";
  getDeviceDetail();
}

function setValue() {
  document.getElementById("isForgotPassword").value = "ResetPassword";
}

function MoveCursorToNextField(object, event) {
  event = (typeof event != 'undefined') ? window.event : event;
  if (event.keyCode == 13) {
    var formsElements = document.forms[0].elements;
    for (var i = 0; i < formsElements.length; i++) {
      var NextFieldCycle = (i == formsElements.length - 1) ? 0 : i + 1;
      if (object == formsElements[i]) {
        formsElements[NextFieldCycle].focus();
        break
      }
    }
    return false;
  }
}

function btnFocusActivate(object, event) {
  console.log('btnFocusActivate');

  event = (typeof event != 'undefined') ? window.event : event;

  if (event.keyCode == 13) {
    const btnAction = document.getElementsByClassName('rectangle');
    if (btnAction !== null) {
      btnAction[0].focus();
    }
  }

}

function hideShowToggle() {
  var newpwd = document.getElementById("newpwd");
  var confirmpwd = document.getElementById("confirmpwd");

  if (newpwd !== null && confirmpwd !== null) {

    if (newpwd.type === "password" && confirmpwd.type === "password") {
      newpwd.type = "text";
      confirmpwd.type = "text";

      document.getElementById('showHidePassword').src = '../images/HidePassword_outlined.svg'
    } else {
      newpwd.type = "password";
      confirmpwd.type = "password";

      document.getElementById('showHidePassword').src = '../images/ShowPassword_outlined.svg'
    }

  } else {
    console.error("newpwd and confirmpwd not exist.");
  }
}

function passwordValidation(doEnableDisableConfirmpwd, btn) {
  var isDisabled = false;
  var passRegexr = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;

  var newpwd = document.getElementById("newpwd");
  var confirmpwd = document.getElementById("confirmpwd");
  var verifying_device = document.getElementById("blinking-text");

  if (newpwd !== null && confirmpwd !== null && verifying_device !== null) {

    if (doEnableDisableConfirmpwd === true) {
      if (newpwd.value.match(passRegexr)) {
        isDisabled = false;
      } else {
        isDisabled = true;
        verifying_device.innerHTML = '';
        confirmpwd.value = '';
      }
    } else {
      if (newpwd.value.match(passRegexr)) {
        isDisabled = false;
        verifying_device.innerHTML = '';
      } else {
        isDisabled = true;
        if (newpwd.value === "") {
          verifying_device.innerHTML = 'Please Enter New Password first and then Confirm it.';
        } else {
          verifying_device.innerHTML = 'The password does not meet the password criteria.';
        }
      }
    }

    if (btn === 'submit' && confirmpwd.disabled === false && newpwd.value !== confirmpwd.value) {
      verifying_device.innerHTML = 'Both passwords must match.';
    }

    confirmpwd.disabled = isDisabled;
  } else {
    console.error('error');
  }
}

function passwordMatch(value) {
  var isDisabled = false;

  var newpwd = document.getElementById("newpwd");
  var confirmpwd = document.getElementById("confirmpwd");
  var submitPassword = document.getElementById("submitPassword");
  var verifying_device = document.getElementById("blinking-text");

  if (newpwd !== null && confirmpwd !== null && submitPassword !== null && verifying_device !== null) {

    if (value === true) {
      if (newpwd.value !== confirmpwd.value) {
        isDisabled = true;
      } else {
        isDisabled = false;
      }
    } else {
      if (newpwd.value !== confirmpwd.value) {
        isDisabled = true;
        verifying_device.innerHTML = 'Both passwords must match.';
      } else {
        isDisabled = false;
        verifying_device.innerHTML = '';
      }
    }

    if (isDisabled === false) {
      submitPassword.type = 'submit';
    } else {
      submitPassword.type = 'text';
    }

    submitPassword.disabled = isDisabled;
  } else {
    console.error('error');
  }
}

function blinkText() {
  var confirmpwd = document.getElementById("confirmpwd");
  var submitPassword = document.getElementById("submitPassword");
  var verifying_device = document.getElementById("blinking-text");

  if (confirmpwd !== null && verifying_device !== null) {

    if (confirmpwd.disabled === true || submitPassword.disabled === true) {
      $('#blinking-text').each(function () {
        var elem = $(this);
        var count = 1;
        var intervalId = setInterval(function () {
          if (elem.css('visibility') == 'hidden') {
            elem.css('visibility', 'visible');
            if (count++ === 3) {
              clearInterval(intervalId);
            }
          } else {
            elem.css('visibility', 'hidden');
          }
        }, 200);
      });
    }

  } else {
    console.error('error');
  }
}
