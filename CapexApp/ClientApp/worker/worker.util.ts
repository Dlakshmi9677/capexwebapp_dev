import * as localForage from "localforage";
import { DoWork, ObservableWorker } from 'observable-webworker';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppConsts} from '../app/components/common/Constants/AppConsts';
import { FABRICS, TreeTypeNames, TypeOfEntity } from '../app/components/globals/Model/CommonModel';
@ObservableWorker()
class WorkerUtil implements DoWork<string, string> {
  // SortingOrderForPhysicalFlow = ["MeterStation", "Plant", "GasBattery", "OilBattery", "CompressorStation", "BoosterStation", "GasInjectionFacility", "WaterInjectionFacility", "Header", "Pad", "InventoryStorage", "Riser", "Satellite", "SteamInjection",
  //   "WaterInjection", "TruckTerminal", "WasteFacility", "TreatingFacility", "GasWell", "OilWell", "GasWellProducingOil", "GasInjectionWell", "GasDisposalWell", "WaterInjectionWell", "WaterDisposalWell", "WaterSourceWell", "District", "Area", "Field"
  // ];
  public work(input$: Observable<any>): Observable<string> {
    return input$.pipe(
      map(message => {
        // console.log("before " + message.Type + " creation", new Date().getMinutes() + ":" + new Date().getSeconds());
        var result;
        switch (message.Type) {
          case TreeTypeNames.ALL:
            result = this.constructDgraphFormatDataWorkerAll(message.flattenData);
            break;
          case TreeTypeNames.PHYSICALFLOW:
            result = this.flatToHierarchyForPhysicalFlow(message.flattenData, message.fabricName);
            break;
          case "DownStreamLocationName":
            result = this.findChildrenDownStream(message.flattenData);
            break;
          case "UpStreamLocationName":
            result = this.findChildrenUpStream(message.flattenData);
            break;
          case "SaveIntoDB":
           // result = this.saveIntoDB(message.flattenData);
            break;
          case "fdcform":
            result = this.fdcForm(message.flattenData);
            break;
          case "IndexedDB":
            result = this.idbMessage(message.flattenData);
            break;
          case "MeasurementConvertionFunction":
            //result = this.MeasurementConvertionFunction(message.flattenData);
            break;


          /**************************unused case but not removing *******************/
          // case TreeTypeNames.LICENSEE:
          //   result = this.CreateLicenseeHeirarchy(message.flattenData);
          //   break;
          // case "PartialHierarachy":
          //     result = this.flatToHierarchyDistrictAreaField(message.flattenData);
          //     break;
          // // case TreeTypeNames.Hierarchy:
          // //   result = this.FDCCreateHierarchy(message.flattenData);
          // //   break;
          // case EntityStateConstantsKeys.entitiesKVP:
          //   result = this.createKeyValuePairEntities(message.flattenData);
          //   break;
          // case EntityStateConstantsKeys.entities:
          //   result = this.constructDgraphFormatDataWorker(message.flattenData);
          //   break;
          // case TreeTypeNames.Hierarchy://EntityStateConstantsKeys.EntitiesPhysicalFlowPartialHierachy:
          //   result = this.entitiesPhysicalFlowPartialHierachy(message.flattenData, message.fabricName);
          //  break;

          default:
            console.log("Tree worker default");
            break;
        }
        // console.log("after " + message.Type + " creation", new Date().getMinutes() + ":" + new Date().getSeconds());
        return result;
      })
    );
  }
  fdcForm(data: any) {
    return data;
  }

  idbMessage(flattenData: any) {
    let data = flattenData.data;
    let options = flattenData.options;

    localForage.config({
      driver: localForage.INDEXEDDB,
      name: 'Visur',
      storeName: options.tenantName
    });

    localForage.getItem("fdc").then((response: any) => {
      if (response && response.fdc) {
        let result = JSON.parse(JSON.stringify(response))

        result.fdc[data.key] = data.value;
        localForage.setItem('fdc', result).then(res => {
          console.log('')
        })
      }
    })

    return "success";
  }

 



  FDCCreateHierarchy(jsonData: any) {
    function setDataToDownStreamLocation(TempArray, isfaciltityDownStreamForHeaader?) {
      TempArray.forEach(payloadData => {
        var FacilitieNode1 = getFacilitieNodeByID(payloadData.locId);
        FacilitieNode1.forEach(FacilitieNode => {
          if (FacilitieNode != null) {
            if (FacilitieNode.Properties && FacilitieNode.Properties['0'] && (FacilitieNode.Properties['0'].HaulLocation != "Off" || FacilitieNode.Properties['0'].HaulLocation != false)) {
              FacilitieNode = null;
            }
          }

          if (FacilitieNode == null) {
            var tempfield = getFieldNodeById(payloadData.data.Parent[0].EntityId)
            if (tempfield != null) {
              var resultData = tempfield.children.filter(ddta =>
                ddta.EntityId == payloadData.data.EntityId
              );
              if (resultData.length == 0) {
                if (!pushIfItsUnique(tempfield.children, payloadData.data)) {
                  tempfield.children.push(JSON.parse(JSON.stringify(payloadData.data)));
                }
              }
            }
          } else {
            if (!pushIfItsUnique(FacilitieNode.children, payloadData.data)) {
              var tempfield = getFieldNodeById(payloadData.data.Parent[0].EntityId)
              var temppayloadData;
              // var isFacilityExistInSameField = isWellExistInField(tempfield.children, payloadData.locId)
              if (tempfield && tempfield.children && isfaciltityDownStreamForHeaader == true) {
                var index = tempfield.children.findIndex(d => d.EntityId == payloadData.data.EntityId)
                var isExist = isWellExistInField(tempfield.children, FacilitieNode.EntityId);
                temppayloadData = tempfield.children.filter(d => d.EntityId == payloadData.data.EntityId);
                if (index != -1 && isExist) {
                  tempfield.children.splice(index, 1)
                }
              }
              if (temppayloadData && temppayloadData.length != 0) {
                FacilitieNode.children.push(JSON.parse(JSON.stringify(temppayloadData[0])));
              }
              else {
                FacilitieNode.children.push(JSON.parse(JSON.stringify(payloadData.data)));
              }
            }
          }
        })
      });
    }

    function onlyUnique(value, index, self) {
      return self.indexOf(value) === index;
    }
    function getDownstreamLocForId(Entity, equipments) {
      var filteredEquipments = [];
      var Locationequipments: any;
      var Equip: any = equipments.filter(equip => equip.Parent[0].EntityId == Entity.EntityId && ((equip["Downstream"] && equip["Downstream"][0].EntityId && equip["Downstream"][0].EntityId != Entity.EntityId) || (equip["Upstream"] && equip["Upstream"][0].EntityId && equip["Upstream"][0].EntityId != Entity.EntityId)));
      var filteredPrimaryEqips: any = Equip.filter(eq1 => eq1.SetAsPrimary && eq1.SetAsPrimary[0] && eq1.SetAsPrimary[0].SetAsPrimary && eq1.SetAsPrimary[0].SetAsPrimary == true);
      if (Equip && Equip.length != 0) {
        if (filteredPrimaryEqips && filteredPrimaryEqips.length != 0) {
          Locationequipments = filteredPrimaryEqips;
        } else {
          Locationequipments = Equip;
        }
      }
      else {
        if (Entity.Downstream && Entity.Downstream[0] && Entity.Downstream[0].EntityId) {
          filteredEquipments.push(Entity.Downstream[0].EntityId);
        }

      }
      if (Locationequipments && Locationequipments.length != 0)
        Locationequipments.filter(equip => equip.Parent[0].EntityId == Entity.EntityId).forEach(equipment => {
          if (Entity.Downstream && Entity.Downstream[0] && Entity.Downstream[0].EntityId) {
            if (Entity.SetAsPrimary && Entity.SetAsPrimary[0] && Entity.SetAsPrimary[0].SetAsPrimary && Entity.SetAsPrimary[0].SetAsPrimary == true) {
              filteredEquipments.push(Entity.Downstream[0].EntityId);
              return filteredEquipments;
            } else {
              if (equipment.Downstream && equipment.Downstream[0].EntityId != Entity.EntityId) {
                //filteredEquipments.push(equipment.Downstream[0].EntityId);
                if (equipment.SetAsPrimary && equipment.SetAsPrimary[0] && equipment.SetAsPrimary[0].SetAsPrimary && equipment.SetAsPrimary[0].SetAsPrimary == true) {
                  filteredEquipments.push(equipment.Downstream[0].EntityId);
                  return filteredEquipments;
                } else {
                  filteredEquipments.push(Entity.Downstream[0].EntityId);
                }
              }
              else {
                filteredEquipments.push(Entity.Downstream[0].EntityId);
              }
            }
          }
          if (equipment.Downstream && equipment.Downstream[0].EntityId != Entity.EntityId) {
            //filteredEquipments.push(equipment.Downstream[0].EntityId);
            if (equipment.SetAsPrimary && equipment.SetAsPrimary[0] && equipment.SetAsPrimary[0].SetAsPrimary && equipment.SetAsPrimary[0].SetAsPrimary == true) {
              filteredEquipments.push(equipment.Downstream[0].EntityId);
              return filteredEquipments;
            } else {
              filteredEquipments.push(equipment.Downstream[0].EntityId);
            }
          }
          if (equipment.Upstream && equipment.Upstream[0].EntityId != Entity.EntityId) {
            filteredEquipments.push(equipment.Upstream[0].EntityId);
          }
        });
      // if (Entity.Downstream && Entity.Downstream[0] && Entity.Downstream[0].EntityId) {
      //   filteredEquipments.push(Entity.Downstream[0].EntityId);
      // }

      filteredEquipments = filteredEquipments.filter(onlyUnique);
      return filteredEquipments.length != 0 ? filteredEquipments : false;
    }

    function getDistNodeById(Entityid) {
      var distNode = null;
      fdcHierarchydataData.filter(node => node.EntityId == Entityid).forEach(dist => {
        distNode = dist;
      })
      return distNode;
    }

    function getAreaNodeById(Entityid) {
      var areaNode = null;
      fdcHierarchydataData.forEach(dist => {
        dist.children.forEach(area => {
          if (area.EntityId == Entityid) {
            areaNode = area;
          }
        });
      })
      return areaNode;
    }

    function getFieldNodeById(Entityid) {
      var fieldNode = null;
      fdcHierarchydataData.forEach(dist => {
        dist.children.forEach(area => {
          area.children.forEach(field => {
            if (field.EntityId == Entityid) {
              fieldNode = field;
            }
          });
        });
      });
      return fieldNode;
    }

    /**
     * get all the faciltiy with EntityId equals to @param Entityid
     * @param Entityid
     * @returns {[]}
     */
    function getFacilitieNodeByID(Entityid) {
      try {
        var facilitieNode = null;
        var arrFacility = []
        fdcHierarchydataData.forEach(dist => {
          dist.children.forEach(area => {
            area.children.forEach(field => {
              field.children.forEach(facilitie => {
                if (facilitie.EntityId == Entityid) {
                  facilitieNode = facilitie;
                  arrFacility.push(facilitie)
                }
                // if(facilitie.children.length > 0)
                // {
                //   facilitie.children.forEach(child => {
                //     if (child.EntityId == Entityid) {
                //       arrFacility.push(child)
                //     }
                //   });
                // }
                if (facilitie.children && facilitie.children.length > 0) {
                  var temp = childrenFacility(facilitie.children, Entityid);
                  arrFacility = [...temp, ...arrFacility]
                }
              });
            });
          });
        });

        if (facilitieNode == null) {
          var facilityarray = jsonData.facilities.filter(node => node.EntityId == Entityid);
          facilitieNode = facilityarray.length > 0 ? facilityarray[0] : null;
        }
        if (arrFacility.length != 0) {
          return arrFacility;
        }
        return [facilitieNode];
      } catch (error) {
        console.log(error)
      }

    }

    function getTimeZone(item) {
      if (item["TimeZone"]) {
        return item["TimeZone"];
      } else {
        return "Africa//Abidjan (GMT+00:00)";
      }
    }

    /**
     * all the facilities which have EntityId equals to @param EntityId
     * @param facilitie all the childrens of facility
     * @param Entityid  Entity Id of faciltiy object, which it will serach in all the childrens
     * @return {[]}
     * this method is getting call from @method getFacilitieNodeByID()
     */
    function childrenFacility(facilitie, Entityid) {
      var arrFacility = []
      facilitie.forEach(child => {
        if (child.EntityId == Entityid) {
          arrFacility.push(child)
        }
        if (child.children && child.children.length > 0) {
          var temp = childrenFacility(child.children, Entityid)
          if (temp) {
            arrFacility = [...arrFacility, ...temp]
          }
        }
      });
      return arrFacility;
    }


    function pushIfItsUnique(arr, TempData) {
      return arr.some(function (el) {
        if (el.EntityId === TempData.EntityId) {
        }
        return el.EntityId === TempData.EntityId;
      });
    }

    var fdcHierarchydataData = [];
    var TempArray = [];
    jsonData.facilities.forEach(facilitie => {
      facilitie['children'] = [];
      facilitie["typeOfField"] = "Location"
    });
    jsonData.wells.forEach(well => {
      well['children'] = [];
      well["typeOfField"] = "Location"
    });
    jsonData.headers.forEach(header => {
      header['children'] = [];
      header["typeOfField"] = "Location"
    });
    var externalLocation = {
      'EntityId': 'HaulLocation',
      'EntityName': 'Haul Locations',
      'EntityType': 'HaulLocation',
      'children': []
    }

    jsonData.district.forEach(dist => {
      dist['children'] = [];
      let timezone = getTimeZone(dist);
      if (!pushIfItsUnique(fdcHierarchydataData, dist)) {
        fdcHierarchydataData.push(dist);
      }
    });
    jsonData.areas.forEach(area => {
      area['children'] = [];
      var tempdist = null;
      if (area.Parent && area.Parent.length > 0) {
        tempdist = getDistNodeById(area.Parent[0].EntityId);
        let timezone = getTimeZone(area);
        // obj = { "EntityName": area.EntityName, "EntityType": area.EntityType, "Children": [], "children": [], "EntityId": area.EntityId, "parent": { "id": area.Parent[0].EntityId, "text": area.Parent[0].EntityName }, "ExistingParent": { "id": area.Parent[0].EntityId, "text": area.Parent[0].EntityName }, "Latitude": 0, "Longitude": 0, "TypeOfField": "Hierarchy", "TimeZone": timezone };
        // allLeftTreeNodes.children.push(obj);
      }
      else {
      }
      if (tempdist != null) {
        if (!pushIfItsUnique(tempdist.children, area)) {
          tempdist.children.push(area);
        }
      }
    });
    jsonData.fields.forEach(field => {
      field['children'] = [];
      if (field.Parent && field.Parent.length > 0) {
        let timezone = getTimeZone(field);
        // obj = { "EntityName": field.EntityName, "EntityType": field.EntityType, "Children": [], "children": [], "EntityId": field.EntityId, "parent": { "id": field.Parent[0].EntityId, "text": field.Parent[0].EntityName }, "ExistingParent": { "id": field.Parent[0].EntityId, "text": field.Parent[0].EntityName }, "Latitude": 0, "Longitude": 0, "TypeOfField": "Hierarchy", "TimeZone": timezone };
        // allLeftTreeNodes.children.push(obj);
        var temparea = getAreaNodeById(field.Parent[0].EntityId)
        if (temparea != null) {
          if (!pushIfItsUnique(temparea.children, field)) {
            temparea.children.push(field);
          }
        }
      }
      else {
      }

    });
    // jsonData.equipments.forEach(equipment => {
    //   if (!equipment['children']) {
    //     equipment['children'] = [];
    //   }
    // });

    function setFacilityInField(TempArray) {
      // var parentForWell = getFieldNodeById(facilitie.Parent[0].EntityId)
      //var isExist = false;
      TempArray.forEach(dsloc => {
        var parentForWell = getFieldNodeById(dsloc.data.Parent[0].EntityId)
        var isExist = isWellExistInField(parentForWell.children, dsloc.data.EntityId);

        if (!isExist) {
          parentForWell.children.push(JSON.parse(JSON.stringify(dsloc.data)));
        }
      })

    }
    // console.log("Entities worker before facilites", new Date().getMinutes() + ":" + new Date().getSeconds())
    jsonData.facilities.forEach(facilitie => {
      let timezone = getTimeZone(facilitie);
      if ((facilitie.Status != null) && facilitie.Status.toUpperCase() != "SUSPENDED") {
        if (facilitie.Properties && facilitie.Properties[0].hasOwnProperty('HaulLocation') && ((<string>facilitie.Properties[0].HaulLocation).toUpperCase() == "ON" || (<string>facilitie.Properties[0].HaulLocation).toUpperCase() == "TRUE")) {
          //allLeftTreeNodes.children.push(facilitie);
          if (!pushIfItsUnique(externalLocation.children, facilitie)) {
            externalLocation.children.push(facilitie);
          }
        } else if (facilitie["Parent"] && facilitie["Parent"][0] && facilitie.Properties && facilitie.Properties[0].hasOwnProperty('HaulLocation') && ((<string>facilitie.Properties[0].HaulLocation).toUpperCase() == "OFF" || (<string>facilitie.Properties[0].HaulLocation).toUpperCase() == "FALSE")) {
          var DownstreamLoc = getDownstreamLocForId(facilitie, jsonData.equipments);
          // obj = { "EntityName": facilitie.EntityName, "EntityType": facilitie.EntityType, "Children": [], "children": [], "EntityId": facilitie.EntityId, "parent": { "id": facilitie.Parent[0].EntityId, "text": facilitie.Parent[0].EntityName }, "ExistingParent": { "id": facilitie.Parent[0].EntityId, "text": facilitie.Parent[0].EntityName }, "HaulLocation": facilitie.Properties[0]["HaulLocation"], "OperatedStatus": facilitie.Properties[0]["OperatedStatus"], "UWI": facilitie.Properties[0]["UWI"], "Suspended": facilitie.Properties[0]["Suspended"], "Latitude": 0, "Longitude": 0, "TypeOfField": "Locations", "TimeZone": timezone, "Status": (facilitie.Status != null) ? facilitie.Status : "Active" };
          // allLeftTreeNodes.children.push(obj);
          if (DownstreamLoc == false) {
            var tempfield = getFieldNodeById(facilitie.Parent[0].EntityId)
            if (tempfield != null) {
              tempfield.children.push(JSON.parse(JSON.stringify(facilitie)));
            }
          }
          else {
            DownstreamLoc.forEach(dsloc => {
              TempArray.push({ 'locId': dsloc, 'data': facilitie })
            });
            var parentForWell = getFieldNodeById(facilitie.Parent[0].EntityId)
            var isExist = false;
            DownstreamLoc.forEach(dsloc => {
              if (!isExist && parentForWell != null) {
                isExist = isWellExistInField(parentForWell.children, dsloc);
              }
            })
            if (!isExist && parentForWell != null) {
              //parentForWell.children.push(facilitie);
              var CheckPrimaryCondtionArray = [];
              if (DownstreamLoc.length != 0) {
                var equipmentData = null;
                DownstreamLoc.forEach(dsloc => {
                  if (jsonData.equipments && jsonData.equipments.length != 0)
                    equipmentData = jsonData.equipments.filter(obj => obj.Downstream && obj.Downstream[0] && obj.Downstream[0].EntityId == dsloc)[0];

                  var facilityExists: boolean = false;
                  jsonData.facilities.forEach(floc => {
                    if (floc.EntityId == dsloc && floc.Parent[0].EntityId != facilitie.Parent[0].EntityId) {
                      if ((facilitie.SetAsPrimary && facilitie.SetAsPrimary[0].SetAsPrimary == true) || (equipmentData != null && equipmentData.SetAsPrimary && equipmentData.SetAsPrimary[0].SetAsPrimary == true)) {
                        facilityExists = true;
                        CheckPrimaryCondtionArray.push(floc);
                      }
                    }
                  });
                  if (!facilityExists) {
                    jsonData.headers.forEach(hloc => {
                      if (hloc.EntityId == dsloc && hloc.Parent[0].EntityId != facilitie.Parent[0].EntityId) {
                        if ((facilitie.SetAsPrimary && facilitie.SetAsPrimary[0].SetAsPrimary == true) || (equipmentData != null && equipmentData.SetAsPrimary && equipmentData.SetAsPrimary[0].SetAsPrimary == true)) {
                          CheckPrimaryCondtionArray.push(hloc);
                        }
                      }
                    });
                  }
                });
              }
              if (CheckPrimaryCondtionArray.length == 0) {
                parentForWell.children.push(facilitie);
              }
            }
          }

        }
        else {
        }
      }
    });
    setDataToDownStreamLocation(TempArray);
    // console.log("Entities worker after facilites", new Date().getMinutes() + ":" + new Date().getSeconds())

    // storing TempArray data of facilities as downstream
    //To use facilites as downstream for delivered.
    var faciltitesForDownStream = JSON.parse(JSON.stringify(TempArray));

    TempArray = [];

    jsonData.headers.forEach(header => {
      let timezone = getTimeZone(header);
      if ((header != null) && header.Status.toUpperCase() != "SUSPENDED") {
        if (header.Properties && header.Properties[0].hasOwnProperty('HaulLocation') && ((<string>header.Properties[0].HaulLocation).toUpperCase() == "ON" || (<string>header.Properties[0].HaulLocation).toUpperCase() == "TRUE")) {
          //allLeftTreeNodes.children.push(header);
          if (!pushIfItsUnique(externalLocation.children, header)) {
            externalLocation.children.push(header);
          }
        } else if (header.Properties && header.Properties[0].hasOwnProperty('HaulLocation') && ((<string>header.Properties[0].HaulLocation).toUpperCase() == "OFF" || (<string>header.Properties[0].HaulLocation).toUpperCase() == "FALSE") && header.Parent && header.Parent.length > 0) {
          var DownstreamLoc = getDownstreamLocForId(header, jsonData.equipments);
          // obj = { "EntityName": header.EntityName, "EntityType": header.EntityType, "Children": [], "children": [], "EntityId": header.EntityId, "parent": { "id": header.Parent[0].EntityId, "text": header.Parent[0].EntityName }, "ExistingParent": { "id": header.Parent[0].EntityId, "text": header.Parent[0].EntityName }, "HaulLocation": header.Properties[0]["HaulLocation"], "OperatedStatus": header.Properties[0]["OperatedStatus"], "UWI": header.Properties[0]["UWI"], "Suspended": header.Properties[0]["Suspended"], "Latitude": 0, "Longitude": 0, "TypeOfField": "Locations", "TimeZone": timezone, "Status": (header.Status != null) ? header.Status : "Active" };
          // allLeftTreeNodes.children.push(obj);
          if (DownstreamLoc == false) {
            var tempfield = getFieldNodeById(header.Parent[0].EntityId)
            if (tempfield != null) {
              tempfield.children.push(header);
            }
          } else {
            DownstreamLoc.forEach(dsloc => {
              TempArray.push({ 'locId': dsloc, 'data': header })
            });

            var parentForWell = getFieldNodeById(header.Parent[0].EntityId)
            var isExist = false;
            DownstreamLoc.forEach(dsloc => {
              if (!isExist && parentForWell != null) {
                isExist = isWellExistInField(parentForWell.children, dsloc);
              }
            })
            if (!isExist && parentForWell != null) {
              //parentForWell.children.push(header);
              var CheckPrimaryCondtionArray = [];
              if (DownstreamLoc.length != 0) {
                var equipmentData = null;
                DownstreamLoc.forEach(dsloc => {
                  if (jsonData.equipments && jsonData.equipments.length != 0)
                    equipmentData = jsonData.equipments.filter(obj => obj.Downstream && obj.Downstream[0] && obj.Downstream[0].EntityId == dsloc)[0];

                  var facilityExists: boolean = false;
                  jsonData.facilities.forEach(floc => {
                    if (floc.EntityId == dsloc && floc.Parent[0].EntityId != header.Parent[0].EntityId) {
                      if ((header.SetAsPrimary && header.SetAsPrimary[0].SetAsPrimary == true) || (equipmentData != null && equipmentData.SetAsPrimary && equipmentData.SetAsPrimary[0].SetAsPrimary == true)) {
                        facilityExists = true;
                        CheckPrimaryCondtionArray.push(floc);
                      }
                    }
                  });
                  if (!facilityExists) {
                    jsonData.headers.forEach(hloc => {
                      if (hloc.EntityId == dsloc && hloc.Parent[0].EntityId != header.Parent[0].EntityId) {
                        if ((header.SetAsPrimary && header.SetAsPrimary[0].SetAsPrimary == true) || (equipmentData != null && equipmentData.SetAsPrimary && equipmentData.SetAsPrimary[0].SetAsPrimary == true)) {
                          CheckPrimaryCondtionArray.push(hloc);
                        }
                      }
                    });
                  }
                });
              }
              if (CheckPrimaryCondtionArray.length == 0) {
                parentForWell.children.push(header);
              }
            }
          }
        }
        else {
          // console.log;
        }
      }
    });
    setDataToDownStreamLocation(TempArray);
    // setFacilityInField(TempArray);
    //calling method to set facility as downstream for delivered type.
    setDataToDownStreamLocation(faciltitesForDownStream, true);
    // setFacilityInField(faciltitesForDownStream);

    TempArray = [];
    // console.log("Entities worker after headers", new Date().getMinutes() + ":" + new Date().getSeconds())

    jsonData.wells.forEach(well => {
      let timezone = getTimeZone(well);
      let downstreamid = well.Downstream != null ? well.Downstream[0]["EntityId"] : "";
      if ((well != null) && well.Status.toUpperCase() != "SUSPENDED") {
        if (well.Properties && well.Properties[0].HaulLocation && ((<string>well.Properties[0].HaulLocation).toUpperCase() == "ON" || (<string>well.Properties[0].HaulLocation).toUpperCase() == "TRUE")) {
          //allLeftTreeNodes.children.push(well);
          if (!pushIfItsUnique(externalLocation.children, well)) {
            externalLocation.children.push(Object.assign({}, well));
          }
        }
        else if (well.Properties && well.Properties[0].HaulLocation && (<string>well.Properties[0].HaulLocation).toUpperCase() == "OFF" && well.Parent && well.Parent.length > 0) {
          var DownstreamLoc = getDownstreamLocForId(well, jsonData.equipments);
          // obj = { "EntityName": well.EntityName, "EntityType": well.EntityType, "Children": [], "children": [], "EntityId": well.EntityId, "parent": { "id": well.Parent[0].EntityId, "text": well.Parent[0].EntityName }, "ExistingParent": { "id": well.Parent[0].EntityId, "text": well.Parent[0].EntityName }, "HaulLocation": well.Properties[0]["HaulLocation"], "OperatedStatus": well.Properties[0]["OperatedStatus"], "UWI": well.Properties[0]["UWI"], "Suspended": well.Properties[0]["Suspended"], "Latitude": 0, "Longitude": 0, "TypeOfField": "Locations", "TimeZone": timezone, "DownStreamLocationId": downstreamid };
          // allLeftTreeNodes.children.push(obj);
          if (DownstreamLoc == false) {
            var tempfield = getFieldNodeById(well.Parent[0].EntityId)
            if (tempfield != null) {
              tempfield.children.push(well);
            }
          }
          else {
            DownstreamLoc.forEach(dsloc => {
              TempArray.push({ 'locId': dsloc, 'data': well })
            });

            //logic to add the well in field if its all the downstreams are exist in another field
            var parentForWell = getFieldNodeById(well.Parent[0].EntityId)
            var isExist = false;
            DownstreamLoc.forEach(dsloc => {
              if (!isExist && parentForWell != null) {
                isExist = isWellExistInField(parentForWell.children, dsloc);
              }
            })
            if (!isExist && parentForWell != null) {
              var CheckPrimaryCondtionArray = [];
              if (DownstreamLoc.length != 0) {
                var equipmentData = null;
                DownstreamLoc.forEach(dsloc => {
                  if (jsonData.equipments && jsonData.equipments.length != 0)
                    equipmentData = jsonData.equipments.filter(obj => obj.Downstream && obj.Downstream[0] && obj.Downstream[0].EntityId == dsloc)[0];

                  var facilityExists: boolean = false;
                  jsonData.facilities.forEach(floc => {
                    if (floc.EntityId == dsloc && floc.Parent[0].EntityId != well.Parent[0].EntityId) {
                      if ((well.SetAsPrimary && well.SetAsPrimary[0].SetAsPrimary == true) || (equipmentData != null && equipmentData.SetAsPrimary && equipmentData.SetAsPrimary[0].SetAsPrimary == true)) {
                        facilityExists = true;
                        CheckPrimaryCondtionArray.push(floc);
                      }
                    }
                  });
                  if (!facilityExists) {
                    jsonData.headers.forEach(hloc => {
                      if (hloc.EntityId == dsloc && hloc.Parent[0].EntityId != well.Parent[0].EntityId) {
                        if ((well.SetAsPrimary && well.SetAsPrimary[0].SetAsPrimary == true) || (equipmentData != null && equipmentData.SetAsPrimary && equipmentData.SetAsPrimary[0].SetAsPrimary == true)) {
                          CheckPrimaryCondtionArray.push(hloc);
                        }
                      }
                    });
                  }
                });
              }
              if (CheckPrimaryCondtionArray.length == 0) {
                parentForWell.children.push(well);
              }
            }
          }
        }
        else {
        }
      }
    });
    // console.log("Entities worker after wells", new Date().getMinutes() + ":" + new Date().getSeconds())

    function deleteFacilityFormField(jsonData1) {
      try {
        jsonData1.forEach(element => {
          if (element.Parent && element.Parent[0] && element.Parent[0].EntityId) {
            var parentForWell = getFieldNodeById(element.Parent[0].EntityId)
            if (parentForWell && parentForWell.children && parentForWell.children.length != 0) {
              var children1 = parentForWell.children.filter(data => data.EntityId == element.EntityId);
              if (children1.length != 0) {
                parentForWell.children.forEach(element1 => {
                  var isExist = isWellExistInField(element1.children, element.EntityId);
                  if (isExist) {
                    var index = parentForWell.children.findIndex(data => data.EntityId == element.EntityId);
                    if (index != -1) {
                      parentForWell.children.splice(index, 1)
                    }
                  }
                });
              }
            }
          }
        });
      }
      catch (e) {
        console.error("error in deleteFacilityFormField" + e)
      }
    }
    deleteFacilityFormField(jsonData.facilities);
    deleteFacilityFormField(jsonData.headers);

    /**
     * if wellEntityId exist method will return true, if does not exist method will return false.
     * @param children chilredn array of field or any location
     * @param wellEntityId method will check if wellEntityId Exist in the field or location
     * @returns {boolean}
     */
    function isWellExistInField(children, wellEntityId) {
      var isExist = false
      children.forEach(ch => {
        if (ch.children && ch.children.length != 0) {
          var temp = isWellExistInField(ch.children, wellEntityId)
          if (temp) {
            isExist = temp;
          }
        }
        if (ch.EntityId == wellEntityId) {
          isExist = true
          return isExist;
        }
      });
      return isExist;
    }
    setDataToDownStreamLocation(JSON.parse(JSON.stringify(TempArray)));
    TempArray = [];
    fdcHierarchydataData.reverse();

    /**
     * @param fdcHierarchydataData1 tree Hierarchy Data
     * @returns sorted tree Hierarchy Data based on EntityName
     */
    function sortFdcHerarchyData(fdcHierarchydataData1) {
      fdcHierarchydataData1.sort((firstEntity, secondEntity) => { return firstEntity["EntityName"].toLowerCase() > secondEntity["EntityName"].toLowerCase() ? 1 : -1 })
      fdcHierarchydataData1.forEach(element => {
        if (element.children.length != 0) {
          sortFdcHerarchyData(element.children);
        }
      });
      return fdcHierarchydataData1;
    }
    var sortedData = sortFdcHerarchyData(fdcHierarchydataData);

    // sorting logic for external location based on EntityName
    if (externalLocation && externalLocation.children && externalLocation.children.length != 0) {
      externalLocation.children.sort((firstEntity, secondEntity) => { return firstEntity["EntityName"].toLowerCase() > secondEntity["EntityName"].toLowerCase() ? 1 : -1 })
    }
    // obj = { "EntityName": "lkjhgfdsa098765", "EntityType": null, "Children": [], "children": [], "area": [], "EntityId": "lkjhgfdsa098765", "parent": { "id": "", "text": null }, "ExistingParent": { "id": "", "text": null }, "Latitude": 0, "Longitude": 0, "TypeOfField": "Hierarchy" };
    // allLeftTreeNodes.children.push(obj);
    sortedData.push(externalLocation);
    // sortedData.push(allLeftTreeNodes);
    return sortedData;
  }

  CreateLicenseeHeirarchy(jsonData: any) {
    var alltheLocations = [...jsonData.facilities, ...jsonData.headers, ...jsonData.wells, ...jsonData.equipments]
    jsonData.facilities.forEach(facilitie => {
      facilitie['children'] = [];
      facilitie["typeOfField"] = "Location"
    });
    jsonData.wells.forEach(well => {
      well['children'] = [];
      well["typeOfField"] = "Location"
    });
    jsonData.headers.forEach(header => {
      header['children'] = [];
      header["typeOfField"] = "Location"
    });
    //function to remove duplicate data from array
    function removeDuplicate(arr) {
      const newArray = arr.filter((thing, index, self) =>
        index === self.findIndex((t) => (
          t.EntityId === thing.EntityId
        ))
      )
      return newArray;
    }

    //all the licencees
    var licencees = []
    alltheLocations.forEach(data => {
      if (data.Licensee && data.Licensee[0]) {
        licencees.push(data.Licensee[0])
        data.Licensee[0].children = [];
      }
    })

    licencees = removeDuplicate(licencees);
    licencees = JSON.parse(JSON.stringify(licencees));
    //check unique
    function onlyUnique(value, index, self) {
      return self.indexOf(value) === index;
    }

    //get the downstream of location's equipments
    function getDownstreamLocForId(Entity, equipments) {
      var filteredEquipments = [];
      var equipmentsForLicensee: any;
      var SetAsPrimary: boolean = false;
      try {
        var Equip: any = equipments.filter(equip => equip.Parent[0].EntityId == Entity.EntityId && equip["Downstream"] && equip["Downstream"][0].EntityId && equip["Downstream"][0].EntityId != Entity.EntityId);
        var filteredPrimaryEqips: any = Equip.filter(eq1 => eq1.SetAsPrimary && eq1.SetAsPrimary[0] && eq1.SetAsPrimary[0].SetAsPrimary && eq1.SetAsPrimary[0].SetAsPrimary == true);
        var filteredSecondaryEqips: any = Equip.filter(eq1 => eq1.SetAsPrimary && eq1.SetAsPrimary[0] && eq1.SetAsPrimary[0].SetAsPrimary && eq1.SetAsPrimary[0].SetAsPrimary == false);
        if (Equip && Equip.length != 0) {
          if (filteredPrimaryEqips && filteredPrimaryEqips.length != 0) {
            equipmentsForLicensee = filteredPrimaryEqips;
          } else {
            equipmentsForLicensee = Equip;
          }
        }
        else {
          if (Entity.Downstream && Entity.Downstream[0] && Entity.Downstream[0].EntityId) {
            filteredEquipments.push(Entity.Downstream[0].EntityId);
          }

        }
        if (equipmentsForLicensee && equipmentsForLicensee.length != 0)
          equipmentsForLicensee.forEach(equipment => {
            //equipments.filter(equip => equip.Parent[0].EntityId == Entity.EntityId).forEach(equipment => {
            //  if(Entity.SetAsPrimary && Entity.SetAsPrimary[0] && Entity.SetAsPrimary[0].SetAsPrimary && Entity.SetAsPrimary[0].SetAsPrimary==true){
            //   filteredEquipments.push(Entity.Downstream[0].EntityId);
            //    return filteredEquipments;
            // }else{
            //   filteredEquipments.push(Entity.Downstream[0].EntityId);
            // }
            if (Entity.Downstream && Entity.Downstream[0] && Entity.Downstream[0].EntityId) {
              //filteredEquipments.push(Entity.Downstream[0].EntityId);
              if (Entity.SetAsPrimary && Entity.SetAsPrimary[0] && Entity.SetAsPrimary[0].SetAsPrimary && Entity.SetAsPrimary[0].SetAsPrimary == true) {
                filteredEquipments.push(Entity.Downstream[0].EntityId);
                return filteredEquipments;
              } else {
                if (equipment.Downstream && equipment.Downstream[0].EntityId != Entity.EntityId) {
                  //filteredEquipments.push(equipment.Downstream[0].EntityId);
                  if (equipment.SetAsPrimary && equipment.SetAsPrimary[0] && equipment.SetAsPrimary[0].SetAsPrimary && equipment.SetAsPrimary[0].SetAsPrimary == true) {
                    filteredEquipments.push(equipment.Downstream[0].EntityId);
                    return filteredEquipments;
                  } else {
                    filteredEquipments.push(Entity.Downstream[0].EntityId);
                  }
                }
                else {
                  filteredEquipments.push(Entity.Downstream[0].EntityId);
                }
              }
            }
            if (equipment.Downstream && equipment.Downstream[0].EntityId != Entity.EntityId) {
              //filteredEquipments.push(equipment.Downstream[0].EntityId);
              if (equipment.SetAsPrimary && equipment.SetAsPrimary[0] && equipment.SetAsPrimary[0].SetAsPrimary && equipment.SetAsPrimary[0].SetAsPrimary == true) {
                SetAsPrimary = true;
                filteredEquipments.push(equipment.Downstream[0].EntityId);
                return filteredEquipments;
              } else {
                if (!SetAsPrimary)
                  filteredEquipments.push(equipment.Downstream[0].EntityId);
              }
            }
          });


        filteredEquipments = filteredEquipments.filter(onlyUnique);
        return filteredEquipments.length != 0 ? filteredEquipments : false;
      } catch (Exception) {
        console.error('Exception in getDownstreamLocForId subscriber of FDCService in  FDC/service at time ' + new Date().toString() + '. Exception is : ' + Exception);
      }
    }


    // function setLicensee()
    // {

    // }

    var tempArray = []

    jsonData.facilities.forEach(facility => {
      if ((facility.Status != null) && facility.Status.toUpperCase() != "SUSPENDED") {
        if (facility.Licensee && facility.Licensee[0] && facility.Licensee[0].EntityId &&
          facility.Properties && facility.Properties[0] && (facility.Properties[0].HaulLocation != "On" || facility.Properties[0].HaulLocation != true)) {
          var downStreamForFaclity = getDownstreamLocForId(facility, jsonData.equipments)
          if (downStreamForFaclity) {
            // downStreamForFaclity.forEach(downStream => {
            //   tempArray.push({ locId: downStream, data: facility })
            // });
            var setAsChildren = false;
            downStreamForFaclity.forEach(downstreamId => {
              if (downstreamId != facility.EntityId) {
                jsonData.facilities.forEach(element => {
                  if (element.EntityId != facility.EntityId
                    && element.Licensee && element.Licensee[0] && element.Licensee[0].EntityId &&
                    element.Licensee[0].EntityId == facility.Licensee[0].EntityId &&
                    element.Properties && element.Properties[0] && (element.Properties[0].HaulLocation != "On" || element.Properties[0].HaulLocation != true) && downstreamId == element.EntityId) {
                    setAsChildren = true;
                    element.children.push(facility);
                  }
                });

                jsonData.headers.forEach(element => {
                  if (element.EntityId != facility.EntityId
                    && element.Licensee && element.Licensee[0] && element.Licensee[0].EntityId &&
                    element.Licensee[0].EntityId == facility.Licensee[0].EntityId &&
                    element.Properties && element.Properties[0] && (element.Properties[0].HaulLocation != "On" || element.Properties[0].HaulLocation != true) && downstreamId == element.EntityId) {
                    setAsChildren = true;
                    element.children.push(facility);
                  }
                });
              }
            })
            if (!setAsChildren) {
              licencees.forEach(data => {
                if (data.EntityId == facility.Licensee[0].EntityId) {
                  data.children.push(facility);
                }
              })
            }
          }
          else {
            licencees.forEach(data => {
              if (data.EntityId == facility.Licensee[0].EntityId) {
                data.children.push(facility);
              }
            })
          }
        }
        else if (facility.Licensee && facility.Licensee[0] && facility.Licensee[0].EntityId &&
          facility.Properties && facility.Properties[0] && (facility.Properties[0].HaulLocation == "On" || facility.Properties[0].HaulLocation == true)) {
          licencees.forEach(data => {
            if (data.EntityId == facility.Licensee[0].EntityId) {
              data.children.push(facility);
            }
          })
        }
      }
    });


    jsonData.headers.forEach(facility => {
      if ((facility.Status != null) && facility.Status.toUpperCase() != "SUSPENDED") {
        if (facility.Licensee && facility.Licensee[0] && facility.Licensee[0].EntityId &&
          facility.Properties && facility.Properties[0] && (facility.Properties[0].HaulLocation != "On" || facility.Properties[0].HaulLocation != true)) {
          var downStreamForFaclity = getDownstreamLocForId(facility, jsonData.equipments)
          if (downStreamForFaclity) {
            // downStreamForFaclity.forEach(downStream => {
            //   tempArray.push({ locId: downStream, data: facility })
            // });
            var setAsChildren = false;
            downStreamForFaclity.forEach(downstreamId => {
              if (downstreamId != facility.EntityId) {
                jsonData.facilities.forEach(element => {
                  if (element.EntityId != facility.EntityId
                    && element.Licensee && element.Licensee[0] && element.Licensee[0].EntityId &&
                    element.Licensee[0].EntityId == facility.Licensee[0].EntityId &&
                    element.Properties && element.Properties[0] && element.Properties[0].HaulLocation != "On" && downstreamId == element.EntityId) {
                    setAsChildren = true;
                    element.children.push(facility);
                  }
                });

                jsonData.headers.forEach(element => {
                  if (element.EntityId != facility.EntityId
                    && element.Licensee && element.Licensee[0] && element.Licensee[0].EntityId &&
                    element.Licensee[0].EntityId == facility.Licensee[0].EntityId &&
                    element.Properties && element.Properties[0] && element.Properties[0].HaulLocation != "On" && downstreamId == element.EntityId) {
                    setAsChildren = true;
                    element.children.push(facility);
                  }
                });
              }
            })
            if (!setAsChildren) {
              licencees.forEach(data => {
                if (data.EntityId == facility.Licensee[0].EntityId) {
                  data.children.push(facility);
                }
              })
            }
          }
          else {
            licencees.forEach(data => {
              if (data.EntityId == facility.Licensee[0].EntityId) {
                data.children.push(facility);
              }
            })
          }
        }
        else if (facility.Licensee && facility.Licensee[0] && facility.Licensee[0].EntityId &&
          facility.Properties && facility.Properties[0] && (facility.Properties[0].HaulLocation == "On" || facility.Properties[0].HaulLocation == true)) {
          licencees.forEach(data => {
            if (data.EntityId == facility.Licensee[0].EntityId) {
              data.children.push(facility);
            }
          })
        }
      }
    });

    jsonData.wells.forEach(facility => {
      if ((facility.Status != null) && facility.Status.toUpperCase() != "SUSPENDED") {
        if (facility.Licensee && facility.Licensee[0] && facility.Licensee[0].EntityId &&
          (facility.Properties && facility.Properties[0] && (facility.Properties[0].HaulLocation != "On" || facility.Properties[0].HaulLocation != true)) || (facility.Properties && !facility.Properties[0])) {
          var downStreamForFaclity = getDownstreamLocForId(facility, jsonData.equipments)
          if (downStreamForFaclity) {
            // downStreamForFaclity.forEach(downStream => {
            //   tempArray.push({ locId: downStream, data: facility })
            // });
            var setAsChildren = false;
            downStreamForFaclity.forEach(downstreamId => {
              if (downstreamId != facility.EntityId) {
                jsonData.facilities.forEach(element => {
                  if (element.EntityId != facility.EntityId
                    && element.Licensee && element.Licensee[0] && element.Licensee[0].EntityId &&
                    element.Licensee[0].EntityId == facility.Licensee[0].EntityId &&
                    element.Properties && element.Properties[0] && (element.Properties[0].HaulLocation != "On" || element.Properties[0].HaulLocation != true) && downstreamId == element.EntityId) {
                    setAsChildren = true;
                    element.children.push(facility);
                  }
                });

                jsonData.headers.forEach(element => {
                  if (element.EntityId != facility.EntityId
                    && element.Licensee && element.Licensee[0] && element.Licensee[0].EntityId &&
                    element.Licensee[0].EntityId == facility.Licensee[0].EntityId &&
                    element.Properties && element.Properties[0] && (element.Properties[0].HaulLocation != "On" || element.Properties[0].HaulLocation != true) && downstreamId == element.EntityId) {
                    setAsChildren = true;
                    element.children.push(facility);
                  }
                });
              }

            })
            if (!setAsChildren) {
              licencees.forEach(data => {
                if (data.EntityId == facility.Licensee[0].EntityId) {
                  data.children.push(facility);
                }
              })
            }
          }
          else {
            licencees.forEach(data => {
              if (data.EntityId == facility.Licensee[0].EntityId) {
                data.children.push(facility);
              }
            })
          }
        }
        else if (facility.Licensee && facility.Licensee[0] && facility.Licensee[0].EntityId &&
          facility.Properties && facility.Properties[0] && (facility.Properties[0].HaulLocation == "On" || facility.Properties[0].HaulLocation == true)) {
          licencees.forEach(data => {
            if (data.EntityId == facility.Licensee[0].EntityId) {
              data.children.push(facility);
            }
          })
        }
      }
    });

    function sortFdcHerarchyData(fdcHierarchydataData1) {
      fdcHierarchydataData1.sort((firstEntity, secondEntity) => { return firstEntity["EntityName"].toLowerCase() > secondEntity["EntityName"].toLowerCase() ? 1 : -1 })
      fdcHierarchydataData1.forEach(element => {
        if (element.children.length != 0) {
          sortFdcHerarchyData(element.children);
        }
      });
      return fdcHierarchydataData1;
    }
    var filteredData = licencees.filter(obj => obj.children && obj.children.length != 0)
    var sortedData = sortFdcHerarchyData(filteredData);
    return sortedData;
  }

  flatToHierarchyForPhysicalFlow(list, fabricName) {
    // console.log(fabricName,"flatToHierarchyForPhysicalFlow")
    let roots = [];
    var json = {
      'EntityId': 'HaulLocation',
      'EntityName': 'Haul Locations',
      'EntityType': 'HaulLocation',
      'children': [],
      'Status': "HaulLocation"
    }
    //if (fabricName != FABRICS.PIGGING && fabricName != FABRICS.OPFFabric)
      roots.push(json);
    let all = {};
    if (!list.length) {
      console.warn("flatToHierarchyForPhysicalFlow list length:" + list.length)
      return [];
    }
    list.forEach(Element => {
      all[Element.EntityId] = Element;
    });
    var key = null;
    var addStatus = false;
    Object.keys(all).forEach(Element => {
      let item = all[Element];
      // key = item.hasOwnProperty("Downstream") ? "Downstream" : item.hasOwnProperty("Upstream") ? "Upstream" : null;
      key = item.hasOwnProperty("Downstream") && item.Downstream.length != 0 ? "Downstream" : item.hasOwnProperty("Upstream") && item.Upstream.length != 0 ? "Upstream" : null;

      if (key != null)
        var updownstreamId = item[key][0] && item[key][0].EntityId ? item[key][0].EntityId : null;

      var updownstrmdata = list.filter(d => d.EntityId == updownstreamId)[0];
      if (updownstrmdata == null || updownstrmdata && !(updownstrmdata.SG && (<string[]>updownstrmdata.SG).includes(fabricName))) {
        // console.log(key ,'data is not available for ::', item);

        delete item.Downstream;
        delete item.Upstream;
      }

      if (!(item[key] && item[key][0] && item[key][0]["EntityId"]) || item.Properties && item.Properties[0] && (item.Properties[0].HaulLocation == "On" || item.Properties[0].HaulLocation == true) || updownstrmdata && updownstrmdata.Properties && updownstrmdata.Properties[0] && (updownstrmdata.Properties[0].HaulLocation == "On" || updownstrmdata.Properties[0].HaulLocation == true)) {
        if (item.Properties && item.Properties[0] && (item.Properties[0].HaulLocation == "On" || item.Properties[0].HaulLocation == true)) {
          var haulLocation = roots.filter(obj => obj.EntityId == 'HaulLocation')
          if (haulLocation && haulLocation.length > 0) {
            haulLocation[0].children.push(item);
            delete all[item.EntityId];
          }
        } else {
          roots.push(item);
        }
      } else if (item[key] && item[key][0]) {
        var id = item[key][0]["EntityId"];
        if (id in all) {
          item = nestedParent(item.EntityId, item, roots);
          roots = nestedParentPush(id, item, roots);
          let p = all[id];
          if (!all[id].children) {
            all[id].children = [];
          }
          if (!addStatus && all[id].children.filter(obj => obj.entityId == item.EntityId).length == 0) {
            all[id].children.push(item);
          }
          addStatus = false;
        }
      } else if (key == null && !item.Parent) {
        var finalIndex = roots.findIndex(obj => obj.EntityId == item.EntityId);
        if (finalIndex == -1) {
          roots.push(item);
        }
        delete all[item.EntityId];
      }
    });

    function nestedParent(id, item, root) {
      root.forEach((obj1, index) => {
        if (obj1 && obj1[key] && obj1[key][0] && obj1[key][0].EntityId && obj1[key][0].EntityId.indexOf(id) != -1) {
          item.children = item.children ? item.children : [];
          item.children.push(obj1);
          delete all[obj1.entityId];
          var finalIndex = roots.findIndex(obj => obj.EntityId == obj1.EntityId);
          roots.splice(finalIndex, 1);
        } else if (obj1.children && obj1.children.length > 0) {
          item = nestedParent(id, item, obj1.children);
        }
      }
      )
      return item;
    }
    function nestedParentPush(id, item1, root1) {
      root1.forEach((obj2, index) => {
        if (obj2.EntityId == id) {
          obj2.children = obj2.children ? obj2.children : [];
          obj2.children.push(item1);
          addStatus = true;
        } else if (obj2.children && obj2.children.length > 0) {
          obj2.children = nestedParentPush(id, item1, obj2.children);
        }
        root1[index] = obj2;
      })
      return root1;
    }

    var haulLocation = roots.filter(obj => obj.EntityId == 'HaulLocation')
    if (haulLocation && haulLocation.length > 0) {
      var haulChildrensList = haulLocation[0].children.filter(obj => obj.children && obj.children.length > 0);
      haulChildrensList.forEach(node => {
        roots.push(node);
        var index = haulLocation[0].children.findIndex(obj => obj.EntityId == node.EntityId);
        haulLocation[0].children.splice(index, 1);
      });
    }
    roots = this.physicalFlowSorting(roots);

    let leftTreeData = roots
    // console.log('lefttreeData....')
    return leftTreeData;
  }


  flatToHierarchyDistrictAreaField(list) {
    let roots = [];
    var key = null;
    let all = {};
    try {
      if (!list.length) {
        return;
      }
      list.forEach(Element => {
        all[Element.EntityId] = Element;
      });
      var addStatus = false;
      Object.keys(all).forEach(Element => {
        let item = all[Element];
        key = "Parent";
        if (!(item[key] && item[key][0] && item[key][0]["EntityId"])) {
          roots.push(item);
        } else if (item[key] && item[key][0]) {
          var id = item[key][0]["EntityId"];
          if (id in all) {
            item = nestedParent(item.EntityId, item, roots);
            roots = nestedParentPush(id, item, roots);
            let p = all[id];
            if (!all[id].children) {
              all[id].children = [];
            }
            if (!addStatus && all[id].children.filter(obj => obj.entityId == item.EntityId).length == 0) {
              all[id].children.push(item);
            }
            addStatus = false;
          }
        }
      });
    }
    catch (e) {
      console.error(e);
    }
    function nestedParent(id, item, root) {
      root.forEach((obj1, index) => {
        if (obj1 && obj1[key] && obj1[key][0] && obj1[key][0].EntityId && obj1[key][0].EntityId.indexOf(id) != -1) {
          item.children = item.children ? item.children : [];
          item.children.push(obj1);
          delete all[obj1.entityId];
          var finalIndex = roots.findIndex(obj => obj.EntityId == obj1.EntityId);
          roots.splice(finalIndex, 1);
        } else if (obj1.children && obj1.children.length > 0) {
          item = nestedParent(id, item, obj1.children);
        }
      }
      )
      return item;
    }
    function nestedParentPush(id, item1, root1) {
      root1.forEach((obj2, index) => {
        if (obj2.EntityId == id) {
          obj2.children = obj2.children ? obj2.children : [];
          obj2.children.push(item1);
          addStatus = true;
        } else if (obj2.children && obj2.children.length > 0) {
          obj2.children = nestedParentPush(id, item1, obj2.children);
        }
        root1[index] = obj2;
      })
      return root1;
    }

    var FilteredLocations = roots;//.filter(obj => (obj.Status && obj.Status.toUpperCase() != "SUSPENDED"));
    FilteredLocations = this.physicalFlowSorting(FilteredLocations);

    return FilteredLocations;
  }

  flatToHierarchyForFacilityCode(list, fabricName) {
    // console.log(fabricName,"flatToHierarchyForFacilityCode")
    let roots = [];
    var json = {
      'EntityId': 'HaulLocation',
      'EntityName': 'Haul Locations',
      'EntityType': 'HaulLocation',
      'children': [],
      'Status': "HaulLocation"
    }
    roots.push(json)
    let all = {};
    if (!list.length) {
      return [];
    }
    list.forEach(Element => {
      all[Element.EntityId] = Element;
    });
    var key = null;
    var addStatus = false;
    Object.keys(all).forEach(Element => {
      let item = all[Element];
      key = item.hasOwnProperty("NestedUnder") ? "NestedUnder" : null;
      if (key != null && item[key] != null && item[key][0] != undefined) {
        var nestedunderId = item[key][0] && item[key][0].EntityId ? item[key][0].EntityId : null;
      }
      var entityData = list.filter(d => d.EntityId == nestedunderId)[0];
      if (entityData == null || entityData && !(entityData.SG && (<string[]>entityData.SG).includes(fabricName))) {
        delete item.NestedUnder;
      }
      if (item.Properties && item.Properties.length != 0 && (item.Properties[0].HaulLocation == "On" || item.Properties[0].HaulLocation == true)) {
        var haulLocation = roots.filter(obj => obj.EntityId == 'HaulLocation')
        if (haulLocation && haulLocation.length > 0) {
          haulLocation[0].children.push(item);
          delete all[item.EntityId];
        }
      }
      else {
        if (item[key] && item[key][0]) {
          var id = item[key][0]["EntityId"];
          if (id in all) {
            item = nestedParent(item.EntityId, item, roots);
            roots = nestedParentPush(id, item, roots);
            let p = all[id];
            if (!all[id].children) {
              all[id].children = [];
            }
            if (!addStatus && all[id].children.filter(obj => obj.entityId == item.EntityId).length == 0) {
              all[id].children.push(item);
            }
            addStatus = false;
          }
        } else if (key == null || item[key] == null) {
          var finalIndex = roots.findIndex(obj => obj.EntityId == item.EntityId);
          if (finalIndex == -1) {
            roots.push(item);
          }
          //delete all[item.EntityId];
        }
      }
    });

    function nestedParent(id, item, root) {
      root.forEach((obj1, index) => {
        if (obj1 && obj1[key] && obj1[key][0] && obj1[key][0].EntityId && obj1[key][0].EntityId.indexOf(id) != -1) {
          item.children = item.children ? item.children : [];
          item.children.push(obj1);
          delete all[obj1.entityId];
          var finalIndex = roots.findIndex(obj => obj.EntityId == obj1.EntityId);
          roots.splice(finalIndex, 1);
        } else if (obj1.children && obj1.children.length > 0) {
          item = nestedParent(id, item, obj1.children);
        }
      }
      )
      return item;
    }
    function nestedParentPush(id, item1, root1) {
      root1.forEach((obj2, index) => {
        if (obj2.EntityId == id) {
          obj2.children = obj2.children ? obj2.children : [];
          obj2.children.push(item1);
          addStatus = true;
        } else if (obj2.children && obj2.children.length > 0) {
          obj2.children = nestedParentPush(id, item1, obj2.children);
        }
        root1[index] = obj2;
      })
      return root1;
    }
    var haulLocation = roots.filter(obj => obj.EntityId == 'HaulLocation')
    if (haulLocation && haulLocation.length > 0) {
      var haulChildrensList = haulLocation[0].children.filter(obj => obj.children && obj.children.length > 0);
      haulChildrensList.forEach(node => {
        roots.push(node);
        var index = haulLocation[0].children.findIndex(obj => obj.EntityId == node.EntityId);
        haulLocation[0].children.splice(index, 1);
      });
    }
    
    roots.forEach(ele=>{
      if(ele.EntityId=='HaulLocation'){
        ele.children=this.getSortedTree(ele.children);
      }
    })
    roots = this.getSortedTree(roots);
    let leftTreeData = roots.map(d => {
      roots = this.getSortedTree(roots);
      return {
        EntityId: d.EntityId,
        EntityType: d.EntityType,
        EntityName: d.EntityName,
        FacilityCode: d.EntityName,
        Parent: d.Parent ? d.Parent : [],
        // Downstream: d.Downstream ? d.Downstream : [],

        children: d.children ? d.children : [],
        TypeOf: d.TypeOf
      }
    });
    // console.log('lefttreeData....',"flatToHierarchyForFacilityCode")
    return leftTreeData;
  }
  physicalFlowSorting(list: any[]) {

    list = list.map(d => {
      return {
        EntityId: d.EntityId,
        EntityType: d.EntityType,
        EntityName: d.EntityName,
        Parent: d.Parent ? d.Parent : [],
        children: d.children ? d.children : [],
        WellType: d.WellType ? d.WellType : "",
        TypeOf: d.TypeOf
      }
    });

    list.forEach(element => {
      if (element.children && element.children.length > 0) {
        element.children = this.physicalFlowSorting(element.children);
      }
    });
    list = this.getSortedTree(list);
    return list;
  }


  getSortedTree(listOfChildren) {
    try {
      listOfChildren.sort((firstEntity, secondEntity) => {
        if (firstEntity['EntityName'] && secondEntity['EntityName'])
          return firstEntity['EntityName'].toLowerCase() > secondEntity['EntityName'].toLowerCase() ? 1 : -1
      })
      return listOfChildren;
    }
    catch (e) {
      console.error(e);
    }
  }

  constructDgraphFormatDataWorkerAll(entities) {
    //console.log("constructDgraphFormatDataWorkerAll...")
    // console.log(entities)

    var tempEntities = JSON.parse(JSON.stringify(entities.filter(entity => entity.typeOfField === "Location")));
    tempEntities = tempEntities.map(d => {
      return {
        EntityId: d.EntityId,
        EntityName: d.EntityName,
        EntityType: d.EntityType,
        Parent: d.Parent ? d.Parent : [],
        children: d.children ? d.children : [],
        WellType: d.WellType ? d.WellType : "",
        TypeOf: d.TypeOf
      }
    });
    tempEntities = this.getSortedTree(tempEntities);

    //console.log("Sorted Entities for All tree:::");
    //console.log(tempEntities.length);
    return JSON.parse(JSON.stringify(tempEntities));
  }

  entitiesPhysicalFlowPartialHierachy(entities, fabricName) {
    // console.log("entitiesPhysicalFlowPartialHierachy...");
    // var dgraphJsonModel = this.constructDgraphFormatDataWorker(entities);

    //TreeTypeNames.PHYSICALFLOW flattenData
    // var flattenData = [...dgraphJsonModel.wells, ...dgraphJsonModel.facilities, ...dgraphJsonModel.headers];
    var physicalFlowData = this.flatToHierarchyForPhysicalFlow(entities.filter(data => data.typeOfField === "Location"), fabricName);

    //PartialHierarachy
    // var finalHierarachy = [...dgraphJsonModel.district, ...dgraphJsonModel.areas, ...dgraphJsonModel.fields];
    //var hierarchyData = entities.filter(data => data.TypeOf === "Area" || data.TypeOf === "Field" || data.TypeOf === "District");
    var partialHierarachData = [];//this.flatToHierarchyDistrictAreaField(hierarchyData);

    if (physicalFlowData) {
      physicalFlowData.forEach(obj => {
        if (obj && obj.Parent && obj.Parent[0]) {
          pushRelatedToParentBased(partialHierarachData, obj);
        }
      });
      partialHierarachData.push(physicalFlowData.filter(obj => obj.EntityId == "HaulLocation")[0]);
    }

    function pushRelatedToParentBased(data, item) {
      data.forEach(element => {
        if (element.EntityId == item.Parent[0]["EntityId"]) {
          element.children = element.children ? element.children : [];
          element.children.push(item);
        } else if (element.children && element.children.length > 0) {
          pushRelatedToParentBased(element.children, item);
        }
      })
    }
    // partialHierarachData.concat(entities.filter(entity => entity.TypeOf === "ananymouslocation"));
    var data = this.physicalFlowSorting(partialHierarachData);

    let hierarcyLeftTreeData = data
    return hierarcyLeftTreeData;
  }

  createKeyValuePairEntities(dGraphdata) {
    var entities = [
      ...dGraphdata.areas,
      ...dGraphdata.district,
      ...dGraphdata.facilities,
      ...dGraphdata.fields,
      ...dGraphdata.headers,
      ...dGraphdata.equipments,
      ...dGraphdata.wells,
      ...dGraphdata.ananymoushaullocations
    ];

    var entitiesKVPObject = {} as any;
    entities.forEach(entity => {
      entitiesKVPObject[entity.EntityId] = entity;
    })
    return entitiesKVPObject;
  }

  constructDgraphFormatDataWorker(entities) {
    var dgraphJsonModel = {
      areas: [],
      district: [],
      equipments: [],
      facilities: [],
      fields: [],
      headers: [],
      wells: [],
      ananymoushaullocations: []
    };

    for (var EntityId in entities) {
      var EntityIdValue = entities[EntityId];
      if (EntityIdValue.hasOwnProperty('EntityType')) {
        if (EntityIdValue.EntityType == "Area" || EntityIdValue.EntityType == "Field" || EntityIdValue.EntityType == "District") {//district,are,field
          switch (EntityIdValue.EntityType) {
            case "Area":
              dgraphJsonModel.areas.push(EntityIdValue);
              break;
            case "Field":
              dgraphJsonModel.fields.push(EntityIdValue);
              break;
            case "District":
              dgraphJsonModel.district.push(EntityIdValue);
              break;
          }
        }
        else {
          if (EntityIdValue.hasOwnProperty('TypeOf')) {//facilities,wells,headers and equipments
            switch (EntityIdValue.TypeOf) {
              case "well":
                dgraphJsonModel.wells.push(EntityIdValue);
                break;
              case "facility":
                dgraphJsonModel.facilities.push(EntityIdValue);
                break;
              case "delivered"://headers
                dgraphJsonModel.headers.push(EntityIdValue);
                break;
              case "equipment":
                dgraphJsonModel.equipments.push(EntityIdValue);
                break;
              // case "ananymouslocation":
              //   dgraphJsonModel.ananymoushaullocations.push(EntityIdValue);
              //   break;
            }
          }
        }

      }
    }//foreach loop end
    return dgraphJsonModel;
  }

  constructDgraphFormatDataWorker2(entities) {
    var dgraphJsonModel = {
      areas: [],
      district: [],
      equipments: [],
      facilities: [],
      fields: [],
      headers: [],
      wells: [],
      ananymoushaullocations: []
    };
    //console.log(entities)
    for (var EntityId in entities) {
      if (entities.hasOwnProperty(EntityId)) {
        var EntityIdValue = entities[EntityId];
        if (EntityIdValue.hasOwnProperty('EntityType')) {
          if (EntityIdValue.EntityType == "Area" || EntityIdValue.EntityType == "Field" || EntityIdValue.EntityType == "District") {//district,are,field
            switch (EntityIdValue.EntityType) {
              case "Area":
                dgraphJsonModel.areas.push(EntityIdValue);
                break;
              case "Field":
                dgraphJsonModel.fields.push(EntityIdValue);
                break;
              case "District":
                dgraphJsonModel.district.push(EntityIdValue);
                break;
            }
          }
          else {
            if (EntityIdValue.hasOwnProperty('TypeOf')) {//facilities,wells,headers and equipments
              switch (EntityIdValue.TypeOf) {
                case "well":
                  dgraphJsonModel.wells.push(EntityIdValue);
                  break;
                case "facility":
                  dgraphJsonModel.facilities.push(EntityIdValue);
                  break;
                case "delivered"://headers
                  dgraphJsonModel.headers.push(EntityIdValue);
                  break;
                case "equipment":
                  dgraphJsonModel.equipments.push(EntityIdValue);
                  break;
                // case "ananymouslocation":
                //   dgraphJsonModel.ananymoushaullocations.push(EntityIdValue);
                //   break;

              }
            }
          }
        }
      }
    }//foreach loop end
    // console.log("Create All create All")
    //console.log(dgraphJsonModel)
    var result1 = JSON.parse(JSON.stringify([...entities.wells, ...entities.facilities, ...entities.headers]));
    return result1;
  }

  findChildrenDownStream(flattenDataMessage) {
    let entities = flattenDataMessage.equipments;
    let locations = [flattenDataMessage.locationId];
    let selectedDownStreamLocation = flattenDataMessage.selectedDownStreamLocation;

    return this.findChildrenDownStreamforEquipment(entities, locations, selectedDownStreamLocation, false);
  }

  findChildrenUpStream(flattenDataMessage) {
    let entities = flattenDataMessage.equipments;
    let locations = [flattenDataMessage.locationId];
    let selectedDownStreamLocation = flattenDataMessage.selectedDownStreamLocation;
    return this.findChildrenUpStreamforEquipment(entities, locations, selectedDownStreamLocation, false);
  }

  findChildrenDownStreamforEquipment(Entities, locations, EntityId, foundEntity?: boolean) {
    try {
     
      return foundEntity;
    } catch (error) {
      console.error(error);
    }

  }

  findChildrenUpStreamforEquipment(Entities, locations, EntityId, foundEntity?: boolean) {
    try {

      return foundEntity;
    } catch (error) {
      console.error(error);
    }

  }
}
