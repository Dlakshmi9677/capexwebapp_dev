import { Query } from '@datorama/akita';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserState, UserStateConstantsKeys } from './user.state.model';
import { UsersStore } from './user.state.store';
import 'rxjs/add/observable/empty'
import { produce } from 'immer';
import * as localForage from 'localforage';
import { debounceTime } from 'rxjs/operators';
@Injectable({ providedIn: 'root' })
export class UsersStateQuery extends Query<UserState> {
  constructor(protected store: UsersStore) {
    super(store);
  }

  /**
   * 
   * To update value of the key item currently present user store
   * 
   * // example
    var key="UserID";
    var innerKey="Fabric";
    var innerValue="fdc";
   * 
   * @param key key of the State item e.g UserID
   * @param innerKey unique key of object
   * @param innerValue value of unique key
   */
  public updateObjectSingleValue(key, innerKey, innerValue) {
    try {
      var stateObject = JSON.parse(JSON.stringify(this.getStateKey(key)));
      if (stateObject.hasOwnProperty(innerKey)) {
        stateObject[innerKey] = innerValue;
        this.add(key, stateObject);
      }
      else {
        console.warn("No key exist to replace for userStore");
      }
    }
    catch (e) {

    }
  }

  /**
   * 
   * To update all value of the key item currently present in Object
   * 
   * // example
    var key="12345678756544896564";
    var value = {Farbic:"HOME"};
   * 
   * @param key key of the State item e.g userID
   * @param value json Object
   */
  public updateObjectMultipleValue(key, value: JSON, options?) {
    try {
      var stateObject = JSON.parse(JSON.stringify(this.getStateKey(key)));
      for (var prop in value) {
        if (value.hasOwnProperty(prop)) {
          if (stateObject.hasOwnProperty(prop)) {
            stateObject[prop] = value[prop];
          }
          else {//need to create usermodel and again need to check there
            if (UserStateConstantsKeys.UserPrefrenceColumnJSON.includes(prop))
              stateObject[prop] = value[prop];
            else
              console.warn("Given payload key does not exist's into UserStore");
          }
        }
      }
      this.add2(key, stateObject);
    }
    catch (e) {

    }
  }



  /**
   * To fully replace key with value
   */
  public add(key, value) {
    try {
      var stateObject = JSON.parse(JSON.stringify(this.getStateKey(UserStateConstantsKeys.userPrefrence)));

      if (stateObject && key === UserStateConstantsKeys.userPrefrence) {
        this.updateObjectMultipleValue(key, value, true);
      }
      else {
        this.add2(key, value);
      }
    }
    catch (e) {

    }
  }

  private add2(key, value) {
    try {
      this.store.update(state => produce(state, draft => {
        draft[key] = value
      }));
    }
    catch (e) {

    }
  }

  public updateState(json:UserState){
    if(json){
      for(var key in json){
        let value = json[key];
        this.add2(key,value);
      }
    }
  }

  public update(path: Array<string>, Value: any) {
    try {
      if (path.length) {
        switch (path.length) {
          case 1:
            this.update1(path[0], Value);
            break;
          case 2:
            this.update2(path[0], path[1], Value)
            break;
          case 3:
            this.update3(path[0], path[1], path[2], Value);
            break;
          case 4:
            this.update4(path[0], path[1], path[2], path[3], Value);
            break;
          default: break;
        }
      }
      else {
        console.warn("Unable to update UserStore with array path string length zero")
      }
    }
    catch (e) {
      console.warn("user.state.query" + e)
    }
  }

  private update1(key1, value) {
    this.store.update(state => produce(state, draft => {
      draft[UserStateConstantsKeys.userPrefrence][key1] = value;
    }))
  }

  private update2(key1, key2, value) {
    this.store.update(state => produce(state, draft => {
      draft[UserStateConstantsKeys.userPrefrence][key1][key2] = value;
    }))
  }

  private update3(key1, key2, key3, value) {
    this.store.update(state => produce(state, draft => {
      draft[UserStateConstantsKeys.userPrefrence][key1][key2][key3] = value;
    }))
  }

  private update4(key1, key2, key3, key4, value) {
    this.store.update(state => produce(state, draft => {
      draft[UserStateConstantsKeys.userPrefrence][key1][key2][key3] = value;
    }))
  }

  /**
   * To get all keys of state
   */
  getAll(): UserState {
    return this.getValue();
  }

  /**
   * get all as observable
   */
  get$(): Observable<UserState> {
    return this.select();
  }

  /**
   * 
   * @param key object key e.g. like Fabric,Home etc etc
   */
  getStateKey(key) {
    return this.getValue()[key];
  }

  /**
   * 
   */
  getPathValue$(path: Array<string>): Observable<any> {
    var data$;
    if (path.length) {
      switch (path.length) {
        case 1:
          data$ = this.getLevel1$(path[0]);
          break;
        case 2:
          data$ = this.getLevel2$(path[0], path[1]);
          break;
        case 3:
          data$ = this.getLevel3$(path[0], path[1], path[2]);
          break;
        case 4:
          break;
        default: break;
      }
      return data$;
    }
    else {
      console.warn("Unable to get UserStore with array path string length zero")
    }
  }

  getLevel1$(key1: string): Observable<any> {
    return this.select(state => (
      state[UserStateConstantsKeys.userPrefrence] != undefined &&
      state[UserStateConstantsKeys.userPrefrence][key1] != undefined) ? state[UserStateConstantsKeys.userPrefrence][key1] : null);
  }

  getLevel2$(key1: string, key2: string): Observable<any> {
    return this.select(state => (
      state[UserStateConstantsKeys.userPrefrence] != undefined &&
      state[UserStateConstantsKeys.userPrefrence][key1] != undefined &&
      state[UserStateConstantsKeys.userPrefrence][key1][key2] != undefined) ?
      state[UserStateConstantsKeys.userPrefrence][key1][key2] : null);
  }

  getLevel3$(key1: string, key2: string, key3: string): Observable<any> {
    return this.select(state => (
      state[UserStateConstantsKeys.userPrefrence] != undefined &&
      state[UserStateConstantsKeys.userPrefrence][key1] != undefined &&
      state[UserStateConstantsKeys.userPrefrence][key1][key2] != undefined
      && state[UserStateConstantsKeys.userPrefrence][key1][key2][key3] != undefined) ?
      state[UserStateConstantsKeys.userPrefrence][key1][key2][key3] : null);
  }

  deleteIndexedDB(tenantName) {
    try {
      return localForage.dropInstance({
        name: "Visur"
      }, function (err) {
        if (err == null) {
          console.warn("Success fully deleted IDB Instance");
        }
        else {
          console.warn(err);
        }
      });
    }
    catch (e) {

    }
  }

  clearStore() {
    return localForage.clear();
  }

  /**
   * Full update
   * @param key key of the state
   * @param value value of the state object
   */
  private updateTest(userID: string, value) {
    this.store.update(state => state[userID] = value)
  }

  private updateLevel1(userID: string, key1, value) {
    this.store.update(state => state[userID][key1] = value)
  }

  /**
   * 
   * @param userID "60b08287-59f8-49b7-ba52-6331bdb61ff0"
   * @param key "Data"
   * @param key2 "SetAsDefault"
   * @param value {Type: "string", Value: "ABC"}
   */
  private updateLevel2StateTest(userID: string, key, key2, value) {
    this.store.update((state: any) => ({
      ...state, id: key2, [userID]: {
        ...state[userID], [key]: {
          ...state[userID][key], [key2]: {
            ...state[userID][key][key2], Value: value
          }
        }
      }
    })
    );
  }

  private updateLevel2ImmerTest(userID: string, key, key2, value) {
    this.store.update(state => produce(state, draft => {
      draft.id = key2;
      draft[userID][key][key2]["Value"] = value;
    })
    )
  }

  /**
 * To add new item into state key
 * 
 * @param key e.g UserID
 * @param propKey key to add into existing state object
 * @param propValue value for propKey which is adding into existing state Object
 */
  private addNewItemIntoAsKeyValue(key, propKey, propValue) {
    var stateObject = JSON.parse(JSON.stringify(this.getStateKey(key)));
    stateObject[propKey] = propValue;
    this.add(key, stateObject);
  }
  selectStateUsers(){
    return this.select().pipe(debounceTime(250));
  }
}

