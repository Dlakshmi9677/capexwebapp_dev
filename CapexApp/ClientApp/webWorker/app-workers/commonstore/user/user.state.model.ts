import { ID } from '@datorama/akita';

// export interface UserState {
//   id: ID;
//   userPrefrence: any;
//   modifiedTime:number;
//   viewedFabrics: Array<any>;
//   windowCloseTime:number;
//   entityFilter:boolean;
// }

export interface UserState {
  id: ID;
  userPrefrence: any;
  modifiedTime:number;
  viewedFabrics: Array<any>;
  windowCloseTime:number;
  entityFilter:boolean;

  username: string;
  firstname: string,
  lastname: string,
  userid: string;
  token: string;
  tenantname: string;
  tenantid: string;
  password: string;
  emailid: string;
  ischangepassword: boolean;
  createpassword: string;
  confirmpassword: string;
  domain: string;
  rpotpcode: string;
  otpcode: string;
  devicedetail: string;
  url: string;
  logintime:Date;
  modifiedtime: Date,
  validuser: boolean,
  syncfrequency : number,
  applicationid:string,
  companyname:string,
  companyid:string
}



export class UserStateConstantsKeys {
  public static userPrefrence = "userPrefrence";
  public static FieldDataCapture = "Field Data Capture";
  public static SetAsDefault = "SetAsDefault";
  public static SetAsDefaultHierarchy = "Hierarchy";
  public static modifiedTime="modifiedTime";
  public static viewedFabrics="viewedFabrics";
  public static entityFilter="entityFilter";
  public static windowCloseTime="windowCloseTime";
  public static SetAsDefaultValue="Value";
  public static ContextMenuData="ContextMenuData";
  public static OperationalForms="Operational Forms";
  public static BI="Business Intelligence";
  public static EntityManagement="Entity Management";
  public static Pigging="Pigging";
  public static LeftTreeSetAsDefaultValue = { "Type": "string", "Value": "Lists" };
  public static EntityManagementLeftTreeDefaultValue = { "Type": "string", "Value": "Production Management" }
  public static Security = "Security";


  public static UserPrefrenceColumnJSON=[
    "Pigging",
    "Entity Management",
    "Field Data Capture",
    "Operational Forms",
    "Business Intelligence",
    "Security",
    "Fabric",
    "UserPinnedTabs",
    "Theme",
    "UserId",
    "Capabilities",
    "ZoomPercentage",
  ]

  public static setAsDefaultFabrics=[
    "Pigging",
    "Entity Management",
    "Field Data Capture",
    "Operational Forms",
    "Business Intelligence",
    "Security"
  ]

  public static deleteKeysFromUserPrefrence=[
    "People & Companies",
    "Asset Management"
  ]

}
