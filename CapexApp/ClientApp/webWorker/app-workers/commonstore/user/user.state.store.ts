import { Store, StoreConfig } from '@datorama/akita';
import { UserState } from './user.state.model'
import { Injectable } from '@angular/core';

export function createInitialState(): UserState {
    return {
    id: '',
    userPrefrence: null,
    entityFilter:false,
    modifiedTime:null,
    viewedFabrics:[],
    windowCloseTime:0,
     token: null,
     tenantname: null,
     tenantid:  null,
     username:  null,
     firstname: null,
     lastname: null,
     password:  null,
     emailid:  null,
     userid:  null,
     ischangepassword: false,
     createpassword:  null,
     confirmpassword:  null,
     domain:  '',
     rpotpcode:  null,
     otpcode:  null,
     devicedetail:  null,
     url:  null,
     logintime: new Date(),
     modifiedtime: null,
     validuser: true,
     syncfrequency : 0,
     applicationid:null,
     companyname:null,
     companyid:null
    };
  }

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'users' })
export class UsersStore extends Store<UserState> {
    constructor() {
        super(createInitialState());     
    }
}

