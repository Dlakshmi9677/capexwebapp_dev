/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
export interface RequestOptions {
    ignoreCache?: boolean;
    headers?: { [key: string]: string };
    // 0 (or negative) to wait forever
    timeout?: number;
}

export const DEFAULT_REQUEST_OPTIONS = {
    ignoreCache: false,
    headers: {
        Accept: 'application/json, text/javascript, text/plain',
    },
    // default max duration for a request
    timeout: 600000,
};

export interface RequestResult {
    ok: boolean;
    status: number;
    statusText: string;
    data: string;
    json: <T>() => T;
    headers: string;
}

function queryParams(params: any = {}) {
    return Object.keys(params)
        .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
        .join('&');
}

function withQuery(url: string, params: any = {}) {
    const queryString = queryParams(params);
    return queryString ? url + (url.indexOf('?') === -1 ? '?' : '&') + queryString : url;
}

function parseXHRResult(xhr: XMLHttpRequest): RequestResult {
    return {
        ok: xhr.status >= 200 && xhr.status < 300,
        status: xhr.status,
        statusText: xhr.statusText,
        headers: xhr.getAllResponseHeaders(),
        data: xhr.responseText,
        json: <T>() => JSON.parse(xhr.responseText) as T,
    };
}

function errorResponse(xhr: XMLHttpRequest, message: string | null = null): RequestResult {
    return {
        ok: false,
        status: xhr.status,
        statusText: xhr.statusText,
        headers: xhr.getAllResponseHeaders(),
        data: message || xhr.statusText,
        json: <T>() => JSON.parse(message || xhr.statusText) as T,
    };
}

export const mapXMLHttpRequest= new Map<string,XMLHttpRequest>();

export function httpRequest_abort($event){
    mapXMLHttpRequest.forEach(abortMapEvent,$event)
    // mapXMLHttpRequest.clear()
  }
  
function abortMapEvent(value,key,map){
    //To access event i.e. this
    if(key.split('_')[1]!=this){
        value.abort();
        map.delete(key);
    }
}
function abortSignal(restMethodName:string,key:string,xhr:XMLHttpRequest){
    // if(mapXMLHttpRequest.size){
        //abort all previous request
        mapXMLHttpRequest.set(restMethodName,xhr);
        // let key = restMethodName.split('_')[1];
        httpRequest_abort(key);
    // }
    // else if(restMethodName) {
    //         // let key = restMethodName.split('_');
    //         // if(key && key[1]=="DataEntry"){
    //             mapXMLHttpRequest.set(restMethodName,xhr);
    //         // }
    // }
}
export function restRequest(method: 'get' | 'post',
    url: string,
    queryParams: any = {},
    body: any = null,
    options: RequestOptions = DEFAULT_REQUEST_OPTIONS,restMethodName:string) {

    const ignoreCache = options.ignoreCache || DEFAULT_REQUEST_OPTIONS.ignoreCache;
    const headers = options.headers || DEFAULT_REQUEST_OPTIONS.headers;
    const timeout = options.timeout || DEFAULT_REQUEST_OPTIONS.timeout;

    return new Promise<RequestResult>((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open(method, withQuery(url, queryParams));

        if (headers) {
            Object.keys(headers).forEach(key => xhr.setRequestHeader(key, headers[key]));
        }

        if (ignoreCache) {
            xhr.setRequestHeader('Cache-Control', 'no-cache');
        }

        xhr.timeout = timeout;
        
        // if(restMethodName){
        //     let key = restMethodName.split('_');
        //     if(key&&key[1]=="DataEntry"){
        //         mapXMLHttpRequest.set(restMethodName,xhr);
        //     }
        // }
         let key = restMethodName.split('_');
         //if((key && key!="Default") ||( body && body.AbortSignal))
          if(key && key.length>2){
            abortSignal(restMethodName,key[2],xhr);
          }
          else{
            if(key[1]!="Default")
            abortSignal(restMethodName,key[1],xhr);
          }

        xhr.onload = evt => {
            resolve(parseXHRResult(xhr));
        };

        xhr.onerror = evt => {
            resolve(errorResponse(xhr, 'Failed to make request.'));
        };

        xhr.ontimeout = evt => {
            resolve(errorResponse(xhr, 'Request took longer than expected.'));
        };

        xhr.onabort = evt =>{
            resolve(errorResponse(xhr, 'Request Cancelled'));
        };

        if (method === 'post' && body) {
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.send(JSON.stringify(body));
        } else {
            xhr.send();
        }        
    });
}
