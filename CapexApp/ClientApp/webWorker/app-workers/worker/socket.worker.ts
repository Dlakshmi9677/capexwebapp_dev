
import { ReconnectingWebSocket } from '../socket/ReconnectingWebSocket';
import { WorkerMessage } from '../shared/worker-message.model';
import { WORKER_TOPIC } from '../shared/worker-topic.constants';
import { RequestResult } from '../http/httpRequest';
import { MessageModel } from '../../../app/components/globals/Model/Message'

export class SocketWorker {
    workerCtx: any;
    created: Date;
    url;
    ws;
    methodType;
    reconnectSocket: boolean = false;
    constructor(workerCtx: any) {
        this.workerCtx = workerCtx;
        this.created = new Date();
    }
    WebSocketWorkerBroker($event: MessageEvent) {
        const { topic, restMethodName, dataPayload } = $event.data as WorkerMessage;
        const workerMessage = new WorkerMessage(topic, restMethodName, dataPayload);
        let methodType = workerMessage.dataPayload.method;

        switch (topic) {
            case WORKER_TOPIC.WS:
                switch (methodType) {
                    case "connect":
                        this.connect(workerMessage);
                        break;
                    case "open":
                        this.create(workerMessage);//sending all messaging when socket open...ClientId and read Person.
                        break;
                    case "next":
                        if (workerMessage.dataPayload.body != null)
                            this.create(workerMessage);

                        break;
                    case "close":  //wrote for Desktop Case {Even After Logout Also othersession messages coming //means Socket Still Alive}
                    this.close(this.ws);
                        break;
                    default:
                        break;
                    //other case
                }
                break;
            default:

                break;
        }
    }

    create(message: WorkerMessage) {
        var self = this;
        var requestBeforeOpened = new Array();

        this.methodType = message.dataPayload.method;
        if (this.methodType != "next" || !this.url) {
            this.url = message.dataPayload.url;
            this.ws = new ReconnectingWebSocket(this.url, false);
        }


        let Restbody: any = message.dataPayload.body ? message.dataPayload.body : null;

        this.ws.onopen = function (event) {
            if (self.methodType != "next") {
                for (var key in Restbody) {
                    if (Restbody.hasOwnProperty(key)) {
                        self.ws.send(JSON.stringify(Restbody[key]));
                    }
                }
            }
        }

        this.ws.onmessage = function (event) {
            // console.log("Received Message from server: " + event.data);
            self.sendMessageToClient(event, message);
        }

        this.ws.onerror = function (event) {
            console.log(event);
        }

        this.ws.onclose = function (event) {
            console.log(event);
            self.reconnectSocket = true;
        }

        switch (this.ws.readyState) {
            case WebSocket.OPEN:
                this.ws.send(JSON.stringify(Restbody));
                break;
            case WebSocket.CONNECTING:
                if (this.reconnectSocket) {
                    // this.socketConnectionObservable.next(ws.readyState);
                }
                requestBeforeOpened.push(JSON.stringify(Restbody));
                break;
            case WebSocket.CLOSED:
                // $('#socket-alert').modal('show');
                break;
            case WebSocket.CLOSING:
                //  $('#socket-alert').modal('show');
                break;
            default: break;
        }
    }
    close(ws: WebSocket) {
        try {
            ws.close()
        } catch (e) {
            console.error(e);
        }
    }
    connect(message: WorkerMessage) {
        var self = this;
        if (this.ws)
            this.close(this.ws)

        this.ws = null;
        this.url = message.dataPayload.url;
        this.ws = new ReconnectingWebSocket(this.url, true);
        this.ws.onopen = function (event) {
            if(message.dataPayload&&message.dataPayload.method=="connect"){
                let msg = new MessageModel()
                msg.DataType='SocketConnected';
                let data = {data:JSON.stringify(msg),returnValue:true}
                self.sendMessageToClient(data, message);
            }
        }
    }

    private sendMessageToClient(event, message: WorkerMessage) {
        this.returnWorkerResult(event, message);
    }

    private returnWorkResults(message: WorkerMessage): void {
        this.workerCtx.postMessage(message);
    }

    private returnWorkerResult(response, workerMessage) {
      this.returnWorkResults(new WorkerMessage(workerMessage.topic, workerMessage.restMethodName, this.mapResponse(response)));
    }

    private mapResponse(response): RequestResult {
        let requestResult: RequestResult = { ok: false, data: '', status: 200, headers: '', statusText: '', json: null };
        requestResult.ok = response.returnValue;
        requestResult.data = JSON.stringify(response.data);
        requestResult.status = 200;
        requestResult.headers = "content-type: application/json; charset=utf-8";
        requestResult.statusText = "OK";
        return requestResult;
    }
}
