/* <project-root>/src/worker/main.worker.ts */

import { AppWorkers } from './app-workers/worker/app.workers';
import { SocketWorker } from './app-workers/worker/socket.worker';
import { WorkerMessage } from './app-workers/shared/worker-message.model';
import { WORKER_TOPIC } from './app-workers/shared/worker-topic.constants';
import { GisMapWorker } from './app-workers/worker/gis.worker';
import { httpRequest_abort } from './app-workers/http/httpRequest';

export const httpWorker = new AppWorkers(self);
export const wsWorker = new SocketWorker(self);
export const gisMapWorker = new GisMapWorker(self);

addEventListener('message', ($event: MessageEvent) => {
  const { topic } = $event.data as WorkerMessage;
  routeOnMessage(topic, $event);
});

function routeOnMessage(topic, $event) {
  switch (topic) {
    case WORKER_TOPIC.RESTHTTP:
      httpWorker.workerBroker($event);
      break;
    case WORKER_TOPIC.WS:
      wsWorker.WebSocketWorkerBroker($event);
      break;
    case WORKER_TOPIC.GisMap:
      gisMapWorker.workerBroker($event);
      break;
    case WORKER_TOPIC.AbortEvent:
      abortPreviousRequests($event)
      break;
    default:
      break;
  }
}

function abortPreviousRequests($event){
  const { topic, restMethodName, dataPayload ,AppConfig} = $event.data as WorkerMessage;
  const workerMessage = new WorkerMessage(topic, restMethodName, dataPayload,AppConfig);
  let body = dataPayload.body;
  httpRequest_abort(body);
  httpWorker.returnAbortEventMessage(body, workerMessage);
}