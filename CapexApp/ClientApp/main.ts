import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

// import * as localForage from 'localforage';
// import { persistState } from '@datorama/akita';
// import { StorageConfiguration } from './StorageConfig';

if (environment.production) {
  enableProdMode();
  window.console.log = function () { };
  window.console.warn = function () { };
}

// localForage.config({
//   driver: localForage.INDEXEDDB,
//   name: 'Visur',
//   storeName: "tourmalineqa"
// });

// persistState({include: ["entities",'users'], storage: localForage });
//  persistState();
// localForage.config({
//   driver: localForage.INDEXEDDB,
//   name: StorageConfiguration.StorageName,
//   version: StorageConfiguration.version//,
//  // storeName: StorageConfiguration.objectStorageName
// });

//  persistState({ include: StorageConfiguration.includes, storage: localForage });

platformBrowserDynamic().bootstrapModule(AppModule).then(() => {
  if ('serviceWorker' in navigator && environment.production) {
   // navigator.serviceWorker.register('ngsw-worker.js');
  }
}).catch(err => console.error("bootstramodule Errors ", err));
