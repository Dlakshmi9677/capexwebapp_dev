export class StorageConfiguration {
  public static StorageName= 'Visur';
  public static version=1.0;
  public static objectStorageName="Capabilities"
  public static excludes=[];
  public static includes=["fdc","fdc2"];
  public static objectStorageKey="Visur"
}