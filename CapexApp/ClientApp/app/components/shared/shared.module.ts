import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { QuillModule } from 'ngx-quill';
// import { CustomPipeToPassFunction } from 'visur-angular-common';
// import { FdcHeaderDataValue } from '../fabrics/FDC/components/CommonDataEntryHeaderComponent/commondataentrygrid.component';
import { ScrollEventModule } from 'ngx-scroll-event';
// import { NgDragDropModule } from 'ng-drag-drop';
import { Draggable } from '../globals/directives/draggable.directive';
import { AngularHierarchyTreeComponent } from '../globals/components/angular-hierarchy-tree/angular-hierarchy-tree.component'
import { CdkTableModule } from '@angular/cdk/table';
import { ScrollbarModule } from '../template/LayoutComponents/leftsidebar/scrollbar/index';
import { RouterModule } from '@angular/router';
// import { AngularDraggableModule } from 'angular2-draggable';
import { NgSelectModule } from '@ng-select/ng-select';
import { CommonContextMenuComponent } from '../common/common-context-menu/commoon-context-menu.component'
// import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { LoadingBarModule } from '../common/ngx-loading-bar/index';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';


const NG_MATERIAL = [MatAutocompleteModule, CdkTableModule, MatButtonModule,
  MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatChipsModule, MatDatepickerModule, MatDialogModule, MatDividerModule, MatExpansionModule,
  MatGridListModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatNativeDateModule, MatPaginatorModule, MatProgressBarModule,
  MatProgressSpinnerModule, MatRadioModule, MatRippleModule, MatSelectModule, MatSidenavModule, MatSliderModule, MatSlideToggleModule,
  MatSnackBarModule, MatSortModule, MatStepperModule, MatTableModule, MatTabsModule, MatToolbarModule, MatTooltipModule];



import { FlexLayoutModule } from '@angular/flex-layout';


import { CapabilityTreeComponent } from '../template/LayoutComponents/leftsidebar/CapabilityTree/capability.tree.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { TreeModule } from '@circlon/angular-tree-component';
import { DraggableDialogDirective } from '../globals/directives/custom-draggable.directive';
import { AutowidthDirective } from '../globals/directives/auto-width.directive';
import {IsOutOfBoundDirective} from '../globals/directives/tooltip.directive';
import {IsEllipsisActiveDirective} from '../globals/directives/is-ellipsis-active.directive';
// import { DynamicClassDirective } from '../common/form-types/dynamic-class-directive';
import {ClickOutsideDirective} from '../globals/directives/click-outside.directive';
import { KeypadInputboxBinding } from '../globals/directives/keypad-inputbox.directive';
import {VisurPipeModule} from 'visur-angular-common';
import { KeplerReactWrapper } from '../globals/components/react-keplergl/KeplerReactWrapper';
// const SHARED_COMPONENTS = [
// ]



@NgModule({
  imports: [CommonModule,VisurPipeModule,
    RouterModule,
    NgSelectModule,
    DragDropModule,
    ScrollbarModule,
    NG_MATERIAL,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    // NgDragDropModule.forRoot(),
    ScrollEventModule,
    LoadingBarModule,
    // AngularDraggableModule,
    TreeModule,
    // QuillModule.forRoot(),
    FlexLayoutModule//,
    //  OwlDateTimeModule,
    // OwlNativeDateTimeModule
  ],
  declarations: [
    // SHARED_COMPONENTS,
    DraggableDialogDirective,
    ClickOutsideDirective,
    KeypadInputboxBinding,
    AutowidthDirective,
    IsOutOfBoundDirective,
    IsEllipsisActiveDirective,
    Draggable,
    // FdcHeaderDataValue,
    CapabilityTreeComponent,
    AngularHierarchyTreeComponent,
    CommonContextMenuComponent,
    KeplerReactWrapper,

    // CustomPipeToPassFunction//,
    // DynamicClassDirective
  ],
  exports: [
    DragDropModule,
    FlexLayoutModule,
    NG_MATERIAL,
    LoadingBarModule,
    CommonModule,
    // SHARED_COMPONENTS,
    Draggable,
    NgSelectModule,
    DraggableDialogDirective,
    ClickOutsideDirective,
    AutowidthDirective,
    IsOutOfBoundDirective,
    IsEllipsisActiveDirective,
    ScrollEventModule,
    VisurPipeModule,
    // NgDragDropModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    ScrollbarModule,
    // FdcHeaderDataValue,
    CapabilityTreeComponent,
    AngularHierarchyTreeComponent,
    CommonContextMenuComponent,
    KeplerReactWrapper,
    // AngularDraggableModule,
    // QuillModule,
    // CustomPipeToPassFunction,
    TreeModule//,
    // DynamicClassDirective
  ],
  entryComponents: [ ],

})

export class SharedModule { }
