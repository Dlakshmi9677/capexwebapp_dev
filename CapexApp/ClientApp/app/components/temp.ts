export const testData = {
    "project": {
      "parent": "e9b64456-adf2-478a-b182-75cabee4d142",
      "projectname": "TestProject05",
      "datestarted": "2020-08-05",
      "projectuom": "Metric"
    },
    "dailyjournals": [
      {
        "entityid": "00fb6ea3-5420-4ce8-8ef8-b8c8c539c834",
        "journalcount": "9",
        "uuid": "00fb6ea3-5420-4ce8-8ef8-b8c8c539c834",
        "journaldate": "2020-10-07",
        "treeStatus": "collapsed",
        "journalnumber": "2020-10-07-Auto-test-2-10/08/2020-0009",
        "status": "NULL"
      },
      {
        "entityid": "03f1a962-e74e-4fc4-b418-bc2097b3cb2b",
        "journalcount": "7",
        "uuid": "03f1a962-e74e-4fc4-b418-bc2097b3cb2b",
        "journaldate": "2020-10-04",
        "treeStatus": "collapsed",
        "journalnumber": "Mon Oct 05 2020 00:00:00 GMT+0530 (India Standard Time)-Vishnuvardhan-Desktop -0007",
        "status": "NULL"
      },
      {
        "entityid": "0db45d08-f2cf-488a-a3fa-092358bef2f0",
        "journalcount": "11",
        "uuid": "0db45d08-f2cf-488a-a3fa-092358bef2f0",
        "journaldate": "2020-11-04",
        "treeStatus": "collapsed",
        "journalnumber": "2020-11-04-faroc-das-0011",
        "status": "NULL"
      },
      {
        "entityid": "133e3abe-16d1-4b08-8bf4-e8dd32b7742c",
        "journalcount": "4",
        "uuid": "133e3abe-16d1-4b08-8bf4-e8dd32b7742c",
        "journaldate": "2020-10-01",
        "treeStatus": "collapsed",
        "journalnumber": "2020-10-01-trigger-0-0004",
        "status": "NULL"
      },
      {
        "entityid": "13eabd48-a2aa-4c7d-b796-23dbfc8d3be0",
        "journalcount": "3",
        "uuid": "13eabd48-a2aa-4c7d-b796-23dbfc8d3be0",
        "journaldate": "2020-09-09",
        "treeStatus": "collapsed",
        "journalnumber": "2020-09-09-0-0-0003",
        "status": "In Progress"
      },
      {
        "entityid": "1ff23320-2f68-4011-9f03-29bf06209822",
        "journalcount": "1",
        "uuid": "1ff23320-2f68-4011-9f03-29bf06209822",
        "journaldate": "2020-09-09",
        "treeStatus": "collapsed",
        "journalnumber": "2020-09-09-0-0-0001",
        "status": "NULL"
      },
      {
        "entityid": "24fd17ed-2f2b-4b8c-99ff-13af8a829a02",
        "journalcount": "10",
        "uuid": "24fd17ed-2f2b-4b8c-99ff-13af8a829a02",
        "journaldate": "2020-11-04",
        "treeStatus": "collapsed",
        "journalnumber": "2020-11-04-sailesh-kumar swain -0010",
        "status": "NULL"
      },
      {
        "entityid": "28ec896b-cc68-4b34-92f7-ba4f5ce7f36f",
        "journalcount": "8",
        "uuid": "28ec896b-cc68-4b34-92f7-ba4f5ce7f36f",
        "journaldate": "2020-10-06",
        "treeStatus": "collapsed",
        "journalnumber": "2020-10-06-Auto-test-1-10/08/2020-0008",
        "status": "NULL"
      },
      {
        "entityid": "523c29d1-073c-47a8-ad22-b0f26885a561",
        "journalcount": "5",
        "uuid": "523c29d1-073c-47a8-ad22-b0f26885a561",
        "journaldate": "2020-10-02",
        "treeStatus": "collapsed",
        "journalnumber": "2020-10-02-Test02-0-0005",
        "status": "NULL"
      },
      {
        "entityid": "5476eb22-77b9-4f97-a701-19fe4d15bcbf",
        "journalcount": "2",
        "uuid": "5476eb22-77b9-4f97-a701-19fe4d15bcbf",
        "journaldate": "2020-09-09",
        "treeStatus": "collapsed",
        "journalnumber": "2020-09-09-0-0-0002",
        "status": "NULL"
      },
      {
        "entityid": "dd1ff021-37c7-4627-81d0-8ff471e1f420",
        "journalcount": "12",
        "uuid": "dd1ff021-37c7-4627-81d0-8ff471e1f420",
        "journaldate": "2020-11-04",
        "treeStatus": "collapsed",
        "journalnumber": "2020-11-04-michel-Johnshon-0012",
        "status": "In Progress"
      },
      {
        "entityid": "eaf94ac9-31c9-4fd0-8916-276b0befc0dd",
        "journalcount": "4",
        "uuid": "eaf94ac9-31c9-4fd0-8916-276b0befc0dd",
        "journaldate": "2020-09-29",
        "treeStatus": "collapsed",
        "journalnumber": "2020-09-29-rajib-0-0004",
        "status": "NULL"
      }
    ],
    "premico":{
      "schema": {
        "type": "object",
        "properties": {
          "premicopanel": {
            "virtualKey": true,
            "roles": [
              "Administrator",
              "Support",
              "Welding Inspector",
              "Inspector",
              "Coating Inspector",
              "NDT Inspector",
              "Senior Inspector",
              "Chief Inspector",
              "Office Manager",
              "Assistant Chief Inspector",
              "Default"
            ],
            "type": "object",
            "buttonEvent": "route",
            "properties": {
              "premico": {
                "treeFromRelation": "nestedunderwbs",
                "addable": false,
                "roles": [
                  "Administrator",
                  "Support",
                  "Welding Inspector",
                  "Inspector",
                  "Coating Inspector",
                  "NDT Inspector",
                  "Senior Inspector",
                  "Chief Inspector",
                  "Office Manager",
                  "Assistant Chief Inspector",
                  "Default"
                ],
                "overrideRoles": {},
                "deletable": false,
                "treeToRelation": "wbs",
                "type": "array",
                "buttonEvent": "route",
                "items": {
                  "pageParameters": "premico.entityid",
                  "type": "object",
                  "buttonEvent": "route",
                  "properties": {
                    "parent": {
                      "actualName": "Parent",
                      "type": "string"
                    },
                    "entitytimezone": {
                      "actualName": "EntityTimeZone",
                      "hide": true,
                      "format": "date-time",
                      "placeholder": "EntityTimeZone",
                      "title": "EntityTimeZone",
                      "type": "string",
                      "required": false
                    },
                    "level": {
                      "type": "number"
                    },
                    "nestedunderwbs": {
                      "actualName": "NestedUnderWBS",
                      "hide": true,
                      "readonly": true,
                      "isTreeColumn": false,
                      "title": "Nested Under WBS",
                      "type": "string",
                      "required": false
                    },
                    "entityname": {
                      "actualName": "EntityName",
                      "hide": false,
                      "readonly": true,
                      "isTreeColumn": true,
                      "title": "Entity Name",
                      "type": "string",
                      "required": false
                    },
                    "parententityid": {
                      "actualName": "ParentEntityId",
                      "hide": true,
                      "readonly": true,
                      "isTreeColumn": false,
                      "title": "Parent Entity ID",
                      "type": "string",
                      "required": false
                    },
                    "wbs": {
                      "actualName": "WBS",
                      "hide": true,
                      "readonly": true,
                      "isTreeColumn": false,
                      "title": "WBS",
                      "type": "string",
                      "required": false
                    },
                    "entityid": {
                      "actualName": "EntityID",
                      "hide": true,
                      "placeholder": "EntityID",
                      "title": "EntityID",
                      "type": "string",
                      "required": false
                    },
                    "uuid": {
                      "actualName": "UUID",
                      "type": "string"
                    },
                    "treeStatus": {
                      "default": "collapsed",
                      "type": "string"
                    },
                    "loaded": {
                      "type": "string"
                    },
                    "createdby": {
                      "actualName": "CreatedBy",
                      "hide": true,
                      "defaultValue": "$state:fullName",
                      "placeholder": "CreatedBy",
                      "title": "CreatedBy",
                      "type": "string",
                      "required": false
                    },
                    "subtype": {
                      "actualName": "SubType",
                      "hide": false,
                      "readonly": true,
                      "isTreeColumn": false,
                      "title": "SubType",
                      "type": "string",
                      "required": false
                    },
                    "modifieddatetime": {
                      "actualName": "ModifiedDateTime",
                      "hide": true,
                      "placeholder": "ModifiedDateTime",
                      "title": "ModifiedDateTime",
                      "type": "string",
                      "required": false
                    },
                    "companyname": {
                      "actualName": "CompanyName",
                      "hide": false,
                      "readonly": true,
                      "isTreeColumn": false,
                      "title": "CompanyName",
                      "type": "string",
                      "required": false
                    },
                    "modifiedby": {
                      "actualName": "ModifiedBy",
                      "hide": true,
                      "defaultValue": "$state:fullName",
                      "placeholder": "ModifiedBy",
                      "title": "ModifiedBy",
                      "type": "string",
                      "required": false
                    },
                    "id": {
                      "actualName": "Id",
                      "hide": true,
                      "readonly": true,
                      "isTreeColumn": false,
                      "title": "Id",
                      "type": "string",
                      "required": false
                    },
                    "createddatetime": {
                      "actualName": "CreatedDateTime",
                      "hide": true,
                      "placeholder": "CreatedDateTime",
                      "title": "CreatedDateTime",
                      "type": "string",
                      "required": false
                    }
                  },
                  "routeOnSelect": true,
                  "primary": true
                },
                "routeOnSelect": true,
                "primary": true
              }
            }
          }
        }
      },
      "app": "Construction",
      "roles": [
        {
          "role": "Administrator",
          "permissions": {
            "read": true,
            "create": true,
            "update": true,
            "delete": true
          }
        },
        {
          "role": "Support",
          "permissions": {
            "read": true,
            "create": true,
            "update": true,
            "delete": true
          }
        },
        {
          "role": "Welding Inspector",
          "permissions": {
            "read": true,
            "create": true,
            "update": true,
            "delete": true
          }
        },
        {
          "role": "Inspector",
          "permissions": {
            "read": true,
            "create": true,
            "update": true,
            "delete": true
          }
        },
        {
          "role": "Coating Inspector",
          "permissions": {
            "read": true,
            "create": true,
            "update": true,
            "delete": true
          }
        },
        {
          "role": "NDT Inspector",
          "permissions": {
            "read": true,
            "create": true,
            "update": true,
            "delete": true
          }
        },
        {
          "role": "Senior Inspector",
          "permissions": {
            "read": true,
            "create": true,
            "update": true,
            "delete": true
          }
        },
        {
          "role": "Chief Inspector",
          "permissions": {
            "read": true,
            "create": true,
            "update": true,
            "delete": true
          }
        },
        {
          "role": "Office Manager",
          "permissions": {
            "read": true,
            "create": true,
            "update": true,
            "delete": true
          }
        },
        {
          "role": "Assistant Chief Inspector",
          "permissions": {
            "read": true,
            "create": true,
            "update": true,
            "delete": true
          }
        },
        {
          "role": "Default",
          "permissions": {
            "read": true,
            "create": true,
            "update": true,
            "delete": true
          }
        }
      ],
      "unitsOfMeasurement": "",
      "queries": [
        {
          "sheetName": "LemLabor",
          "query": "select Payload from Welds.Weld where json_extract(Payload,'$.WeldNumber')='param1' order by  json_extract(Payload,'$.Date') desc",
          "property": "CopyPrevious"
        },
        {
          "sheetName": "Header",
          "query": "select json_extract(Payload,'$.WDSNumber') as entityname, json_extract(Payload,'$.UUID') as entityid from wds_number where json_extract(Payload,'$.WeldType')='WPS' or\n json_extract(Payload,'$.WeldType')='param1'",
          "property": "WDSNumber",
          "queryParameter": "param1=Header.WeldType"
        },
        {
          "sheetName": "Header",
          "query": "WeldPass_Specification",
          "property": "TempMin",
          "samplevalue": 50,
          "queryParameter": "WeldPass=Root"
        },
        {
          "sheetName": "Header",
          "query": "WeldPass_Specification",
          "property": "TempMax",
          "samplevalue": 100,
          "queryParameter": "WeldPass=Header.TestPass"
        }
      ],
      "tableName": "",
      "layout": [
        {
          "fxLayoutAlign": "flex-start",
          "fxLayoutWrap": "wrap",
          "justify-content": "space-between",
          "type": "flex",
          "items": [
            {
              "fxFlex": "0 0 calc(100%)",
              "expanded": true,
              "type": "div",
              "title": "Premico",
              "collapsible": true,
              "items": [
                {
                  "fxLayoutWrap": "wrap",
                  "fxLayoutGap": "10px",
                  "type": "flex",
                  "items": [
                    {
                      "listItems": 0,
                      "type": "table",
                      "items": [
                        {
                          "width": "100",
                          "title": "Entity Name",
                          "type": "string",
                          "key": "premicopanel.premico[].entityname"
                        },
                        {
                          "width": "50",
                          "title": "SubType",
                          "type": "string",
                          "key": "premicopanel.premico[].subtype"
                        },
                        {
                          "width": "50",
                          "title": "CompanyName",
                          "type": "string",
                          "key": "premicopanel.premico[].companyname"
                        }
                      ],
                      "key": "premicopanel.premico",
                      "height": "calc(100vh - 126px)",
                      "dropDataPointers": "entities",
                      "contextMenu": "expand all",
                      "allowDrag": "true",
                      "allowDrop": "false"
                    }
                  ],
                  "fxLayout": "row"
                }
              ],
              "key": "premicopanel"
            }
          ],
          "fxLayout": "row"
        }
      ],
      "categoryType": "",
      "enums": {
        "reportType": [
          "Any",
          "Access Mowing & Fencing",
          "Assistant Chief Inspector ReportBackfill Inspection ReportBag/Bolt-On Weight Inspection Report",
          "Bending Inspection Report",
          "Bores Inspection Report",
          "Caliper Tool Indication Inspection Report",
          "Chief Inspector Report",
          "Clean-up Inspection Report",
          "Coating Inspection Report",
          "Construction Report Daily",
          "Crossing As-Built Inspection Report",
          "Ditch Inspection Report",
          "Drive Pile Installation Report",
          "ELD Inspection Report",
          "Excavation Inspection Report",
          "Foreign Facility Crossing As-Built Inspection Report",
          "General Inspection Report",
          "Ground Disturbance Inspection Report",
          "Haul & String Inspection Report",
          "Horizontal Directional Drilling Inspection Report",
          "Hydostatic Pressure Test Yield Plot Log",
          "Hydrotest Inspection Report",
          "Hydrovac Inspection Report",
          "Job Safety Analysis Form",
          "Lower-In Inspection Report",
          "Pigging Inspection Report",
          "Pipeline Inspection/Repair Report",
          "Scheduled Day Off Report",
          "Screw Anchor Inspection Report",
          "Screw Pile Inspection Report",
          "Stockpile Inspection Report",
          "Test Lead Inspection Report",
          "Tie-In Inspection Report",
          "W-1 Daily Weld Report GMWQPR",
          "W-1 Daily Weld Report MFCWQPR",
          "W-1 Daily Weld Report SMWQPR",
          "W-14 Weld Crack Inv Report",
          "W-15 Arc Burn Repair",
          "W-16 Pipe Damage Assessment Report",
          "W-17 Pipe End Magnetism Report",
          "W-1M Senior Welder's Summary Report",
          "W-2 Daily Mainline Weld Report",
          "W-3 Pipe Quality Report GMPQR",
          "W-3 Pipe Quality Report SMPOR",
          "W-4 Daily Repair Welding",
          "W-5 Tier-in Section Welding Report",
          "W-6A Tie-In Welding As-Built Report",
          "W-6B Section Welding As-Built Report",
          "W-7 Transition Quality Report"
        ],
        "reportCategory": [
          "Any",
          "Craft",
          "General",
          "Welding",
          "Management"
        ],
        "reviewStatus": [
          "Unknown",
          "Pending",
          "In Progress",
          "Deficiency ",
          "Acceptable"
        ],
        "weldStatus": [
          "WQT",
          "Complete",
          "Tested",
          "Inspected",
          "Approved"
        ],
        "Yes-No-NA": [
          "Yes",
          "No",
          "NA"
        ]
      },
      "parentPage": "",
      "TableName": "SecurityGroupSchedulePremico",
      "pageType": "",
      "$state": "",
      "Version": "5",
      "options": {
        "submit": true
      },
      "page": "SecurityGroupSchedulePremico",
      "tenant": "All",
      "breadcrumbDisplayName": "",
      "droppedRowsMapping": [
        {
          "dropDataPointer": "entities",
          "field": "entityname",
          "value": "entityname"
        },
        {
          "dropDataPointer": "entities",
          "field": "entityid",
          "value": "uuid"
        },
        {
          "dropDataPointer": "entities",
          "field": "parententityid",
          "value": "parententityid"
        },
        {
          "dropDataPointer": "entities",
          "field": "uuid",
          "value": "uuid"
        }
      ]
    }
  }