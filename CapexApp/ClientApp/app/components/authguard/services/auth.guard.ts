import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor() {
  }
  canActivate(  next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
   return true;
  }
  callback(x) {
    setTimeout(() => {
      return true;
    }, 1000);
  }
}
