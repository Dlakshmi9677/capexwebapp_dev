import { AppConfig } from '../../globals/services/app.config';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, Route } from '@angular/router';
import { Injectable } from '@angular/core';
import { CommonService } from '../../globals/CommonService';

@Injectable()
export class CapabilityAuthGaurd implements CanActivate, CanLoad {

  constructor(private router: Router, private commonService: CommonService,public appConfig:AppConfig) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    try {
      for (var i = 0; i < this.appConfig.AllHeaderRoutingListService.length; i++) {
        if (state.url == this.appConfig.AllHeaderRoutingListService[i].routerLink) {
          //check the status
          if (this.appConfig.AllHeaderRoutingListService[i].status.toUpperCase() == "MARKETING") {
            this.router.navigate(['marketingpage', route.url[0].path]);
            return false;
          }
          return true;
        }
      }
      this.router.navigate(['']);
      return false;
    }
    catch (ex) {
      this.commonService.printException("canActivate", "capability-authguard.service.ts", ex);
    }
  }

  canLoad(route: Route): boolean {
    try {
      if (this.appConfig.AllHeaderRoutingListService.length <= 0) {
        setTimeout(function () {
          for (var i = 0; i < this.appConfig.AllHeaderRoutingListService.length; i++) {
            if (route.path == this.appConfig.AllHeaderRoutingListService[i].baseRoute) {
              //check the status
              if (this.appConfig.AllHeaderRoutingListService[i].status.toUpperCase() == "MARKETING") {
                this.router.navigate(['marketingpage', route.path]);
                return false;
              }
              return true;
            }
          }
          this.router.navigate(['']);
          return false;
        }, 1000);
      }
      else {
        for (var i = 0; i < this.appConfig.AllHeaderRoutingListService.length; i++) {
          if (route.path == this.appConfig.AllHeaderRoutingListService[i].baseRoute) {
            return true;
          }
        }
        this.router.navigateByUrl('Home');
        return false;
      }
    }
    catch (ex) {
      this.commonService.printException("canLoad", "capability-authguard.service.ts", ex);
    }
  }
}
