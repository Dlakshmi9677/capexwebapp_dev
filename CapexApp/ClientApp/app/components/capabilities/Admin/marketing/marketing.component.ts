import { Component, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CommonService } from '../../../globals/CommonService';


@Component({
    selector: 'marketing',
    templateUrl: 'marketing.component.html',
    styleUrls: ['./marketing.component.scss']
})

export class MarketingComponent implements  OnDestroy{

  isSingleProperty = true;
  takeUntilDestroyObservables=new Subject();
  constructor(public commonService: CommonService) {
    // this.observableSubscripton()       
  }

  // observableSubscripton() {
  //   this.commonService.checkTreeStatus.pipe(takeUntil(this.takeUntilDestroyObservables)).subscribe(res => {
  //     this.isSingleProperty = !this.isSingleProperty;
  //   }, error => { this.commonService.printException("observableSubscripton","marketing.component.ts",error) });
  // }

  ngOnDestroy() {
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }

}
