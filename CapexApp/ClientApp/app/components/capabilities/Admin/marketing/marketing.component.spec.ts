import { async, inject, TestBed, ComponentFixture} from '@angular/core/testing';
import { MockBackend} from '@angular/http/testing';
import { XHRBackend} from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ComponentFactoryResolver} from '@angular/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { MessagingService } from '../../../globals/services/MessagingService';
import {ActivityCenterService  } from '../../../fabrics/activity-center/services/activity-center.service';
import { CommonService } from '../../../globals/CommonService';
import { FormsModule } from '@angular/forms';
import { MarketingComponent } from './../../../capabilities/Admin/marketing/marketing.component';
import { TitleCasePipe } from '../../../globals/pipes/titlecasepipe';

describe("MarketingComponent", () => {
    let component: MarketingComponent;
    let fixture: ComponentFixture<MarketingComponent>;
    let commonservice;
    let messagingservice;
    let data: any;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule, RouterTestingModule, FormsModule],
            declarations: [MarketingComponent, TitleCasePipe],
            providers: [
                CommonService, MessagingService, ActivityCenterService,
                { provide: XHRBackend, useClass: MockBackend },
                { provide: 'BASE_URL', useValue: "http://localhost" },
                ComponentFactoryResolver
            ],
            schemas: [
                NO_ERRORS_SCHEMA
            ]
        })
            .compileComponents();
    }));
    beforeEach(inject([CommonService], comm => {
        commonservice = comm;
        fixture = TestBed.createComponent(MarketingComponent);
        component = fixture.componentInstance;

    }));
    beforeEach(inject([MessagingService], service => {
        messagingservice = service;
        messagingservice.initializeWebSocketConnection();
    }));
    it('should create MarketingComponent', () => {
        expect(component).toBeTruthy();
    });
    it('check tree status Subcription observable', () => {
        commonservice.checkTreeStatus.next('true');
        commonservice.checkTreeStatus.subscribe(res => {
            expect(res).toBe('true');
        });
    });
});
