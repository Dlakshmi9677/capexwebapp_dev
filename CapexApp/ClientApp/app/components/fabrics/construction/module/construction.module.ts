import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule1 } from '../../../shared/shared1.module';
import { ConstructionService } from '../services/construction.service';
import { ConstructionComponent } from '../components/construction/construction.component';
import { routing } from './construction.routing';
import { ResizableModule } from 'angular-resizable-element';



@NgModule({
  declarations: [
    ConstructionComponent
  ],
  imports: [
    CommonModule,
    SharedModule1,
    ResizableModule,
    routing
  ],
  providers: [
    ConstructionService
  ]
})
export class ConstructionModule {

}
