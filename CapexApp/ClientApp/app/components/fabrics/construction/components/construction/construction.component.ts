import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { FABRICS } from '../../../../globals/Model/CommonModel';
import { Subject } from 'rxjs';
import { MatIconRegistry } from '@angular/material/icon';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { MatDialog } from '@angular/material/dialog';
import { CommonService } from '../../../../globals/CommonService';
import { FDCIcons, PiggingIcons, IconJson } from '../../../../globals/icons';
import { takeUntil } from 'rxjs/operators';
import { ConstructionService } from '../../services/construction.service';

@Component({
  selector: 'app-construction',
  templateUrl: './construction.component.html',
  styleUrls: ['./construction.component.scss']
})
export class ConstructionComponent implements OnInit, AfterViewInit, OnDestroy {
  capability = FABRICS.CONSTRUCTION;

  takeUntilDestroyObservables$ = new Subject();


  constructor(public constructionService: ConstructionService, public iconRegistry: MatIconRegistry, public router: Router, public route: ActivatedRoute, public sanitizer: DomSanitizer, public Commonservice: CommonService, public dialog: MatDialog) {

    this.Commonservice.lastOpenedFabric = this.Commonservice.getFabricNameByUrl(this.router.url);
  }

  ngOnInit(): void {
    this.Commonservice.changeFavIcon(FABRICS.CONSTRUCTION);
  }

  ngAfterViewInit(): void {
    this.registerIcon();
  }

  registerIcon() {
    var fdcIcons = Object.assign(FDCIcons, PiggingIcons, IconJson);
    for (let element in fdcIcons) {
      this.iconRegistry.addSvgIcon(element, this.sanitizer.bypassSecurityTrustResourceUrl(fdcIcons[element]));
    }
  }

  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables$);
  }



  ngOnDestroy(): void {
    this.takeUntilDestroyObservables$.next();
    this.takeUntilDestroyObservables$.complete();
  }

}
