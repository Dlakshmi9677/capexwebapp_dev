import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstructionFormTemplateComponent } from './construction-form-template.component';

describe('ConstructionFormTemplateComponent', () => {
  let component: ConstructionFormTemplateComponent;
  let fixture: ComponentFixture<ConstructionFormTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConstructionFormTemplateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstructionFormTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
