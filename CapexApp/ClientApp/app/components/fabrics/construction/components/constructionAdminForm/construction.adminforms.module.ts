import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule1 } from '../../../../shared/shared1.module';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { ConstructionAdminFormsComponent } from '../constructionAdminForm/construction.adminforms.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: ConstructionAdminFormsComponent,
    children: []
  }
];


@NgModule({
  declarations: [
    ConstructionAdminFormsComponent
  ],
  imports: [
    CommonModule,
    SharedModule1,
    NgxExtendedPdfViewerModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]

})
export class ConstructionAdminFormsModule {

}
