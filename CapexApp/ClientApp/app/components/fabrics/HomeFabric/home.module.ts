import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule1 } from '../../shared/shared1.module';
import { HomeFabricComponent } from './components/homefabric.component';
import { routing } from './home.routing';
const HOME_COMPONENTS = [HomeFabricComponent];
@NgModule({
    imports: [CommonModule, SharedModule1, routing],
    declarations: [HOME_COMPONENTS]
})
export class HomeModule { }