
// AoT compilation doesn't support 'require'.
//External library imports
import { Component } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AppConsts } from 'ClientApp/app/components/common/Constants/AppConsts';
import { FABRICS } from 'ClientApp/app/components/globals/Model/CommonModel';
//App constants or model classes
import { EntityTypes } from 'ClientApp/app/components/globals/Model/EntityTypes';
import { MessageType, Routing } from 'ClientApp/app/components/globals/Model/Message';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { build, version } from '../../../../../../../CapexApp/package.json';
import { EXTERNAL_LINK_CANNOT_OPEN } from '../../../common/Constants/AttentionPopupMessage';
import { PopUpAlertComponent } from '../../../common/PopUpAlert/popup-alert.component';
import { CommonService } from '../../../globals/CommonService';
import { PopupOperation } from '../../../globals/Model/AlertConfig';




declare let $: any;
@Component({
  selector: 'homefabric',
  templateUrl: 'homefabric.component.html',
  styleUrls: ['./homefabric.component.styles.scss']
})

export class HomeFabricComponent {
  takeUntilDestroyObservables = new Subject();

  isListShow: boolean = false;
  // json;
  // checkWidth = false;
  // isSingleProperty = true;
  highlightimage = false;
  isinitilize = false;
  isList = false;

  private desktopAppVersion: typeof version = version;
  private downloadLink: typeof build = build;

  public desktopAppDownloadLink = this.downloadLink.publish[0].url
    + 'visur_setup_v'
    + this.desktopAppVersion
    + '_win_x64.exe'; // visur_setup_v${version}_${os}_x64.${ext}

  homejson = [
    {
      'name': 'WelcomeSection',
      'lable': 'Don\'t show Welcome Section',
      'value': true,
      'type': "checkbox"
    },
    {
      'name': 'Hints',
      'lable': 'Disable Hints(Self-Learning)',
      'value': false,
      'type': "checkbox"
    },
    {
      'name': 'Placeholder',
      'lable': 'placeholder',
      'value': false,
      'type': "checkbox"
    }
  ];



  historyJson = [
    { 'Source': '/wwwroot/images/Home Screen svg/VISUR Desktop.png', 'EntityName': '00/10-23-054-23W5/0(14-16)', 'EntityId': '90e87c7c-6d05-3eb3-9b70-7b62c07ac099', 'TimeStamp': '02/05/2019', 'url': 'http://localhost:4200/Home' },
    { 'Source': '/wwwroot/images/Home Screen svg/VISUR Desktop.png', 'EntityName': '00/10-23-054-23W5/0(14-16)', 'EntityId': '90e87c7c-6d05-3eb3-9b70-7b62c07ac099', 'TimeStamp': '02/05/2019', 'url': 'http://localhost:4200/Home' },
    { 'Source': '/wwwroot/images/Home Screen svg/VISUR Desktop.png', 'EntityName': '00/10-23-054-23W5/0(14-16)', 'EntityId': '90e87c7c-6d05-3eb3-9b70-7b62c07ac099', 'TimeStamp': '02/05/2019', 'url': 'http://localhost:4200/Home' },
    { 'Source': '/wwwroot/images/Home Screen svg/VISUR Desktop.png', 'EntityName': '00/10-23-054-23W5/0(14-16)', 'EntityId': '90e87c7c-6d05-3eb3-9b70-7b62c07ac099', 'TimeStamp': '02/05/2019', 'url': 'http://localhost:4200/Home' },
    { 'Source': '/wwwroot/images/Home Screen svg/VISUR Desktop.png', 'EntityName': '00/10-23-054-23W5/0(14-16)', 'EntityId': '90e87c7c-6d05-3eb3-9b70-7b62c07ac099', 'TimeStamp': '02/05/2019', 'url': 'http://localhost:4200/Home' },
    { 'Source': '/wwwroot/images/Home Screen svg/VISUR Desktop.png', 'EntityName': '00/10-23-054-23W5/0(14-16)', 'EntityId': '90e87c7c-6d05-3eb3-9b70-7b62c07ac099', 'TimeStamp': '02/05/2019', 'url': 'http://localhost:4200/Home' }

  ]
  isDesktop: boolean = false;
  VisurUrl = 'https://visur.one';
  constructor(public commonService: CommonService, public dialog: MatDialog) {
    commonService.lastOpenedFabric = "Home";
    this.commonService.updateViewedFabric(FABRICS.HOME);
    this.commonService.IsFdcGlobalAdmin = false;
    // commonService.channelListHideShow = false;
    // commonService.isAssetsPalleteCreated = false;
    // commonService.isPalleteCreated = false;
    this.commonService.showRightsideBar = false;
    // this.commonService.isHeaderButtonOpenStatus
    $('#headerMenu').hide();

    if (this.commonService.CustomerAppsettings.env.UseAsDesktop) {
      this.isDesktop = true;
    }

    this.commonService.doActivity$.pipe(this.compUntilDestroyed()).subscribe(event => {
      try {
        this.doAction(event)
      } catch (e) {
        console.error('Exception in doActivity$ of homefabric.components at time ' + new Date().toString() + '. Exception is : ' + e);
      }
    });

    this.commonService.HomeFabricSettingJson.pipe(this.compUntilDestroyed()).subscribe((res: any) => {
      try {
        if (res)
          this.homejson = res;
        this.isListShow = this.homejson[0]['value'];
        this.isinitilize = true;
      } catch (e) {
      }
    });

    //  commonService.checkTreeStatus.pipe(this.compUntilDestroyed()).subscribe(res => {
    //         this.checkWidth = true;
    //         this.isSingleProperty = !this.isSingleProperty;
    //     });


    this.commonService.addNew$.next(this.commonService.lastOpenedFabric);
  }
  ngOnInit() {
    this.commonService.changeFavIcon(FABRICS.HOME);
    this.commonService.loadingBarAndSnackbarStatus("", "");
    setTimeout(() => { this.UpdateUserPreferanceRead(); }, 1000)


  }

  onChange(data, obj) {
    this.isListShow = this.homejson[0]['value'];
    this.UpdateUserPreferanceUpdate(this.homejson);
  }

  UpdateUserPreferanceUpdate(jsonData) {
    let payload = { "HOME": jsonData, };
    var dateTime = new Date().toISOString().slice(0, 19).replace('T', ' ');

    var userPayload = { "email": this.commonService.username, "cacheName": "TenantDetails", "UpdatePref": payload, "ModifiedDateTime": dateTime };
    this.commonService.sendMessageToServer(JSON.stringify(userPayload), EntityTypes.UserPreference, this.commonService.currentUserName, EntityTypes.UserPreference, MessageType.UPDATE, Routing.AllFrontEndButOrigin, '', FABRICS.COMMON.toUpperCase())
    this.commonService.updateIndexDBUserPrefrence(null, null, null, "HOME", jsonData);
  }

  UpdateUserPreferanceRead() {
    let userpreferanceCapability = {
      "Fabric": null,
      "UserPinnedTabs": null,
      "Theme": null,
      "Capabilities": null,
      "ZoomPercentage": this.commonService.ZoomPercentage
    };
    this.commonService.appConfig.AllHeaderRoutingListService.forEach((element: any) => {
      if (element.title)
        userpreferanceCapability[element.title] = null;
    })
    var userPayload = { "email": this.commonService.username, "cacheName": "TenantDetails", "Permission": userpreferanceCapability };
    this.commonService.sendMessageToServer(JSON.stringify(userPayload), EntityTypes.UserPreference, this.commonService.currentUserName, EntityTypes.UserPreference, MessageType.READ, Routing.OriginSession, '', FABRICS.COMMON.toUpperCase())
  }

  doAction(event) {
  }

  changeimage() {
    this.highlightimage = false;
  }
  highlighimage() {
    this.highlightimage = true;
  }

  ngOnDestroy() {
    try {
      this.commonService.showRightsideBar = true;
      this.takeUntilDestroyObservables.next();
      this.takeUntilDestroyObservables.complete();
    } catch (e) {
      console.error('Exception in ngOnDestroy() of HomeFabricComponent  at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in ngOnDestroy() of HomeFabricComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables);
  }
  changeGridView() {
    this.isList = false;
  }

  changeListView() {
    this.isList = true;
  }

  Changed(ev) {
    console.log(ev);
  }
  /**
   * External Url Navigation Incase of Desktop
   * @param event clickevent triggered from HTML 
   * @param url URL To BE NAVIGATED
   */
  navigateToExternalUrlInDesktopApp(event, url) {
    // try {
    //   if (this.commonService.CustomerAppsettings.env.UseAsDesktop == true && this.commonService.onlineOffline) {
    //     console.log(event);
    //     console.log(url);
    //     this.commonService.ipc.send("OpenExternalLink", event, url);
    //   } else {
    //     this.openOfflineDialog(new Array(EXTERNAL_LINK_CANNOT_OPEN));
    //   }

    // } catch (e) {
    //   this.commonService.appLogException(new Error('Exception in navigateToExternalUrlInDesktopApp() of HomeFabricComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    // }


  }
  openOfflineDialog(messageToshow) {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: false,
        content: [messageToshow],
        subContent: [],
        operation: PopupOperation.Attention,
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      this.dialog.open(PopUpAlertComponent, matConfig);
    } catch (e) {
      this.commonService.appLogException(new Error('Exception in openOfflineDialog() of HomeFabricComponent at time ' + new Date().toString() + '. Exception is : ' + e));

    }

  }
}
