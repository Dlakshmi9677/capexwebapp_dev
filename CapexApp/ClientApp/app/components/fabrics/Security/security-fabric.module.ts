import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './Security-fabric.routing';
import { SecurityFabricComponent } from './components/Security-fabric.component';
// import { SharedModule1 } from '../../shared/shared1.module';
import { SharedModule1 } from '../../shared/shared1.module';

import { AuthorizationService } from './services/authorization.service';
import { SecurityRoleComponent } from './components/SecurityRole/securityrole.component';
import { SecurityRoleDeactivateGaurdService } from "./components/SecurityRole/SecurityRoleDeactivateGaurdService";
import { SecurityGroupDeactivateGaurdService } from './components/SecurityGroups/Security-groups-can-deactivate-guard.service';
import { SecurityGroupsComponent } from './components/SecurityGroups/securitygroups.component';
import { SecurityGroupTreeComponent } from './components/security-group-tree/security-group-tree.component';
import { SGFormGeneratorComponent } from './components/sgform-generator/sgform-generator.component';
import { SgformGeneratorService } from './components/sgform-generator/sgform-generator.service';
const SECURITY_COMPONENTS = [SecurityFabricComponent, SecurityRoleComponent, SecurityGroupsComponent,SecurityGroupTreeComponent];

@NgModule({
  imports: [CommonModule, SharedModule1, routing],
  providers: [AuthorizationService, SecurityRoleDeactivateGaurdService, SecurityGroupDeactivateGaurdService,SgformGeneratorService],
  declarations: [SECURITY_COMPONENTS, SGFormGeneratorComponent]
})

export class SecurityModule { }
