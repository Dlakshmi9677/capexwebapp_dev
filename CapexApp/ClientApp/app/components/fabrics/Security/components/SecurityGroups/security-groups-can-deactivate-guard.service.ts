import { Injectable } from "@angular/core";
import { CanDeactivate } from "@angular/router";
import { Observable } from "rxjs";
import { SecurityGroupsComponent } from "./securitygroups.component";
import { AuthorizationService } from "./../../services/authorization.service";


@Injectable()
export class SecurityGroupDeactivateGaurdService implements CanDeactivate<SecurityGroupsComponent>{

    constructor(public authorizationService: AuthorizationService){
    }
  canDeactivate(component: SecurityGroupsComponent): Observable<boolean> {
    // if (this.authorizationService.isSecurityGroupModified) {
    //   return component.canDeactivate()
    // }
    // else
      return Observable.of(true);
  }
}
