import { Injectable } from "@angular/core";
import { CanDeactivate } from "@angular/router";
import { Observable } from "rxjs";
import { SecurityRoleComponent } from "./securityrole.component";
import { CommonService } from "../../../../globals/CommonService";
@Injectable()
export class SecurityRoleDeactivateGaurdService implements CanDeactivate<SecurityRoleComponent> {
    constructor(public commonService: CommonService) {
    }
    canDeactivate(component: SecurityRoleComponent): Observable<boolean> {
        // if (this.commonService.isFormModified) {
        //     return component.canDeactivate();
        // }
        // else
            return Observable.of(true);
    }
}
