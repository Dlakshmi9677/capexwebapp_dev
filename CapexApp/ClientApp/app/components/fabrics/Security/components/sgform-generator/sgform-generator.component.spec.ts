import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SGFormGeneratorComponent } from './sgform-generator.component';

describe('SGFormGeneratorComponent', () => {
  let component: SGFormGeneratorComponent;
  let fixture: ComponentFixture<SGFormGeneratorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SGFormGeneratorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SGFormGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
