import { Query } from '@datorama/akita';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/empty'
import { produce } from 'immer';
import * as localForage from 'localforage';
import { AuthorizationStateModel, AuthorizationStateConstantsKeys, SecurityGroupsEntitiesModel } from './authorization.state.model';
import { AuthorizationStateStore } from './authorization.state.store';

@Injectable({ providedIn: 'root' })
export class AuthorizationStateQuery extends Query<AuthorizationStateModel> {
  constructor(protected store: AuthorizationStateStore) {
    super(store);
  }

  /**
   * 
   * @param key key of the state e.g. SecurityGroups
   * @param value value of the key
   */
  addToState(key, value) {
    var stateValue = this.getStateKey(key);
    if (this.isEmpty(stateValue)) {
      for (var groupKey in value) {
        if (value.hasOwnProperty(groupKey)) {
          this.store.update(state => produce(state, draft => {
            draft[key][groupKey] = value[groupKey];
          }));
        }
      }
    }
    else {
      this.replace(key, value);
    }
  }

  /**
   * To fully replace or add state key with value
   * for example
   *   securityGroups with value
   * 
   */
  replace(key, value) {
    this.store.update(state => ({
      [key]: value
    }));
  }

  /**
   * 
   * @param key key of the state e.g. SecurityGroups etc.
   */
  getStateKey(key) {
    return this.getValue()[key];
  }

  /**
   * To get full state object
   */
  getAll(): AuthorizationStateModel {
    return this.getValue();
  }

  /**
   * 
   * @param fabric key of the Security group
   * @param entityId id of the entities for the securityGroups key
   */
  deleteNodeById(fabric, entityId) {
    var stateExistingData = this.getStateKey(AuthorizationStateConstantsKeys.SecurityGroups);
    this.store.update(state => produce(state, draft => {
      // draft[AuthorizationStateConstantsKeys.SecurityGroups].splice([draft[key].findIndex(obj => obj[innerId] === InnerIdsValue)], 1)
      draft[AuthorizationStateConstantsKeys.SecurityGroups][fabric].forEach(item => {
        if (item.hasOwnProperty('Entities')) {
          item.Entities.splice(item.Entities.findIndex(obj => obj === entityId), 1);
        }
      });
    }));
  }

  /**
   * 
   * @param fabric securityGroup fabric i.e fdc
   * @param EntityName name of the Entity Group i.e AdminiStratorSecurityGroup
   */
  deleteSecurityGroupFromFabric(fabric, EntityName) {
    this.store.update(state => produce(state, draft => {
      let index = draft[AuthorizationStateConstantsKeys.SecurityGroups][fabric].findIndex(obj => obj.EntityName === EntityName);
      if (index != -1) {
        draft[AuthorizationStateConstantsKeys.SecurityGroups][fabric].splice(index, 1);
      }
    }));
  }

  isEmpty(jsonObject) {
    for (var key in jsonObject) {
      if (jsonObject.hasOwnProperty(key))
        return true;
    }
    return false;
  }

  /**
   * 
   * @param fabricList "['fdc', 'OperationalForm', Asset]"
   * @param value entityId eg.. '5e80497a-31da-4021-a7c6-415066ebd51d'  
   */
  addEntityIdToState(fabricList: Array<string>, entityId: string) {
    try {
      fabricList.forEach(fabric => {
        this.store.update(state => produce(state, draft => {
          if (draft[AuthorizationStateConstantsKeys.SecurityGroups][fabric]) {
            draft[AuthorizationStateConstantsKeys.SecurityGroups][fabric].forEach(element => {
              if (element.hasOwnProperty('Entities') && !element["Entities"].includes(entityId)) {
                element["Entities"].push(entityId);
              }
            });
          }
          else {
              console.warn(fabric + " doesn't exist in " + this.store.storeName + " store");
          }
        }));
      });
    }
    catch (e) {
      console.error('Exception in addEntityIdToState() method of AuthorizationServiceStateQuery at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  /**
   * @param securityGroup SecurityGroup object
   * @param fabric Fabric Name eg.. 'fdc', 'OperationalForm' etc..
   */
  addSecurityGroupToState(securityGroup: SecurityGroupsEntitiesModel, fabric: string) {
    try {
      this.store.update(state => produce(state, draft => {
        draft[AuthorizationStateConstantsKeys.SecurityGroups][fabric].push(securityGroup);
      }));
    }
    catch (e) {
      console.error('Exception in addSecurityGroupToState() method of AuthorizationServiceStateQuery at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  /**
   * @param securityGroup SecurityGroup object
   * @param fabric Fabric Name eg.. 'fdc', 'OperationalForm' etc..
   */
  updateSecurityGroupToState(securityGroup: SecurityGroupsEntitiesModel, fabric: string) {
    try {
      this.store.update(state => produce(state, draft => {
        draft[AuthorizationStateConstantsKeys.SecurityGroups][fabric].forEach(element => {
          if (element.hasOwnProperty('EntityId') && element.hasOwnProperty('Entities') && element['EntityId'] == securityGroup.EntityId) {
            securityGroup.Entities.forEach(entity => {
              element['Entities'].push(entity);
            });
          }
        });
      }));
    }
    catch (e) {
      console.error('Exception in updateSecurityGroupToState() method of AuthorizationServiceStateQuery at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  /**
   * @param securityGroup SecurityGroup object
   * @param fabric Fabric Name eg.. 'fdc', 'OperationalForm' etc..
   * @param entityId EntityId eg....123456ertdfgh3456
   */
  deleteEntityFromSecurityGroup(securityGroupId: string, fabric: string, entityId: string) {
    try {
      this.store.update(state => produce(state, draft => {
        draft[AuthorizationStateConstantsKeys.SecurityGroups][fabric].forEach(element => {
          if (element.hasOwnProperty('Entities') && element['EntityId'] == securityGroupId) {
            let index = element.Entities.findIndex(obj => obj === entityId);
            if (index != -1) {
              element.Entities.splice(index, 1);
            }
          }
        });
      }));
    }
    catch (e) {
      console.error('Exception in deleteEntityFromSecurityGroup() method of AuthorizationServiceStateQuery at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  /**
   * @param securityGroupId eg...12345fgh
   * @param fabric Fabric Name eg.. 'fdc', 'OperationalForm' etc..
   */
  deleteSecurityGroup(securityGroupId: string, fabric: string) {
    try {
      this.store.update(state => produce(state, draft => {
        let index = draft[AuthorizationStateConstantsKeys.SecurityGroups][fabric] != undefined ? draft[AuthorizationStateConstantsKeys.SecurityGroups][fabric].findIndex(obj => obj.EntityId === securityGroupId) : -1;
        if (index != -1) {
          draft[AuthorizationStateConstantsKeys.SecurityGroups][fabric].splice(index, 1);
        }
      }));
    }
    catch (e) {
      console.error('Exception in deleteSecurityGroup() method of AuthorizationServiceStateQuery at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }


}
