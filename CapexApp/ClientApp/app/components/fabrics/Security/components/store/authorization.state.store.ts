import { Store, StoreConfig } from '@datorama/akita';
import { Injectable } from '@angular/core';
// import * as localForage from 'localforage';
// import { persistState } from '@datorama/akita';
// import { AppConfig } from "../../../../globals/services/app.config";
import { AuthorizationStateModel, SecurityGroupsFabricsModel } from './authorization.state.model';


@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'authorization', resettable: true })
export class AuthorizationStateStore extends Store<AuthorizationStateModel> {

  // public CustomerAppsettings = AppConfig.AppMainSettings;

  // constructor(private appConfig: AppConfig) {
    constructor() {
    super(createInitialState(JSON.stringify(json)));

    // if ((this.CustomerAppsettings.env.name == "PROD" || this.CustomerAppsettings.env.name == "DEV") && this.CustomerAppsettings.env.UseAsDesktop == false) {
    //   if (this.appConfig.tenantName) {
    //     localForage.config({
    //       driver: localForage.INDEXEDDB,
    //       name: 'Visur',
    //       storeName: this.appConfig.tenantName
    //     });
    //     persistState({ key: this.storeName, include: [this.storeName], storage: localForage });
    //   } else {
    //     console.error(this.storeName + " TenantName can not be undefinded");
    //   }
    // }
  }
}

const json = {
  SecurityGroups: {} as SecurityGroupsFabricsModel,
};

const createInitialState = (json): AuthorizationStateModel => {
  return JSON.parse(json);
}
