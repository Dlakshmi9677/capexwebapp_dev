import { FABRICS } from '../../../../globals/Model/CommonModel';

export interface AuthorizationStateModel extends SecurityGroupsFabricsModel{
    SecurityGroups:SecurityGroupsFabricsModel
}

export interface SecurityGroupsFabricsModel{

}

export interface SecurityGroupsEntitiesModel{
    EntityId : string,
    EntityName : string,
    Entities: Array<any>
}

export class AuthorizationStateConstantsKeys extends FABRICS{
    public static SecurityGroups = "SecurityGroups";
}

