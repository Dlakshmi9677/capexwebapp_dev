import {Component, ElementRef, EventEmitter, Input, NgModule, Output, ViewChild} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { alertPopUp } from 'ClientApp/app/components/common/AttentionPopUp/popup';
import { DELETE_ROWS, NEW_ROWS, ROWS_DROPPED } from 'ClientApp/app/components/common/Constants/AppConsts';
import { FOUR_CHARACTERS_REQUIRED } from 'ClientApp/app/components/common/Constants/AttentionPopupMessage';
import { CommonService } from 'ClientApp/app/components/globals/CommonService';
import { AngularTreeMessageModel, TreeOperations } from 'ClientApp/app/components/globals/Model/CommonModel';
import { ITreeConfig } from 'ClientApp/app/components/globals/Model/EntityTypes';
import { debounced } from 'ClientApp/app/components/template/LayoutComponents/leftsidebar/tree.component';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-security-group-tree',
  template: `
    <div id="security-filter-search">
    <div class="security-tree-left-area textSelectionDBClickRestict">
    <mat-icon  *ngIf="showExpandAll" class='expand-btn  float-l'
    [svgIcon]=" isExpand ? 'CollapseAll' : 'ExpandAll' " (click)="method1CallForClick()"
    ></mat-icon>
    <div class="security-tree-label" [ngClass]=" !isSearchText? 'visible float-l' : 'hidden float-l' ">{{title}}</div>
    <div class='float-l security-search-texts'
    [ngClass]="isSearchText?  'search-width visible' : 'hidden' ">
    <input class="search-text" [(ngModel)]='filterText' id="filter" type="search" placeholder="   Filter" autofocus
      autocomplete="off" (click)="searchTextFieldClick($event);" (keyup)="debouncedClick($event)" #inputSearch />
    <mat-icon matSuffix class="clear-icon" matTooltip='CLOSE' svgIcon="V3 CloseCancel" (click)="clearFilter()">
    </mat-icon>
    </div>
    </div>
    <div class="security-tree-right-area">
      <mat-icon *ngIf="showFilter" matSuffix class="search-icon float-r" role="img" aria-hidden="true" svgIcon="V3 LeftPaneFilter"
        matTooltip='FILTER' [matTooltipDisabled]="commonService.tabletMode ? true : false"
        [ngClass]="{'full-opacity': '' }" (click)="filterIconClick($event)"></mat-icon> 
      <mat-icon *ngIf="showSearch" matSuffix class="search-icon float-r" role="img" aria-hidden="true" svgIcon="MainSearch"
        matTooltip='SEARCH' [matTooltipDisabled]="commonService.tabletMode ? true : false"
        [ngClass]="{'full-opacity': isSearchText }" (click)="searchIconClick($event)"></mat-icon>
    </div>
  </div>
    <div *ngIf="type == 'tree'" class="SecurityGroupTree">
      <capability-tree [HierarchyTreeName]="treeName"></capability-tree>
    </div>
    <div *ngIf="type == 'form'" class="SecurityGroupTree">
    <app-sgform-generator [events]="emitEventsToChild.asObservable()" [schemaPage]="schemaPage"  (commonOutputEmitter)="OutputEmitter($event)">
    </app-sgform-generator>
    </div>
`,
styleUrls: [ './security-group-tree.component.scss' ]
})
export class SecurityGroupTreeComponent {

@Input() treeName;
@Input() title;
@Input() type;
@Input() schemaPage;
@Input() events: Observable<any>;
@Input() showFilter:boolean = false;
@Input() showSearch:boolean = false;
@Input() showExpandAll:boolean = false;
@Output() commonOutputEmitter = new EventEmitter();
emitEventsToChild: Subject<any> = new Subject<any>();
takeUntilDestroyObservables = new Subject();

debouncedClick
text = ''
filterText = null;
isExpand = false;
isSearchText = false;
isActive = false;
@ViewChild('inputSearch', { static: false }) _Inputelement : ElementRef; 


    constructor(public commonService: CommonService,public dialog: MatDialog) {

        this.debouncedClick = debounced($event => {
            this.searchEntity($event)
          }, 400);
    }
    
    ngOnInit(): void {
      if(this.events){
        this.events.pipe(this.compUntilDestroyed()).subscribe((event) => this.emitEventToChild(event));
      }
    }

    compUntilDestroyed(): any {
      return takeUntil(this.takeUntilDestroyObservables);
    }

    method1CallForClick(){
        if(this.isExpand==true){
            this.ExpandCollapse()
        }
        else{
          this.isExpand=false
          this.ExpandCollapse()
        }
    }
    
    
    ExpandCollapse() {
      this.isExpand = !this.isExpand;
      this.UpdateExpandAndCollapse();
    }
    
    UpdateExpandAndCollapse() {
      var configdata: ITreeConfig = { "treeExpandAll": this.isExpand };
      this.sendDataToAngularTree(TreeOperations.treeConfig, configdata);
    }
    
    searchIconClick(event) {
      try {
        this.isSearchText = !this.isSearchText;
        
        if (!this.isSearchText) {
          this.isActive = this.isSearchText;
          if(this.filterText)
          this.clearFilter();
          this.commonService.searchActive = false;
        }
        else {
          this.isActive = this.isSearchText;
          this.setFocus()
          this.commonService.searchActive = true;
        }
      }
      catch (e) {
        console.error('Exception in onClickFilter() of securitygroups.component at SecurityGroup time ' + new Date().toString() + '. Exception is : ' + e);
      }
    }

    filterIconClick(event) {
      try {
          this.emitEventToChild({id:"filter"});
      }
      catch (e) {
        console.error('Exception in filterIconClick() of securitygroups.component at SecurityGroup time ' + new Date().toString() + '. Exception is : ' + e);
      }
    }    
  
    setFocus() {
      if ( this._Inputelement && this._Inputelement.nativeElement)
        setTimeout(() => { this._Inputelement.nativeElement.focus(); }, 0)
    }
    
    searchEntity(event) {
      try {
        let value;
        if(event)
        value = event.target.value ? event.target.value : '';
           var message: AngularTreeMessageModel = {
            "fabric": this.commonService.lastOpenedFabric,
            "treeName": [this.treeName],
            "treeOperationType": TreeOperations.entityFilter,
            "treePayload": value
           };
           if (event && event.keyCode == 13 && value.length < 4) {
            new alertPopUp().openDialog(FOUR_CHARACTERS_REQUIRED, this.dialog);
           }
           else if (value.length > 3) {
            this.text = this.filterText = value;
            if(this.type == 'form'){
              this.emitEventToChild({id:"searchinput",value:value});
            }
            else{
              this.commonService.sendDataFromAngularTreeToFabric.next(message);
            }
           }
           else if ((value.length <= 3)) {
            message.treePayload = ''
            this.text = '';
            if(this.type == 'form'){
              this.emitEventToChild({id:"searchinput",value:value});
            }
            else{
              this.commonService.sendDataFromAngularTreeToFabric.next(message);
              this.isExpand=true
              this.ExpandCollapse()
            }
           }
      }
      catch (e) {
        console.error('Exception in searchEntity() of securitygroups.component at SecurityGroup time ' + new Date().toString() + '. Exception is : ' + e);
      }
    }
    
    clearFilter() {
      try {
        if(this.filterText){
          this.filterText = '';
          this.text = '';
          if(this.type == 'form'){
            this.emitEventToChild({id:"searchinput",value:''});
          }
          else{
            var message: AngularTreeMessageModel = {
              "fabric": this.commonService.lastOpenedFabric,
              "treeName": [this.treeName],
              "treeOperationType": TreeOperations.entityFilter,
              "treePayload": ''
            };
            this.commonService.sendDataFromAngularTreeToFabric.next(message);
          }
          this.commonService.searchActive = false;
        }
        this.isSearchText = false;
        this.isActive = false;
        this.setFocus()
      }
      catch (e) {
        console.error('Exception in clearFilter() of securitygroups.component at SecurityGroup time ' + new Date().toString() + '. Exception is : ' + e);
      }
      
    }
    
    searchTextFieldClick($event){
    
    }

    sendDataToAngularTree(treeOperationType: string, payload: any) {
        try {
          var treeMessage: AngularTreeMessageModel = {
            "fabric": this.commonService.lastOpenedFabric,
            "treeName": [this.treeName],
            "treeOperationType": treeOperationType,
            "treePayload": payload,
          }
          this.commonService.sendDataToAngularTree.next(treeMessage);
        } catch (e) {
          console.error('exception in sendDataToAngularTree() of SecurityGroupsComponent at time ' + new Date().toString() + '. exception is : ' + e);
        }
      }

      OutputEmitter(event){
        let buttonEvent: string = event.buttonEvent ? event.buttonEvent.toLowerCase() : null;
        switch(buttonEvent){
          case NEW_ROWS:
          case DELETE_ROWS:  
          this.commonOutputEmitter.emit(event);
            break;
          default:
            
            break;  
        }
      }

      emitEventToChild(event) {
        this.emitEventsToChild.next(event);
      }

      ngOnDestroy(){
        this.takeUntilDestroyObservables.next();
        this.takeUntilDestroyObservables.complete();
      }

}
