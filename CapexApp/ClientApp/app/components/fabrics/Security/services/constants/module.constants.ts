export const ROLES = 'roles';
export const SECURITYGROUPS = 'securitygroups';
export const SECURITY_GROUPS = 'Security Groups';
export const SECURITY_ROOTNODE = 'SecurityRootNode';

export const PARENTTREENODE = 'althingTreeRoot';