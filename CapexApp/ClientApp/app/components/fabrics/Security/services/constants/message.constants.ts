export const MessageEntityTypes = {
    ALLFORMSCHEMANAMES: "FormSchemaNames",
    ALLROLES: "AllRoles",
    ROLESCHEMAPERMISSIONS: "RoleSchemaPermissions",
    SECURITYGROUPS: "SecurityGroups"
}

export const DataTypes = {
    FORMSCHEMAROLES: 'FormSchemaRoles'
}