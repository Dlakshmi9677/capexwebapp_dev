import { Routes, RouterModule } from '@angular/router';
import { SecurityFabricComponent } from './components/Security-fabric.component'
import { SecurityRoleComponent } from './components/SecurityRole/securityrole.component';
import { SecurityRoleDeactivateGaurdService } from "./components/SecurityRole/SecurityRoleDeactivateGaurdService";
import { SecurityGroupsComponent } from './components/SecurityGroups/securitygroups.component';
import { SecurityGroupDeactivateGaurdService } from './components/SecurityGroups/Security-groups-can-deactivate-guard.service';

const routes: Routes = [
  {
    path: '', component: SecurityFabricComponent, children: [
      {
        path: "editRole",
        component: SecurityRoleComponent,
        canDeactivate: [SecurityRoleDeactivateGaurdService]
      },
      {
        path: "NewRole",
        component: SecurityRoleComponent,
        canDeactivate: [SecurityRoleDeactivateGaurdService]
      },
      {
        path: "editSecuritygroup",
        component: SecurityGroupsComponent,
        canDeactivate: [SecurityGroupDeactivateGaurdService]
      },
      {
        path: "NewSecuritygroup",
        component: SecurityGroupsComponent,
        canDeactivate: [SecurityGroupDeactivateGaurdService]
      },
      {
        path: "SecurityGroup/:OperationType/:SecurityGroupID",
        component: SecurityGroupsComponent,
        canDeactivate: [SecurityGroupDeactivateGaurdService]
      },
      {
        path: "SecurityGroup/:LeftNavEntityId/:OperationType/:SecurityGroupID",
        component: SecurityGroupsComponent,
        canDeactivate: [SecurityGroupDeactivateGaurdService]
      }
    ]
  }
];

export const routing = RouterModule.forChild(routes);
