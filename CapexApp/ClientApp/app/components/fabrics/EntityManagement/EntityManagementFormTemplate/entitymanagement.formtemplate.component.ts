import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { TreeModel } from '@circlon/angular-tree-component';
import { AppConsts, peopleEntityTypes } from 'ClientApp/app/components/common/Constants/AppConsts';
import { Mandatory_Message } from 'ClientApp/app/components/common/Constants/AttentionPopupMessage';
import { PopUpAlertComponent } from 'ClientApp/app/components/common/PopUpAlert/popup-alert.component';
import { CommonService } from 'ClientApp/app/components/globals/CommonService';
import { PopupOperation } from 'ClientApp/app/components/globals/Model/AlertConfig';
import { FABRICS, FabricsNames, MessageKind, ReceiveFromComponentMyFormEvent, SendToComponentMyFormEvent, TreeDataEventResponseModel, TreeOperations, TreeTypeNames } from 'ClientApp/app/components/globals/Model/CommonModel';
import { EntityTypes } from 'ClientApp/app/components/globals/Model/EntityTypes';
import { MessageModel, Routing } from 'ClientApp/app/components/globals/Model/Message';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { FabricRouter } from '../../../globals/Model/FabricRouter';
import { EntityTypesReverseKVPair } from '../../../common/Constants/JsonSchemas';
import { EntityManagementService } from '../services/EntityManagementService';

export const PREVIOUS = 'PREVIOUS';
export const NEXT = 'NEXT';
export const CHANGE = 'CHANGE';
@Component({
  selector: 'entity-management-formtemplate',
  templateUrl: './entitymanagement.formtemplate.component.html',
  styleUrls: ['./entitymanagement.formtemplate.component.scss']
})

export class EntityManagementFormTemplate implements OnInit, OnDestroy {

  headerConfig = [];
  locationName;
  listOfChildren = [];
  myForm: FormGroup;
  tab = '';
  AdminData;
  Schema;
  entityData;
  RouteParam: any = {};
  nodeId = [];
  dialogRef;
  isForm: boolean;
  entityMgmtCommonInputVariable = { "updated": true };
  treeIndexTab = "";
  activeroute = false;
  formInitialized: boolean = false;
  userId;
  param;
  mutualFabricTab: Array<any> = [];
  takeUntilDestroyObservables = new Subject();
  constructor(public entitymgmtService: EntityManagementService, public commonService: CommonService, private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder, public dialog: MatDialog) {


    this.entitymgmtService.EntityMgmtCommonObservable
      .pipe(filter((data: any) =>
        data.MessageKind == MessageKind.DELETE)).pipe(this.compUntilDestroyed())
      .subscribe((message: MessageModel) => {
        this.deleteOperation(message);
      });

    this.entitymgmtService.EntityMgmtCommonObservable
      .pipe(filter((data: any) =>
        data.FormType == "Security&Permissions")).pipe(this.compUntilDestroyed())
      .subscribe((res: any) => {
        this.myForm.controls.listOfChildren["controls"].forEach((item: FormGroup) => {
          if (item.value.EntityId == res.FormType) {
            item.controls.Enable.setValue(res.value, { emitEvent: false });
            this.userId = res.userId;
          }
        });
      });

    let tempSubscription = this.entitymgmtService.onTreeEventChanges.filter(obj => obj.TreeType == this.commonService.treeIndexLabel).subscribe(data => {
      try {
        setTimeout(() => {  // to highlight the activeSelected node after reloading
          this.creatEntityMangementForm();
        }, 2500);

        if (this.entityData && this.router.url && this.router.url.includes(this.entityData.EntityId)) {
          this.entitymgmtService.sendDataToAngularTree([this.param.tab], TreeOperations.ActiveSelectedNode, this.entityData.EntityId);
        }
        tempSubscription.unsubscribe();
      }
      catch (e) {
        this.commonService.appLogException(new Error('Exception in onTreeEventChanges subscriber of constructor of EntityManagement.formtemplate.component' + new Date().toString() + '. Exception is : ' + e));
      }
    });

    this.commonService.getComponentMyFormEvent$
      .pipe(this.compUntilDestroyed())
      .filter((event: SendToComponentMyFormEvent) => event.ComponentName == "EntityManagementFormTemplate")
      .subscribe((resEvent: SendToComponentMyFormEvent) => {
        let sendtocomponent: ReceiveFromComponentMyFormEvent = {
          ComponentName: "EntityManagementFormTemplate",
          MyForm: this.myForm
        };
        (<EventEmitter<any>>resEvent.Event).next(sendtocomponent);
      });

  }

  // lastScrollTop=0;
  // isScrollBasedRead=true;
  // listOfChildrenScroll=[];
  // previousChildrenSize=0;
  // @HostListener("scroll", ['$event'])
  // onScroll(event) {
  //  if(this.isScrollBasedRead) {
  //   let scrollTop = event.target.scrollTop;
  //   if(scrollTop > this.lastScrollTop) {//downscroll
  //     let doc:any = document.querySelector('#EM-Form-Gen');
  //     if(doc){
  //       //let scrollHeight= doc.scrollHeight;
  //       let itemCount=this.listOfChildren.length;
  //       let rowHeight=42;
  //       let nodePadding=0;
  //       //let viewportHeight=doc.offsetHeight;
  //       let noOfChildrenToShow = this.getVisibleNodesCount('#EM-Form-Gen',itemCount,rowHeight,scrollTop,nodePadding);

  //       if(noOfChildrenToShow > this.previousChildrenSize)
  //       this.previousChildrenSize=noOfChildrenToShow;

  //       const locFormsArray = this.myForm.controls.listOfChildren as FormArray;

  //       if(noOfChildrenToShow <= this.listOfChildren.length) { //&& noOfChildrenToShow > locFormsArray.controls.length){
  //         this.previousChildrenSize++;
  //         let itemArray = this.listOfChildrenScroll.slice(locFormsArray.controls.length,this.previousChildrenSize);
  //         if(itemArray && itemArray.length)
  //         this.addControlToListOfChildren(itemArray);


  //         if(this.previousChildrenSize==this.listOfChildren.length)
  //         this.isScrollBasedRead=false;
  //       }
  //     }
  //   }
  //   else {//upscroll

  //   }
  //   this.lastScrollTop = scrollTop;
  //  }
  // }

  getVisibleNodesCount(ScrollAreaIDOrClass, itemCount, rowHeight = 42, scrollTop = 0.800000011920929, nodePadding = 0) {
    let doc: any = document.querySelector(ScrollAreaIDOrClass);
    let viewportHeight = doc.offsetHeight;

    //const totalContentHeight = itemCount * rowHeight;
    let startNode = Math.floor(scrollTop / rowHeight) - nodePadding;
    startNode = Math.max(0, startNode);
    let visibleNodesCount = Math.ceil(viewportHeight / rowHeight) + 2 * nodePadding;
    visibleNodesCount = Math.min(itemCount - startNode, visibleNodesCount);
    return visibleNodesCount;
  }

  ngOnInit() {
    setTimeout(() => {
      this.creatEntityMangementForm();
    }, 0);
  }
  /**moved code into side header for navigating capability to capability just commented testing purpose */
  // checkEntityExistOrNot(){
  //   Observable.combineLatest(
  //     [this.route.params,
  //       this.route.queryParams])
  //     .pipe(this.compUntilDestroyed())
  //   .subscribe(([params,queryParams]) => {
  //     params=JSON.parse(JSON.stringify(params))
  //     params.expand=queryParams.expand;
  //     params.leftnav=queryParams.leftnav;
  //     if(Object.keys(this.RouteParam).length<=0||this.RouteParam.tab!=params.tab||this.RouteParam.EMTab!=params.EMTab||this.RouteParam.EntityID!=params.EntityID){
  //     if(this.commonService.getFabricNameByUrl(this.router.url)==FABRICS.ENTITYMANAGEMENT){
  //       if(AppConsts.mutualFabric.includes(this.entitymgmtService.getCapabilityFormNameBasedOnTab(params.tab))){
  //         let entityData: any = this.commonService.getEntitiesById(params.EntityID);
  //         let capabilityName = this.commonService.getCapabilityNamesBy(entityData.Capabilities)
  //         if(!capabilityName.includes(this.entitymgmtService.getCapabilityFormNameBasedOnTab(params.tab))){
  //           this.RouteParam = JSON.parse(JSON.stringify(params));
  //           delete this.RouteParam.EntityID;
  //           delete this.RouteParam.EMTab;
  //           this.router.navigate([FABRICS.ENTITYMANAGEMENT], { queryParams: this.RouteParam });
  //           this.entitymgmtService.openCommonAttentionPopup(ENTITY_NOT_EXIST(params.tab))
  //         }
  //         else{
  //           this.creatEntityMangementForm(params);
  //         }
  //       }
  //       else{
  //         this.creatEntityMangementForm(params);
  //       }
  //       this.RouteParam = JSON.parse(JSON.stringify(params));
  //     }
  //   }
  //   })
  // }
  creatEntityMangementForm() {
    Observable.combineLatest([this.route.params, this.route.queryParams]).pipe(this.compUntilDestroyed())
      .subscribe(([params, queryParams]) => {
        try {
          params = JSON.parse(JSON.stringify(params));
          params.expand = queryParams.expand;
          params.leftnav = queryParams.leftnav;
          if (Object.keys(this.RouteParam).length <= 0 || this.RouteParam.tab != params.tab || this.RouteParam.EMTab != params.EMTab || this.RouteParam.EntityID != params.EntityID) {
            this.treeIndexTab = params.tab;
            this.tab = params.EMTab;
            this.param = params;
            this.commonService.getEntitiesById$(params.EntityID).first().pipe(this.compUntilDestroyed()).subscribe((entity: any) => {
              if (entity) {
                this.commonService.entityData = entity;
                this.entityData = entity;
                this.entityMgmtCommonInputVariable['ParentData'] = entity;
                this.locationName = entity.EntityName;
                this.RouteParam = JSON.parse(JSON.stringify(params));
                this.getListOfChildrens(params.EntityID);
              }
            });
          }
          else
            this.RouteParam = JSON.parse(JSON.stringify(params));
          this.setHeader(params);

        } catch (e) {
          this.commonService.appLogException(new Error('Exception inroute.queryParams subscriber of constructor of EntityManagement.formtemplate.component in  EntityManagementFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
        }
      });

  }
  ScrubberConcept(res) {
    try {
      if (this.myForm && this.myForm.controls && this.myForm.controls.listOfChildren) {
        let controlArray: FormArray = this.myForm.controls.listOfChildren as FormArray;
        var index = controlArray.controls.findIndex(control => control.value.EntityId == res.EntityId);
        if (index != -1)
          controlArray.removeAt(index);
        // list of children remove
        var indexList = this.listOfChildren.findIndex(element => element.EntityId == res.EntityId);
        if (indexList != -1)
          this.listOfChildren.splice(indexList, 1);
        this.commonService.ListOfVesselsEquipments.forEach(element => {
          index = controlArray.controls.findIndex(control => control.value.EntityId == element.EntityId);
          if (index != -1) {
            controlArray.removeAt(index);
          }
          var indexList = this.listOfChildren.findIndex(element => element.EntityId == element.EntityId);
          if (indexList != -1)
            this.listOfChildren.splice(indexList, 1);

        });
        this.myForm.controls.listOfChildren = controlArray;
        this.myForm.markAsTouched();
      }
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in ScrubberConcept() of DataEntryComponent in DataEntryFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  deleteOperation(message) {
    switch (message.Fabric.toUpperCase()) {
     
      case FABRICS.PEOPLEANDCOMPANIES.toUpperCase():
        if (message.EntityID == this.entityData.EntityId) {
          this.closeForm();
        }
      default:
        break;
    }

  }
  VesselsEntityNameUpdate(res, vessel) {
    if (this.myForm && this.myForm.controls && this.myForm.controls.listOfChildren) {
      this.myForm.controls.listOfChildren['controls'].forEach(control => {

      });
    }
  }
  entitytype(entitytype) {
    try {
      return EntityTypesReverseKVPair[entitytype] ? EntityTypesReverseKVPair[entitytype] : entitytype;
    }
    catch (e) {
    }
  }
  setHeader(RouteParam) {
    this.headerConfig = [
      {
        'source': JSON.parse(RouteParam.expand) ? 'V3 CollapsAll' : 'V3 ExpandAll',
        'title': JSON.parse(RouteParam.expand) ? 'Collapse All' : 'Expand All',
        'routerLinkActive': false,
        'id': 'expandcollapse',
        'class': 'icon21-21',
        'float': 'left',
        'type': 'icon',
        'show': true
      },
      {
        'source': 'V3 CloseCancel',
        'title': 'Close',
        'routerLinkActive': false,
        'id': 'close',
        'class': 'icon21-21',
        'float': 'right',
        'type': 'icon',
        'show': true
      },
      {
        'source': 'FDC_Checkmark_Icon_Idle',
        'title': 'Save',
        'routerLinkActive': false,
        'id': 'save',
        'class': 'icon21-21',
        'float': 'right',
        'type': 'icon',
        'show': true
      },
    
      {
        'source': 'V3 PeepsAndComps',
        'title': this.locationName,
        'routerLinkActive': false,
        'id': 'location',
        'class': 'icon21-21',
        'float': 'left',
        'type': 'select',
        'show': true,
        'uppercase': 'Isuppercase'
      }
    ];
  }
  getListOfChildrens(entityId) {
    let entityData: any = this.commonService.getEntitiesById(entityId);
    let capabilityName: any = [];
    switch (this.treeIndexTab) {
      default:
        break;
    }
    this.listOfChildren = [];
    if (this.tab == "People & Companies" || this.tab == "Person")
      this.listOfChildren.push({ "EntityName": entityData.EntityName, "EntityType": entityData.EntityType, "EntityId": entityData.EntityId, "LocationId": entityData.EntityId, TypeOfEntity: entityData.TypeOf, "Enable": true, tab: this.tab, "expand": true });
    else
      this.listOfChildren.push({ "EntityName": entityData.EntityName, "EntityType": entityData.EntityType, "EntityId": entityData.EntityId, "LocationId": entityData.EntityId, TypeOfEntity: entityData.TypeOf, "Enable": true, tab: this.tab, "expand": false });

    this.addlistOfChildren(this.RouteParam.EntityID, this.RouteParam.EMTab, entityData.EntityType);
    this.isForm = true;
    if (this.treeIndexTab) {
      this.entitymgmtService.sendDataToAngularTree([this.RouteParam.tab], TreeOperations.deactivateSelectedNode, "");
      if (this.commonService.searchActive) {
        this.entitymgmtService.activateEntityWhenSearchActive(this.RouteParam);
      }
      else {
        this.entitymgmtService.sendDataToAngularTree([this.RouteParam.tab], TreeOperations.ActiveSelectedNode, this.RouteParam.EntityID);
      }
    }
    if (capabilityName.length > 0 && !capabilityName.includes(this.commonService.getCapabilityFormNameBasedOnTab(this.treeIndexTab))) {
      this.closeForm();
    }

  }

 
  PiggingformConfigSchemaSubscription(message) {
    try {
      let obj: any = {};
      let payload: any = JSON.parse(message['Payload']);
      var formSchema = payload["Schema"] ? JSON.parse(payload["Schema"]) : null;
      const control = this.myForm.controls['listOfChildren'] as FormArray;
      if (payload["AdminData"]) {
        this.AdminData = JSON.parse(payload["AdminData"]);
        if (this.AdminData && this.AdminData.length > 0) {
          this.AdminData.forEach(entity => {
            let type = entity.Purpose ? "Sender" : "Receiver";
            obj = { "EntityName": entity.EntityName, "EntityType": entity.EntityType, "Type": type, "EntityId": entity.EntityId, "Enable": true, tab: this.tab, "expand": false };
            this.listOfChildren.push(obj);
            this.listOfChildren = this.getSortingListOfChildren(this.listOfChildren);
            let index = this.listOfChildren.findIndex(d => d.EntityId == obj.EntityId);
            control.insert(index, this.formBuilder.group(obj));
          });
        }
      }
      this.Schema = formSchema.Schema;
    } catch (error) {

    }
  }
  getData(EntityId, accordion?) {
    try {
      switch (this.tab) {
        default:
          return null;
          break;
      }
    } catch (error) {
    }
  }
  ChangeLocation(ev) {
    try {
      switch (ev.opreation) {
        case PREVIOUS:
          this.getPrevLocation();
          break;
        // case 'NEXT':
        // break;

        case NEXT:
          this.getNextLocation();
          break;

        case CHANGE:
          break;
      }

      if (ev && ev.opreation && (ev.opreation === PREVIOUS || ev.opreation === NEXT)) {
        this.commonService.isNextOrPreviousClicked = true;
      }
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in ChangeLocation() of EntityManagement.formtemplate in EntityManagementFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  getPrevLocation() {

    /**
     * new logic started
     * this logic is written to get prev locations
     */
    this.commonService.event = "drillUp";
    var treeName = this.RouteParam.tab;

    this.entitymgmtService.getAngularTreeEvent([treeName], TreeOperations.GetTreeInstance).
      first().
      subscribe((response: TreeDataEventResponseModel) => {
        let treeNames: Array<string> = response.treeName;
        let tree = response.treePayload;
        if (tree && treeNames.indexOf(treeName) > -1) {
          var tempdata = this.commonService.previousLocationData;
          var model: TreeModel = tree.treeModel;
          model.focusPreviousNode();
        }

        let RouteParam = JSON.parse(JSON.stringify(this.RouteParam));
        RouteParam.EntityID = this.commonService.previousLocationData.EntityId;
        let EntityType = this.commonService.previousLocationData.EntityType;
        if (RouteParam.EMTab == EntityTypes.PERSON && (EntityType == EntityTypes.OFFICE || EntityType == EntityTypes.COMPANY))
          RouteParam.EMTab = TreeTypeNames.CompaniesOrPeople;
        else if (RouteParam.EMTab == TreeTypeNames.CompaniesOrPeople && EntityType == EntityTypes.PERSON)
          RouteParam.EMTab = EntityTypes.PERSON;
        if (treeName) {
          this.entitymgmtService.sendDataToAngularTree([treeName], 'ActiveSelectedNode', RouteParam.EntityID);
        }
        this.navigateBasedOnQueryParams(RouteParam);
      });


  }


  getNextLocation() {
    try {
      this.commonService.event = "drillDown";
      var treeName = this.RouteParam.tab;
      this.entitymgmtService.getAngularTreeEvent([treeName], TreeOperations.GetTreeInstance).
        first().
        subscribe((response: TreeDataEventResponseModel) => {
          let treeNames: Array<string> = response.treeName;
          let tree = response.treePayload;
          if (tree && treeNames.indexOf(treeName) > -1) {
            var model: TreeModel = tree.treeModel;
            if (!this.commonService.previousLocationData) {
              let data = this.commonService.getAllNodeByEntityId(tree,this.RouteParam.EntityID)
              if (data)
                this.commonService.previousLocationData = data[0];
            }
            var tempdata = this.commonService.previousLocationData;
            model.focusNextNode();
            //this logic is added to get next location
            //this logic is added to get next location
            if (tempdata && tempdata.children && tempdata.children.length != 0)
              this.commonService.previousLocationData = tempdata.children[0];
            if (this.commonService.previousLocationData)
              this.nodeId.push(this.commonService.previousLocationData.EntityId);
            if (this.commonService.previousLocationData && this.commonService.previousLocationData.children && this.commonService.previousLocationData.children.length != 0) {
              for (var a in this.nodeId) {
                if (this.nodeId[a] == this.commonService.previousLocationData.EntityId) {
                  model.isNodeFocused;
                }
              }
            }
            /**  next locatiion logic end  */

            // if (!this.commonService.previousLocationData) {
            //   let data = this.getFirstChildLocation(model.getFirstRoot());
            //   if (data)
            //     this.commonService.previousLocationData = data.data;
            // }
            let RouteParam = JSON.parse(JSON.stringify(this.RouteParam));
            RouteParam.EntityID = this.commonService.previousLocationData.EntityId;
            let EntityType = this.commonService.previousLocationData.EntityType;
            if (RouteParam.EMTab == EntityTypes.PERSON && (EntityType == EntityTypes.OFFICE || EntityType == EntityTypes.COMPANY))
              RouteParam.EMTab = TreeTypeNames.CompaniesOrPeople;
            else if (RouteParam.EMTab == TreeTypeNames.CompaniesOrPeople && EntityType == EntityTypes.PERSON)
              RouteParam.EMTab = EntityTypes.PERSON;
            if (treeName) {
              this.entitymgmtService.sendDataToAngularTree([treeName], 'ActiveSelectedNode', RouteParam.EntityID);
            }

            RouteParam.EMTab = (RouteParam.EMTab === 'People & Companies') ? 'Person' : RouteParam.EMTab;
            this.navigateBasedOnQueryParams(RouteParam);
          }
        });
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in getNextLocation() of EntityManagement.formtemplate.component in EntityManagementFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  getFirstChildLocation(dist) {
    try {
      var temp = dist.getFirstChild();
      if (temp) {
        this.getFirstChildLocation(temp);
      }
      if (temp.data.typeOfField == "Location") {
        return temp;
      }
    }
    catch (e) {
      console.warn("first location doesnt exist");
    }
  }
  getEntityTypeWithoutSpace(value) {
    return value.replace(/^\s+|\s+$/g, "");
  }
  getSortingListOfChildren(listOfChildren) {
    try {
       return listOfChildren;
    }
    catch (e) {
      console.error('Exception in getSortingListOfChildren() of EntityManagement.formtemplate.component in EntityManagementFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }



  createGroup() {
    var group = this.formBuilder.group([]);
    try {
      let controlarray = this.formBuilder.array([], { updateOn: "blur" });
      group.addControl('listOfChildren', controlarray);
      const control = group.controls['listOfChildren'] as FormArray;

      // if(this.isScrollBasedRead){
      //   let noOfChildrenToShowInitial = this.getVisibleNodesCount("#EM-Form-Gen",this.listOfChildren.length);
      //   this.listOfChildrenScroll=this.listOfChildren;
      //   let itemToShowArray = this.listOfChildren.slice(0,noOfChildrenToShowInitial);
      //   itemToShowArray.forEach((obj: any) => {
      //     control.push(this.formBuilder.group(obj));
      //   });
      // }
      // else{
      this.listOfChildren.forEach((obj: any) => {
        control.push(this.formBuilder.group(obj));
      });
      // }
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in createGroup() of EntityManagement.formtemplate.component in EntityManagementFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
    return group;
  }

  addControlToListOfChildren(itemArray: Array<any>) {
    if (this.myForm && this.myForm.controls && this.myForm.controls.listOfChildren) {
      //this.myForm.controls.listOfChildren.controls
      const controls = this.myForm.controls['listOfChildren'] as FormArray;
      itemArray.forEach(obj => {
        controls.push(this.formBuilder.group(obj));
      });
    }
  }

  entityMagntCommonOutputEmitter(data) {
    try {
      switch (data.OperationType) {
        case 'Delete':
          if (this.myForm && this.myForm.controls && this.myForm.controls.listOfChildren) {
            let controlArray: FormArray = this.myForm.controls['listOfChildren'] as FormArray;
            this.myForm.controls.listOfChildren['controls'].forEach((control, index) => {
              if (control.value.EntityId == data.Payload.EntityId) {
                this.listOfChildren.splice(index, 1);
                controlArray.removeAt(index);
                let arrayIndex = this.commonService.emPumpStatus.findIndex(item => item.ENTITYID == data.Payload.EntityId);
                if (arrayIndex >= 0) {
                  this.commonService.emPumpStatus.splice(arrayIndex, 1);
                }
              }
            });
          }
          break;
        case 'RemoveAccordianBasedOnPermission':
          if (this.myForm && this.myForm.controls && this.myForm.controls.listOfChildren) {
            let controlArray: FormArray = this.myForm.controls['listOfChildren'] as FormArray;
            this.myForm.controls.listOfChildren['controls'].forEach((control, index) => {
              if (control.value.AccordionName == data.AccordianName) {
                this.listOfChildren.splice(index, 1);
                controlArray.removeAt(index);
              }
            });
          }
          break;
        case 'ScrubberConcept':
          this.ScrubberConcept(data);
          break;
        case "formInitializeStatus":
          this.formInitialized = true;
          break;
        default:
          break;
      }
    } catch (error) {

    }
  }
  ClickEvent(data) {
    let RouteParam = JSON.parse(JSON.stringify(this.RouteParam));
    if (data && data.id) {
      try {
        switch (data.id) {
          // case "close":
          //   //this.closeForm();
          //   break;
          case 'expandcollapse':
            RouteParam.expand = !JSON.parse(this.route.queryParams['_value'].expand);
            this.listOfChildren.forEach(element => {
              element.expand = RouteParam.expand;
            });
            this.navigateBasedOnQueryParams(RouteParam);
            this.headerExpandAll(data,RouteParam);
            break;
          case "save":
          case "close":
            if(data.id=="close" && this.treeIndexTab != FABRICS.PEOPLEANDCOMPANIES)
            {
              this.closeForm();
            }
            this.commonService.emFormSubmitEvent.next({id:data.id,tree:this.treeIndexTab});
            break;
          case TreeTypeNames.SECURITY:
          case TreeTypeNames.PeopleAndCompanies:
          case TreeTypeNames.ENTITYMANAGEMENT:
          case TreeTypeNames.BusinessIntelligence:
          case EntityTypes.PERSON:
            RouteParam.EMTab = data.id;
            if (data.IsEquipment) {
              if (this.myForm.invalid) {
                let message = Mandatory_Message;
                this.openDeActivateDialog(message, PopupOperation.AlertConfirm);
                this.dialogRef.componentInstance.emitResponse.subscribe(res => {
                  this.showEquipmentDataBasedOnSwitchedCapability(data);
                });
              } else {
                this.showEquipmentDataBasedOnSwitchedCapability(data);
              }
            }
            else
              this.navigateBasedOnQueryParams(RouteParam);
            break;
        }

      } catch (ex) {

      }
    }
  }

  headerExpandAll(data,RouteParam){
    let expandPayload = {"type":"expandCollapseAll","value":RouteParam.expand}
    this.commonService.headerExpandCollapseEvent.next({id:expandPayload.type,tree:this.treeIndexTab,value:expandPayload});
  }

  private closeForm() {
    let params = JSON.parse(JSON.stringify(this.RouteParam));
    delete params.EntityID;
    this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC], { queryParams: params });
    this.entitymgmtService.sendDataToAngularTree([this.RouteParam.tab], TreeOperations.deactivateSelectedNode, "");
  }

  navigateBasedOnQueryParams(RouteParam) {
    this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC, 'Settings', RouteParam.EntityID, RouteParam.tab, RouteParam.EMTab], { queryParams: RouteParam });
  }
  removelistOfChildren() {
    this.listOfChildren = this.listOfChildren.filter(element => this.RouteParam.EntityID == element['EntityId']);
  }

  showEquipmentDataBasedOnSwitchedCapability(eventData) {
    // if (this.myForm && this.myForm.controls && this.myForm.controls.listOfChildren) {
    //   let controlArray: FormArray = this.myForm.controls['listOfChildren'] as FormArray;
    //   this.myForm.controls.listOfChildren['controls'].forEach((control, index) => {
    //     if (control.value.EntityId == eventData.EntityId) {
    //       var equipmentIndex = this.listOfChildren.findIndex(entity => entity.EntityId == eventData.EntityId);
    //       let equipmentData: any = this.commonService.getEntitiesById(eventData.EntityId);

    //       if (equipmentData) {

    //         let SwitchedData = { "EntityName": equipmentData.EntityName, "EntityType": equipmentData.EntityType, "EntityId": equipmentData.EntityId, LocationId: this.RouteParam.EntityID, "Active": (equipmentData.Active && equipmentData.Active != null && equipmentData.Active != "") ? equipmentData.Active : true, "Enable": true, expand:true, tab: eventData.id }
    //         this.listOfChildren.splice(equipmentIndex, 1, SwitchedData);
    //         controlArray.removeAt(index);
    //         controlArray.insert(index, this.formBuilder.group(SwitchedData));
    //       } else {
    //         let switchedSchema;
    //         let capability = [FABRICS.OPFFabric]
    //         if (eventData.title == FABRICS.OPFFabric)
    //           switchedSchema = { "EntityId": eventData.EntityId, "EntityType": eventData.EntityType, "EntityName": "", LocationId: this.RouteParam.EntityID, "Capability": [capability], "IsEntityMgmtDrag": true, "Enable": true, tab: eventData.id };
    //         else
    //           switchedSchema = { "EntityId": eventData.EntityId, "EntityType": eventData.EntityType, "EntityName": "", LocationId: this.RouteParam.EntityID, "IsEntityMgmtDrag": true, "Enable": true, tab: eventData.id }
    //         controlArray.removeAt(index);
    //         controlArray.insert(index, this.formBuilder.group(switchedSchema));

    //       }
    //     }
    //   })
    // }
  }

  addlistOfChildren(EntityId, EMTab, EntityType) {
    let fabricName = this.commonService.getCapabilityFormNameBasedOnTab(EMTab);

    let entityData: any = this.commonService.getEntitiesById(EntityId);
    switch (this.treeIndexTab) {
      case TreeTypeNames.PeopleAndCompanies: {
        switch (entityData.EntityType) {
          case EntityTypes.COMPANY:
            this.listOfChildren.push({ "EntityName": "Offices", EntityType: "Offices", Company: entityData.EntityId, EntityId: entityData.EntityId + '_Offices', TypeOfAccordian: "Group", tab: TreeTypeNames.PeopleAndCompanies, "Enable": true, "expand": true });
            break;
          case EntityTypes.PERSON:
            if ([ TreeTypeNames.BusinessIntelligence].indexOf(this.tab) >= 0) {
              this.userId = entityData.EntityId;
              this.listOfChildren.push({ "EntityName": "Security & Permissions", EntityType: "Security", EntityId: "Security&Permissions", ParentId: EntityId, ParentType: EntityType, Type: "SecurityGroup", tab: this.tab, "Enable": true, "expand": false });
            }
            break;
          default:
            break;
        }
      }
      case TreeTypeNames.BusinessIntelligence: {
        switch (entityData.EntityType) {
          case EntityTypes.Category:
          case EntityTypes.DashBoard:
            this.userId = this.commonService.currentUserId;
            this.listOfChildren.push({ "EntityName": "Security & Permissions", EntityType: "Security", EntityId: "Security&Permissions", ParentId: EntityId, ParentType: EntityType, Type: "SecurityGroup", tab: TreeTypeNames.BusinessIntelligence, "Enable": true });
            break;
        }

      }
        break;
    }
    this.myForm = this.createGroup();

  }

  deactivateNode() {
    if (this.mutualFabricTab.includes(this.RouteParam.tab)) {
      this.mutualFabricTab.forEach(element => {
        this.entitymgmtService.sendDataToAngularTree([element], TreeOperations.deactivateSelectedNode, "");
      });
    }
    else
      this.entitymgmtService.sendDataToAngularTree([this.RouteParam.tab], TreeOperations.deactivateSelectedNode, "");
  }

  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables);
  }

  ngOnDestroy() {
    this.deactivateNode();
    this.commonService.entityData = undefined;
    this.entitymgmtService.isSchemaRead = false;
    this.commonService.emPumpStatus = [];
    this.entitymgmtService.EMLocationcontextmenueDeleteOperation = false;
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();

  }
  makeRoutingForCanDeactive() {
    this.route.queryParams.subscribe(params => {
      let param = JSON.parse(JSON.stringify(params));
      delete param['EntityID'];
      this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC], { queryParams: param });
    });
  }
  onDrop(event) {
    let obj: any = {};
    const control = this.myForm.controls['listOfChildren'] as FormArray;
    var locationType = this.commonService.getEntitiesById(this.RouteParam.EntityID);
    this.listOfChildren.push(obj);
    this.listOfChildren = this.getSortingListOfChildren(this.listOfChildren);
    let index = this.listOfChildren.findIndex(d => d.EntityId == obj.EntityId);
    control.insert(index, this.formBuilder.group(obj));
    this.entitymgmtService.isSchemaRead = true;
  }
  canDeactivate(): Observable<boolean> {
    var response;
    if (this.commonService.switchButton != "expandcollapse" && (!this.commonService.logoutbtn && this.commonService.popupformclose != true || this.myForm.invalid)) {
      let message = null;
      if (this.myForm.invalid) {
        message = Mandatory_Message;
        this.openDeActivateDialog(message, PopupOperation.AlertConfirm);
        response = this.dialogRef.componentInstance.emitResponse.map(res => {
          return true;
        });
      }
    }
    return response;
  }
  openDeActivateDialog(messageToshow, Operation?): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: Operation != undefined ? true : false,
        content: [messageToshow],
        subContent: [],
        operation: Operation != undefined ? Operation : PopupOperation.Attention
      };
      let matConfig = new MatDialogConfig();
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      matConfig.id = messageToshow;
      if (!this.dialog.getDialogById(messageToshow)) {
        this.dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);

        this.dialogRef.componentInstance.emitCancelResponse.subscribe((res) => {
          let EntityId = this.route.queryParams['_value'].EntityID;
          if (EntityId)
            this.entitymgmtService.sendDataToAngularTree([this.treeIndexTab], TreeOperations.ActiveSelectedNode, EntityId);
        });
      }
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in  openDeActivateDialog() of EntityManagementFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  deleteOrificeEntityBasedOnGOrFactor(message) {
    if (this.myForm && this.myForm.controls && this.myForm.controls.listOfChildren) {
      let controlArray: FormArray = this.myForm.controls['listOfChildren'] as FormArray;
      this.myForm.controls.listOfChildren['controls'].forEach((control, index) => {
        if (control.value.EntityId == message.EntityId && message.EntityType == EntityTypes.ORIFICE) {

          var a = this.listOfChildren.findIndex(entity => entity.EntityId == message.EntityId);
          this.listOfChildren.splice(a, 1);
          controlArray.removeAt(index);
        }
      });

    }
  }
}
