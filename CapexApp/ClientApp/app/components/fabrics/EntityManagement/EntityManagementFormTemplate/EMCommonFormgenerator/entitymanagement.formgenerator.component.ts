import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FABRICS, TreeTypeNames, Datatypes, MessageKind } from 'ClientApp/app/components/globals/Model/CommonModel';
import { EntityTypes } from 'ClientApp/app/components/globals/Model/EntityTypes';
import { FabricRouter } from 'ClientApp/app/components/globals/Model/FabricRouter';
import { Subject } from 'rxjs';
import { takeUntil } from "rxjs/operators";
import { AppConsts, BiEntityTypes, peopleEntityTypes, ConstructionEntityTypes } from '../../../../common/Constants/AppConsts';
import { CommonService } from '../../../../globals/CommonService';
import { EntityTypesReverseKVPair, IconNameJson } from '../../../../common/Constants/JsonSchemas';
import { EntityManagementService } from '../../services/EntityManagementService';
import { Routing } from 'ClientApp/app/components/globals/Model/Message';
@Component({
  selector: 'entitymanagement-Formgenerator',
  templateUrl: './entitymanagement.formgenerator.component.html',
  styleUrls: ['./entitymanagement.formgenerator.component.scss']
})

export class EntityManagementFormgenerator implements OnInit, OnDestroy {
  takeUntilDestroyObservables = new Subject();

  // formSchema:any;
  JsonData;
  RouteParam: any = {};
  expandCollapse = false
  @Input() listOfChildren = [];
  @Input() tab = null
  @Input() isNewEntity = false;
  @Input() jsonAdminData
  @Input() Schema
  @Input() LocationData
  @Input()
  FormData: any = {};
  @Input()
  IsNewEntity: boolean;
  @Output()
  IsFormValid: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output()
  FormDataOutPutChange: EventEmitter<any> = new EventEmitter<any>();
  @Input()
  FormDataOutPut;
  @Input() IsEntityMgmtDrag
  @Input() entityMgmtCommonInputVariable: any = {};
  @Input() userId;
  @Input() EntityType;
  iconName = null
  formName = null
  @Input() EntitySchema: any;
  @Output() Click = new EventEmitter();
  @Output() entityMagntCommonOutputEmitter = new EventEmitter();
  @Input() myForm: any;
  @Input() schemaGenInfo: any;
  isFormInitialized: boolean = true;
  issubFormInitize: boolean = false
  expandCollapseAll = false;
  matIcon = ''
  EnabledCapabilities = []
  ExpandSingle_Icon_Idle = "V3 ToggleDown";
  CollapsSingle_Icon_Idle = "ExpandAccordion";
  listItemHovered = false;
  contentReadingEnable = false;
  constructor(private entitymgmtService: EntityManagementService, public commonService: CommonService, private router: Router, private route: ActivatedRoute, private formBuilder: FormBuilder) {
    // this.commonService.WelltypeMeasuredTested$.pipe(this.compUntilDestroyed()).subscribe(res => {
    //   if((AppConsts.Locations.includes(EntityTypesReverseKVPair[this.myForm.getRawValue().EntityType]) || (this.myForm.controls &&this.myForm.controls['EntityType'] &&AppConsts.Locations.includes(this.myForm.controls['EntityType'].value))) && res == 'Effluent'){
    //     this.iconName = IconNameJson['EffluientGasWell'];
    //   }
    //   else{
    //     this.iconName=IconNameJson[EntityTypesReverseKVPair[this.myForm.getRawValue().EntityType]]?IconNameJson[EntityTypesReverseKVPair[this.myForm.getRawValue().EntityType]]:IconNameJson[this.myForm.getRawValue().EntityType]?IconNameJson[this.myForm.getRawValue().EntityType]:this.myForm.getRawValue().EntityType;
    //   }
    // });
  }


  ngOnInit() {
    this.issubFormInitize = true;
    // this.formSchema=formPayload;
   // let formdata = this.myForm.value
    this.formName = this.checkInitialization()
    if (!this.tab)
      this.tab = this.route.queryParams['_value']['tab']
    //formdata['tab'] = this.tab
   // this.addlistofcapability(formdata);
    if (AppConsts.mutualFabric.includes(this.commonService.getFabricNameByTab(this.tab)) && this.LocationData) {
      this.entityMgmtCommonInputVariable.ParentData = this.LocationData
    }
    this.initializeIcon();
    this.route.queryParams.pipe(this.compUntilDestroyed()).subscribe((params: any) => {
      try {

        if (this.isNewEntity || (this.IsEntityMgmtDrag && this.entitymgmtService.isSchemaRead)) {
          this.entitymgmtService.isSchemaRead = false;
          this.expandCollapse = true;
          this.matIcon = this.expandCollapse ? this.ExpandSingle_Icon_Idle : this.CollapsSingle_Icon_Idle;
        }
        // else if ((formdata.expand && Object.keys(this.RouteParam).length <= 0 || formdata.expand && this.RouteParam.expand == params.expand) && params.tab == TreeTypeNames.PeopleAndCompanies) {
        //   this.expandCollapse = true;
        //   this.matIcon = this.expandCollapse ? this.ExpandSingle_Icon_Idle : this.CollapsSingle_Icon_Idle;
        // }
        else if (Object.keys(this.RouteParam).length <= 0 || JSON.parse(this.RouteParam.expand) != JSON.parse(params.expand) || this.RouteParam.EMTab != params.EMTab) {
          this.expandCollapse = JSON.parse(params.expand)
          this.matIcon = this.expandCollapse ? this.ExpandSingle_Icon_Idle : this.CollapsSingle_Icon_Idle;
          if (params.tab == TreeTypeNames.BusinessIntelligence && !this.RouteParam.expand) {
            this.expandCollapse = false;
            this.matIcon = this.expandCollapse ? this.ExpandSingle_Icon_Idle : this.CollapsSingle_Icon_Idle;
          }
          if (!this.contentReadingEnable) {
            this.contentReadingEnable = JSON.parse(params.expand);
          }
        }
        if (params.tab == TreeTypeNames.PeopleAndCompanies || this.listOfChildren.length == 1) {
          this.RouteParam = JSON.parse(JSON.stringify(params));
          this.RouteParam.expand = this.expandCollapse;
          this.router.navigate([], { relativeTo: this.route, queryParams: this.RouteParam });
        }
        this.RouteParam = JSON.parse(JSON.stringify(params));
      } catch (e) {
        this.commonService.appLogException(new Error('Exception inroute.queryParams subscriber of constructor in EntityManagement.formgenerator.component at time ' + new Date().toString() + '. Exception is : ' + e));
      }
    })
    //if (this.myForm.value.EntityId)
    //  this.GetDataAndSchemaForPeopleAndCompany();
    //this.myForm = this.createGroup(this.myForm,formdata);
  }

  initializeIcon() {
    let dataValue = this.myForm.value;
    let EntityType = dataValue["EntityType"].Value;
    switch (EntityType) {
      // case 'Pig':
      //   if (this.jsonAdminData) {
      //     if (this.jsonAdminData.Purpose)
      //       this.iconName = "Sender"
      //     else
      //       this.iconName = "Receiver"
      //   } else if (this.myForm && this.myForm.value) {
      //     if (this.myForm.value.Purpose)
      //       this.iconName = "Sender"
      //     else
      //       this.iconName = "Receiver"
      //   }
      //   break;
      case EntityTypes.Category:
        this.iconName = "CategoryTreeIcon"
        break;
      case EntityTypes.DashBoard:
        this.iconName = "DashBoardTreeIcon"
        break;

        case EntityTypes.TaggedItems:
        this.iconName = "TaggedItemsTreeIcon"
        break;

      default:
        this.iconName = EntityType
        break;
    }
    this.iconName = IconNameJson[this.iconName] ? IconNameJson[this.iconName] : this.iconName
  }

  expandPannel(expand) {
    try {
      this.contentReadingEnable = true;
      this.expandCollapse = expand
      let expandCollapseAll = true;
      this.matIcon = expand ? this.ExpandSingle_Icon_Idle : this.CollapsSingle_Icon_Idle;
      this.listOfChildren.forEach(element => {
        if (element.EntityId == this.schemaGenInfo.EntityId)
          element.expand = expand
      })
      if (this.listOfChildren.filter(d => d.expand).length == this.listOfChildren.length)
        this.RouteParam.expand = true
      else if (this.listOfChildren.filter(d => !d.expand).length == this.listOfChildren.length)
        this.RouteParam.expand = false
      this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC, 'Settings', this.RouteParam.EntityID, this.RouteParam.tab, this.RouteParam.EMTab], { queryParams: this.RouteParam });
    } catch (error) {
      console.error()
    }
  }

  checkInitialization() {
    let entityType = this.myForm.getRawValue().EntityType;
    if (entityType)
      entityType = this.myForm.getRawValue().EntityType;
    if (peopleEntityTypes.includes(entityType)) {
      return "People";
    }
    // else if (AppConsts.PiggingEntities.includes(EntityType)) {
    //   return "Pigging";
    // }
    else if (entityType == "Security") {
      return "Security";
    }
    else if (BiEntityTypes.includes(entityType)) {
      return "BI";
    }
    else if (ConstructionEntityTypes.includes(entityType)) {
      return EntityTypes.Construction;
    }
    // else {
    //   return "fdc";
    // }
  }

  entityMagntCommonOutput(data) {
    switch (data.OperationType) {
      // case "changePurpose":
      //   this.iconName = data.Payload ? "Sender" : "Receiver";
      //   this.iconName = IconNameJson[this.iconName] ? IconNameJson[this.iconName] : this.iconName
      //   break;
      default:
        this.entityMagntCommonOutputEmitter.emit(data);
        break;
    }
  }

  ClickEvent(event) {
    let dataValue = this.myForm.value;
    let EntityType = dataValue["EntityType"].toString();
    switch (event.type) {
      case 'click':
        let EntityType = EntityTypesReverseKVPair[this.myForm.value["EntityType"].toString()] ? EntityTypesReverseKVPair[this.myForm.value["EntityType"].toString()] : this.myForm.getRawValue().EntityType
        let data = {};
        switch (event.payload.title) {
          default:
            // this.formSchema = {}
            break;
        }
        // if(AppConsts.Locations.includes(EntityType)&&data['treeName']){
        //   this.commonService.EntityManagementCommonFormObservable.next(data);
        // }
        // if(event.id!='Well_Entity')
        //this.tab=event.payload.id
        event.payload.routerLinkActive = true
        // if (event && event.payload && this.myForm && this.myForm.value && EntityType == EntityTypes.TANK) {
        //   event.payload["IsEquipment"] = true;
        //   event.payload["EntityType"] = EntityType;
        // }
        event.payload["EntityId"] = dataValue["EntityId"].toString();//this.schemaGenInfo.EntityID;
        if (this.commonService.treeIndexLabel == TreeTypeNames.PeopleAndCompanies)
          this.Click.emit(event.payload);
        // let index=this.myForm.controls.listOfChildren['controls'].findIndex(data => event.title == data['value']['tab']);
        // if(index==-1){
        //   let formdata=this.myForm.getRawValue()
        //   delete formdata['listOfChildren']
        //   formdata['tab']=event.title
        //   const control = this.myForm.controls['listOfChildren'] as FormArray;
        //   control.push(this.formBuilder.group(formdata));
        // }
        break;
      // case 'EnableDisable':
      //   if(event.payload.)
      //   this.EnabledCapabilities
      //   break;
      default:
        break;
    }

  }
  createGroup(group, formdata) {
    try {
      // let controlarray = this.formBuilder.array([], { updateOn: "blur" });
      // group.addControl('listOfChildren', controlarray);
      // const control = group.controls['listOfChildren'] as FormArray;
      // control.push(this.formBuilder.group(formdata));
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in createGroup() in EntityManagement.formgenerator at time ' + new Date().toString() + '. Exception is : ' + e));
    }
    return group;
  }

  hovevOnMouseEnter() {
    this.listItemHovered = true;
    var slides = document.getElementsByClassName('toggle-expand-collapse');
    if (this.commonService.isCreateNewEntity || this.isNewEntity) {
      this.listItemHovered = false;
      for (let i = 0; i < slides.length; i++) {
        const slide = slides[i] as HTMLElement;
        slide.style.cursor = 'default';
      }
    }
  }

  hovevOffOnOmouseLeave() {
    this.listItemHovered = false;
  }

  FormEnableDisable(event: any) {
    if (event && event.EnableDisableSecurityAndPermissionForm != undefined) {
      this.entitymgmtService.EntityMgmtCommonObservable.next({ FormType: "Security&Permissions", value: event.EnableDisableSecurityAndPermissionForm, userId: event.userId });
    }
  }

  addlistofcapability(formdata) {
    try {
     
    }
    catch (ex) {

    }
  }
  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables);
  }
  ngOnDestroy() {
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();

  }
  GetDataAndSchemaForPeopleAndCompany() {
    let payload: any = {}
    payload.EntityId = this.myForm.value.EntityId;
    payload.EntityType = this.myForm.value.EntityType;
    payload.Info = 'Details';
    payload.DataType = EntityTypes.EntityInfo;
    if (this.EntityType == "Person") {
      payload.FormCapability = this.tab;
      let capabilityid = this.commonService.getCapabilityId(this.tab);
      payload.CapabilityId = capabilityid ? capabilityid : "";
    }
    else {
      payload.FormCapability = this.EntityType !== 'Person' && this.tab === 'Person' ? 'People' : this.tab;

      if (payload.FormCapability === '' && this.tab === '' && this.myForm && this.myForm.value && this.myForm.value.tab === 'Companies/People') {
        payload.FormCapability = 'People';
      }

      if (payload.FormCapability === 'Security' && this.commonService.isNextOrPreviousClicked) {
        this.commonService.isNextOrPreviousClicked = false;
        payload.FormCapability = 'People'
      } else {
        this.commonService.isNextOrPreviousClicked = false;
      }
    }

    payload.FormType = "Configuration"
    this.expandCollapse = true;
    payload.Fabric = this.commonService.lastOpenedFabric;
    this.commonService.sendMessageToServer(JSON.stringify(payload), Datatypes.PEOPLE, payload.EntityId, this.EntityType, MessageKind.READ, Routing.OriginSession, payload.Info, FABRICS.PEOPLEANDCOMPANIES);

  }
}


