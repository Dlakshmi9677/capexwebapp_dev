import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EMGroupTypeComponent } from './em-group-type.component';

describe('EMGroupTypeComponent', () => {
  let component: EMGroupTypeComponent;
  let fixture: ComponentFixture<EMGroupTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EMGroupTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EMGroupTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
