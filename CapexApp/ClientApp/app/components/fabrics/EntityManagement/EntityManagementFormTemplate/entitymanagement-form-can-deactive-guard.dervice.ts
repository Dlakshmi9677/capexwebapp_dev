import { Injectable } from "@angular/core";
import { CanDeactivate } from "@angular/router";
import { Observable } from "rxjs";
import { EntityManagementFormTemplate } from "./entitymanagement.formtemplate.component";


@Injectable()
export class EntityManagementFormsDeactivateGaurdService implements CanDeactivate<EntityManagementFormTemplate>{

  canDeactivate(component: EntityManagementFormTemplate): Observable<boolean> {
    if (component.myForm && component.myForm.invalid && component.entityMgmtCommonInputVariable && component.entityMgmtCommonInputVariable["updated"] && component.entitymgmtService.EMLocationcontextmenueDeleteOperation==false) {
      return component.canDeactivate()
    }
    else{
      component.entityMgmtCommonInputVariable["updated"] = true;
      return Observable.of(true);
    }
  }
}
