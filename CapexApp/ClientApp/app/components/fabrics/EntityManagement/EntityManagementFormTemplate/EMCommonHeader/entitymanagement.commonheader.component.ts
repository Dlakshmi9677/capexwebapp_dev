import { Component, ComponentFactoryResolver, ChangeDetectorRef, OnInit, OnDestroy,Input,EventEmitter, Output, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { CommonService } from '../../../../globals/CommonService';
import {EntityManagementService} from '../../services/EntityManagementService';
import {IconNameJson,EntityTypesReverseKVPair} from '../../../../common/Constants/JsonSchemas'
import { TreeTypeNames, FABRICS, TreeOperations, MessageKind, ComponentInstanceEventMessageModel, TypeOfEntity } from 'ClientApp/app/components/globals/Model/CommonModel';
import { AppConsts } from 'ClientApp/app/components/common/Constants/AppConsts';
import { EntityTypes } from 'ClientApp/app/components/globals/Model/EntityTypes';
import { PiggingIcons } from 'ClientApp/app/components/globals/icons';
import { MatMenuTrigger } from '@angular/material/menu';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { DISABLE_CAPABILITY, DISABLE_EQUIPMENT, EM_DISABLE_LOCATION, EM_DISABLE_EQUIPMENT_ALERT } from '../../../../common/Constants/AttentionPopupMessage'
import { Routing } from 'ClientApp/app/components/globals/Model/Message';
import { PopupOperation } from 'ClientApp/app/components/globals/Model/AlertConfig';
import {MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PopUpAlertComponent } from 'ClientApp/app/components/common/PopUpAlert/popup-alert.component';
import { takeUntil, filter } from 'rxjs/operators';
import { Observable, Subject, of } from "rxjs";
import { FormGroup } from '@angular/forms';


@Component({
    selector: 'entity-management-commonheader',
    templateUrl: './entitymanagement.commonheader.component.html',
    styleUrls: ['./entitymanagement.commonheader.component.scss']
  })

  export class EntityManagementCommonHeader implements OnInit, OnDestroy{
    takeUntilDestroyObservables=new Subject();

    ShowheaderTabs:boolean=false;
    @Output() Click = new EventEmitter();
    @Input() tab=''
    @Input() isNewEntity
    @Input() EntityId
    @Input() EntityType
    @Input() EntityName=""
    @Input() iconName
    @Input() EnabledCapabilities=[]
    @Input() LocationId
    @Input() myForm: FormGroup;
    icon
    option
    timecancel=[]
    timeoutFlag
    haullocatioupdate = false;
    contextMenuPosition = { x: '0px', y: '0px' };
    @ViewChild(MatMenuTrigger, {static: false})
    contextMenu: MatMenuTrigger;
    commonHeader = [
     {
        'source':  {true:'V3 PeepsAndComps',false:'V3 PeepsAndComps'} ,
        'title': FABRICS.PEOPLEANDCOMPANIES,
        'id': TreeTypeNames.PeopleAndCompanies,
        'routerLinkActive': false,
        'alise': 'People & Companies',
        'class': 'icon21-21',
        'type': 'tree',
        'add': false,
        'search': true,
        'show': true,
        'addtooltip': 'ADD',
        'isEnable':false,
        'CapabilityId':this.commonService.getCapabilityId(FABRICS.PEOPLEANDCOMPANIES)
      },
      {
        'source': { true: 'V3 BusinessIntelligence', false: 'V3 BusinessIntelligence' },
        'title': FABRICS.BUSINESSINTELLIGENCE,
        'routerLinkActive': false,
        'id': TreeTypeNames.BusinessIntelligence,
        'class': 'icon21-21',
        'float': 'left',
        'type': 'icon',
        'show': true,
        'viewname': null,
        'isEnable': false,
        'CapabilityId': this.commonService.getCapabilityId(FABRICS.BUSINESSINTELLIGENCE)
      },
      {
        'source': { true: 'V3 EntityManagement', false: 'V3 EntityManagement' },
        'title': FABRICS.ENTITYMANAGEMENT,
        'routerLinkActive': false,
        'id': 'Entity Management',
        'class': 'icon21-21',
        'float': 'left',
        'type': 'icon',
        'show': true,
        'viewname': null,
        'isEnable': false,
        'CapabilityId': this.commonService.getCapabilityId(FABRICS.ENTITYMANAGEMENT)
      },
      {
        'source':  {true:'V3 Security',false:'V3 Security_disabled'},
        'title': FABRICS.SECURITY,
        'routerLinkActive': false,
        'id': TreeTypeNames.SECURITY,
        'class': 'icon21-21',
        'float': 'left',
        'type': 'icon',
        'show': true,
        'viewname':null,
        'isEnable':false,
        'CapabilityId':this.commonService.getCapabilityId(FABRICS.SECURITY)
      }
    ];


  defaultTabsJson=  {
      "Company":[TreeTypeNames.PeopleAndCompanies,TreeTypeNames.SECURITY],
      "Office": [TreeTypeNames.PeopleAndCompanies],
      "Person": [TreeTypeNames.SECURITY, TreeTypeNames.BusinessIntelligence, TreeTypeNames.ENTITYMANAGEMENT, TreeTypeNames.PeopleAndCompanies],
    }
    ngUnSubscribe = new Subject();
    disableEquipment:boolean=false;
    public unsubscribe:Subject<any> = new Subject();
    currentDisabledEnabledCapabalityId;
    DownTimeLocations: any =[];

    constructor(private commonService:CommonService,private entitymanagementService:EntityManagementService,private route: ActivatedRoute,public dialog: MatDialog){


        if(this.entitymanagementService.getEMFormComponentInstance$){
          this.entitymanagementService.getEMFormComponentInstance$
          .pipe(takeUntil(this.ngUnSubscribe)).pipe(filter((data: ComponentInstanceEventMessageModel) =>
            (!this.isNewEntity &&data && data.OperationType && data.OperationType =="locationDisableUpdate" && data.EntityId!=this.EntityId && data.EntityId==this.LocationId ))).subscribe(data => {
                let EntityType=EntityTypesReverseKVPair[this.EntityType]?EntityTypesReverseKVPair[this.EntityType]:this.EntityType;
                if(data && data.PayLoad && data.PayLoad["Data"]){
                  let headerData= data.PayLoad["Data"];
                  headerData.isEnable=true;
                  if(this.commonHeader && this.commonHeader.length!=0){
                    let index=this.commonHeader.findIndex(obj=>obj.title== headerData.title);
                    if(index!=-1){
                      this.commonHeader[index].isEnable=true;
                      this.disableEquipment=true;
                      this.ContextMenuClick(this.commonHeader[index]);
                    }
                  }
                }

            });
        }


    }


  ngOnInit() {
    setTimeout(() => {
        this.ShowheaderTabs = true;
        if (this.EntityName == "FuelFlareVent")
          this.EntityName = "Fuel, Flare & Vent";
        let typeOfEntity = this.returnType(EntityTypesReverseKVPair[this.EntityType]?EntityTypesReverseKVPair[this.EntityType]:this.EntityType);
        //this.commonHeader = JSON.parse(JSON.stringify(this.commonHeader));
        let capabilitiesToShow = this.defaultTabsJson[typeOfEntity];
        let locationData = this.commonService.getEntitiesById(this.LocationId);
        if (capabilitiesToShow && capabilitiesToShow.length == 1 )
          var showcapabilityicons = this.showCapabilityIcons(capabilitiesToShow);
      if (showcapabilityicons && showcapabilityicons.length != 0) {
        let showcapability = [];
        this.commonHeader = this.commonHeader.filter(function (item) {
          showcapabilityicons.forEach(cap => {
            if (cap.id == item.id) {
              item.show = cap.show;
              showcapability.push(item);
            }
          });
        })
        this.commonHeader = showcapability;
      }
      else
        if (this.EntityType == EntityTypes.TANK && capabilitiesToShow && capabilitiesToShow.length == 2 && locationData && locationData.Capability && locationData.Capability.length == 2) {
          let capability = this.checkCapabilitiesToShow(capabilitiesToShow, locationData);
          if (capability.length == 1) {
            var showCapabilityIconsForTank = this.showCapabilityIconsForTank(capability);
          }
          if (showCapabilityIconsForTank && showCapabilityIconsForTank.length != 0) {
            let showcapabilitys = [];
            this.commonHeader = this.commonHeader.filter(function (item) {
              showCapabilityIconsForTank.forEach(cap => {
                if (cap.id == item.id) {
                  item.isEnable = cap.isEnable;
                  showcapabilitys.push(item);
                }
              });
            });
            this.commonHeader = showcapabilitys;
          }
        }
        else
        if (capabilitiesToShow&&capabilitiesToShow.length != 0){
            this.commonHeader = this.commonHeader.filter(function (item) {
              return capabilitiesToShow.includes(item.id);
            });
          }
        else {
          this.commonHeader=[]
        }
        switch (this.route.queryParams['_value'].tab) {
          case TreeTypeNames.PeopleAndCompanies: {
            let defaultenablelist = [TreeTypeNames.PeopleAndCompanies, TreeTypeNames.BusinessIntelligence, TreeTypeNames.ENTITYMANAGEMENT, TreeTypeNames.SECURITY]
            this.commonHeader.forEach(element => {
              if (defaultenablelist.includes(element.id)) {
                element.isEnable = true;
                if (this.EnabledCapabilities.indexOf(element.title) == -1)
                this.EnabledCapabilities.push(element.title)
              }
            })
          }
            break;
      }
      this.commonHeader = this.commonHeader.filter(item => this.commonService.getAdminFabriclist().indexOf(item.id) >= 0)
      }, 0);
  }
  getIcon(){
    return ;
  }
  showCapabilityIcons(capabilitiesToShow: any) {
    let data = []
    data.forEach(res => {
      if (capabilitiesToShow.includes(res.id)) {
        res.show = true;
      }
    });
    return data;
  }
  showCapabilityIconsForTank(capability) {
    let data = []
    data.forEach(res => {
      if (capability.includes(res.id)) {
        res.isEnable = true;
      }
    });
    return data;
  }
  checkCapabilitiesToShow(capabilitiesToShow, locationData) {
    let capability = [];
    capabilitiesToShow.forEach(cap => {


    });
    return capability;
  }
  onContextMenu(event: any, list) {
    if(this.commonHeader.length>1&&!this.myForm.invalid){
      // switch (this.route.queryParams['_value'].tab) {
      //  default:
      //  break;
      // }
    }
  }
  EnableCapability(event:any,list){
    if(list.isEnable){
      this.option='Disable Capability'
   }else{
      this.option='Enable Capability'
  }
     this.contextMenuPosition.x = event.x + 'px';
     this.contextMenuPosition.y = event.y + 'px';
     this.contextMenu.menuData = list;
     this.contextMenu.openMenu();
  }
    //ContextMenuClick(head){
    //  let disablelocation:boolean=false;
    //  let EntityType=EntityTypesReverseKVPair[this.EntityType]?EntityTypesReverseKVPair[this.EntityType]:this.EntityType;

    //  let data=head?head:this.contextMenu.menuData
    //  this.currentDisabledEnabledCapabalityId = data.CapabilityId;
    //   if(data &&this.commonService.treeIndexLabel == data.id && data.isEnable&&!AppConsts.Equipment.includes(this.EntityType)) {
    //      this.entitymanagementService.openCommonAttentionPopup(new Array(DISABLE_CAPABILITY));
    //    return;
    //  }
    //  else{
    //    if(AppConsts.Equipment.includes(this.EntityType)&&this.EnabledCapabilities.length==1&&data.isEnable){
    //      var currentEntity = this.commonService.getEntitiesById(this.EntityId);
    //      if (!this.EntityType&&currentEntity) {
    //        this.EntityType = currentEntity.EntityType;
    //        if (!this.EntityName) {
    //          this.EntityName = currentEntity.EntityName;
    //        }
    //      }
    //      if(!this.disableEquipment){
    //        this.openDialog();
    //      }else{
    //        this.removeEquipmentRelationShip(head);
    //      }

    //   }
    //   else if(data.isEnable){

    //      if(AppConsts.Locations.includes(EntityType)&& !this.isNewEntity)  {
    //        disablelocation=true;
    //        this.checkforEquipmentsExistUnderLocation(data);

    //      }else{
    //        this.disableCurrentCapability(data);
    //      }

    //    }
    //      else{
    //      data.isEnable = true
    //      if (this.EnabledCapabilities.indexOf(data.title) == -1) {
    //        this.EnabledCapabilities.push(data.title)
    //      }
    //      }
    //    }
    //    let Entity = this.commonService.getEntitiesById(this.EntityId);
    //    if(this.entitymanagementService.getFormName()=="Settings" && !disablelocation){
    //      disablelocation=false;
    //      //let EntityType=EntityTypesReverseKVPair[this.EntityType]?EntityTypesReverseKVPair[this.EntityType]:this.EntityType
    //      if(Entity){
    //        // Entity.Capability=JSON.parse(JSON.stringify(this.EnabledCapabilities))  ///This Commented Code Moved to Method
    //        // this.commonService.UpadateCapabilityInState(Entity)
    //        // this.entitymanagementService.addEntityIntoLeftTree(this.EntityId);
    //        // var payload;
    //        // var jsonString = JSON.stringify({Capability:this.EnabledCapabilities});
    //        // let EntityType=EntityTypesReverseKVPair[this.EntityType]?EntityTypesReverseKVPair[this.EntityType]:this.EntityType
    //        // payload = {
    //        //   "EntityId": this.EntityId, "EntityName": this.EntityName,
    //        //   "TenantName": this.commonService.tenantName, "TenantId": this.commonService.tenantID,
    //        //   "EntityType": EntityType, "Fabric": FABRICS.ENTITYMANAGEMENT, "EntityInfoJson": jsonString,
    //        // };
    //        // this.commonService.sendMessageToServer(JSON.stringify(payload), 'UPDATECAPABILITY', this.EntityId, EntityType, MessageKind.UPDATE,
    //        //   Routing.AllFrontEndButOrigin, null, FABRICS.ENTITYMANAGEMENT.toUpperCase());
    //           this.sendRequestToUpdateCapability();
    //          // if(!this.isNewEntity && AppConsts.Locations.includes(EntityType) && data.isEnable==false &&this.option=="Disable Capability"){
    //          //   this.disableEquipmentBasedOnLocationStatus(data);
    //          // }
    //      }
    //    }
    //    if(!this.isNewEntity && AppConsts.Locations.includes(EntityType) && data.isEnable==false &&this.option=="Disable Capability"){
    //      this.commonService.removeEntityExistInLists(Entity.EntityId, data.title)
    //    }
    //  //this.Click.emit({type:'EnableDisable',payload:this.contextMenu.menuData});
    //}

  ContextMenuClick(head,isenable?) {
    setTimeout(() => {
        let disablelocation: boolean = false;
        let EntityType = EntityTypesReverseKVPair[this.EntityType] ? EntityTypesReverseKVPair[this.EntityType] : this.EntityType;

        let data = head ? head : this.contextMenu.menuData
        if (isenable != undefined) {
          head.isEnable = isenable
        }
        this.currentDisabledEnabledCapabalityId = data.CapabilityId;
        if(data &&this.commonService.treeIndexLabel == data.id && data.isEnable) {
          this.entitymanagementService.openCommonAttentionPopup(new Array(DISABLE_CAPABILITY));
          return;
        }
        else if ( this.EnabledCapabilities.length == 1  && data.isEnable && data.show) {
          var currentEntity = this.commonService.getEntitiesById(this.EntityId);
          if (!this.EntityType&&currentEntity) {
            this.EntityType = currentEntity.EntityType;
            if (!this.EntityName) {
              this.EntityName = currentEntity.EntityName;
            }
          }
          if(!this.disableEquipment){
            this.openDialog();
          }
          else{
            this.removeEquipmentRelationShip(head);
          }
          this.backendprocess(data, disablelocation, EntityType);
        }

    }, 0);
  }

  enableDisableCapabilities(data, disablelocation, EntityType) {
    try {
      if (data.isEnable) {

          this.disableCurrentCapability(data);
        
      }
      else{
        data.isEnable = true
        if (this.EnabledCapabilities.indexOf(data.title) == -1) {
          this.EnabledCapabilities.push(data.title)
        }
        this.EnablingCapabilities(data);
      }
      this.backendprocess(data, disablelocation, EntityType);
    }
    catch (ex) {

    }
  }
EnablingCapabilities(data){
  let Entity = this.commonService.getEntitiesById(this.EntityId);
  let fabric = this.getFabricNamebyTab();
  this.PushDownstreamLocations(Entity,fabric,true,data.title);
  if (this.DownTimeLocations && this.DownTimeLocations.length > 0){
    this.DownTimeLocations.forEach(element => {
      if (element && element.EntityId){
      this.backendprocessForChildren(data, false, element.EntityType,element.EntityId);
      }
    });
    this.DownTimeLocations = [];
  }
}
  backendprocess(data, disablelocation, EntityType) {
    try {
      let Entity = this.commonService.getEntitiesById(this.EntityId);
      if (this.entitymanagementService.getFormName() == "Settings" && !disablelocation) {
        disablelocation = false;
        if (Entity) {
          this.sendRequestToUpdateCapability();
        }
      }
    } catch (ex) {

    }
  }
  backendprocessForChildren(data, disablelocation, EntityType,entityid) {
    try {
      let Entity = this.commonService.getEntitiesById(entityid);
      if (this.entitymanagementService.getFormName() == "Settings" && !disablelocation) {
        disablelocation = false;
        if (Entity) {
          this.sendRequestToUpdateCapability(Entity.EntityId,Entity.EntityType,Entity.EntityName);
        }
      }

    } catch (ex) {

    }
  }

    disableCurrentCapability(data,enabledCaps?){
      try{
        data.isEnable=false
        let EnabledCapabilities = enabledCaps? enabledCaps:this.EnabledCapabilities;
        let index=EnabledCapabilities.findIndex(d=>d==data.title)
        if(index>=0){
          EnabledCapabilities.splice(index,1)
          if(data.id==this.tab){
              if(EnabledCapabilities.length==1){
               let capability = {};
               capability = EnabledCapabilities[0];
               let changetab=this.commonHeader.filter(d=>d.title==capability)[0]
               this.onClick(changetab)
              }else{
            let changetab=this.commonHeader.filter(d=>d.id==this.commonService.treeIndexLabel)[0]
            this.onClick(changetab)
          }
          }

          if(this.entitymanagementService.getFormName()=="Settings")
          this.entitymanagementService.sendDataToAngularTree([data.id], TreeOperations.deleteNodeById, this.EntityId);
        }
      }catch(e){
        this.commonService.appLogException(new Error('Exception in disableCurrentCapability() of EMFormTemplate  at time ' + new Date().toString() + '. Exception is : ' + e));
      }

    }
    onClick(head: any) {
      if(head.isEnable&&!this.isNewEntity)
      setTimeout(() => {
        this.Click.emit({type:'click',payload:head});
      }, 300);
    }
    eventEmitSingleClick(event)
    {
      if(event.id != this.tab)
          this.onClick(event)
      // this.timeoutFlag = false;
      // let delay = 400;
      // this.timecancel.push( setTimeout(() => {
      //   if(!this.timeoutFlag){
      //     this.onClick(event)
      //   }
      // }, delay));
  }

  eventEmitEntityNameClick() {
    if (this.EntityType == EntityTypes.PERSON)
      this.Click.emit({ type: 'click', payload: { id: EntityTypes.PERSON} });
  }

    eventEmitDoubleClick(event) {
      // this.timeoutFlag = true;
      // this.ContextMenuClick(event)
      // this.timecancel.forEach(ele=>{
      //   clearTimeout(ele)
      // })
    }
    compUntilDestroyed():any {
      return takeUntil(this.takeUntilDestroyObservables);
      }
  ngOnDestroy() {
    this.ngUnSubscribe.next();
    this.ngUnSubscribe.complete();
    this.takeUntilDestroyObservables.next();
this.takeUntilDestroyObservables.complete();

  }
  returnType(EntityType) {
    return EntityType;
  }

  openDialog(messageType?) {
    try {
      let config = {
        header: 'Delete ' + this.EntityName,
        isSubmit: true,
        content: [DISABLE_EQUIPMENT],
        operation: PopupOperation.Delete,
        EntityName: this.EntityName
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      let dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
      dialogRef.componentInstance.emitResponse.subscribe((Type: any) => {
        if (Type != null) {

        }
      });
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in openDialog2() of EMFormTemplate  at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  disableEquipmentBasedOnLocationStatus(data){
    try{
      let payload={"EnabledCapabilities":JSON.parse(JSON.stringify(this.EnabledCapabilities)),"Data":JSON.parse(JSON.stringify(data))}
      let model: ComponentInstanceEventMessageModel = {
        fabric: FABRICS.ENTITYMANAGEMENT, EntityType: this.EntityType, EntityId:this.EntityId, EntityName: this.EntityName, FieldName: 'locationDisableUpdate',
        OperationType: "locationDisableUpdate", PayLoad: payload, Event: new EventEmitter()
      }
      this.entitymanagementService.getEMFormComponentInstance$.next(model);
    }catch(e){
      this.commonService.appLogException(new Error('Exception in disableEquipmentBasedOnLocationStatus() of EMFormTemplate  at time ' + new Date().toString() + '. Exception is : ' + e));

    }

  }

  sendRequestToUpdateCapability(entityid?,entityType?,entityname?){
    try{
      this.disableEquipment=false;
      let currentId = entityid?entityid:this.EntityId;
      let currentType = entityType?entityType:this.EntityType;
      let Entity = this.commonService.getEntitiesById(currentId);
      let entityName = entityname?entityname:this.EntityName;
      if(Entity){
        Entity.Capability=JSON.parse(JSON.stringify(this.EnabledCapabilities))
        this.commonService.UpadateCapabilityInState(Entity)
        this.entitymanagementService.addEntityIntoLeftTree(currentId);
        var payload;
        var jsonString = JSON.stringify({Capability:this.EnabledCapabilities});
        let EntityType=EntityTypesReverseKVPair[currentType]?EntityTypesReverseKVPair[currentType]:currentType;
        payload = {
          "EntityId": currentId, "EntityName": entityName,
          "TenantName": this.commonService.tenantName, "TenantId": this.commonService.tenantID,
          "EntityType": EntityType, "Fabric": FABRICS.ENTITYMANAGEMENT, "EntityInfoJson": jsonString,"Type":this.option,"capabilityId" : this.currentDisabledEnabledCapabalityId
        };
        this.commonService.sendMessageToServer(JSON.stringify(payload), 'UPDATECAPABILITY', currentId, EntityType, MessageKind.UPDATE,
          Routing.AllFrontEndButOrigin, null, FABRICS.ENTITYMANAGEMENT.toUpperCase());
      }
    }catch(e){
      this.commonService.appLogException(new Error('Exception in sendRequestToUpdateCapability() of EMFormTemplate  at time ' + new Date().toString() + '. Exception is : ' + e));

    }

  }

  openDialogWhileDisablingLocation(messageToShow,data?) {
    try {
      let config = {
        header: EM_DISABLE_LOCATION,
        isSubmit: true,
        content: [messageToShow],
        subContent: [],
        operation: PopupOperation.AlertConfirm,
      };

      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '600px';
      matConfig.disableClose = true;
      let dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
      dialogRef.componentInstance.emitResponse.subscribe((result: any) => {
        if (result != null) {
          if(this.entitymanagementService.getFormName()=="Settings"){
            let Entity = this.commonService.getEntitiesById(this.EntityId);
            let EntityType=EntityTypesReverseKVPair[this.EntityType]?EntityTypesReverseKVPair[this.EntityType]:this.EntityType
            if(Entity){
                 this.DisablingCapabilities(data);
 
            }
          }
        }
      });
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in openDialogWhileDisablingLocation() of EMFormTemplate  at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  checkforEquipmentsExistUnderLocation(data){
   

  }
  getFabricNamebyTab() {
    let capability;
    return capability;
  }
  DisablingCapabilities(data) {
    this.disableCurrentCapability(data);
    this.sendRequestToUpdateCapability();
    let Entity = this.commonService.getEntitiesById(this.EntityId);
    let fabric = this.getFabricNamebyTab();
    this.PushDownstreamLocations(Entity, fabric);
    if (this.DownTimeLocations && this.DownTimeLocations.length > 0) {
      this.DownTimeLocations.forEach(element => {
        if (element && element.EntityId != this.EntityId) {
          this.disableCurrentCapability(data, element.Capabilities);
          this.sendRequestToUpdateCapability(element.EntityId, element.EntityType, element.EntityName);
        }
      });
      this.DownTimeLocations = [];
    }
  }
  PushDownstreamLocations(node, fabric, isEnabling?, enablingCap?) {
 
  }
  removeEquipmentRelationShip(headerData){
     this.disableEquipment=false;
  }

  enableDisableCapabilityBasedOnHaullocation(data) {
   
  }

  isEquipmentexistinlocation(data) {
    try {
      this.haullocatioupdate = false;//haullocation update time no nned to show popup
      let Entity = this.commonService.getEntitiesById(this.EntityId);
      let EntityType = EntityTypesReverseKVPair[this.EntityType] ? EntityTypesReverseKVPair[this.EntityType] : this.EntityType
      if (Entity) {
        this.disableCurrentCapability(data);
        this.sendRequestToUpdateCapability();

      }
    } catch (ex) {

    }
  }
  }
