import { EventEmitter, Output, Directive } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { FabricRouter } from 'ClientApp/app/components/globals/Model/FabricRouter';
import { of, Subject } from 'rxjs';
import { auditTime, distinctUntilChanged, pairwise, startWith, takeUntil } from 'rxjs/operators';
import { FabricsNames } from '../../../globals/Model/CommonModel';
import { Routing } from '../../../globals/Model/Message';
import { Mesurements, ReverseMesurements } from '../../../common/Constants/JsonSchemas';
import { dialogUtilCommon } from './dialog.util';
import { IEntityInterface } from './EntityInterface';
import { EntityTypes } from 'ClientApp/app/components/globals/Model/EntityTypes';

// @Injectable({ providedIn: 'root' })
@Directive()
export class EntityCommonUtil extends IEntityInterface {
  takeUntilDestroyObservables=new Subject();

  @Output() changeName = new EventEmitter();
  dateForFDCEntity;
  DownTimeLocations = []

  ReverseMesurements;
  Mesurements;
  constructor(public myForm: FormGroup, public formBuilder: FormBuilder) {
    super();
    this.ReverseMesurements = ReverseMesurements;
    this.Mesurements = Mesurements;
  }

  /*****************Entity CommonMethods ******************/

  initializeObservable(instance:any,index:number){

  }
  AddControlList(arg){

  }
  destroyFormInstance(myForm) {
  }
  formGroupAuditTimeValueChangesSubscription(){};



  initializeEntityHandler() {
    try {
      if (this.myForm && this.myForm.controls && Object.keys(this.myForm.controls).length != 0) {
      //GlobalAdmin GeneralSetting value changes
        if(this.myForm.controls[this.groupName] &&this.myForm.controls[this.groupName]["controls"]){
          this.commonsubscriptionForListAndHeader(this.myForm.controls[this.groupName])
        } 
        if(this.myForm.controls){
          this.commonsubscriptionForListAndHeader(this.myForm)
        }
        
      }
      var list = this.myForm.get("ListsData") as FormArray;
      if (this.myForm&&list) {
        this.listDataControls(list);
      }
    }
    catch (ex) {
    this.commonservice.appLogException(new Error('Exception in initializeEntityHandler() of globaladminutil in globaladmin.util.ts at time ' + new Date().toString() + '. Exception is : ' + ex));
    }
    }
    listDataControls(myForm: FormArray) {
      try {
        if (myForm)
        for (var element in myForm.controls) {
          let controls = myForm.controls[element]["controls"];
          for (var control in controls) {
          var formControl = controls[control]
          this.subsubscribeToFormControlValueChanges(<FormGroup>myForm.get(element), formControl, control);
          }
        }
      }
      catch (ex) {
        this.commonservice.appLogException(new Error('Exception in listDataControls() of globaladminutil in globaladmin.util.ts at time ' + new Date().toString() + '. Exception is : ' + ex));
      }
      }
    subsubscribeToFormControlValueChanges(group: FormGroup, fctrl: FormControl, propertyName) {
	 
      if (fctrl) {
        fctrl
        .valueChanges
        .pipe(startWith(null), pairwise())
        .pipe(distinctUntilChanged())
        .subscribe(([prev, next]: [any, any]) => {
          try {
          var configId = group.get("ConfigId");
          if (configId) {////GlobalAdmin which all having Nested Accordian like Well and Non well and ffv
          this.PreviousOrCurrentLocations[this.EntityName] = this.PreviousOrCurrentLocations[this.EntityName] ? this.PreviousOrCurrentLocations[this.EntityName] : {};
          this.PreviousOrCurrentLocations[this.EntityName][configId.value] = this.PreviousOrCurrentLocations[this.EntityName][configId.value] ? this.PreviousOrCurrentLocations[this.EntityName][configId.value] : {};
          this.PreviousOrCurrentLocations[this.EntityName][configId.value][propertyName] = {};
          this.PreviousOrCurrentLocations[this.EntityName][configId.value][propertyName]["prev"] = prev;
          this.PreviousOrCurrentLocations[this.EntityName][configId.value][propertyName]["next"] = next;
    
          //
          let propertyValue = next;
          if (this.ValueChangedFields) {
            this.ValueChangedFields[propertyName] = propertyValue;
          }
          if(!this.ListValueChangesForField[configId.value]){
            this.ListValueChangesForField[configId.value]={}
          }
          this.ListValueChangesForField[configId.value][propertyName]=propertyValue
  
          this.valueChangesForList[configId.value] = true;
        }
  
          }
          catch (ex) {
          this.commonservice.appLogException(new Error('Exception in subsubscribeToFormControlValueChanges() of globaladminutil in globaladmin.util.ts at time ' + new Date().toString() + '. Exception is : ' + ex));
          }
        });
      }
    }
    commonsubscriptionForListAndHeader(group){
      if(group.controls)
      for (var control in group.controls) {
        if(this.groupName!==control)
        if (group.controls[control].controls) {
          this.newListControlSubscription(group.controls[control])
        }
        else {
        this.ecahFieldSubscription(group, control)
        }
      }
    }
    newListControlSubscription(group){
      for (var subcontrol in group.controls) {
        this.ecahFieldSubscription(group, subcontrol,)
        }
    }
    newControlSubscription(group, propertyName) {
      group.valueChanges
        .pipe(this.compUntilDestroyed())
        .distinctUntilChanged().subscribe(propertyVal => {
          try {
            this.valueChanges(propertyName, propertyVal);
          }
          catch (ex) {
            this.commonservice.appLogException(new Error('Exception in newControlSubscription() of entitybaseutil in entitybase.util.ts at time ' + new Date().toString() + '. Exception is : ' + ex));
          }
        })
    }
    sendMessagetobackend(entityData, info, Datatype, Type?) {
      try {
       
        
      }
      catch (ex) {
        this.commonservice.appLogException(new Error('Exception in sendMessagetobackend() of entitybaseutil in entitybase.util.ts at time ' + new Date().toString() + '. Exception is : ' + ex));
  
      }
    }
    ecahFieldSubscription(group, control) {
      var formControl = (<AbstractControl>group.controls[control])
      formControl.valueChanges.subscribe(data=>{
      });
      of(control)
        .combineLatest((<FormGroup>formControl).valueChanges)
        .pipe(auditTime(10))
        .pipe(takeUntil(this.AuditUnsubscribe), this.compUntilDestroyed(), distinctUntilChanged())
        .subscribe(property => {
          try {
            if (this.ValueChangedFields) {
              let propertyName = property[0];
              let propertyValue = property[1];
              if (propertyValue && propertyValue.EntityName) {
                this.ValueChangedFields[propertyName] = propertyValue.EntityName;
              } else {
                this.ValueChangedFields[propertyName] = propertyValue;
              }
              this.valueChanges(propertyName, propertyValue,group)
            }
          }
          catch (ex) {
            this.commonservice.appLogException(new Error('Exception in initializeEntityHandler() of globaladminutil in globaladmin.util.ts at time ' + new Date().toString() + '. Exception is : ' + ex));
          }
        });
    }
  addControlSubscrption() {}
  ClickEvent(event,index){}
  valueChanges(propertyName, propertyVal,controls?) {}
  NullifyValuechangedFieldsObject() {
    //this.ValueChangedFields={};
  }
  
  eventHandler(data: any) {
    let payload = data.Payload;
    switch (data.EventType) {
      case "KeyUp":
        this.onKeyUpEventHandler(payload.event, payload.properties, payload.mdtriggerValue, payload.configId)
        break;
      
        default:
         break;
    }
  }
  clickEventHandler(event, index?) {
    try {
      switch (event.actualname) {
        case "deleteIcon":
          let EntityData = this.myForm.controls['ListsData']['controls'][event.index]
          new dialogUtilCommon(this).openDialogForDeleteCommon(event, index);
        default:
          break;
      }
    } catch (e) {
      this.commonservice.appLogException(new Error('Exception in clickEventHandler() of EntityBased in Utils at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  checkHierarchyUsingOrNot(EntityData: any, event) {
    let isExists = false;
    let locationdata = EntityData.value;
  }
OpenDialogBox(isExists, event){
  if (isExists) {
    isExists = false;
    new dialogUtilCommon(this).OpenDialogForHierarchyDelete(event, this.EntityName);
  }
  else  {
        new dialogUtilCommon(this).OpenDialogForHierarchyDeletion(event, this.EntityName);
  }
}
  onKeyUpEventHandler(event: any, properties: any, MDtriggerValue: any, configIdd) {
    try {
      this.groupName = this.myForm.controls['ListsData'] ? 'ListsData' : 'ListData'
      switch (properties.type) {
          case "number":
            switch (properties.actualname) {
              
               default:
                break;
            }
          break;
       default :
        break;
      }

    }
    catch (e) {
      console.error('Exception in onKeyUp() of EntityBaseUtil in FDC Util at time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }
  bindDropdownoptions() {
    try {

    }
    catch (ex) {
      this.commonservice.appLogException(new Error('Exception in bindDropdownoptions() of entitybaseutil in entitybase.util.ts at time ' + new Date().toString() + '. Exception is : ' + ex));

    }
  }
  triggerDetectChangesGlobalAdmin(value) {
    this.commonservice.triggerDetectChangesGlobalAdminUtil$.next({
      key: this.EntityName,
      cd: value,
      [this.EntityName]: this.valueChangesForList
    });
  }
  compUntilDestroyed():any {
    return takeUntil(this.takeUntilDestroyObservables);
    }
    ngOnDestroy(): void {
      //Called once, before the instance is destroyed.
      //Add 'implements OnDestroy' to the class.
      this.takeUntilDestroyObservables.next();
this.takeUntilDestroyObservables.complete();


    }
}
