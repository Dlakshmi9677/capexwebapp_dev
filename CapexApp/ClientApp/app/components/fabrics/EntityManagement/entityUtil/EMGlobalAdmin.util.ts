import { EventEmitter, OnDestroy, Directive } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { EntityTypes } from "ClientApp/app/components/globals/Model/EntityTypes";
import { Routing } from 'ClientApp/app/components/globals/Model/Message';
import { WORKER_TOPIC } from 'ClientApp/webWorker/app-workers/shared/worker-topic.constants';
import { Subject } from "rxjs";
import { takeUntil } from 'rxjs/operators';
import { Admin, Configuration } from "../../../common/Constants/AppConsts";
import { ComponentInstanceEventMessageModel, FABRICS, FabricsNames, MessageKind } from '../../../globals/Model/CommonModel';
import { EntityCommonUtil } from "./entitybase.util";

@Directive()
export class EMGlobalAdmin extends EntityCommonUtil implements OnDestroy {
	ngUnSubscribe=new Subject();
	takeUntilDestroyObservables=new Subject();

	constructor(
		public myForm: FormGroup,
		public formBuilder: FormBuilder,
		public dialog: MatDialog
	) {
		super(myForm, formBuilder);
	}
	initializeObservable(intance: any, index) {
		this.emService.getEMFormComponentInstance$
			.pipe(takeUntil(this.unsubscribe)).subscribe((data: ComponentInstanceEventMessageModel) => {
				switch (data.OperationType) {
					case "HierarchyOptionUpdate":
						this.updateHieharchyOptions(); //recalcute volume if Density update or Create
						break;
					case "HierarchyOptionUpdateWithDISTRICTchange":
						this.updateHieharchyOptions();
						if (this.EntityName == "Field")
							this.bind_Field_with_Changed_DistrictIn_Area(data);
						break;
					case "ChildrenOptionUpdateWhileHierarchydelete":
						this.updateHieharchyOptions();
						if (this.EntityName!=EntityTypes.DISTRICT && this.EntityName != data.EntityType)
							this.bind_Area_and_Field_with_Replaced_Parent_whileDeleting_Hierarchy(data);
						break;

					default:
						if (data["MessageKind"] == MessageKind.DELETE && data["DataType"] == "SUCCESS") {
							this.deleteOperation_For_EMadmin(data);
						}
						break;
				}
			});
	}
	triggerDetectChangesGlobalAdmin(value) {
		this.commonservice.triggerDetectChangesGlobalAdminUtil$.next({
		  key: this.EntityName,
		  cd: value,
		  [this.EntityName]: this.valueChangesForList
		});
	  }
	valueChanges(propertyName, propertyValue,group?) {
		try {
			if (propertyValue && propertyValue.EntityName) {
				this.ValueChangedFields[propertyName] = propertyValue.EntityName;
			} else {
				this.ValueChangedFields[propertyName] = propertyValue;
			}
		
			this.globalAdminUpdate(propertyName, propertyValue,group)
			this.filterHierarchyDropDownBasedonDistrictOrArea(propertyName, propertyValue,group);
			
		} catch (ex) {
			this.commonservice.appLogException(
				new Error(
					"Exception in valueChanges() of deliveredutil in delivered.util.ts  at time " +
					new Date().toString() +
					". Exception is : " +
					ex
				)
			);
		}
	}



	bindDropDownsForFieldEntity(entityId, districtId) {
		try {

		}
		catch (e) {
			this.commonservice.appLogException(new Error('Exception in bindDropDownsForFieldEntity() of EMGlobalAdmin in EMGlobalAdmin.util at time ' + new Date().toString() + '. Exception is : ' + e));
		}
	}

	bind_Field_with_Changed_DistrictIn_Area(updatedData) {
		try {
			if(this.myForm&&this.myForm.controls&&this.myForm.controls.ListsData){
			this.myForm.controls.ListsData["controls"].forEach(ele => {
				if (ele.controls.Area.value == updatedData.EntityId) {
					ele.controls.District.setValue(updatedData.PayLoad.propertyValue, { emitEvent: false });
				}
			})
		}
		} catch (e) {
			this.commonservice.appLogException(new Error('Exception in bind_Field_with_Changed_DistrictIn_Area() of EMGlobalAdmin in EMGlobalAdmin.util at time ' + new Date().toString() + '. Exception is : ' + e));
		}
	}

	bind_Area_and_Field_with_Replaced_Parent_whileDeleting_Hierarchy(DeleteData) {
		try {
			if(this.myForm && this.myForm.controls && this.myForm.controls.ListsData){
				switch(DeleteData.EntityType){
					case EntityTypes.DISTRICT: 
					this.myForm.controls.ListsData["controls"].forEach(ele => {
					   if (ele.controls.District.value == DeleteData.PayLoad.OldParentId) {
						   ele.controls.District.setValue(DeleteData.PayLoad.NewParentId, { emitEvent: false });
						   ele.controls.DistrictId.setValue(DeleteData.PayLoad.NewParentId, { emitEvent: false });
					   }
				   })
					break;
					case EntityTypes.AREA:
					   this.myForm.controls.ListsData["controls"].forEach(ele => {
						   if (ele.controls.Area.value == DeleteData.PayLoad.OldParentId) {
							var newDistrictId : any=this.commonservice.getEntityParent(DeleteData.PayLoad.NewParentId);
							   ele.controls.Area.setValue(DeleteData.PayLoad.NewParentId, { emitEvent: false });
							   ele.controls.AreaId.setValue(DeleteData.PayLoad.NewParentId, { emitEvent: false });
							   ele.controls.District.setValue(newDistrictId.EntityId, { emitEvent: false });
							   ele.controls.DistrictId.setValue(newDistrictId.EntityId, { emitEvent: false });
						   }
					   })
					break;
				   }
			}


			// this.myForm.controls.ListsData["controls"].forEach(ele => {
			// 	if (ele.controls.District.value == DeleteData.PayLoad.OldDistrictId) {
			// 		ele.controls.District.setValue(DeleteData.PayLoad.NewDistrictId, { emitEvent: false });
			// 	}
			// })
		} catch (e) {
			this.commonservice.appLogException(new Error('Exception in bind_Field_with_Changed_DistrictIn_Area() of EMGlobalAdmin in EMGlobalAdmin.util at time ' + new Date().toString() + '. Exception is : ' + e));
		}
	}

	filterHierarchyDropDownBasedonDistrictOrArea(propertyName, propertyValue, group) {
		try {

		} catch (e) {
			this.commonservice.appLogException(new Error('Exception in filterHierarchyDropDownBasedonDistrictOrArea() of EMGlobalAdmin in EMGlobalAdmin.util at time ' + new Date().toString() + '. Exception is : ' + e));
		}

	}

	HierarchyParentReplacementInState(message, OldParentId) {
		try {
					 this.commonservice.deleteEntitiesByIds(OldParentId);

					 if( message.EntityType!=EntityTypes.FIELD){
						let model: ComponentInstanceEventMessageModel = {
							fabric: FABRICS.ENTITYMANAGEMENT, EntityType: message.EntityType, EntityId: "", EntityName: this.EntityName, FieldName: '',
							OperationType: "ChildrenOptionUpdateWhileHierarchydelete", PayLoad: { "OldParentId": OldParentId, "NewParentId": message.EntityId }, Event: new EventEmitter()
						}
						this.emService.getEMFormComponentInstance$.next(model);
					 }




	
		}
		catch (e) {
			this.commonservice.appLogException(new Error('Exception in HierarchyParentReplacementInState() of EMGlobalAdmin in EMGlobalAdmin.util at time ' + new Date().toString() + '. Exception is : ' + e));
		}
	}


	globalAdminUpdate(propertyName, propertyValue,group?:FormGroup){
		switch (this.GlobalParentAccordionName) {
			case 'Hierarchy':
				if(group.valid){
					let entityData=group.getRawValue();
					var messageKind = entityData.isNewList?MessageKind.CREATE:MessageKind.UPDATE;
					group.controls.isNewList.setValue(false,{emitEvent:false})
					if(entityData.District)
					entityData.DistrictId=entityData.District
					if(entityData.Area)
					entityData.AreaId=entityData.Area
					this.createOrUpdate(entityData,messageKind)
					if (messageKind == MessageKind.UPDATE && this.EntityName == EntityTypes.AREA && propertyName == EntityTypes.DISTRICT) {
						let model: ComponentInstanceEventMessageModel = {
							fabric: FABRICS.ENTITYMANAGEMENT, EntityType: this.EntityType, EntityId: entityData.EntityId, EntityName: this.EntityName, FieldName: '',
							OperationType: "HierarchyOptionUpdateWithDISTRICTchange", PayLoad: { "propertyName": propertyName, "propertyValue": propertyValue }, Event: new EventEmitter()
						}
						this.emService.getEMFormComponentInstance$.next(model);
					}
					else {
						let model: ComponentInstanceEventMessageModel = {
							fabric: FABRICS.ENTITYMANAGEMENT, EntityType: this.EntityType, EntityId: this.EntityId, EntityName: this.EntityName, FieldName: '',
							OperationType: "HierarchyOptionUpdate", PayLoad: '', Event: new EventEmitter()
						}
						this.emService.getEMFormComponentInstance$.next(model);
					}

				}
				break;
		
			default:
				break;
		}
	}
	createOrUpdate(entityData, messageKind) {
		try {
			if (!entityData.ProductionDay && messageKind == MessageKind.CREATE){
				entityData.ProductionDay = this.commonservice.EMProductionDate;
				this.myForm.controls.ListsData["controls"].forEach((ele,index) => {
					if(ele.value.EntityId==entityData.EntityId){
						const control = this.myForm.controls[this.groupName]["controls"][index] as FormGroup;
						control.addControl("ProductionDay", this.formBuilder.control(entityData.ProductionDay));
					}
				})
			}
			var EntityId = entityData.EntityId;
			entityData.TypeOfField = entityData.EntityType;
			entityData = this.emService.initializeCommonFields(entityData, this.properties, true);
			let jsonString = JSON.stringify(entityData);
			this.emService.AddOrUpdateHierarchyEntitiesInState(entityData)

			if (!entityData.ProductionDay && messageKind == MessageKind.UPDATE) {
				if (entityData.TimeZone && entityData.TimeZone != null) {
					this.commonservice.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonservice.baseUrl + 'api/AlThings/ProductionDate', {}, entityData.TimeZone).subscribe((res: any) => {
						entityData.ProductionDay = new Date(res);
						let updatejsonString = JSON.stringify(entityData);
						this.sendmessagetobackend(EntityId, updatejsonString, entityData, messageKind);
					});
				}
			} else
				this.sendmessagetobackend(EntityId, jsonString, entityData, messageKind);

		}
		catch (e) {
			this.commonservice.appLogException(new Error('Exception in SaveIntoDb() of EMGlobalAdmin in EMGlobalAdmin.util at time ' + new Date().toString() + '. Exception is : ' + e));
		}
	}


	sendmessagetobackend(EntityId, jsonString, entityData, messageKind) {
		let payload = {
			"EntityId": EntityId, "TenantName": this.commonservice.tenantName,
			"TenantId": this.commonservice.tenantID, "EntityType": entityData.EntityType,
			"Fabric": FABRICS.ENTITYMANAGEMENT, "EntityInfoJson": jsonString, "Info": "Admin",
			ProductionDay: entityData.ProductionDay
		};
		var payloadString = JSON.stringify(payload);
		//this.commonservice.sendMessageToServer(payloadString, DataTypes.CONFIGURATION, EntityId, entityData.EntityType, messageKind, Routing.AllFrontEndButOrigin, "Details", FABRICS.FDC.toUpperCase());
	}


	  updateHieharchyOptions(){
		this.bindDropdownoptions();

	  }
	updateDependentEntityFormInstanceData(model: ComponentInstanceEventMessageModel) {
		try {
			switch (model.OperationType) {
				case "get": {
					switch (model.FieldName) {
						case "myForm":
							if (this.myForm)
								(<EventEmitter<any>>model.Event).next(this.myForm.value);
							break;
					}

				}
					break;
				default:
					break;
			}

			//Write Here , If Needed On Anyother Switch Case Based 
			// switch (model.FieldName) {
			//    case "Fieldname":
			//    break;
	        //  default:break;
			//   }
		}
		catch (e) {
			this.commonservice.appLogException(new Error('Exception in updateDependentEntityFormInstanceData() of LocationUtil  at time ' + new Date().toString() + '. Exception is : ' + e));
		}
	}

  // /****
  // * cONFIGURATION VALUECHANGES
  // */

  clickEventHandler(event, index?) {
    try {
      switch (event.actualname) {
        default:
          super.clickEventHandler(event, index);
          break;
      }
    } catch (e) {
      this.commonservice.appLogException(new Error('Exception in clickEventHandler() of EntityBased in Utils at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

	private setValidators(propertyName) {
		let defaultvalue = this.properties[propertyName]["DefaultValue"];
		if (!this.myForm.controls[propertyName]) {
			this.myForm.addControl(propertyName, this.formBuilder.control(defaultvalue));
			this.newControlSubscription(this.myForm.controls[propertyName], propertyName);
		}
		this.myForm.controls[propertyName].setValidators(Validators.required);
		this.myForm.controls[propertyName].updateValueAndValidity({ emitEvent: false });
	}

	private clearValidators(propertyName) {
		let defaultvalue = this.properties[propertyName]["DefaultValue"];
		if (!this.myForm.controls[propertyName]) {
			this.myForm.addControl(propertyName, this.formBuilder.control(""));
			this.newControlSubscription(this.myForm.controls[propertyName], propertyName);
		}
		this.myForm.controls[propertyName].setValue(defaultvalue,{ emitEvent: false });
		this.myForm.controls[propertyName].clearValidators();
		this.myForm.controls[propertyName].updateValueAndValidity({ emitEvent: false });
	}




  addorsetcontrol(propertyName,propertyVal,controls){
	if (!controls[propertyName]) 
		controls[propertyName]= this.formBuilder.control(propertyVal);
	else 
		controls[propertyName].setValue(propertyVal, { emitEvent: false });
  }
  
	bindDropdownoptions() {
		try {
		  if (this.properties) {
			let propertylist = Object.keys(this.properties).filter(propertyName => this.properties[propertyName]["type"] == "select" || this.properties[propertyName]["type"] == "autocomplete");
			propertylist.forEach(propertyName => { 
			switch (propertyName) {
			  case EntityTypes.DISTRICT:
			  case EntityTypes.AREA:
				this.commonservice.getEntititesFromStore$('TypeOf',
				 [propertyName],
				  d => true,
				  d => { return { "EntityId": d.EntityId, "EntityType": d.EntityType, "EntityName": d.EntityName } })
				  .pipe(this.compUntilDestroyed())
				  .subscribe((entities: any) => {
					if (this.properties[propertyName]) {
					  this.properties[propertyName].options = entities;
					  this.triggerDetectChangesGlobalAdmin({ triggerCD: true, markForCheck: true })
					}
				  });
				break;
			  }
			});
		  }
		}
		catch (ex) {
		  this.commonservice.appLogException(new Error('Exception in bindDropdownoptions() in pig.util.ts at time ' + new Date().toString() + '. Exception is : ' + ex));
		}
	  }

	  updateValidateForm(formData,formName?) {
		try {
		  var tempformData = JSON.parse(JSON.stringify(formData))
		  //let ProductionDay=this.piggingService.setHoursBasedOnTimeZoneUTC(this.piggingService.getDateFromRouting())
		  var entityData = this.emService.initializeCommonFields(tempformData, this.properties);
		  //entityData.ProductionDay = entityData.ProductionDate = ProductionDay;
		  entityData = this.updateListChangedValues(formData);
		  if (entityData == null) {
			//this.fdcservice.newListSaving = false;
			return;
		  }
		  super.sendMessagetobackend(entityData, Admin, Configuration, Configuration);
		  this.valueChangesForList={}
		  this.ListValueChangesForField={}
		  this.IsnewList.value=null
		  //ffvutil

		}
		catch (ex) {
		  this.commonservice.appLogException(new Error('Exception in updateValidateForm() of ffvtutil in ffv.util.ts at time ' + new Date().toString() + '. Exception is : ' + ex));
	
		}
	  }
	  updateListChangedValues(data, Type?) {
		try {
		  var finalData = null;
			finalData = JSON.parse(JSON.stringify(data));
			delete finalData[this.groupName];
			finalData = this.emService.initializeCommonFields(finalData, this.properties,null,this.EntityName);
			//let ProductionDay=this.emService.setHoursBasedOnTimeZoneUTC(this.piggingService.getDateFromRouting())
			//finalData.ProductionDay = finalData.ProductionDate = ProductionDay;
			finalData[this.groupName] = [];
		  if (data && data[this.groupName]) {
			for (var configId in this.valueChangesForList) {
			  var index = data[this.groupName].findIndex(value => {
				if (value.ConfigId) {
				  return value.ConfigId == configId;
				} else if (value.EntityId) {
				  return value.EntityId == configId;
				}
			  });
			  if (data[this.groupName][index]) {
				var listData = this.emService.initializeCommonFields(data[this.groupName][index], this.properties,null,this.EntityName);
				if (finalData == null) {
				  finalData = {};
				  finalData[this.groupName] = [];
				}	
				listData.Type = Type;
				finalData[this.groupName].push(listData);
			  }
			}
		  }
		  else {
			finalData = this.emService.initializeCommonFields(data, this.properties,null, this.EntityName);
		  }
		  return finalData;
		} catch (e) {
		  this.commonservice.appLogException(new Error('Exception in updateListChangedValues() of entitybaseutil in entitybase.util.ts at time ' + new Date().toString() + '. Exception is : ' + e));
	
		}
	  }
	

	deleteOperation_For_EMadmin(msg) {
		try {


		} catch (e) {
			this.commonservice.appLogException(new Error('Exception in deleteOperation_For_EMadmin() in EMGlobalAdmin.util.ts at time ' + new Date().toString() + '. Exception is : ' + e));
		}


	}




	/****
	 * for destroying the current formInstance 
	 */
	destroyFormInstance(myForm) {
    this.myForm = this.formBuilder.group({});
    this.ValueChangedFields = {};
    this.AuditUnsubscribe.next();
    this.AuditUnsubscribe.complete();
	this.ngUnSubscribe.next();
    this.ngUnSubscribe.complete();
	}
	
	compUntilDestroyed():any {
		return takeUntil(this.takeUntilDestroyObservables);
		}
	ngOnDestroy() {
		// Called once, before the instance is destroyed.
		// Add 'implements OnDestroy' to the class.
		this.takeUntilDestroyObservables.next();
this.takeUntilDestroyObservables.complete();

	}
}
