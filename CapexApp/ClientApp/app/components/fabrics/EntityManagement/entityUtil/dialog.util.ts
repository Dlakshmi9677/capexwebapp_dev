import { FormArray, FormGroup } from "@angular/forms";
import { MatDialogConfig } from "@angular/material/dialog";
import { DELETE_HIERARCHY, FORM_ARCHIVED2, SELECT_CHECK_MARK, DELETE_EM_DISTRICT_HIERARCHY, DELETE_EM_AREA_HIERARCHY, DELETE_EM_FIELD_HIERARCHY, DELETE_EM_HIERARCHY } from "ClientApp/app/components/common/Constants/AttentionPopupMessage";
import { FABRICS, MessageKind } from "ClientApp/app/components/globals/Model/CommonModel";
import { Routing } from "ClientApp/app/components/globals/Model/Message";
import { PopUpAlertComponent } from "../../../common/PopUpAlert/popup-alert.component";
// import { DataTypes } from "../../../Desktop/SqlLiteCurdOperation/FDC/ProdCalculations/RequiredInputs";
import { PopupOperation } from "../../../globals/Model/AlertConfig";
import { IEntityInterface } from "./EntityInterface";

export class dialogUtilCommon<T extends IEntityInterface> {
  myForm: FormGroup;
  instance: any;
  constructor(private utilObject: T) {
    this.instance = utilObject;
    this.myForm = this.instance.myForm;
  }
 

  openDialog(messageToshow: string): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: false,
        content: [messageToshow],
        subContent: [],
        operation: PopupOperation.Attention
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '629px';
      matConfig.disableClose = true;
      matConfig.id = messageToshow;

      if (!this.utilObject.dialog.getDialogById(matConfig.id)) {
        let dialogRef = this.utilObject.dialog.open(PopUpAlertComponent, matConfig);
        dialogRef.afterClosed().subscribe((res) => {
         
        });
      }
    }
    catch (e) {
      this.utilObject.commonservice.appLogException(new Error('Exception in openDialog() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  openDialogForDeleteCommon(event, index) {
    try {
      let entityNametoDelete = this.utilObject.EntityName
      let config = {
        header: 'ATTENTION!',
        isSubmit: true,
        content: [FORM_ARCHIVED2(entityNametoDelete)],
        subContent: [],
        operation: PopupOperation.DeleteAll
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.disableClose = true;

      let dialogRef = this.utilObject.dialog.open(PopUpAlertComponent, matConfig);

      dialogRef.componentInstance.emitResponse.subscribe((data: any) => {
        if (data.DeleteConfigRecord && data.DeleteProdectionRecord) {
          let EntityData =this.instance.myForm.value[this.instance.groupName][event.index]
          const control: FormArray = this.instance.myForm.controls[this.instance.groupName] as FormArray;
          control.removeAt(event.index);
          let payload = { "EntityId": EntityData.EntityId, 'TenantName': this.instance.commonservice.tenantName, 'TenantId': this.instance.commonservice.tenantID, 'EntityType': EntityData.EntityType, 'Info': EntityData.EntityType, 'Fabric': "FDC" };
          // this.instance.commonservice.sendMessageToServer(JSON.stringify(payload), DataTypes.CONFIGURATION, payload.EntityId, payload.EntityType, MessageKind.DELETE, Routing.AllFrontEndButOrigin, "Details", FABRICS.FDC.toUpperCase());
        }
      });
    }
    catch (e) {
      this.utilObject.commonservice.appLogException(new Error('Exception in openDialogTruck() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }
  OpenDialogForHierarchyDeletion(event, name){
    let config = {
      header: 'Delete ' + name + ' ?',
      isSubmit: true,
      content: [DELETE_EM_HIERARCHY(name)],
      subContent: [],
      operation: PopupOperation.DeleteAll,
    };
    let matConfig = new MatDialogConfig()
    matConfig.data = config;
    matConfig.width = '500px';
    matConfig.disableClose = true;

    let dialogRef = this.utilObject.dialog.open(PopUpAlertComponent, matConfig);

    dialogRef.componentInstance.emitResponse.subscribe((data: any) => {

      if (data.DeleteConfigRecord && data.DeleteProdectionRecord) {
        let EntityData =this.instance.myForm.value[this.instance.groupName][event.index]
        const control: FormArray = this.instance.myForm.controls[this.instance.groupName] as FormArray;
        control.removeAt(event.index);
        let payload = { "EntityId": EntityData.EntityId, 'TenantName': this.instance.commonservice.tenantName, 'TenantId': this.instance.commonservice.tenantID, 'EntityType': EntityData.EntityType, 'Info': EntityData.EntityType, 'Fabric': "FDC" };
      // this.instance.commonservice.sendMessageToServer(JSON.stringify(payload), DataTypes.CONFIGURATION, payload.EntityId, payload.EntityType, MessageKind.DELETE, Routing.AllFrontEndButOrigin, "Details", FABRICS.FDC.toUpperCase());
       this.utilObject.commonservice.deleteEntitiesByIds(EntityData.EntityId);
      }
    });
}
  OpenDialogForHierarchyDelete(event, name){
    try {
      let message;
      if(name == "Area"){
         message = [DELETE_EM_AREA_HIERARCHY];
      }
      if(name == "District"){
        message = [DELETE_EM_DISTRICT_HIERARCHY]
      }
      if(name == "Field"){
        message = [DELETE_EM_FIELD_HIERARCHY];
      }
      let config = {
        header: 'Attention!',
        isSubmit: false,
        content: message,
        subContent: [],
        operation: PopupOperation.Attention
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      if (!this.utilObject.dialog.getDialogById(matConfig.id)) {
        let dialogRef = this.utilObject.dialog.open(PopUpAlertComponent, matConfig);
        dialogRef.afterClosed().subscribe((res) => {
        });
      }
    }
    catch (e) {
      this.utilObject.commonservice.appLogException(new Error('Exception in openDialog() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
    
    }
  OpenDialogForDistrictDelete(event, name,EntityId?) {
    try {
      let config = {
        header: 'Delete ' + name + ' ?',
        isSubmit: true,
        content: [DELETE_HIERARCHY(name)],
        subContent: [SELECT_CHECK_MARK],
        EntityType:name,
        EntityId:EntityId,
        operation: PopupOperation.DeleteHierarachy,
      };

      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;

      let dialogRef = this.utilObject.dialog.open(PopUpAlertComponent, matConfig);

      dialogRef.componentInstance.emitResponse.subscribe((data: any) => {

        if (this.instance.commonservice.ShowHierarchyName) {
          let EntityData = this.instance.myForm.value[this.instance.groupName][event.index]
          const control: FormArray = this.instance.myForm.controls[this.instance.groupName] as FormArray;
          control.removeAt(event.index);
          let payload = { "EntityId": EntityData.EntityId, 'TenantName': this.instance.commonservice.tenantName, 'TenantId': this.instance.commonservice.tenantID, 'EntityType': EntityData.EntityType, 'Info': EntityData.EntityType, 'Fabric': "FDC", "TypeofData": "ReplaceHierarchy", "NewParentId": data.EntityId };
         // this.instance.commonservice.sendMessageToServer(JSON.stringify(payload), DataTypes.CONFIGURATION, payload.EntityId, payload.EntityType, MessageKind.DELETE, Routing.AllFrontEndButOrigin, "Details", FABRICS.FDC.toUpperCase());
          this.instance.HierarchyParentReplacementInState(data, EntityData.EntityId);
          this.utilObject.commonservice.deleteEntitiesByIds(EntityData.EntityId);

        } else {
          let EntityData = this.instance.myForm.value[this.instance.groupName][event.index]
          const control: FormArray = this.instance.myForm.controls[this.instance.groupName] as FormArray;
          control.removeAt(event.index);
          let payload = { "EntityId": EntityData.EntityId, 'TenantName': this.instance.commonservice.tenantName, 'TenantId': this.instance.commonservice.tenantID, 'EntityType': EntityData.EntityType, 'Info': EntityData.EntityType, 'Fabric': "FDC" };
         // this.instance.commonservice.sendMessageToServer(JSON.stringify(payload), DataTypes.CONFIGURATION, payload.EntityId, payload.EntityType, MessageKind.DELETE, Routing.AllFrontEndButOrigin, "Details", FABRICS.FDC.toUpperCase());
          this.utilObject.commonservice.deleteEntitiesByIds(EntityData.EntityId);
        }
      });
    }
    catch (e) {
      this.utilObject.commonservice.appLogException(new Error('Exception in OpenDialogForDistrictDelete()  in Dialogutil() at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
}
