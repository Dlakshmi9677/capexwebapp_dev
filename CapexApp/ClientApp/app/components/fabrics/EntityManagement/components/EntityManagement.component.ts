import { Component, OnInit } from "@angular/core";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ENTITY_NOT_EXIST } from "ClientApp/app/components/common/Constants/AttentionPopupMessage";
import { FDCIcons, IconJson, PiggingIcons } from "ClientApp/app/components/globals/icons";
import { FABRICS, TreeTypeNames } from "ClientApp/app/components/globals/Model/CommonModel";
import { FabricRouter } from "ClientApp/app/components/globals/Model/FabricRouter";
import { Subject } from 'rxjs';
import { filter, takeUntil } from "rxjs/operators";
import { CommonService } from "../../../globals/CommonService";
import { NewEntityBIComponent } from "../../bifabric/components/newentitybidialog/newentitybi.component";
import { EntityManagementService } from '../services/EntityManagementService';

@Component({
  selector: 'EntityManagement',
  templateUrl: './EntityManagement.component.html',
  styleUrls: ['./EntityManagement.component.scss']
})
export class EntityManagementcomponent implements OnInit {
  capability=FABRICS.ENTITYMANAGEMENT;
  takeUntilDestroyObservables=new Subject();
  constructor(public Entitymanagementservice: EntityManagementService, public iconRegistry: MatIconRegistry, public router: Router, public route: ActivatedRoute, public sanitizer: DomSanitizer, public Commonservice: CommonService,public dialog: MatDialog) {
    //this.Entitymanagementservice.GetRightTreeData();
  this.Commonservice.lastOpenedFabric=this.Commonservice.getFabricNameByUrl(this.router.url)

  this.Commonservice.addNewEntity$.pipe(this.compUntilDestroyed()).subscribe((res: any) => {
    try {
      // if (this.Commonservice.treeIndexLabel == 'Search') {
      //   var url = '/' + FabricRouter.ENTITYMANAGEMENT_FABRIC + '/' + FabricRouter.Map + '/search';
      //   this.router.navigate([url], { queryParams: { 'tab': this.Commonservice.treeIndexLabel, 'leftnav': this.Commonservice.isLeftTreeEnable } });
      // } else if (this.Commonservice.treeIndexLabel == 'Lists') {
      //   this.Commonservice.CreatenewList = true;
      //   var url = '/' + FabricRouter.PIGGING + '/' + FabricRouter.Map + '/addnewlist';
      //   this.router.navigate([url], { queryParams: { 'tab': this.Commonservice.treeIndexLabel, 'leftnav': this.Commonservice.isLeftTreeEnable } });
      // }
      // else {
      if (this.capability == TreeTypeNames.BusinessIntelligence) {
        this.openCreateNewForm();
      }
      else {
        let FormName=this.Entitymanagementservice.getFormName()
        if (FormName=="addnewentity") {
          this.Commonservice.RerouteToAddNew = true;
          this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC], { queryParams: { 'tab': this.Commonservice.treeIndexLabel, 'leftnav': this.Commonservice.isLeftTreeEnable } });
        }
        else
          this.Commonservice.RerouteToAddNew = false;

        setTimeout(() => {
          this.router.navigate([FabricRouter.ENTITYMANAGEMENT_FABRIC,'addnewentity'], { queryParams: { 'tab': this.Commonservice.treeIndexLabel, 'leftnav': this.Commonservice.isLeftTreeEnable } });
        }, 100);
      }
     // }
    } catch (e) {
      this.Commonservice.appLogException(new Error("Exception in addNewEntity$ observable in pigging.component at time:" + new Date().toString() + ".Exception is :" + e));
    }
  });
    setTimeout(() => {
      this.Commonservice.updatelastopenedfabricinuserpreferenc(this.Commonservice.lastOpenedFabric);
    }, 1000);
  }

  ngOnInit() {
    this.Commonservice.changeFavIcon(FABRICS.ENTITYMANAGEMENT);
    this.route.queryParams.subscribe(params => {
      this.capability = params.physicalItem ? params.physicalItem : params.tab? (params.tab != 'All' ? params.tab : 'Entity Management') : 'Entity Management';

      if (params.tab == 'People ') {
        this.router.navigate([FABRICS.ENTITYMANAGEMENT], { queryParams: { 'tab': TreeTypeNames.PeopleAndCompanies, 'leftnav': true } });
      }
    });

    this.router.events.pipe(filter(event => event instanceof NavigationEnd))
      .pipe(this.compUntilDestroyed())
      .subscribe((event: NavigationEnd) => {
        let leftnavstate = this.Commonservice.getFabricTabByUrl('leftnav', event.url);
        if (leftnavstate == "false")
          this.capability = "Entity Management";

        this.onRoutechange(event);
      });
  }

  onRoutechange(event){
    if(this.Commonservice.IsEntityExistEMCapability.isNotExist ){
      this.Entitymanagementservice.openCommonAttentionPopup(ENTITY_NOT_EXIST(this.Commonservice.treeIndexLabel));
      this.Commonservice.IsEntityExistEMCapability={}
    }
  }



  ngAfterViewInit() {
    this.registerIcon();
    setTimeout(() => {
      this.Commonservice.addNew$.next();
    }, 100);
  }

  registerIcon() {
    var fdcIcons =Object.assign(FDCIcons, PiggingIcons,IconJson);
    for (let element in fdcIcons) {
      this.iconRegistry.addSvgIcon(
        element,
        this.sanitizer.bypassSecurityTrustResourceUrl(fdcIcons[element]));
    }
  }

  openCreateNewForm() {
    try {
      let matConfig = new MatDialogConfig()
      matConfig.width = '500px';
      matConfig.disableClose = true;

      const dialogRef = this.dialog.open(NewEntityBIComponent, matConfig);

    }
    catch (ex) {
    }

  }

ngOnDestroy(): void {
  this.takeUntilDestroyObservables.next();
  this.takeUntilDestroyObservables.complete();
}
compUntilDestroyed():any {
  return takeUntil(this.takeUntilDestroyObservables);
}

}
