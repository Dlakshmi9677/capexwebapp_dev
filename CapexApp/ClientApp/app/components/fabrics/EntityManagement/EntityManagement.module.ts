import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntityManagementcomponent } from './components/EntityManagement.component';
import { routing } from './EntityManagement.routing';
import { SharedModule1 } from '../../shared/shared1.module';
import { EntityManagementService } from './services/EntityManagementService';
import { BIFabricService } from '../bifabric/services/BIFabricService';
import { PeopleActivitiesService } from '../PeopleFabric/services/people-activities-service';
import { ConstructionService } from '../construction/services/construction.service';

@NgModule({
  imports: [CommonModule, routing, SharedModule1],
  providers: [EntityManagementService, BIFabricService, PeopleActivitiesService, ConstructionService],
  declarations: [EntityManagementcomponent]
})

export class EntityManagementModule {

}
