import { RouterModule, Routes } from '@angular/router';
import { EmAdminFormComponent } from '../em-admin-form/em-admin-form-components/em-admin-form.component';
import { EMAdminGaurdService } from './em-admin-form-components/em-admin-form-guard.service';


const routes: Routes = [
  {
    path: '', component: EmAdminFormComponent ,canDeactivate: [EMAdminGaurdService],
     children: [
    ]
  }
];

export const routing = RouterModule.forChild(routes);

