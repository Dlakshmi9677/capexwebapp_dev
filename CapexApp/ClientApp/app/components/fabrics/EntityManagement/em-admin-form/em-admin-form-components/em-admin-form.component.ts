import { Component, OnInit } from '@angular/core';
import { CommonService } from 'ClientApp/app/components/globals/CommonService';
import { Router, ActivatedRoute } from '@angular/router';
import { FabricRouter } from 'ClientApp/app/components/globals/Model/FabricRouter';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PopupOperation } from 'ClientApp/app/components/globals/Model/AlertConfig';
import { Observable } from 'rxjs';
import { Mandatory_Message } from 'ClientApp/app/components/common/Constants/AttentionPopupMessage';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PopUpAlertComponent } from 'ClientApp/app/components/common/PopUpAlert/popup-alert.component';
import { FABRICS, TreeOperations } from 'ClientApp/app/components/globals/Model/CommonModel';
import { EntityManagementService } from '../../services/EntityManagementService';
import { Subject} from 'rxjs';
import {  takeUntil } from "rxjs/operators";

@Component({
  selector: 'app-em-admin-form',
  templateUrl: './em-admin-form.component.html',
  styleUrls: ['./em-admin-form.component.scss']
})
export class EmAdminFormComponent implements OnInit {
  isExpand : boolean
  queryParams={}
  myForm :FormGroup
  ListsofData:any=[]
  intialized:boolean
  entityMgmtCommonInputVariable = {"updated":true};
  expandCollapsebooleanValue : boolean = true;
  dialogRef: any;
  headerConfig = [
    {
      'source': 'V3 CollapsAll',
      'title': !this.isExpand ? 'Expand All' : 'Collapse All',
      'routerLinkActive': false,
      'id': 'expandcollapse',
      'class': 'icon21-21',
      'float': 'left',
      'type': 'icon',
      'show': true
    },
    {
      'source': 'V3 Documents',
      'title': 'ENTITY MANAGEMENT ADMIN',
      'routerLinkActive': false,
      'id': 'documents',
      'class': 'title',
      'float': 'left',
      'type': 'title',
      'show': true,
      'uppercase':'Isuppercase'
    },
    {
      'source': 'V3 CloseCancel',
      'title': 'Close',
      'routerLinkActive': false,
      'id': 'close',
      'class': 'icon21-21',
      'float': 'right',
      'type': 'icon',
      'show': true
    }
  ];
  takeUntilDestroyObservables=new Subject();

  constructor(public Entitymanagementservice: EntityManagementService,private commonService : CommonService,private router:Router,private route :ActivatedRoute,public formBuilder :FormBuilder,public dialog: MatDialog) { }

  ngOnInit() {
    this.route.queryParams.pipe(this.compUntilDestroyed()).subscribe(params => {
      this.queryParams = JSON.parse(JSON.stringify(params));
    });
    this.getAdminFormData()
  }
  getAdminFormData() {
    try{
      this.myForm = this.formBuilder.group({});
      // this.cdRef.markForCheck();
       this.intialized=true;
    }
    catch (e) {
      console.error('Exception in getAdminFormData() of '+this.constructor.name+'   at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in getAdminFormData() of '+this.constructor.name+'   at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  ClickEvent(data) {
    try {
      // this.navButton = data.id;
      let url = '';
      switch (data.id) {
        case 'close':
          this.expandCollapsebooleanValue = true;
          this.CloseForm();
          break;
        case 'expandcollapse':
          {
            this.expandCollapsebooleanValue = false;
            data.source = !this.isExpand ? 'V3 ExpandAll' : 'V3 CollapsAll';
            data.title= this.isExpand ?  'Expand All':'Collapse All' 
            this.isExpand = !this.isExpand;
            this.router.navigate([FABRICS.ENTITYMANAGEMENT,'EMGlobalAdmin', { EntityType: "emadmin", Expand: this.isExpand }], { queryParams: this.queryParams });
          }
      }
    }
    catch (e) {
      console.error('Exception in ClickEvent() of '+this.constructor.name+'  at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in ClickEvent() of '+this.constructor.name+'   at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  entityMagntCommonOutputEmitter(event){

  }
  CloseForm() {
    try {
      this.router.navigate([  FabricRouter.ENTITYMANAGEMENT_FABRIC], { queryParams: this.queryParams });
    } catch (e) {
      console.error('Exception in CloseForm() of '+this.constructor.name+'   at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in CloseForm() of '+this.constructor.name+'   at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  canDeactivate(): Observable<boolean> {
    var response;
    if (this.commonService.switchButton != "expandcollapse" && (!this.commonService.logoutbtn && this.commonService.popupformclose != true || this.myForm.invalid)) {
      let message = null;
      if (this.myForm.invalid) {
          message = Mandatory_Message
          this.openDeActivateDialog(message, PopupOperation.AlertConfirm);
          response = this.dialogRef.componentInstance.emitResponse.map(res => {
            return true;
          })
        } 
    }
    return response;
  }

  openDeActivateDialog(messageToshow, Operation?): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: Operation != undefined ? true : false,
        content: [messageToshow],
        subContent: [],
        operation: Operation != undefined ? Operation : PopupOperation.Attention
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;
      matConfig.id = messageToshow;
    if (!this.dialog.getDialogById(messageToshow)) {
      this.dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
    }

    this.dialogRef.componentInstance.emitCancelResponse.subscribe((res) => {
      this.Entitymanagementservice.sendDataToAngularTree([this.queryParams["tab"]], TreeOperations.deactivateSelectedNode, "");
    });
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in  openDeActivateDialog() of EntityManagementFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  
  compUntilDestroyed():any {
    return takeUntil(this.takeUntilDestroyObservables);
    }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }
}
