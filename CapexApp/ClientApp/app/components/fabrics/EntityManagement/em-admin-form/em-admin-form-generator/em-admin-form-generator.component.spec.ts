import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmAdminFormGeneratorComponent } from './em-admin-form-generator.component';

describe('EmAdminFormGeneratorComponent', () => {
  let component: EmAdminFormGeneratorComponent;
  let fixture: ComponentFixture<EmAdminFormGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmAdminFormGeneratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmAdminFormGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
