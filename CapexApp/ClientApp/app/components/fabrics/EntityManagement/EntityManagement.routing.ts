import { RouterModule, Routes } from '@angular/router';
import { EntityManagementcomponent } from './components/EntityManagement.component';
const routes: Routes = [
    {
        path: '', component: EntityManagementcomponent, children: [
            {
                path: 'addnewentity',
                loadChildren: () => import('../../template/AddNewComponents/addnew.form.module').then(m => m.AddNewFormModule)
            },
            {
                path: 'Settings/:EntityID/:tab/:EMTab',
                loadChildren: () => import('./EntityManagementFormTemplate/entitymanagement.forms.module').then(m => m.EntityManagementFormsModule)
            },
            // {
            //   path: 'copyentity',
            //   loadChildren: () => import('../../fabrics/FDC/components/FDCStepperFormTemplate/fdcstepperform.module').then(m => m.FDCStepperFormModule)
            // },
            {
                path: 'EMGlobalAdmin',
                loadChildren: () => import('./em-admin-form/em-admin-form.module').then(m => m.EmAdminFormModule)
            }
        ]

    }
];

export const routing = RouterModule.forChild(routes);
