import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
// import { untilDestroyed } from 'ngx-take-until-destroy';
import { ALREADY_EXIST3, SELECTALL_INPUT_FIELDS } from 'ClientApp/app/components/common/Constants/AttentionPopupMessage';
import { Subject } from "rxjs";
import { takeUntil } from 'rxjs/operators';
import { AppConsts } from '../../../../common/Constants/AppConsts';
import { PopUpAlertComponent } from '../../../../common/PopUpAlert/popup-alert.component';
import { BIFabricService } from '../../../../fabrics/bifabric/services/BIFabricService';
import { CommonService } from '../../../../globals/CommonService';
import { PopupOperation } from '../../../../globals/Model/AlertConfig';
import { FABRICS, FabricsNames,Datatypes, TreeTypeNames, TreeOperations } from '../../../../globals/Model/CommonModel';
import { EntityTypes } from '../../../../globals/Model/EntityTypes';
import { FabricRouter } from '../../../../globals/Model/FabricRouter';
import { IModelMessage, Routing } from '../../../../globals/Model/Message';
import { MessagingService } from '../../../../globals/services/MessagingService';

export const _filter = (opt: any, value: any): any => {
  const filterValue = value ? value.toLowerCase() : '';

  return opt.filter(item => item["viewValue"].toLowerCase().indexOf(filterValue) === 0);
};



@Component({
  selector: 'newentitybi',
  templateUrl: 'newentitybi.component.html',
  styleUrls: ['./newentitybi.component.styles.scss'],
})

export class NewEntityBIComponent implements OnDestroy, OnInit {
  @ViewChild(MatAutocompleteTrigger, {static: false}) autocompleteTrigger: MatAutocompleteTrigger;
  /**
 * parentList
 */

 // parentList: any = [];

  /**
   * bindedParentName
   */

  bindedParentName;

  /**
  * declaring input variable myForm using angular @Input() directive
  */

  @Input() myForm: FormGroup;

  /**
* declaring input variable schema using angular @Input() directive
*/

  @Input() schema: any;
  /**
  * declaring input variable data using angular @Input() directive
  */

  @Input() data: any;
  /**
 * observable
 */

  property = { 'Label': "" };
  EntityTypesArray = [];
  datas;
  List2;
  newEntityUpdate = true;
  DisableCategoryProperty: boolean = false;

  headerConfig = [
    {
      'source': 'V3 General',
      'title': 'Create New Entity',
      'routerLinkActive': false,
      'id': 'title',
      'class': 'icon21-21',
      'float': 'left',
      'type': 'title',
      'show': true,
      'uppercase':'Isuppercase'
    },
    {
      'source': 'V3 CloseCancel',
      'title': 'cancel',
      'routerLinkActive': false,
      'id': 'close',
      'class': 'icon21-21',
      'float': 'right',
      'type': 'icon',
      'show': true
    },
    {
      'source': 'FDC_Checkmark_Icon_Idle',
      'title': 'Save',
      'routerLinkActive': false,
      'id': 'save',
      'class': 'icon21-21',
      'float': 'right',
      'type': 'icon',
      'show': true
    }
  ];
 staticSchema = "{\"Schema\":\"{\\r\\n    \\\"EntityType\\\": {\\r\\n      \\\"type\\\": \\\"select\\\",\\r\\n      \\\"label\\\": \\\"Entity Type\\\",\\r\\n      \\\"actualname\\\": \\\"EntityType\\\",\\r\\n      \\\"validation\\\": {\\r\\n        \\\"required\\\": true,\\r\\n        \\\"minLength\\\": \\\"\\\",\\r\\n        \\\"maxLength\\\": \\\"\\\",\\r\\n        \\\"pattern\\\": \\\"\\\",\\r\\n        \\\"custom\\\": \\\"\\\"\\r\\n      },\\r\\n      \\\"condition\\\": {\\r\\n        \\\"show\\\": true\\r\\n      },\\r\\n      \\\"disable\\\": false,\\r\\n      \\\"bindLabel\\\": \\\"EntityName\\\",\\r\\n      \\\"update\\\": true,\\r\\n      \\\"options\\\": [\\r\\n        {\\r\\n          \\\"Value\\\": \\\"Category\\\",\\r\\n          \\\"EntityName\\\": \\\"Category\\\"\\r\\n        },\\r\\n        {\\r\\n          \\\"Value\\\": \\\"DashBoard\\\",\\r\\n          \\\"EntityName\\\": \\\"DashBoard\\\"\\r\\n        }\\r\\n      ]\\r\\n    },\\r\\n    \\\"Parent\\\": {\\r\\n      \\\"type\\\": \\\"select\\\",\\r\\n      \\\"label\\\": \\\"Parent Entity\\\",\\r\\n      \\\"actualname\\\": \\\"Parent\\\",\\r\\n      \\\"validation\\\": {\\r\\n        \\\"required\\\": false,\\r\\n        \\\"minLength\\\": \\\"\\\",\\r\\n        \\\"maxLength\\\": \\\"\\\",\\r\\n        \\\"pattern\\\": \\\"\\\",\\r\\n        \\\"custom\\\": \\\"\\\"\\r\\n      },\\r\\n      \\\"condition\\\": {\\r\\n        \\\"show\\\": true\\r\\n      },\\r\\n      \\\"disable\\\": false,\\r\\n      \\\"update\\\": true,\\r\\n      \\\"bindLabel\\\": \\\"text\\\",\\r\\n      \\\"options\\\": []\\r\\n    },\\r\\n    \\\"EntityName\\\": {\\r\\n      \\\"type\\\": \\\"string\\\",\\r\\n      \\\"label\\\": \\\"Entity Name\\\",\\r\\n      \\\"actualname\\\": \\\"EntityName\\\",\\r\\n      \\\"validation\\\": {\\r\\n        \\\"required\\\": true,\\r\\n        \\\"minLength\\\": \\\"\\\",\\r\\n        \\\"maxLength\\\": \\\"\\\",\\r\\n        \\\"pattern\\\": \\\"\\\",\\r\\n        \\\"custom\\\": \\\"\\\"\\r\\n      },\\r\\n      \\\"disable\\\": false,\\r\\n      \\\"condition\\\": {\\r\\n        \\\"show\\\": true\\r\\n      },\\r\\n      \\\"bindLabel\\\": \\\"EntityName\\\",\\r\\n      \\\"update\\\": true\\r\\n    },\\r\\n    \\\"DataSource\\\": {\\r\\n      \\\"type\\\": \\\"select\\\",\\r\\n      \\\"label\\\": \\\"Data Source\\\",\\r\\n      \\\"actualname\\\": \\\"DataSource\\\",\\r\\n      \\\"validation\\\": {\\r\\n        \\\"required\\\": true,\\r\\n        \\\"minLength\\\": \\\"\\\",\\r\\n        \\\"maxLength\\\": \\\"\\\",\\r\\n        \\\"pattern\\\": \\\"\\\",\\r\\n        \\\"custom\\\": \\\"\\\"\\r\\n      },\\r\\n      \\\"condition\\\": {\\r\\n        \\\"show\\\": false\\r\\n      },\\r\\n      \\\"disable\\\": false,\\r\\n      \\\"update\\\": true,\\r\\n      \\\"bindLabel\\\": \\\"name\\\",\\r\\n      \\\"options\\\": []\\r\\n    }\\r\\n  }\",\"DataModel\":\"{\\r\\n    \\\"Parent Entity\\\": {\\r\\n      \\\"type\\\": \\\"singleselect\\\",\\r\\n      \\\"Value\\\": null\\r\\n    },\\r\\n    \\\"EntityType\\\": {\\r\\n      \\\"type\\\": \\\"singleselect\\\",\\r\\n      \\\"Value\\\": null\\r\\n    },\\r\\n    \\\"Entity Name\\\": {\\r\\n      \\\"type\\\": \\\"string\\\",\\r\\n      \\\"Value\\\": null\\r\\n    },\\r\\n    \\\"Data Source\\\": {\\r\\n      \\\"type\\\": \\\"singleselect\\\",\\r\\n      \\\"Value\\\": null\\r\\n    }\\r\\n  }\",\"EntityType\":\"AddNewBi\",\"FormName\":\"AddNewBi\",\"Fabric\":null,\"EntityId\":null,\"Data\":null}";
  
 
  currentCategoryType: any;
  formGroupControl;
  EntityTypesOptions = [];
  NewProperties = {}
  keys = [];
  Lists = [];
  takeUntilDestroyObservables=new Subject();

  sortByProperty = function (property) {
    return function (x, y) {
      return ((x[property].toLowerCase() === y[property].toLowerCase()) ? 0 : ((x[property].toLowerCase() > y[property].toLowerCase()) ? 1 : -1));
    };
  };

  constructor(public commonService: CommonService, public router: Router, public formBuilder: FormBuilder,
    public messagingService: MessagingService, public cdRef: ChangeDetectorRef,
    public dialog: MatDialog, public BIFabricService: BIFabricService,
    public route: ActivatedRoute, private fb: FormBuilder, public dialogRef: MatDialogRef<NewEntityBIComponent>) {
    dialogRef.disableClose = true;

    this.BIFabricService.getlistofdatasource(this.commonService.tenantName);

   

      // this.commonService.isCreateEntity2 = true;

    // this.commonService.allNames$.pipe(this.compUntilDestroyed()).subscribe((res: any) => {
    //   this.List2 = res;
    //   this.bindedParentName = '';
    //   this.parentList = this.commonService.getEntititesBy('EntityType', ['Category'], d => { return { "id": d.EntityId, "text": d.EntityName } })
    //     .map(d => JSON.parse(JSON.stringify(d))).sort(this.sortByProperty('text'));
    // });
        

  }



  createFormGroups() {
    try {
      this.keys.forEach(propertyName => {
        if (propertyName != 'Parent')
          var group = this.formBuilder.control(null, Validators.required);
        else
          var group = this.formBuilder.control(null);
        // subscribing to all the properties for value change event
        this.myForm.addControl(propertyName, group);

        group.valueChanges.subscribe(propertyVal => {

            if (propertyVal) {
              this.createControlAndChangesSubscription(propertyName, propertyVal);
              // this.valueChangesState = true;
              this.cdRef.detectChanges();
            }
        })
      });

    }
    catch (e) {
      console.error('Exception in createFormGroups() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
      this.commonService.appLogException(new Error('Exception in  createFormGroups() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  createControlAndChangesSubscription(name, value) {
    try {
          if (name == 'EntityType') {
            if (value != null) {
              this.BIFabricService.datasourcelist.sort((a, b) => (a.name > b.name) ? 1 : -1)
              this.NewProperties['DataSource']['options'] = this.BIFabricService.datasourcelist;
            } else {
              this.myForm.controls['DataSource'].setValue(null, { emitEvent: false });
            }
            this.NewProperties["DataSource"]['condition']['show'] = value.Value == 'Category' ? false : true;
            if (value.Value == 'Category') {
              this.myForm.removeControl("DataSource");
            } else if (!this.myForm.contains("DataSource")) {
              var group = this.formBuilder.control(null, Validators.required);
              this.myForm.addControl("DataSource", group);
            }
          }
    }
    catch (e) {
      console.error('Exception in createControlAndChangesSubscription() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
      this.commonService.appLogException(new Error('Exception in  createControlAndChangesSubscription() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }



  ClickEvent(ev) {
    switch (ev.id) {
      case 'close':
        this.dialogRef.close();
        this.dialog.ngOnDestroy();
        break;
      case 'save':
        this.nextFormShow();
        break;
    }
  }






  filter(filterValue) {
    if (filterValue) {
      var EntityTypeArray = this.EntityTypesArray.filter(option => option.toLowerCase().includes(filterValue.toLowerCase()));
      if (EntityTypeArray.length == 0) {
        filterValue = filterValue.slice(0, filterValue.length - 1);
        EntityTypeArray = this.EntityTypesArray.filter(option => option.toLowerCase().includes(filterValue.toLowerCase()));
      }
      return EntityTypeArray;
    }
    else {
      return this.EntityTypesArray;
    }
  }

  /**
   * @example example of openDialog method
   * openDialog (messageToshow) {
   *
   *          //todo
   * }
   */

  openDialog(messageToshow): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: false,
        content: [messageToshow],
        subContent: [],
        operation: PopupOperation.Attention
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;

      this.dialog.open(PopUpAlertComponent, matConfig);
    }
    catch (ex) {
      console.error(ex);
    }
  }



  ngOnInit() {
    try {
      let fabricName="";
      this.route.params.pipe(this.compUntilDestroyed()).subscribe(params => {
        let url = this.router.url;
        fabricName = this.commonService.getFabricNameByUrl(url);
      });
      this.myForm = this.formBuilder.group({});
      this.formGroupControl = this.formBuilder.group({});
      fabricName = fabricName == FABRICS.ENTITYMANAGEMENT ? FABRICS.BUSINESSINTELLIGENCE : fabricName;
      this.addNewEntitySchemaSubscription(JSON.parse(this.staticSchema));
      // this.commonService.readAddNewSchema(fabricName).filter(d => d).pipe(this.compUntilDestroyed()).subscribe(res => {
       
      //   this.addNewEntitySchemaSubscription(res);
      // })

    } catch (e) {
      console.error('Exception in ngOnInit() of NewEntityBiComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in ngOnInit() of NewEntityBiComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }





  createBIFabricDataSource(data) {
    let date = new Date().toLocaleDateString();
    var entityID = this.commonService.GetNewUUID();
    var entityType = data.EntityType;
    data.TenantName =  this.commonService.tenantName ;
    data.TenantId =  this.commonService.tenantID ;
    data.CreatedBy =  this.commonService.CurrentUserEntityName ;
    data.ModifiedBy =  this.commonService.CurrentUserEntityName ;
    data.Modified =  date ;
    data.Created =  date ;
    data.EntityId =  entityID ;
    data.Dashboardurl =  "" ;
    data.EntityName =  data["Entity Name"] ;
    data.LastChat = this.commonService.localtoTimestamp();
    data.Parent = data["Parent Entity"] ? data["Parent Entity"].id : "";
    data.ParentType = data["Parent Entity"] ? data["Parent Entity"].type : "";

    if (data["Parent Entity"] == null) {
      data["Parent Entity"] = "";
    }
    var validate = true;

    var found = this.commonService.CheckExistEntityProperty('EntityName', data["Entity Name"], FABRICS.BUSINESSINTELLIGENCE);
    if (found) {
      this.commonService.DeleteEntityType = data["Entity Name"];
      this.commonService.TypeVal = entityType;
      validate = false;
    } else validate = true;

    if (!validate) {
      this.openDialog(ALREADY_EXIST3(this.commonService.TypeVal, this.commonService.DeleteEntityType));
    }

    if (validate == true) {
      var fabric = this.commonService.GetFabricName('/' + this.commonService.lastOpenedFabric);
      fabric = fabric == FABRICS.ENTITYMANAGEMENT ? FABRICS.BUSINESSINTELLIGENCE : fabric;
      data["Payload"] = this.BIFabricService.appendPayload(data);
      var jsonString = JSON.stringify(data);
      var payload = { "EntityId": entityID, "TenantName": this.commonService.tenantName, "TenantId": this.commonService.tenantID, "EntityType": entityType, "Fabric": fabric, "Payload": jsonString };
      var payloadString = JSON.stringify(payload);

      var message = this.commonService.getMessage();
      message.EntityID = entityID;

      message.EntityType = entityType;
      message.Payload = payloadString;
      message.DataType = Datatypes.BUSINESSINTELLIGENCEENTITIES;
      message.MessageKind = "CREATE";
      message.Type = "Details";

      message.Routing = Routing.AllFrontEndButOrigin;
      var Icon = "https://althingsblob.blob.core.windows.net/icons/BICategory_Icon_47x47.svg";;
      if (entityType == "DashBoard") {
        Icon = "https://althingsblob.blob.core.windows.net/icons/Dashboard_BI_Icon_Idle.svg";
        try {
          this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderStart, "Creating Entity...");
          this.commonService.DataSourceSelection = data["Data Source"].name;
            this.BIFabricService.PostNewDashBoard(data.EntityName, message);
            // this.BIFabricService.AddDashBoardCount = 1;
            this.dialogRef.close();
            this.dialog.ngOnDestroy();
        } catch (ex) {

        }

      }
      else {
        this.commonService.BiObservableCount = 1;
        this.commonService.BiObservableCount = 0;
      }
      
      const infoJsonTest: any=JSON.parse(message.Payload);
      const infoJson: any =JSON.parse(infoJsonTest.Payload) ;
      const info: any = this.getInfoJson(infoJson);

      let NewNode: any = {
        'InsertAtPosition': infoJson["Parent Entity"] != "" ? infoJson["Parent Entity"]["id"] :"althingTreeRoot",
        'NodeData': info,
        'SortByName': true
      }
      this.commonService.sendDataToAngularTree1(TreeTypeNames.ALL, TreeOperations.AddNewNode, NewNode);
      this.commonService.sendDataToAngularTree1(TreeTypeNames.BusinessIntelligence, TreeOperations.AddNewNode, NewNode);
      
      this.BIFabricService.newdashboarddata.push({ "Icon": Icon, "EntityType": entityType, "EntityId": entityID, "Name": name, "IsPersonal": "False", "Dashboardurl": "" });
      this.BIFabricService.messageforserver = message;
      if (message.EntityType == "Category") {
        let imessageModel: IModelMessage = this.commonService.getMessageWrapper(  {Payload : message.Payload,DataType : Datatypes.BUSINESSINTELLIGENCEENTITIES,EntityType :  message.EntityType,EntityID :  message.EntityID,MessageKind : "CREATE"});
        this.commonService.postDataToBackend(imessageModel, AppConsts.CREATING).subscribe(response => {
          this.commonService.loadingBarAndSnackbarStatus(AppConsts.loaderComplete, AppConsts.CREATED, AppConsts.LOADER_MESSAGE_DURATION);
          this.BIFabricService.Create = true;
          this.BIFabricService.sendToBiCreate.next(imessageModel);

        });
        this.dialogRef.close();
        this.dialog.ngOnDestroy();
      }
    }
  }

  getInfoJson(infoJson) {
    try {
      var info: any = new Payload();
      info.Capability = infoJson["Capability"] != null ? infoJson["Capability"] : this.commonService.capabilityFabricId;
      info.children = [];
      info.EntityType = infoJson["EntityType"]
      info.EntityName = infoJson["EntityName"]
      if (info.EntityType == EntityTypes.DashBoard)
      info.Dashboardurl = infoJson["Dashboardurl"] != null ? infoJson["Dashboardurl"] : '';
      info.EntityId = infoJson["EntityId"]
      if (infoJson["Parent Entity"])
        info.Parent = infoJson["Parent Entity"].toString() != "" ? [{ EntityId: infoJson["Parent Entity"].id, EntityName: infoJson["Parent Entity"].text }] : [{ EntityName: "lkjhgfdsa098765", EntityId: "lkjhgfdsa098765" }];
      info["SG"] = [FABRICS.BUSINESSINTELLIGENCE];
      info["Capabilities"] = this.commonService.capabilityFabricId;
      return info;
    }
    catch (ex) {

    }
  }



  nextFormShow() {
    if (this.myForm.valid) {
      this.data["Entity Name"] = this.myForm.value.EntityName;
      this.data["EntityType"] = this.myForm.value.EntityType.Value;
      this.data["Parent Entity"] = this.myForm.value.Parent;

      if (this.data["EntityType"] != 'Category') {
        this.data["Data Source"] = this.myForm.value.DataSource;
        this.data["DataSource"] = this.data["Data Source"] ? this.data["Data Source"].name : "";
      }
      this.createBIFabricDataSource(this.data);
    }
    else {
      this.openDialog(SELECTALL_INPUT_FIELDS);
    }    
  }

  

 
 


  ngOnDestroy() {
    try {
      this.commonService.isCreateNewEntity = false;
      this.commonService.Addformclose = false;
      this.DisableCategoryProperty = false;
      this.takeUntilDestroyObservables.next();
      this.takeUntilDestroyObservables.complete();
      if (this.data) {
        this.commonService.allExpandState = false;
        if (this.data.EntityType) { this.data.EntityType = ""; }
        if (this.data["Entity Name"]) { this.data["Entity Name"] = ""; }
        // this.commonService.parentFieldVar = null;
        this.commonService.FormSchemaData$ = undefined;
      }
    } catch (e) {
      console.error('Exception in ngOnDestroy() of NewEntityBiComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in ngOnDestroy() of NewEntityBiComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  compUntilDestroyed():any {
    return takeUntil(this.takeUntilDestroyObservables);
  }

  addNewEntitySchemaSubscription(payloadObj:any){
    try {
      //var payloadObj = JSON.parse(message.Payload);

        var jsonschema = JSON.parse(payloadObj.Schema);
        var datamodel = JSON.parse(payloadObj.DataModel);
        this.commonService.FormSchemaData$ = {
          "Schema": jsonschema, "Data": datamodel
      }
      this.schema = jsonschema;
      this.data = datamodel;
      this.NewProperties = this.schema;

      this.myForm = this.formBuilder.group({});
      this.formGroupControl = this.formBuilder.group({});
      if (!this.commonService.isCapbilityAdmin()) {
        var optionlist = [];
        this.NewProperties['EntityType']['options'].forEach(item => {
          if (item.Value == EntityTypes.DashBoard && this.BIFabricService.permissionScope.DashBoard["Createroles"] && this.BIFabricService.permissionScope.DashBoard["Createroles"].length > 0 && this.BIFabricService.permissionScope.DashBoard["Createroles"][0].Create) {
            optionlist.push(item);
          }
          else if (item.Value == EntityTypes.Category && this.BIFabricService.permissionScope.Category["Createroles"] && this.BIFabricService.permissionScope.Category["Createroles"].length > 0 && this.BIFabricService.permissionScope.Category["Createroles"][0].Create) {
            optionlist.push(item);
          }
        });
        this.NewProperties['EntityType']['options'] = optionlist;
      }
      this.EntityTypesOptions = this.NewProperties['EntityType']['options'];
      this.NewProperties['Parent']['options'] = this.commonService.getEntititesBy('EntityType', ['Category'], d => { return { "id": d.EntityId, "text": d.EntityName } },FABRICS.BUSINESSINTELLIGENCE)
      .map(d => JSON.parse(JSON.stringify(d))).sort(this.sortByProperty('text'));
      this.keys = Object.keys(this.NewProperties);
      this.Lists = [];
      this.keys.forEach((res) => { this.Lists.push(this.NewProperties[res]); });
      this.createFormGroups();

      this.cdRef.detectChanges();

    } catch (e) {
    }
  }
}

class Payload {
  public Capability;
  public children;
  public EntityType;
  public parent;
  public EntityId;
  public EntityName;
  public Icon;

}