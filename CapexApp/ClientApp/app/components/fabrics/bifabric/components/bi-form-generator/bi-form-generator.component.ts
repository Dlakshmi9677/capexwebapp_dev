import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConsts } from 'ClientApp/app/components/common/Constants/AppConsts';
import { CommonService } from 'ClientApp/app/components/globals/CommonService';
import { Datatypes, FABRICS, FabricsNames, MessageKind, TreeOperations, TreeTypeNames } from 'ClientApp/app/components/globals/Model/CommonModel';
import { EntityTypes } from 'ClientApp/app/components/globals/Model/EntityTypes';
import { MessageModel, Routing } from 'ClientApp/app/components/globals/Model/Message';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { EntityCommonStoreQuery } from '../../../../common/state/entity.state.query';
import { BIFabricService } from '../../../../fabrics/bifabric/services/BIFabricService';
import { FDCFormGeratorCommon } from 'ClientApp/app/components/common/services/FDCFormGeneratorCommonService/FDCFormGeratorCommon';
// import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'bi-form-generator',
  templateUrl: './bi-form-generator.component.html',
  styleUrls: ['./bi-form-generator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BIFormGeneratorComponent implements OnInit {

  @Input() EntityId;
  @Input() myForm;
  @Input() tab;
  @Input() EntityType;
  @Input() EntityName;
  @Input() IsEntityManagementForm: boolean;
  @Input() IsEntityMgmtDrag: boolean;

  intialized: boolean;
  expandCollapse: boolean;
  isAuditSubscribe: boolean;
  columnRowSpanCount: number;
  triggerCD: any;
  createDeletePermission: any
  jsonData: any = {}
  jsonAdminData: any = {}
  METER_JSON: any
  properties: any;
  headerGrid: any;
  contentGrid: any;
  ValueChangedFields: any = {}

  contentGrid1 = {
    numberOfCol: 2,
    columnRowSpan: 2
  }

  @Output() formGeneratorOutputEmitter = new EventEmitter();
  @Output() entityMagntCommonOutputEmitter = new EventEmitter();
  takeUntilDestroyObservables=new Subject();
  constructor(public dialog: MatDialog, public commonService: CommonService, public biFabricService: BIFabricService, public formBuilder: FormBuilder, public FDCFormGeratorCommonService: FDCFormGeratorCommon, private cdRef: ChangeDetectorRef, public router: Router, public route: ActivatedRoute, public entityCommonStoreQuery: EntityCommonStoreQuery) {
    this.triggerCD = new Subject();

    this.biFabricService.BICommonObservable.pipe(
      filter((data: any) =>
        data.Type != "AddNewForm" && data.MessageKind == MessageKind.READ && this.EntityId == data.EntityID))
      .pipe(this.compUntilDestroyed())
      .subscribe((message: MessageModel) => {
        this.addNewEntityFormSchemaSubscription(message);
      });



    
    this.commonService.triggerDetectChangesGlobalAdminUtil$.pipe(this.compUntilDestroyed()).subscribe(
      res => {
        this.triggerCD.next("CD");
        this.cdRef.markForCheck();
      },
      error => this.commonService.appLogException(new Error(error)),
      () => { }
    )
  }

  ngOnInit() {
    let payload: any = {}
    this.route.queryParams.pipe(this.compUntilDestroyed()).subscribe((params:any) => {
      try {
          this.expandCollapse = JSON.parse(params.expand)
      }
      catch (err) {
      }
    })

    payload.EntityId = this.EntityId
    payload.EntityType = this.EntityType
    payload.Info = 'Details';
    payload.DataType = EntityTypes.EntityInfo;
    payload.FormCapability = this.tab;
    payload.FormType = "Configuration"
    this.expandCollapse = true;
    payload.Fabric = this.commonService.lastOpenedFabric;
    this.commonService.sendMessageToServer(JSON.stringify(payload), Datatypes.BUSINESSINTELLIGENCE, payload.EntityId, this.EntityType, MessageKind.READ, Routing.OriginSession, payload.Info, FABRICS.BUSINESSINTELLIGENCE);
  }
  addNewEntityFormSchemaSubscription(res) {
    try {
      let payload: any = JSON.parse(res['Payload']);
      this.jsonData = JSON.parse(payload['Data']);
      this.METER_JSON = JSON.parse(payload["Schema"]);
      this.createDeletePermission = { Create: true, Delete: true, Read: true, Update: true };
      this.properties = this.METER_JSON['Properties'];
      this.headerGrid = this.METER_JSON['Schema']['accordion']['header'];
      this.contentGrid = this.METER_JSON['Schema']['accordion']['content'];
      this.METER_JSON["Controls"] = [];
      this.initalizeSchema();

      //Added options in property from state
      this.bindDropdownoptions();

      this.commonService.popupformclose = true;
      this.commonService.RerouteToAddNew = false;
      this.intialized = true
      this.FDCFormGeratorCommonService.MakeDetectChanges(this.cdRef);

      setTimeout(() => {
        //for creating an audit time subscription
        this.isAuditSubscribe = true;
      }, 300);

      setTimeout(() => {
        this.commonService.loadingBarAndSnackbarStatus("", "");
      }, 4000);
      //}
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in addNewEntityFormSchemaSubscription() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  initalizeSchema() {
    try {
      this.setDefaultScope(this.properties, this.commonService.isCapbilityAdmin());
      if (this.contentGrid) {
        this.columnRowSpanCount = 0;
        this.adminSchemaAccordionContent();
      }
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in CommonIterationData() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  setDefaultScope(properties: any, isFabricAdmin: boolean) {
    for (let propertyLabel in properties) {
      properties[propertyLabel].update = true;
      properties[propertyLabel].scope = "Update";
    }
  }
  adminSchemaAccordionContent() {
 
        this.adminSchemaAccordionContentTypeOthers();

  }
  adminSchemaAccordionContentTypeOthers() {

    /**
     * Default Normal accordions data looping
     */


    for (let data of [this.contentGrid.grid.cols, this.headerGrid.grid.cols]) {
      for (let col of data) {
        if (col.type == 'grid' || col.type == 'table') {
          this.FDCFormGeratorCommonService.gridLoop(col, this.properties, this.EntityType, this.METER_JSON, this.EntityName)
        }
      }
    }

    /**
     * to Finding the RowSpan of the accordion
     */
    for (let data of [this.contentGrid.grid.cols]) {
      for (let col of data) {
        if (col.type == 'object') {
          let elementlength = 0;
          col.elements.forEach(cols => {
            if (cols.type == "button")
              elementlength += 0.7;
            else
              elementlength += 1;
          });
          this.columnRowSpanCount = (this.columnRowSpanCount >= elementlength) ? this.columnRowSpanCount : elementlength;
        }
        else if (col.type == 'grid' || col.type == 'table') {
          let colslength = 0;
          col.grid.cols.forEach(column => {
            colslength += Number(column.colspan);
          });
          let nestedRowSpanLength = Math.ceil(colslength / col.grid.numberOfCols)
          this.columnRowSpanCount = (this.columnRowSpanCount >= nestedRowSpanLength) ? this.columnRowSpanCount : nestedRowSpanLength;
          var cols = col.grid.cols;
          this.FDCFormGeratorCommonService.sort(cols)
        }
      }
    }

    this.adminSchemaAccordionSetJSONDataControlAndGroup();
  }

  adminSchemaAccordionSetJSONDataControlAndGroup() {
    this.createFullGroup();
    this.FDCFormGeratorCommonService.MakeDetectChanges(this.cdRef);
    if (this.jsonData != null && this.jsonData['EntityType']) {
      let Unreversedname = this.jsonData['EntityType']

    }

    if (this.jsonData) {
      this.SetFormControlsData(this.jsonData);
    }
  }

  updateProperties() {
    if (this.jsonData.VisurUser) {
      if (this.properties) {
        let excludePropertiesTypesArray = ["Icon", "deleteIcon", "Coll", "VisurUser"];
        for (var propKey in this.properties) {
          let property = this.properties[propKey];
          if (property && excludePropertiesTypesArray.indexOf(propKey) == -1 && property.conditional) {
            property.conditional.show = this.jsonData.VisurUser;
          }
        }
      }
    }
  }

  SetFormControlsData(JsonData, MessageKind?, DataType?) {
    try {
      for (var field in JsonData) {
        let fieldValue;
        if (JsonData[field] != null && JsonData[field] != undefined) {
          fieldValue = JsonData[field];
        }
        if (this.properties != undefined && this.properties[field] && this.properties[field].hasOwnProperty("decimalPrecision")) {
          var decimalplaces = this.properties[field].decimalPrecision;
          fieldValue = Number(fieldValue).toFixed(decimalplaces.maxLength);
        }
        this.SetEachControlData(JsonData, field, fieldValue);
      }
      this.cdRef.detectChanges();
      this.triggerCD.next("CD");
      var setime = 400;

      setTimeout(() => {
        this.isAuditSubscribe = true;
      }, setime);
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in SetFormControlsData() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  SetEachControlData(JsonData, field, fieldValue) {
    try {
      var changed;
      if (this.properties != undefined && this.properties[field]) {
        if (this.properties[field].hasOwnProperty("decimalPrecision")) {
          var decimalplaces = this.properties[field].decimalPrecision;
          fieldValue = isNaN(fieldValue) == false ? fieldValue : 0;
          changed = Number(fieldValue).toFixed(decimalplaces.maxLength);
        } else {
          var measures = fieldValue;
          if (this.properties[field] && (this.properties[field].type == "checkbox" || this.properties[field].type == "boolean" || this.properties[field].type == "toggle") && this.properties[field]["checkboxvalue"]) {
            var checktrue = this.properties[field]["checkboxvalue"].true;
            if (measures == checktrue) {
              changed = true;
            }
            else if (measures == true) {
              changed = true;
            }
            else {
              changed = false;
            }
          } else {
            changed = fieldValue;
          }
          if (this.myForm.controls[field]) {
            if (changed)
              this.myForm.controls[field].setValue(changed, { emitEvent: false });
          }
          else {
            this.myForm.addControl(field, this.formBuilder.control(changed, null));
          }
          return;
        }
      }
      if (JsonData[field] != null && this.properties != undefined && this.properties[field] && (this.properties[field].type != 'Icon' && this.properties[field].type != 'Img' && this.properties[field].type != 'double')) {
        changed = fieldValue;
      } else if (changed == undefined) {
        changed = fieldValue;
      }
      if (this.myForm.controls[field]) {
        this.myForm.controls[field].setValue(changed, { emitEvent: false });
      }
      else {
        this.myForm.addControl(field, this.formBuilder.control(changed, null));
      }
      this.cdRef.detectChanges();
      this.triggerCD.next("CD");
      this.myForm.markAsTouched();
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in  SetEachControlData() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  createFullGroup() {
    try {
      let group;
      let propertyTypeArray = ["img", "Icon", "button"];
      let controls = this.METER_JSON["Controls"];
      controls.filter(propertyName => this.properties[propertyName] ? propertyTypeArray.indexOf(this.properties[propertyName].type) == -1 : false).
        map(propertyName => {
          group = this.CommonCreateGroup(propertyName, this.properties, this.EntityType);
          if (this.myForm.controls[propertyName]) {
            this.myForm.setControl(propertyName, group);
          }
          else {
            this.myForm.addControl(propertyName, group);
          }
          group.valueChanges
            .pipe(this.compUntilDestroyed())
            .distinctUntilChanged().subscribe(propertyVal => {
              try {
                this.valueChanges(propertyName, propertyVal);
              }
              catch (ex) {
                this.commonService.appLogException(new Error('Exception in newControlSubscription() of bi-formgenerator in bi-formgenerator.ts at time ' + new Date().toString() + '. Exception is : ' + ex));
              }
            })
          this.cdRef.markForCheck();
        });

    } catch (e) {
      this.commonService.appLogException(new Error('Exception in createFullGroup() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  public CommonCreateGroup(propertyName, properties, EntityType) {
    try {
      const generalproperties = properties[propertyName];
      let group = this.formBuilder.control({});
      if (generalproperties) {
        var defaultvalue: any;
        defaultvalue = properties[propertyName]["DefaultValue"];
        var maxLength = generalproperties["validation"] != null && generalproperties["validation"]["maxLength"] != "" && generalproperties["validation"]["maxLength"] != null ? +generalproperties["validation"]["maxLength"] : Number.MAX_VALUE;
        var minLength = generalproperties["validation"] != null && generalproperties["validation"]["minLength"] != "" && generalproperties["validation"]["minLength"] != null ? +generalproperties["validation"]["minLength"] : 0;
        var requiredstatus = properties[propertyName]["validation"] != null ? properties[propertyName]["validation"]["required"] : false;
        var pattern = properties[propertyName]["validation"] != null ? properties[propertyName]["validation"]["pattern"] : false;
        if (generalproperties && generalproperties.type == "select" && defaultvalue == "") {
          defaultvalue = null;
        }
        if (
          generalproperties.type == "toggle" ||
          generalproperties.type == "checkbox" ||
          generalproperties.type == "boolean"
        ) {
          group =
            this.formBuilder.control(defaultvalue, {
              updateOn: "change",
              validators: []
            })
        }
        else if (requiredstatus) {
          group = this.formBuilder.control("", {
            validators: [
              Validators.required,
              Validators.pattern(pattern),
              Validators.minLength(minLength),
              Validators.maxLength(maxLength)
            ]
          });
        } else if (!requiredstatus && pattern) {
          group = this.formBuilder.control(defaultvalue, {
            validators: [Validators.pattern(pattern),
            Validators.minLength(minLength),
            Validators.maxLength(maxLength)]
          });
        } else {
          group = this.formBuilder.control(defaultvalue
            , {
              validators: [
                Validators.minLength(minLength),
                Validators.maxLength(maxLength)]
            });
        }
      }
      return group;
    } catch (e) {
      this.commonService.appLogException(
        new Error(
          "Exception in CommonCreateGroup() of PeopleFormGenerator in People-form-generator at time " +
          new Date().toString() +
          ". Exception is : " +
          e
        )
      );
    }
  }
  async CloseForm() {
    try {
      this.commonService.resetControl.next();
      this.router.navigate(['../../'], { relativeTo: this.route, queryParams: { 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
    } catch (error) {

    }
  }


 


  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }

  compUntilDestroyed():any {
    return takeUntil(this.takeUntilDestroyObservables);
  }

  bindDropdownoptions() {
    try {
      if (this.properties) {
        let propertylist = Object.keys(this.properties).filter(propertyName => this.properties[propertyName]["type"] == "select" || this.properties[propertyName]["type"] == "autocomplete");
        propertylist.forEach(propertyName => {
          switch (propertyName) {
            case "Parent":
              if (this.properties.Parent) {
                let categoryid = this.jsonData && this.jsonData.EntityType == EntityTypes.Category ? this.jsonData.EntityId : null;
                this.properties.Parent.options = this.commonService.getEntititesBy('EntityType', ['Category'], d => { return { "id": d.EntityId, "text": d.EntityName, "EntityName": d.EntityName } })
                  .filter((d: any) => categoryid ? d.id != categoryid : true).map(d => JSON.parse(JSON.stringify(d)));
                if (categoryid) {
                  let catagoriesList = this.commonService.getEntititesBy('EntityType', ['Category'], d => { return d }).map(d => JSON.parse(JSON.stringify(d)));
                  catagoriesList.forEach(i => {
                    i.children = catagoriesList.filter(ch => ch.Parent && ch.Parent.length > 0 && ch.Parent[0]["EntityId"] == i.EntityId);
                  });
                  catagoriesList = catagoriesList.filter(d => d.EntityId == categoryid)
                  this.properties.Parent.options = this.properties.Parent.options.filter(item => catagoriesList && catagoriesList.length>0 ? !this.commonService.DoSearch(catagoriesList[0], item.id, false) : true)
                }
                this.commonService.triggerDetectChangesGlobalAdminUtil$.next("CD");
              }
              break;
          }
        });
      }
    }
    catch (ex) {
      this.commonService.appLogException(new Error('Exception in bindDropdownoptions() of entitybaseutil in entitybase.util.ts at time ' + new Date().toString() + '. Exception is : ' + ex));

    }
  }

  eventHandlerSubscribe(data: any) {
    try {
      
    } catch (e) {
      this.commonService.appLogException(new Error('Exception in eventHandlerSubscribe() of BIFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));

    }
  }


  clickEvent(event, index?) {
    try {
      
    }
    catch (e) {
      this.commonService.appLogException(e);
    }
  }

  valueChanges(propertyName, propertyValue, controls?) {
    try {
      if (propertyValue && propertyValue.EntityName) {
        this.ValueChangedFields[propertyName] = propertyValue.EntityName;
      } else {
        this.ValueChangedFields[propertyName] = propertyValue;
      }
      this.BiConfigValueChanges(propertyName, propertyValue)

    } catch (ex) {
      this.commonService.appLogException(
        new Error(
          "Exception in valueChanges() of bi-formgenerator in bi-formgenerator.ts  at time " +
          new Date().toString() +
          ". Exception is : " +
          ex
        )
      );
    }
  }

  BiConfigValueChanges(propertyName, propertyVal) {
    try {
      switch (propertyName) {
        case "EntityName":
          let entitydata = this.commonService.getEntitiesById(this.EntityId);
          if (entitydata) {
            var isExist = this.commonService.CheckExistEntityProperty('EntityName', propertyVal, FABRICS.BUSINESSINTELLIGENCE);
            if ((isExist && propertyVal != entitydata.EntityName) || !propertyVal) {
              this.myForm.controls[propertyName].setErrors({ 'isExist': true });
            }
            else if (propertyVal == entitydata.EntityName) {
              this.myForm.controls[propertyName].setErrors(null);
            }
            else {           
              entitydata.EntityName = propertyVal;
              this.RenameNode(entitydata)
            }
          }
          break;
        case "Parent": {
          let entitydata = this.commonService.getEntitiesById(this.EntityId);
          let parententitydata = propertyVal ? this.commonService.getEntitiesById(propertyVal) : null;
          if (entitydata["Parent"] != (undefined && null)) {
            delete entitydata['Parent'];
            if (parententitydata) {
              entitydata["Parent"] = [];
              entitydata["Parent"][0] = { EntityId: parententitydata.EntityId, EntityName: parententitydata.EntityName };
            }
          }
          else if (parententitydata) {
            entitydata["Parent"] = [];
            entitydata["Parent"][0] = { EntityId: parententitydata.EntityId, EntityName: parententitydata.EntityName };
          }

          //  State Update
          //  -----------------------------------------------------------------------------
          this.entityCommonStoreQuery.upsert(this.EntityId, entitydata);
          
          this.SendmessagetoBackend(entitydata);
          this.commonService.sendDataToAngularTree1(TreeTypeNames.BusinessIntelligence, TreeOperations.deleteNodeById, this.EntityId);
          let NewNode: any = {
            'InsertAtPosition': parententitydata ? parententitydata.EntityId : "althingTreeRoot",
            'NodeData': entitydata
          }
          this.commonService.sendDataToAngularTree1(TreeTypeNames.BusinessIntelligence, TreeOperations.AddNewNode, NewNode);
          this.commonService.sendDataToAngularTree1(TreeTypeNames.BusinessIntelligence, TreeOperations.ActiveSelectedNode, this.EntityId);
        }
          break;
      }
    }
    catch (ex) {
      this.commonService.appLogException(new Error('Exception in BiConfigValueChanges() of bi-formgenerator in bi-formgenerator.ts  at time ' + new Date().toString() + '. Exception is : ' + ex));
    }
  }

  RenameNode(data) {
    try {
      this.entityCommonStoreQuery.upsert(data.EntityId, data);
      this.SendmessagetoBackend(data);
      if (data.EntityType == EntityTypes.DashBoard) {
        this.updateDashboardNameinzoomdata(data);
      }
      let payld = { "EntityId": data.EntityId, "NodeData": data };
      this.biFabricService.sendDataToAngularTree(TreeTypeNames.BusinessIntelligence, TreeOperations.updateNodeById, payld);
    } catch (e) {
      console.error('Exception in RenameNode() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  updateDashboardNameinzoomdata(data) {
    try {
      var split: string[] = data.Dashboardurl.split('+');
      var dashboardid = split[1].split('?')[0];
      var baseurl = this.commonService.zoomdataURl + "/api/dashboards/" + dashboardid;
      this.biFabricService.getClient(baseurl).pipe(this.compUntilDestroyed()).subscribe((response: any) => {
        response.name = data.EntityName;
        this.biFabricService.UpdateDashBoard(response, baseurl);
      });
    } catch (e) {
      console.error('Exception in updateDashboardNameinzoomdata() of BIFabricComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  SendmessagetoBackend(data) {
    try {
      var message = this.commonService.getMessage();
      var modifiedBy = this.commonService.currentUserName;
      var currentDateTime = new Date();
      var date = currentDateTime.toUTCString();
      var modified = date;
      var entityType = data.EntityType;
      var entityID = data.EntityId;
      let newEnitiyName = data.EntityName;
      let parent = data.Parent && data.Parent.length > 0 ? { "id": data.Parent[0]["EntityId"], "text": data.Parent[0]["EntityName"] } : "";
      let parentid = data.Parent && data.Parent.length > 0 ?  data.Parent[0]["EntityId"] : "";
      var datatosend = {
        "EntityName": newEnitiyName, "Modified": modifiedBy, "ModifiedBy": modifiedBy, "Capability": data.Capability, "EntityType": data.EntityType, "ModifiedDateTime": modified,
        "EntityId": data.EntityId, "Parent Entity": parent, "Parent": parentid,
        "TenantName": this.commonService.tenantName, "TenantId": this.commonService.tenantID
      };
      if (entityType == EntityTypes.DashBoard) {
        datatosend["Dashboardurl"] = data.Dashboardurl;
      }
      var jsonString = JSON.stringify(datatosend);
      var payload = { "EntityId": entityID, "TenantName": message.TenantName, "TenantId": message.TenantID, "EntityType": entityType, "Payload": jsonString };
      message.Payload = JSON.stringify(payload);
      this.commonService.sendMessageToServer(message.Payload,Datatypes.BUSINESSINTELLIGENCEENTITIES, entityID, entityType, MessageKind.PARTIALUPDATE, Routing.AllFrontEndButOrigin, "Details", FABRICS.BUSINESSINTELLIGENCE);

    }
    catch (e) {
      console.error('Exception in editListName() of BIFabricComponent at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

}
