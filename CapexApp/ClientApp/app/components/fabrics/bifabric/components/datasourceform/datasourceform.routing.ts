﻿import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddDataSourceComponent } from './datasourceform.component'
const routes: Routes = [
    {
        path: '', component: AddDataSourceComponent
    }
];

export const routing = RouterModule.forChild(routes);