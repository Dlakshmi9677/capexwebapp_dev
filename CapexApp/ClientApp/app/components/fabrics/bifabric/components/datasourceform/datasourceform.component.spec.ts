import { async, inject, TestBed, ComponentFixture, fakeAsync, tick } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { XHRBackend, Response, ResponseOptions } from '@angular/http';
import { HttpClientModule, HttpClient, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';
import { FabricRouter } from '../../../../globals/Model/FabricRouter';
import { AddDataSourceComponent } from './datasourceform.component';
import 'rxjs/add/operator/catch';
import {IAppConfig} from '../../../../globals/Model/app-config.model';
import {AppConfig}  from '../../../../globals/services/app.config';
import 'rxjs/add/operator/do';
import{ActivityCenterService} from '../../../activity-center/services/activity-center.service'
import { ScrollEventModule } from 'ngx-scroll-event';
import 'rxjs/add/operator/toPromise';
import { NO_ERRORS_SCHEMA } from '@angular/core'
import { expand } from 'rxjs/operator/expand';
import { By } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import{ BIFabricService} from '../../services/BIFabricService';
import { Component, HostListener, ComponentFactoryResolver, ViewContainerRef, ViewChild } from '@angular/core';
import { ComponentFixtureAutoDetect } from '@angular/core/testing';
import { CommonService } from '../../../../globals/CommonService';
import{ TaskFabricService} from '../../../taskfabric/services/taskfabric.service'
import { BIFabricComponent } from '../bifabric.component';
import { AddanalysisComponent } from '../addanalysis/addanalysis.component';
import { MessagingService } from '../../../../globals/services/MessagingService';
import { AppInsightsService } from '../../../../common/app-insight/appinsight-service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { ConnectionBackend } from '@angular/http';
import { NgForm } from '@angular/forms';
import { DesktopService} from '../../../../Desktop/DesktopService';
 import { TitleCasePipe } from '../../../../globals/pipes/titlecasepipe';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, Params, } from '@angular/router';
 // declare let env : any;
describe('AddDataSourceComponent component', () => {

    let component: AddDataSourceComponent;
    let fixture: ComponentFixture<AddDataSourceComponent>;
    let commonservice;
    let messagingservice;
    let appconfig: AppConfig;
    let service;
    let appIService;
    let collaborationservice;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule, RouterTestingModule, ScrollEventModule,HttpClientModule
            ],
            declarations: [AddDataSourceComponent,NgForm,TitleCasePipe],
          providers: [
                CommonService, MessagingService, BIFabricService, DesktopService, AppInsightsService,AppConfig, TaskFabricService, ActivityCenterService,
               { provide: 'BASE_URL', useValue: "http://localhost" },
                { provide: 'instrumentationKey', useValue: '36935597-7d97-4635-ab0b-409a8c6e4223' },
                { provide: 'instrumentationKey', useValue: '36935597-7d97-4635-ab0b-409a8c6e4223' },
                { provide: XHRBackend, useClass: MockBackend }, { provide: 'BASE_URL', useValue: 'http://localhost' }
                ,
            ],
            schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();
    }));
      beforeEach(inject([AppConfig], AppService => {
    appconfig = AppService;
    appconfig.CreateBaseURL();
  }));
    beforeEach(inject([CommonService], (service) => {
        commonservice = service;
    }));
        beforeEach(inject([MessagingService], (service) => {
        messagingservice = service;
         messagingservice.initializeWebSocketConnection();
    }));

    // beforeEach(inject([BIFabricService], (service) => {
    //     collaborationservice = service;
    // }));
     
    
    beforeEach(() => {
        fixture = TestBed.createComponent(AddDataSourceComponent);
        const app = fixture.debugElement.componentInstance;
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
   it('CreateComponent', async(() => {
       expect(component).toBeTruthy();
   }));

        it('UpdateForm', async(() => {
          var r1 = {"payload":{$schema:"http://json-schema.org/draft-04/schema#",additionalProperties:false,Properties:{DataSource:{options:{0:'TankDatasource',1:'TruckTicketDatasource'}},required:["DataSource"]}}};
          var r2 = {"payload":{DataSource:{Value:{0:'TankDatasource',1:'TruckTicketDatasource'}},Type:"data"}};
         fixture.componentInstance.EntityName = "Select DataSource";   
         var res = [r1,r2];  
          fixture.detectChanges();
         // spyOn(fixture.componentInstance,'ViewProperties');
          fixture.detectChanges();
          fixture.componentInstance.UpdateForm(r1);
          fixture.detectChanges();
         // expect(fixture.componentInstance.ViewProperties).toHaveBeenCalled();
          expect(fixture.componentInstance.dbFormVisible).toBeTruthy();
           
        }));
    
     it('CloseForm', async(() => {
      var url = '/' + FabricRouter.BUSINESSINTELLIGENCE_FABRIC;
         let navigateSpy = spyOn((<any>component).router, 'navigate');
            fixture.detectChanges(); 
            fixture.componentInstance.CloseForm();
            fixture.detectChanges();
             expect(navigateSpy).toHaveBeenCalledWith([url]);
    }));
              it('validateForm', async(() => {
              //  fixture.componentInstance.data["Datasource"]= ''
            fixture.componentInstance.data["DataSource"].Value = "FFVDatasource";
            fixture.componentInstance.BIFabricService.datasourcelist = [{id:"5b682afe60b2e013c416d243",name:"TankDatasource"},
                {id:"5b682d1b60b2e013c416d2e6",name:"TruckTicketDatasource"},
                {id:"5b682e6260b2e013c416d32e",name:"PDMeterDatasource"},
                {id:"5b68285460b2e013c416cf31",name:"FFVDatasource"},
                {id:"5b68286a60b2e013c416cf67",name:"BatteryDatasource"},
                {id:"5b68287760b2e013c416d08b",name:"TurbineDatasource"},
                
                ];
            fixture.detectChanges();
            fixture.componentInstance.ValidateForm();
            fixture.detectChanges();

              }));
           
});