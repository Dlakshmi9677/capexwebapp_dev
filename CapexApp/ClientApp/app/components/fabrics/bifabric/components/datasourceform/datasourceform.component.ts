import { Subscription, Subject } from 'rxjs';
import { Component, Input, ComponentFactoryResolver, ComponentRef} from '@angular/core';
import { CommonService } from '../../../../globals/CommonService';
import { Router } from '@angular/router';
import { MessagingService } from '../../../../globals/services/MessagingService'
import { FabricRouter } from '../../../../globals/Model/FabricRouter';
import { AppInsightsService } from '../../../../common/app-insight/appinsight-service';
import { BIFabricService } from '../../../../fabrics/bifabric/services/BIFabricService';
import { UUIDGenarator } from 'visur-angular-common'
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PopupOperation } from '../../../../globals/Model/AlertConfig';
import { PopUpAlertComponent } from '../../../../common/PopUpAlert/popup-alert.component';
// import { untilDestroyed } from 'ngx-take-until-destroy';
import { CHART_TYPE, DATA_SOURCE } from 'ClientApp/app/components/common/Constants/AttentionPopupMessage';
import { takeUntil } from 'rxjs/operators';

declare let $: any;

@Component({
  selector: 'datasourceform',
  templateUrl: './datasourceform.component.html',
  styleUrls: ['./datasourceform.component.style.scss']
})
export class AddDataSourceComponent {
  json;
  CardSchemaData$: Subscription;
  data
  SchemaData$: Subscription;
  static _ref: ComponentRef<AddDataSourceComponent>;
  static componentRef;
  EntityName
  username
  DataModelJson
  SchemaJson
  dbFormVisible: boolean = false;
  @Input() Operation;
  htmlFormSchema: Array<any>
  isValid = true;
  parentname
  NameList
 // zoomdataURl: any;
  takeUntilDestroyObservables=new Subject();




  /**
  * Constructor for injecting the serivices in @class AddDataSourceComponent
  * @param logger for injecting all services from AppInsightsService
  * @param BIFabricService Injects fdc service like BI module  observables ,variables or any services can be shared among all components
  * @param commonService Injects common service like common observables ,variables or any services can be shared among all components
  * @param router Injects angular routing service with which routing or navigation can be done
  * @param http http For injecting http service
  * @example example of constructor
  * constructor(public commonService: CommonService, public router: Router, public http: Http){
  *   //Todo
  * }
  */


  constructor(private logger: AppInsightsService, public commonService: CommonService, public router: Router,
    public messagingService: MessagingService, public BIFabricService: BIFabricService,
    public componentFactoryResolver: ComponentFactoryResolver,
    public dialog: MatDialog) {
    let Dashboardlist = [];

    if (FabricRouter.BUSINESSINTELLIGENCE_FABRIC) {
      this.EntityName = "Select DataSource";
      Dashboardlist = [];
      this.commonService.isDisable = true;
      this.BIFabricService.datasourcelist.forEach((res: any) => {
        try {
          Dashboardlist.push(res['name']);
          //this.BIFabricService.Dashboardlist.push(res['name']);
        }
          catch (e) {
          console.error('Exception in datasourcelist Observable of datasourceformComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
          }
      });


      this.BIFabricService.newdashboardobservable$.pipe(this.compUntilDestroyed()).subscribe((res) => {
        try {
          if (this.commonService.lastOpenedFabric == FabricRouter.BUSINESSINTELLIGENCE_FABRIC) {
            this.ValidateForm();
          }
        } catch (e) {
          console.error('Exception newdashboardobservable$  Observable of datasourceformComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
        }
      });

      let schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "object",
        "title": "Select Data Source",
        "additionalProperties": false,
        "required": ["DataSource"],
        "properties": {
          "DataSource": {
            "type": "enum",
            "options": Dashboardlist
          }
        }
      };

      let data = {
        "DataSource": {
          "type": "string",
          "Value": Dashboardlist
        }
      };

      var data1 = { "type": "schema", "payload": schema };
      var data2 = { "type": "data", "payload": data };
      this.UpdateForm([data1, data2]);

    }
    this.commonService.isDisable = true;

  } //constractor End

  /**
   * @class AddDataSourceComponent
   * @method UpdateForm
   * @description Triggers when user submits the AddNewEntity form of EntityType:DashBoard
   * DastaSource form generated with fields exists in schema
   * @param {Array<any>}res: Array contains type and Payload 
   * @example example of UpdateForm method
   * UpdateForm () {
   * 
   *          //todo
   * }
   */
  UpdateForm(res) {
    try {
      res.forEach(json => {
        if (json['type'] == "schema") {
          this.SchemaJson = json["payload"];
          this.ViewProperties(json['payload']);
        } else if (json['type'] == "data") {
          this.data = json['payload'];
          this.commonService.DataSourceSelection = this.data;
          this.DataModelJson = this.data;
          if (this.htmlFormSchema) {
            this.dbFormVisible = true;
          }

        }
      })

    } catch (e) {
      console.error('Exception in UpdateForm() of datasourceformComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in UpdateForm() of datasourceformComponent at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e));
    }
  }

  /**
   * @class AddDataSourceComponent
    * @method UpdateForm
    * @description For showing the list of json schema which is called form UpdateForm() method
    * @param {object} categoryJSON categoryJSON is the list of json schema
    * @example 
    * This is the example to ViewProperties
    * ViewProperties(categoryJSON){
    *  // Todo
    * }
    */
  @Input() schema
  ViewProperties(categoryJSON) {
    try {
      this.htmlFormSchema = null;
      this.addObjectInfoSchema(categoryJSON);
    } catch (e) {
      console.error('Exception in ViewProperties() of datasourceformComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in ViewProperties() of datasourceformComponent at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e));
    }
  }

  openDialog(messageToshow): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: false,
        content: [messageToshow],
        subContent: [],
        operation: PopupOperation.Attention
      };
      let matConfig = new MatDialogConfig()
      matConfig.data = config;
      matConfig.width = '500px';
      matConfig.disableClose = true;

      this.dialog.open(PopUpAlertComponent, matConfig);
    }
    catch (e) {
      console.error('Exception in openDialog() of datasourceformComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  ChartLoad1 = false;
  listofchart = [];
  sourceSelected;
  showChartList = false;
  ShoWChartList(event) {
    try {
      this.ChartLoad1 = true;
      // this.BIFabricService.DashboardCustomchartlist = [];
      this.sourceSelected = event.value;
      var data = event.value;
      var datasourceid;
      var visualid;
      var dashboardjson = [];
      var result = [];
      var guid = UUIDGenarator.generateUUID().toString();
      this.BIFabricService.datasourcelist.forEach((res: any) => {
        if (data == res['name']) {
          datasourceid = res['id'];
        }
      });

      if (datasourceid) {
        $("#ChartDiv").css({ "opacity": "0.5" });
        let url = this.commonService.zoomdataURl + "/api/visdefs/" + datasourceid;
        this.BIFabricService.SelecteddatasourceID = datasourceid;
        this.BIFabricService.getClient(url).pipe(this.compUntilDestroyed()).subscribe((response: any) => {
          result = response;
          // this.BIFabricService.Dashboardchartlist = result;
          // this.BIFabricService.DashboardCustomchartlist = [];
          this.listofchart = [];
          response.forEach((ele: any) => {
            if (ele["type"].toString().trim() == "CUSTOM") {
              var icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Bars_Histogram.svg";

              switch (ele['name']) {

                case "2_level_pie":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Two_Level_Pie.svg";
                  ele['name'] = "Two_Level_Pie";
                  break;
                case "Area charts":
                case "Area charts (1)":
                case "Area_charts":
                case "AreaCharts.":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Area.svg";
                  break;
                case "Bars__horizontal":
                case "Bars horizontal":
                case "Bars horizontal clustered":
                case "Bars: HorizontalClustered":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Bars_Horizontal.svg";
                  break;
                case "Bars vertical stacked multimetric":
                case "Bars: Vertical Stacked MultiMetric":
                case "Bars__vertical_stacked_multimetric":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Bars_Multiple_Metrics.svg";
                  break;
                case "Bars histogram":
                case "Bars_histogram":
                case "Bars:Histogram":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Bars_Histogram.svg";
                  break;
                case "Bars horizontal stacked":
                case "Bars_horizontal_stacked":
                case "Bars horizontal stacked old":
                case "Horizontal_Stacked_Bar":
                case "Bars:Horizontal_Stacked_Old":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Bars_Horizontal.svg";
                  break;
                case "Bars vertical clustered":
                case "Bars_vertical_clustered":
                case "Bars_VerticalClustered":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Bars_Vertical_Clusters.svg";
                  break;
                case "Donut Chart":
                case "Donut.":
                case "Donut_Chart":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Donut.svg";
                  break;
                case "Kpi chart":
                case "Kpi_Chart":
                case "KPI_New":
                case "KPI.":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/KPI.svg";
                  break;
                case "line___bars_trend":
                case "Line & Bars Trend.":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Lines_and_Bars_Trend.svg";
                  break;
                case "Line trend attribute values":
                case "line_trend_attribute_values":
                case "Line Trend:Attribute Values":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Line_Trend_Attribute_Values.svg";
                  break;
                case "Line trend multiple metrics":
                case "line_trend_multiple_metrics":
                case "Line Trend:Multiple Metrics":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Line_Trend_Multiple_Metrics.svg";
                  break;
                case "Packed bubbles":
                case "Packed_bubbles":
                case "Packed Bubbles.":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Packed_Bubbles.svg";
                  break;
                case "Pie chart":
                case "Pie_Chart":
                case "Pie.":
                case "2_Level_Pie.":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Pie.svg";
                  break;
                case "Scatter plot":
                case "Scatter_plot":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Scatter_Plot.svg";
                  break;
                case "Tree map":
                case "Tree_map":
                case "Tree Map.":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Tree_Map.svg";
                  break;
                case "Vertical stacked clustered":
                case "Vertical_Stacked_Clustered":
                case "Vertical_stacked_cluster_chart":
                case "Bars:Vertical_Stacked_Clustered":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Vertical_Stacked_Clusters.svg";
                  break;
                case "Bars_vertical_stacked":
                case "Bars:Vertical_Stacked":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Bars_Vertical_Stacked.svg";
                  break;
                case "Box_plot":
                case "Box Plot.":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Box_Plot.svg";
                  break;
                case "Floating_bubbles":
                case "Floating Bubbles.":
                case "Floating_bubbles (1)":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Floating_Bubbles.svg";
                  break;
                case "Heat_map":
                case "Heat Map.":
                  icon = "wwwroot/images/Bi/BI_Chart_Icons_v1/Heat_Map.svg";
                  break;
              }
              this.listofchart.push({ "name": ele['name'], "icon": icon, "visId": ele['visId'] });
            }
            $("#ChartDiv").css({ "opacity": "1" });
            this.ChartLoad1 = false;
          });
        });
        this.showChartList = true;


      }
    } catch (e) {
      console.error('Exception in ShoWChartList() of datasourceformComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
    }






  /**
 *@class AddDataSourceComponent
 *@method addObjectInfoSchema
 * @description To display fields according to schema
 * @example example of addObjectInfoSchema(schemaObject) method
 *  addObjectInfoSchema(schemaObject){
 *     //todo
 *    }
 */




  addObjectInfoSchema(schemaObject) {
    try {
      this.htmlFormSchema = new Array<any>();
      if (this.data) {
        this.dbFormVisible = true;
      }
      try {
        schemaObject = JSON.parse(schemaObject)
      } catch (e) {
        console.error("notJson");
      }
      finally {
        this.SchemaJson = schemaObject;
      }
      if (schemaObject !== undefined) {
        var requiredFields = schemaObject["required"];
        var properties = schemaObject["properties"];

        for (var index in requiredFields) {
          var labelName = requiredFields[index];
          var propertyType = properties[labelName]["type"]
          var propertyclass = properties[labelName]["class"]
          var propertywideClass = properties[labelName]["wideclass"]

          var propertyValues = properties[labelName]["options"]

          this.htmlFormSchema.push({
            "Label": labelName == 'DataSource' ? 'Data Source' :labelName,
            "actualName": labelName ,
            "Type": propertyType,
            "Class": propertyclass,
            "Values": propertyValues,
            "Wideclass": propertywideClass,
          });

        }

      }
 
    } catch (e) {
      console.error('Exception in addObjectInfoSchema() of datasourceformComponent  at time ' + new Date().toString() + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in addObjectInfoSchema() of datasourceformComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  /**
      * @class AddDataSourceComponent
      * @method ValidateForm
      * @description to validate and save the user submitted form data to backend
      * @event will trigger when user click on save button exist in userform
      * @example example of ValidateForm method
      * ValidateForm () {
      * 
      *      //todo
      * }
      */

  ValidateForm() {
    try {
      $("#dbformTemplatePosition").css({
        "display": "none"
      });
    //  let url = '/' + FabricRouter.BUSINESSINTELLIGENCE_FABRIC;
    //  this.router.navigate([url]);
      var data = this.data['DataSource']['Value'];
      var datasourceid;
      var visualid;
      var dashboardjson = [];
      var result = [];
      var guid = UUIDGenarator.generateUUID().toString();
      this.BIFabricService.datasourcelist.forEach((res: any) => {
        if (data == res['name']) {
          datasourceid = res['id'];
        }
      });

      if (datasourceid) {
        let url = this.commonService.zoomdataURl + "/api/visdefs/" + datasourceid;
        this.BIFabricService.SelecteddatasourceID = datasourceid;
        this.BIFabricService.getClient(url).pipe(this.compUntilDestroyed()).subscribe((response: any) => {
          result = response;
          // this.BIFabricService.Dashboardchartlist = result;
          // this.BIFabricService.DashboardCustomchartlist = [];
          // response.forEach((ele: any) => {
          //   if (ele["type"].toString().trim() == "CUSTOM") {
          //     this.BIFabricService.DashboardCustomchartlist.push(ele);
          //   }
          // });
            result.forEach(res => {
              if (res['type'] == "RAW_DATA_TABLE") {
                visualid = res['visId'];
              }
            });
            if (visualid) {
              var url = this.commonService.zoomdataURl + "/api/visdefs/" + datasourceid + "/" + visualid;
              this.BIFabricService.getClient(url).pipe(this.compUntilDestroyed()).subscribe((res: any) => {
                var resultarray = [];
                res.widgetId = guid;
                res.ownerName = this.commonService.zoomdataadminusername;
                res.layout = { "col": 1, "row": 1, "rowSpan": 12, "colSpan": 16 };
                delete res['id'];
                resultarray = res;
                if (this.BIFabricService.databoardjson.length > 1)
                  this.BIFabricService.databoardjson.splice(0, 1);
                dashboardjson = JSON.parse(this.BIFabricService.databoardjson[0]['json']);
                dashboardjson['visualizations'] = [resultarray];
                dashboardjson['selectedWidgetId'] = guid;
                dashboardjson['ownerName'] = this.commonService.zoomdataadminusername;
                this.BIFabricService.PutNewChartDashBoard(dashboardjson).pipe(this.compUntilDestroyed()).subscribe((res: any) => {
                  var dburl = this.commonService.zoomdataURl + "/api/dashboards/" + res['id'] + "/key?expirationDate=2118-09-15T19:15:30.000";
                  this.BIFabricService.getClient(dburl).pipe(this.compUntilDestroyed()).subscribe((res: any) => {
                    this.BIFabricService.dashboarurl = res['accountId'] + "+" + this.BIFabricService.dashboardid + "?__target=embedded&key=" + res['token'];
                    this.commonService.BiObservableCount = 1;
                    this.BIFabricService.callNewDashboardObservable.next(this.BIFabricService.dashboarurl);
                  });
                });
              });
            }
          
        });
      }

    //  document.getElementById("BIFabricDrawingArea").style.opacity = "unset";

    } catch (e) {
      console.error('Exception in ValidateForm() of datasourceformComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in ValidateForm() of datasourceformComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  AnalysisValidate() {
    try {

      if (this.sourceSelected) {
        if (this.chartSelected) {
          this.AnalysisValidate2();
        } else
          this.openDialog(CHART_TYPE);

      } else
        this.openDialog(DATA_SOURCE);
    } catch (e) {
      console.error('Exception in AnalysisValidate() of datasourceformComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
      this.logger.logException(new Error('Exception in AnalysisValidate() of datasourceformComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }


  ChartLoad = false;
  visualid;
  form2 = false;
  AnalysisValidate2() {
    try {
      document.getElementById("dbAddNewFormContent1").style.display = "none";
      this.ChartLoad = true;
      this.form2 = true;
      var guid = UUIDGenarator.generateUUID().toString();
      var dashboardjson: Array<string> = [];
      //get visualizationId of selected chart
      this.listofchart.forEach(data => {
        if (data.name == this.chartSelected) {
          this.visualid = data.visId;

        }

      })
      var visualid = this.visualid;
      if (visualid) {

      }

      var datasourceid = this.BIFabricService.SelecteddatasourceID;
      var dashboardid = this.BIFabricService.dashboardid;
      var visualurl = this.commonService.zoomdataURl + "/api/visdefs/" + datasourceid + "/" + visualid;
      var dashboardurl = this.commonService.zoomdataURl + "/api/dashboards/" + dashboardid;

      /**
       * @description called from dataSource component 
       * through RestAPI getting response of dashboard and Visualization info
       */
      this.BIFabricService.getClient(dashboardurl).pipe(this.compUntilDestroyed()).subscribe((response: any) => {
        try {
          dashboardjson = response;
          this.BIFabricService.getClient(visualurl).pipe(this.compUntilDestroyed()).subscribe((res: any) => {
            var resultarray = {};
            res.widgetId = guid;
            res.ownerName = this.commonService.zoomdataadminusername;
            var length: number = dashboardjson['visualizations'].length;
            dashboardjson['visualizations'][0]['layout'] = { "col": 1, "row": 1, "rowSpan": 6, "colSpan": 8 };

            if (length != 0) {
              var last = length - 1;
              var col = dashboardjson['visualizations'][last]['layout']['col'];
              var row = dashboardjson['visualizations'][last]['layout']['row'];
              var newrow = row + 6;
              col = col == 1 ? 9 : 1;
              if (row != 1 && newrow != row) {
                row = row + 6;
              }
              res.layout = { "col": col, "row": row, "rowSpan": 6, "colSpan": 8 };
            }
            delete res['id'];
            resultarray = res;
            dashboardjson['visualizations'].push(resultarray);
            dashboardjson['selectedWidgetId'] = guid;
            dashboardjson['ownerName'] = this.commonService.zoomdataadminusername;


            /**
             * @description : request for add chart into the dashBoard 
             */
            this.BIFabricService.PutNewChartDashBoard(dashboardjson).pipe(this.compUntilDestroyed()).subscribe((res: any) => {
              var dburl = this.commonService.zoomdataURl + "/api/dashboards/" + res['id'] + "/key?expirationDate=2118-09-15T19:15:30.000";
              this.BIFabricService.getClient(dburl).pipe(this.compUntilDestroyed()).subscribe((res: any) => {
                this.BIFabricService.dashboarurl = res['accountId'] + "+" + this.BIFabricService.dashboardid + "?__target=embedded&key=" + res['token'];
                this.commonService.BiObservableCount = 1;
                this.BIFabricService.ChartCreate = true;
                this.BIFabricService.callNewDashboardObservable.next(this.BIFabricService.dashboarurl);
                this.commonService.BiObservableCount = 0;
                this.ChartLoad = false;

                setTimeout(() => {
                  this.form2 = false;
                  this.CloseForm();
                }, 1000);


              });
            });
          });
        } catch (e) {
          console.error('Exception in getClient Observable of datasourceformComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
        }
      });
    }
    catch (e) {
      console.error('Exception in AnalysisValidate2()   of datasourceformComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  chartSelected;
  chartSelectionChange(event) {
    this.chartSelected = event.value;
  }

  /**
  * @class AddDataSourceComponent
  * @method CloseForm
  * @description to close the form and routing to "../bifabric" url
  * @event will trigger when user click on close button exist in userform
  * @example example of CloseForm method
  * CloseForm () {
  * 
  *      //todo
  * }
  */
  CloseForm() {
    try {
      document.getElementById("BIFabricDrawingArea").style.opacity = "unset";
      if (this.form2) {

        this.form2 = false;
        let url = '/' + FabricRouter.BUSINESSINTELLIGENCE_FABRIC;
        this.router.navigate([url]);
      }
      var url = '/' + FabricRouter.BUSINESSINTELLIGENCE_FABRIC;
      this.router.navigate([url]);
    } catch (e) {
      console.error('Exception in CloseForm() of datasourceformComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  /**
  * @class AddDataSourceComponent
  * @method destroy
   * @description to destroy variables
  * @example example of destroy method
  * destroy () {
  * 
  *      //todo
  * }
  */

  ngOnDestroy() {
    try {
      if (AddDataSourceComponent._ref) {
        AddDataSourceComponent._ref.destroy();
        AddDataSourceComponent._ref = null;
        this.takeUntilDestroyObservables.next();
        this.takeUntilDestroyObservables.complete();
      }
    } catch (e) {
      console.error('Exception in ngOnDestroy() of datasourceformComponent  at time ' + new Date().toISOString().slice(0, 19).replace('T', ' ') + '. Exception is : ' + e);
    }
  }

  compUntilDestroyed():any {
    return takeUntil(this.takeUntilDestroyObservables);
  }

  checkfield() {
    var temp = this.commonService.DataSourceSelection.DataSource.Value;
    this.isValid = typeof temp != "string" ? true : false;
  }

}
