import { async, inject, TestBed, ComponentFixture, fakeAsync, tick } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { XHRBackend, Response, ResponseOptions } from '@angular/http';
import { Observable } from 'rxjs';
import {HttpClientModule, HttpClient } from '@angular/common/http'; 
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';


import {IAppConfig} from '../../../globals/Model/app-config.model';
import {AppConfig}  from '../../../globals/services/app.config';
import 'rxjs/add/operator/do';
import{ActivityCenterService} from '../../../fabrics/activity-center/services/activity-center.service'
import { ScrollEventModule } from 'ngx-scroll-event';
import 'rxjs/add/operator/toPromise';
import { DesktopService} from '../../../Desktop/DesktopService';
import { NO_ERRORS_SCHEMA } from '@angular/core'
// import { expand } from 'rxjs/operator/expand';
import { By } from '@angular/platform-browser';
import { FabricRouter } from '../../../globals/Model/FabricRouter';
import { Routes, RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import{ BIFabricService} from '../services/BIFabricService';
import { Component, HostListener, ComponentFactoryResolver, ViewContainerRef, ViewChild } from '@angular/core';
import { ComponentFixtureAutoDetect } from '@angular/core/testing';
import { CommonService } from '../../../globals/CommonService';
import{ TaskFabricService} from '../../../fabrics/taskfabric/services/taskfabric.service'
import { BIFabricComponent } from './bifabric.component';

import { MessagingService } from '../../../globals/services/MessagingService';
import { AppInsightsService } from '../../../common/app-insight/appinsight-service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { ConnectionBackend } from '@angular/http';
import { NgForm } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, Params, } from '@angular/router';
declare let $: any;
 // declare let env : any;
describe('BIFabricComponent component', () => {

    let component: BIFabricComponent;
    let fixture: ComponentFixture<BIFabricComponent>;
    let commonservice;
    let messagingservice;
    let appconfig: AppConfig;
    let service;
    let appIService;
    let collaborationservice;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule, RouterTestingModule, ScrollEventModule,HttpClientModule
            ],
            declarations: [BIFabricComponent,NgForm],
          providers: [
                CommonService, MessagingService, DesktopService, BIFabricService, AppInsightsService,AppConfig, TaskFabricService, ActivityCenterService,
               { provide: 'BASE_URL', useValue: "http://localhost" },
                { provide: 'instrumentationKey', useValue: '36935597-7d97-4635-ab0b-409a8c6e4223' },
                { provide: 'instrumentationKey', useValue: '36935597-7d97-4635-ab0b-409a8c6e4223' },
                { provide: XHRBackend, useClass: MockBackend }, { provide: 'BASE_URL', useValue: 'http://localhost' }
                ,
            ],
            schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
        }).compileComponents();
    }));
       beforeEach(inject([AppConfig], AppService => {
    appconfig = AppService;
    appconfig.CreateBaseURL();
  }));
    beforeEach(inject([CommonService], (service) => {
        commonservice = service;
    }));
        beforeEach(inject([MessagingService], (service) => {
        messagingservice = service;
         messagingservice.initializeWebSocketConnection();
    }));

    // beforeEach(inject([BIFabricService], (service) => {
    //     collaborationservice = service;
    // }));
     
    
    beforeEach(() => {
        fixture = TestBed.createComponent(BIFabricComponent);
        const app = fixture.debugElement.componentInstance;
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    
    it('SendAddNewDashboardObservable', async(() => {  
       
       var res = '{"MessageId":"b18f2eee-9f53-4057-a4b0-3c5b2c13b145","ClientID":"594de983-5f1a-4f38-b398-0aca04515bd3","TenantName":"althingin","TenantID":"42ab3f0e-8a7f-4f23-9cb3-16bf89a38bed","UserID":"c3f759fb-ecc6-4b83-3ab9-80c757ab5ce4","EntityID":"cdf1efb3-9f96-4ffb-9122-eb674322128f","EntityType":"DashBoard","PayLoad":"{\"EntityId\":\"cdf1efb3-9f96-4ffb-9122-eb674322128f\",\"TenantName\":\"althingin\",\"TenantId\":\"42ab3f0e-8a7f-4f23-9cb3-16bf89a38bed\",\"EntityType\":\"DashBoard\",\"Fabric\":\"BI\",\"EntityInfoJson\":\"{\\\"Capability\\\":{\\\"type\\\":\\\"string\\\",\\\"Value\\\":\\\"Business Intelligence\\\"},\\\"EntityType\\\":{\\\"type\\\":\\\"string\\\",\\\"Value\\\":\\\"DashBoard\\\"},\\\"Name\\\":{\\\"type\\\":\\\"string\\\",\\\"Value\\\":\\\"SendNew\\\"},\\\"Parent Entity\\\":{\\\"type\\\":\\\"singleselect\\\",\\\"Value\\\":{\\\"id\\\":\\\"34831a1b-8726-4b03-84bc-1a1ebac44811\\\",\\\"text\\\":\\\"demo23\\\"}},\\\"TenantName\\\":{\\\"Type\\\":\\\"String\\\",\\\"Value\\\":\\\"althingin\\\"},\\\"TenantId\\\":{\\\"Type\\\":\\\"String\\\",\\\"Value\\\":\\\"42ab3f0e-8a7f-4f23-9cb3-16bf89a38bed\\\"},\\\"CreatedBy\\\":{\\\"Type\\\":\\\"String\\\",\\\"Value\\\":\\\"umarfaraz\\\"},\\\"ModifiedBy\\\":{\\\"Type\\\":\\\"String\\\",\\\"Value\\\":\\\"umarfaraz\\\"},\\\"Modified\\\":{\\\"Type\\\":\\\"String\\\",\\\"Value\\\":\\\"8/16/2018\\\"},\\\"Created\\\":{\\\"Type\\\":\\\"String\\\",\\\"Value\\\":\\\"8/16/2018\\\"},\\\"EntityId\\\":{\\\"Type\\\":\\\"String\\\",\\\"Value\\\":\\\"cdf1efb3-9f96-4ffb-9122-eb674322128f\\\"},\\\"Dashboardurl\\\":{\\\"Type\\\":\\\"String\\\",\\\"Value\\\":\\\"\\\"}}\"}","DataType":"bifabric","MessageKind":"CREATE","Type":"Details","Routing":"AllFrontEndButOrigin"}';
        fixture.componentInstance.bIFabricService.SendAddNewDashboardObservable.next(res);
         expect(fixture.componentInstance.messageforserver).toBeDefined();
 spyOn(component.messageService,'sendMessageToserver');

 var s = fixture.componentInstance.messageforserver;
 
 fixture.componentInstance.messageforserver = s;
    //fixture.componentInstance.messageforserver = '{clientID:"f19b8d19-7f71-4abb-b4a1-81be71e11dd6",DataType:"bifabric",EntityID:"dd73cfbb-0f29-4de9-8746-b71e10a6793f",EntityType:"DashBoard",MessageId:"5608f8e6-860c-4516-90b6-7249c3c3f8b2",MessageKind:"CREATE",PayLoad:"{"EntityId":"dd73cfbb-0f29-4de9-8746-b71e10a6793f","TenantName":"althingin","TenantId":"42ab3f0e-8a7f-4f23-9cb3-16bf89a38bed","EntityType":"DashBoard","Fabric":"BI","EntityInfoJson":"{\'Capability\':{\'type\':\'string\',\'Value\':\'Business Intelligence\'},\'EntityType\':{\'type\':\'string\',\'Value\':\'DashBoard\'},\'Name\':{\'type\':\'string\',\'Value\':\'when\'},\'Parent Entity\':{\'type\':\'singleselect\',\'Value\':{\'id\':\'257fbf81-4e5d-4e78-8764-d136d3b49d56\',\'text\':\'Tester\'}},\'TenantName\':{\'Type\':\'String\',\'Value\':\'althingin\'},\'TenantId\':{\'Type\':\'String\',\'Value\':\'42ab3f0e-8a7f-4f23-9cb3-16bf89a38bed\'},\'CreatedBy\':{\'Type\':\'String\',\'Value\':\'umarfaraz\'},\'ModifiedBy\':{\'Type\':\'String\',\'Value\':\'umarfaraz\'},\'Modified\':{\'Type\':\'String\',\'Value\':\'8/13/2018\'},\'Created\':{\'Type\':\'String\',\'Value\':\'8/13/2018\'},\'EntityId\':{\'Type\':\'String\',\'Value\':\'dd73cfbb-0f29-4de9-8746-b71e10a6793f\'},\'Dashboardurl\':{\'Type\':\'String\',\'Value\':\'\'}}"}",Routing:,"AllFrontEndButOrigin",TenantID:"42ab3f0e-8a7f-4f23-9cb3-16bf89a38bed",TenantName:"althingin",Type:"Details",UserID:"c3f759fb-ecc6-4b83-3ab9-80c757ab5ce4"}';
     var res = "5b595cf660b26f53221d738a+5b7179ac60b2e013c4170cd2?__target=embedded&key=NZCXA7lw1U";
         fixture.detectChanges();
        fixture.componentInstance.bIFabricService.callNewDashboardObservable.next(res);
     
        // expect(component.messageService.sendMessageToserver).toHaveBeenCalled();
    }));
    it('callNewDashboardObservable', async(() => {  
          var info:any=new Array();
          info.MessageId = "2d9105df-6b0d-4b50-babd-398fcab5ced8"
        info.ClientID = "f19b8d19-7f71-4abb-b4a1-81be71e11dd6";
        info.DataType = "bifabric";
        info.EntityID = "dd73cfbb-0f29-4de9-8746-b71e10a6793f";
        info.EntityType = "DashBoard";
        info.MessageKind = "CREATE";
       // info.PayLoad = '{"EntityId":"6f3ec8df-dba8-4de2-b27c-4dff7d625cb3","TenantName":"althingin","TenantId":"42ab3f0e-8a7f-4f23-9cb3-16bf89a38bed","EntityType":"DashBoard","Fabric":"BI","EntityInfoJson":"{\"Capability\":{\"type\":\"string\",\"Value\":\"Business Intelligence\"},\"EntityType\":{\"type\":\"string\",\"Value\":\"DashBoard\"},\"Name\":{\"type\":\"string\",\"Value\":\"freedom\"},\"Parent Entity\":{\"type\":\"singleselect\",\"Value\":{\"id\":\"lkjhgfdsa098765\",\"text\":\"lkjhgfdsa098765\"}},\"TenantName\":{\"Type\":\"String\",\"Value\":\"althingin\"},\"TenantId\":{\"Type\":\"String\",\"Value\":\"42ab3f0e-8a7f-4f23-9cb3-16bf89a38bed\"},\"CreatedBy\":{\"Type\":\"String\",\"Value\":\"umarfaraz\"},\"ModifiedBy\":{\"Type\":\"String\",\"Value\":\"umarfaraz\"},\"Modified\":{\"Type\":\"String\",\"Value\":\"8/14/2018\"},\"Created\":{\"Type\":\"String\",\"Value\":\"8/14/2018\"},\"EntityId\":{\"Type\":\"String\",\"Value\":\"6f3ec8df-dba8-4de2-b27c-4dff7d625cb3\"},\"Dashboardurl\":{\"Type\":\"String\",\"Value\":\"https://bi.althing.io/analytics/visualization/5b595cf660b26f53221d738a+5b726c6c60b2e013c41714e4?__target=embedded&key=yGLBZOf5vu\"}}"}';
        info.Routing = "AllFrontEndButOrigin";
        info.TenantID = "42ab3f0e-8a7f-4f23-9cb3-16bf89a38bed";
        info.TenantName = "althingin";
        info.Type = "Details";
     var x ={"EntityId":"bda42dd7-64b5-4828-b356-f32bdebd42f5","TenantName":"althingin","TenantId":"42ab3f0e-8a7f-4f23-9cb3-16bf89a38bed","EntityType":{"type":"String","Value":"DashBoard"},"Fabric":"BI","EntityInfoJson":'{"Capability":{"type":"string", "Value":"Busin" },"EntityType":{"type":"string", "Value":"DashBoard"},"Name":{"type":"string","Value":"SendNew"},"Parent Entity":{"type":"singleselect", "Value":{"id":"34831a1b-8726-4b03-84bc-1a1ebac44811","text":"demo23"} },"TenantName":{"Type":"String", "Value":"althingin" }, "TenantId":{ "Type":"String", "Value":"42ab3f0e-8a7f-4f23-9cb3-16bf89a38bed" }, "CreatedBy":{ "Type":"String", "Value":"umarfaraz" }, "ModifiedBy":{ "Type":"String","Value":"umarfaraz" },"Modified":{"Type":"String", "Value":"8/16/2018" }, "Created":{  "Type":"String","Value":"8/16/2018"}, "EntityId":{"Type":"String", "Value":"cdf1efb3-9f96-4ffb-9122-eb674322128f"}}'};
  info.PayLoad = JSON.stringify(x);     
  fixture.componentInstance.messageforserver = info;
            fixture.detectChanges();
        fixture.componentInstance.bIFabricService.callNewDashboardObservable.next(info);
        expect(fixture.componentInstance.dashboardurl).toBeDefined();
  }));
            
        it('DeleteAll', async(() => {  
            fixture.componentInstance.DeleteAll();
            fixture.detectChanges();
            expect(fixture.componentInstance.commonService.BiObservableCount).toEqual(0);
        }));
        it('NotToDelete', async(() => {  
            fixture.componentInstance.NotToDelete();
            fixture.detectChanges();
            expect(fixture.componentInstance.commonService.BiObservableCount).toEqual(0);
        }));

           it('getlistofdatasource', async(() => { 
          //  spyOn(fixture.componentInstance.bIFabricService,'getClient');
            fixture.detectChanges(); 
            fixture.componentInstance.getlistofdatasource("althingin");
            fixture.detectChanges();
            expect(fixture.componentInstance.bIFabricService.getClient).toBeDefined();
        }));

        it('addNewEntity', async(() => { 
           var url = '/' + FabricRouter.BUSINESSINTELLIGENCE_FABRIC + '/addnew';
           let navigateSpy = spyOn((<any>component).router, 'navigate');
            fixture.detectChanges(); 
            fixture.componentInstance.commonService.addNewEntity$.next();
            fixture.detectChanges();
             expect(navigateSpy).toHaveBeenCalledWith([url]);
            //expect(fixture.componentInstance.bIFabricService.getClient).toBeDefined();
        }));
           it('addChart', async(() => {          
           fixture.componentInstance.commonService.tenantName = "althingin";
           fixture.componentInstance.commonService.lastOpenedFabric = "bifabric";
           let url = '/' + fixture.componentInstance.commonService.lastOpenedFabric + '/datasource';
           let navigateSpy = spyOn((<any>component).router, 'navigate');
            fixture.detectChanges(); 
            fixture.componentInstance.addChart();
            fixture.detectChanges();
             expect(navigateSpy).toHaveBeenCalledWith([url]);
            //expect(fixture.componentInstance.bIFabricService.getClient).toBeDefined();
        }));
        it('SelectedNode-DashBoard', async(() => { 
            var res = {data:{"fosterChildren":[],"Capability":"Business Intelligence","Children":[],"EntityType":"DashBoard","Name":"when","Dashboardurl":"https://bi.althing.io/analytics/visualization/5b595cf660b26f53221d738a+5b7179ac60b2e013c4170cd2?__target=embedded&key=NZCXA7lw1U","EntityId":"dd73cfbb-0f29-4de9-8746-b71e10a6793f","parent":{"id":"257fbf81-4e5d-4e78-8764-d136d3b49d56","text":"Tester"},"Icon":"https://althingsblob.blob.core.windows.net/icons/Dashboard_BI_Icon_Idle.svg"}};
            fixture.componentInstance.commonService.selectedNode.next(res);
             expect(fixture.componentInstance.bIFabricService.dashboardid).toEqual('5b7179ac60b2e013c4170cd2');
        }));
          it('SelectedNode-Category', async(() => { 
            var res = {data:{"fosterChildren":[],"Capability":"Business Intelligence","Children":[],"EntityType":"Category","Name":"when","Dashboardurl":""}};
            fixture.componentInstance.commonService.selectedNode.next(res);
             expect(fixture.componentInstance.showbutton).toBeDefined();
        }));

});
