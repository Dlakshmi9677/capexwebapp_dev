import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { CommonModule } from '@angular/common';
import { BIFabricComponent } from './components/bifabric.component';
import { BIFabricService } from './services/BIFabricService';
import { routing } from './bi.routing';
import { SharedModule1 } from '../../shared/shared1.module';
import { AngularMaterialModule } from '../../../../app/app.module';
import { HttpClientModule } from '@angular/common/http';

const BI_COMPONENTS = [BIFabricComponent];

@NgModule({
  imports: [CommonModule, SharedModule1, routing, AngularMaterialModule, HttpClientModule],
  providers: [BIFabricService],
  declarations: [BI_COMPONENTS]
})

export class BIModule { }
