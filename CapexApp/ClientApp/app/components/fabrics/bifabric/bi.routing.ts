import { Routes, RouterModule } from '@angular/router';
import { BIFabricComponent } from './components/bifabric.component';
// import {CreateNewListComponent} from '../../template/GlobalAddNewComponents/AddNewList/create-newlist-dialog/create-newlist.component';
const routes: Routes = [
    {
        path: '', component: BIFabricComponent,
        children: [
            // {
            //     path: 'addnew',
            //     // loadChildren: '../../template/GlobalAddNewComponents/AddNew/new.form.module#AddFormModule'
            //     loadChildren: () => import('../../template/GlobalAddNewComponents/AddNew/new.form.module').then(m => m.AddFormModule)
            // },
            {
                path: 'datasource', 
                // loadChildren: './components/datasourceform/datasourceform.module#DBFormGeneratorModule'
                loadChildren: () => import('./components/datasourceform/datasourceform.module').then(m => m.DBFormGeneratorModule)
            },
            // {
            //   path: 'user',
            // //   loadChildren: '../../common/UserActivities/user.module#UserRoutingModule'
            // loadChildren: () => import('../../common/UserActivities/user.module').then(m => m.UserRoutingModule)
            // }, 
            // {
            //   path: 'capabilitylistsearch',
            // //   loadChildren: '../../common/CapabilityListSearch/capabilitylistsearch.module#CapabilityListSearchModule'
            // loadChildren: () => import('../../common/CapabilityListSearch/capabilitylistsearch.module').then(m => m.CapabilityListSearchModule)
            // },
        //     {
        //         path: 'addnewlist', component: CreateNewListComponent
        //   }
        ]
    }
];

export const routing = RouterModule.forChild(routes);
