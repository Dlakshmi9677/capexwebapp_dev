import { Component, OnInit } from '@angular/core';
import { FABRICS } from 'ClientApp/app/components/globals/Model/CommonModel';

@Component({
  selector: 'app-reporting',
  templateUrl: './reporting.component.html',
  styleUrls: ['./reporting.component.scss']
})
export class ReportingComponent implements OnInit {
  capability = FABRICS.REPORTING;

  constructor() { }

  ngOnInit(): void {
  }

}
