import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReportingComponent} from './../Components/reporting.component'
import { SharedModule1 } from '../../../shared/shared1.module';
import { AngularMaterialModule } from '../../../../app.module';
import { HttpClientModule } from '@angular/common/http';
import {routing} from './../Routing/reporting.routing'
import {ReportingService} from './../Service/reporting.service'

@NgModule({
  declarations: [ReportingComponent],
  imports: [
    CommonModule, SharedModule1, routing, AngularMaterialModule, HttpClientModule
  ],
  providers: [ReportingService],

})
export class ReportingModule { }
