import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../../../../globals/CommonService';

@Component({
  selector: 'app-map-source',
  templateUrl: './map-source.component.html',
  styleUrls: ['./map-source.component.scss']
})
export class MapSourceComponent implements OnInit {
  isInitialize:boolean;
  constructor(private activateRoute:ActivatedRoute,private commonService:CommonService) { 
    //this.dashboardurl= this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
  }

  ngOnInit(): void {
    this.activateRoute.params.subscribe(params=>{
      this.isInitialize=false
      setTimeout(() => {
        this.isInitialize=true
      }, 0);
    })
  }
  ngOnDestroy(){
    console.log('ngOndestroy')
  }

}
