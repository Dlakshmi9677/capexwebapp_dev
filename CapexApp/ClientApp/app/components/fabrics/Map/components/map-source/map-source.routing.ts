import { Routes, RouterModule } from '@angular/router';
import { MapSourceComponent } from './map-source.component';
// import {CreateNewListComponent} from '../../template/GlobalAddNewComponents/AddNewList/create-newlist-dialog/create-newlist.component';
const routes: Routes = [
    {
        path: '', component: MapSourceComponent,
        children: [
        ]
    }
];

export const routing = RouterModule.forChild(routes);
