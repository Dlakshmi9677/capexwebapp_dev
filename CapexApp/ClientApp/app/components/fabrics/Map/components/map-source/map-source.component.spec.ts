import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapSourceComponent } from './map-source.component';

describe('MapSourceComponent', () => {
  let component: MapSourceComponent;
  let fixture: ComponentFixture<MapSourceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapSourceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapSourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
