import { Routes, RouterModule } from '@angular/router';
import { MapComponent } from './components/map.component';
const routes: Routes = [
    {
        path: '', component: MapComponent,
        children: [
            {
                path: 'map/:EntityID', 
                loadChildren: () => import('../Map/components/map-source/map-source.module').then(m => m.MapSourceModule)
            }
        ]
    }
];

export const routing = RouterModule.forChild(routes);
