import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { CommonModule } from '@angular/common';
import { MapComponent } from './components/map.component';
import { MapService } from './services/map.service';
import { routing } from './map.routing';
import { SharedModule1 } from '../../shared/shared1.module';
import { AngularMaterialModule } from '../../../../app/app.module';
import { HttpClientModule } from '@angular/common/http';

const MAP_COMPONENTS = [MapComponent];

@NgModule({
  imports: [CommonModule, SharedModule1, routing, AngularMaterialModule, HttpClientModule],
  providers: [MapService],
  declarations: [MAP_COMPONENTS]
})

export class MapModule { }
