export declare class AlertConfig {
  operation?: PopupOperation;
  isSubmit?: boolean;
  header?: string;
  content?: Array<string>;
  subContent?: Array<string>;
}


export class PopupOperation {
  public static Attention = "Attention";
  public static Delete = "Delete";
  public static DeletePeople = "DeletePeople";
  public static DeleteAll = "DeleteAll";
  public static AlertConfirm = "AlertConfirm";
  public static PermissionAlertConfirm = "PermissionAlertConfirm";
  public static FacilityCode = "FacilityCode";
  public static LastTestDate = "LastTestDate";
  public static TargetThreshold = "TargetThreshold";
  public static EventDeleteConfirm = "EventDelete";
  public static EMMutualFabricDelete = "EMMutualFabricDelete";
  public static DeleteDownStreamLocation = "DeleteDownStreamLocation";
  public static updateVesselsLocationField = "updateVesselsLocationField";
  public static DeleteNestedUnderFacilityCode = "DeleteNestedUnderFacilityCode";
  public static DeleteFacilityCode = "DeleteFacilityCode";
  public static DeleteFacilityCodeIsUsing = "DeleteFacilityCodeIsUsing";
  public static DeleteNestedUnderFacilityCodeIsUsing = "DeleteNestedUnderFacilityCodeIsUsing";
  public static DeleteBasedonEffectiveDate = "DeleteBasedonEffectiveDate";
  public static DeleteHierarachy = "DeleteHierarachy";
}
