
export class FabricRouter {
  public static HOME_FABRIC = "Home";
  public static PEOPLE_FABRIC = "People & Companies";
  public static SECURITY_FABRIC = "Security";
  public static BUSINESSINTELLIGENCE_FABRIC = "Business Intelligence";
  public static MARKETING = "marketingpage";
  public static MAPS_FABRIC = "Maps";
  public static ENTITYMANAGEMENT_FABRIC = "Entity Management";
  public static CONSTRUCTION_FABRIC = "Construction";
  public static REPORTING_FABRIC = "Reporting";
  public static POSSIBLEPIPELINEINTEGRITY_FABRIC = "Possible Pipeline Integrity";
  public static FABRICATION_FABRIC = "Fabrication";
  public static PROJECTS_FABRIC = "Projects";

}
