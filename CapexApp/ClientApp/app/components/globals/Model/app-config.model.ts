export interface IAppConfig {
  env: {
    name: string;
    ApplicationName:string;
    UseAsVScode: boolean;
    UseAsDesktop: boolean;
    BaseURL: string;
    domain: string;
    devicename: string;
    username: string;
    password: string;
    companyName: string;
    UseAsLocal: boolean;
    UseAsPackage: boolean;
    desktopauthguardurl: string;
    ConstructionApiUrl: string;
  };
  appInsights: {
    instrumentationKey: string;
  };
  althingsBlob: {
    url: string;
  }
}
