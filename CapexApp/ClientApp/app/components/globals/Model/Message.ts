

export class MessageData {

    constructor(
        public TenantName,
        public TenantID,
        public IsTimeMahcinedEnabled,
        public DateTime,
        public JSON) {
    }
}



export class MessageModel {
    public MessageId;
    public TenantName;
    public TenantID;
    public EntityID;
    public IsAdmin;
    public EntityType;
    public EntityName;
    public DataType;
    public MessageKind;
    public Routing;
    public Type;
    public Fabric;
    public ReceivingComponent;
    public UserID;
    public Payload;
    public ClientID;
    public RecordId;
    public ProductionDay;
    public CapabilityId;
    public SourceSystem;
    public AbortSignal;
    public ApplicationName;
    constructor(
    ) {
    }

}

export class Message {
    public Type;
    constructor(public MessageId,
        public TenantName,
        public TenantID,
        public EntityID,
        public EntityType,
        public DataType,
        public MessageKind,
        public Routing,
        public PayLoad,
        public ClientID

    ) {
    }

}

export class MessageType {

    public static CREATE_OR_READ = 'CREATE_OR_READ';

    public static CREATE = 'CREATE';

    public static READ = 'READ';

    public static UPDATE = 'UPDATE';

    public static DELETE = 'DELETE';

    public static SELECT = 'SELECT';

}

export class Type {


    public static Batch = 'Batch'
}
export const MessageDataTypes = {

    GRAPHDBSYNC: 'GraphDBSync',
    SECURITYGROUPS: 'SecurityGroups',
    FILTER:'Filter'
}
export class Routing {
    public static ALL = 'AllFrontEnd';
    public static OriginSession = 'OriginSession';
    public static AllButOriginSession = 'AllButOriginSession';
    public static AllFrontEndButOrigin = 'AllFrontEndButOrigin';
    public static SelectedParticipantSession = 'SelectedParticipantSession';


    static getRouting(operation) {
        switch (operation) {
            case "READ":
                return Routing.OriginSession
            case "CREATE":
                return Routing.AllButOriginSession
            case "UPDATE":
                return Routing.AllButOriginSession
        }
    }
}

export interface IModelMessage {
    MessageId: string;
    TenantName: string;
    TenantId: string;
    UserId: string;
    CapabilityId: string;
    EntityId: string;
    EntityType: string;
    DataType: string;
    MessageKind: string;
    Routing: string;
    Payload: string;
    ClientId: string;
    SourceSystem: string;
    Fabric:string;
    Application:string;
    IsAdmin:boolean;
    ApplicationName:string;


}

