import * as jsonPath from "jsonpath";
import { VirtualKey, Properties, STATE_VARIABLE, STATE_FILTER, STATE_FIRST_NAME, STATE_FULL_NAME, STATE_LAST_NAME, STATE_PROJECT_ID, STATE_PROJECT_NAME, STATE_PROJECT_UOM, STATE_TENANT_NAME, STATE_TENANT_ID, STATE_USER_ID, STATE_USER_NAME, STATE_ENTITY_ID, STATE_PAGE, STATE_PARENT_ENTITY_ID, STATE_PARENT_PAGE } from '../common/Constants/AppConsts';
import { JsonPointer } from '@visur/formgenerator-core'
import { replaceAll } from './helper.functions';
import { ApplicationState } from '../common/Constants/application.state';
export function getNode(dataPointer, schema) {
  if (dataPointer) {
    let pathString = getPathString(dataPointer);
    return jsonPath.query(schema, `$${pathString}`, 1);
  }
}

export function getPage(event, schema) {
  let dataPointer: string = event.dataPointer;
  if (dataPointer) {
    let pathString = getPathString(dataPointer);
    let node = jsonPath.query(schema, `$${pathString}`, 1);
    return node[0].page;
  }
  return null;
}

export function getInlineDataPointer(schema, inputDataPointer) {
  let inlinePointer: string = '';
  const dataPointer: string = inlinePointer = '/' + (<string>inputDataPointer).split('/')[1];
  let node = getNode(dataPointer, schema);
  if (node && node.length > 0) {
    let data: any = {};
    const props: any = node[0].properties;
    if (props && Object.keys(props).length > 0) {
      for (const key in props) {
        const prop: any = props[key];
        if (prop.type == "array") {
          data[key] = {};
          inlinePointer += '/' + key;

        }
      }
    }
  }

  return inlinePointer;
}

export function getDataPointer(key: string, schema: any) {
  key = key != null ? key.toLowerCase() : null;
  let isKeyFound = { "found": false };
  const properties: any = schema.properties;
  const keys: string[] = Object.keys(properties);

  if (keys.includes(key)) {
    return '/' + key;
  }

  let dataPointer: string = '';
  for (let prop of keys) {
    if (!isKeyFound.found) {
      dataPointer = '';
      const node: any = properties[prop];
      if (node.hasOwnProperty(VirtualKey) && node.virtualKey) {
        dataPointer = getNestedProperty(node, key, prop, dataPointer, isKeyFound);
      }
    }
  }
  return dataPointer;
}

export function getNestedProperty(node: any, key: string, prop: string, dataPointer: string, isKeyFound) {
  dataPointer += '/' + prop;
  if (node.hasOwnProperty(VirtualKey) && node.virtualKey && node.hasOwnProperty(Properties)) {
    for (const innerProp of Object.keys(node.properties)) {
      if (key == innerProp) {
        isKeyFound.found = true;
        return dataPointer += '/' + innerProp;
      }
      const innerNode: any = node.properties;
      getNestedProperty(innerNode, key, innerProp, dataPointer, isKeyFound);
    }
  }
  return dataPointer;
}

export function getQueryDataByProperty(queries: Array<any>, queryName) {
  return getQueryData(queries, queryName, 'property')
}
export function getQueryData(queries: Array<any>, queryName, field) {
  let queryNode = queries.find(query => query[field] == queryName);
  if (queryNode) {
    return queryNode;
  }
  else {
    return null;
  }
}
export function getQuery(queries: Array<any>, queryName) {
  let queryNode = queries.find(query => query.property == queryName);
  if (queryNode) {
    return queryNode.query;
  }
  else {
    return null;
  }
}

export function getQueryParam(queries: Array<any>, queryName) {
  let queryNode = queries.find(query => query.property == queryName);
  if (queryNode) {
    return queryNode.queryParameter;
  }
  else {
    return null;
  }
}

export function getDataWithPath(dataPointer: string, jsonData) {
  let data = {};
  if (dataPointer.indexOf("/") == 0) {
    dataPointer = dataPointer.substring(1, dataPointer.length);
  }
  let paths = dataPointer.split("/");
  let dataPathsReverse = paths.reverse();

  for (let i = 0; i < dataPathsReverse.length; i++) {
    let d = {};
    if (i == 0) {
      d[dataPathsReverse[i]] = jsonData;
    }
    else {
      d[dataPathsReverse[i]] = data;
    }
    data = d;
  }
  return data;
}

export function getPathStringForData(key: string, data) {
  let keys = [];
  loop1:
  for (let k of Object.keys(data)) {
    keys = [];
    if (k == key) {
      keys.push(k);
      break;
    } else {
      keys.push(k);
      for (let k1 of Object.keys(data[k])) {
        if (k1 == key) {
          keys.push(k1);
          break loop1;
        }
      }
    }
  }
  let path = "$";
  for (let k of keys) {
    path += `.${k}`;
  }
  return path;
}

export function getPathString(dataPointer: string) {
  if (dataPointer.indexOf("/") == 0) {
    dataPointer = dataPointer.substring(1, dataPointer.length);
  }
  let paths = dataPointer.split("/");

  let pathString = "";
  for (let path of paths) {
    pathString += ".properties." + path;
  }
  return pathString;
}

export function getDataFromJson(d: string, data) {
  let formField = null;
  let field = null;
  let fieldIn = null;

  if (d.indexOf("/") >= 0) {
    formField = d.split("/");  // /Header/DailyReportNumber
    field = formField[formField.length - 1];     // DailyReportNumber
    fieldIn = formField[formField.length - 2];  //Header
  } else {
    formField = d.split(".");  // Header.DailyReportNumber
    field = formField[1];     // DailyReportNumber
    fieldIn = formField[0];  //Header

  }

  let fieldInNode = jsonPath.nodes(data, fieldIn, 1);
  let paramValue = null;
  if (fieldInNode.length == 1) {
    let val = fieldInNode[0].value;
    if (val) {
      paramValue = val[field];
    }
  }
  return paramValue;
}
export function getStateValue(key, state: ApplicationState) {
  let value = null;
  if (state) {
    switch (key) {
      case STATE_FIRST_NAME:
        value = state.firstName
        break;
      case STATE_FULL_NAME:
        value = state.fullName
        break;
      case STATE_LAST_NAME:
        value = state.lastName
        break;
      case STATE_PROJECT_ID:
        value = state.projectId
        break;
      case STATE_PROJECT_NAME:
        value = state.projectName
        break;
      case STATE_PROJECT_UOM:
        value = state.projectUOM
        break;
      case STATE_TENANT_ID:
        value = state.tenantId
        break;
      case STATE_TENANT_NAME:
        value = state.tenantName
        break;
      case STATE_USER_ID:
        value = state.userId
        break;
      case STATE_USER_NAME:
        value = state.userName
        break;
      case STATE_ENTITY_ID:
        value = state.entityId
        break;
      case STATE_PAGE:
        value = state.page
        break;
      case STATE_PARENT_ENTITY_ID:
        value = state.parentEntityId
        break;
      case STATE_PARENT_PAGE:
        value = state.parentPage
        break;
    }

  }
  return value;
}

export function getKeyValuePair(schema, queryParamString: string, liveData: any, arrayIndex, state: ApplicationState, paramData?) {
  let data = {};
  if (queryParamString != null) {
    let queryParams = queryParamString.split(";");
    for (let queryParam of queryParams) {
      let param = queryParam.split("=");
      if (param.length == 2) {
        if (param[1].indexOf(STATE_VARIABLE) >= 0) {
          let stateVariable = param[1]
          let value = getStateValue(stateVariable, state);
          data[param[0]] = value;
        } else if (liveData && param[1].indexOf(":") > 0) {
          let dt = param[1];
          dt = replaceAll(dt, ":", ".");
          dt = dt.toLowerCase();
          let dataPointer = JsonPointer.getPointer(schema, dt, arrayIndex, true, true);
          let pointer = JsonPointer.parseObjectPath(dataPointer);
          let fieldData = JsonPointer.get(liveData, pointer);
          data[param[0]] = fieldData;
        } else if (paramData) {
          data[param[0]] = paramData[param[1]];
        } else {
          data[param[0]] = param[1];
        }
      }
    }
  }
  return data;
}

export function getSign(dataPointer, schema) {
  if (dataPointer) {
    let pathString = getPathString(dataPointer);
    return jsonPath.query(schema, `$${pathString}`, 1);
  }
}