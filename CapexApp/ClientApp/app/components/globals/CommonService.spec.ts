﻿import { TestBed, async, inject } from '@angular/core/testing';
import { Injectable } from '@angular/core';
import {
    XHRBackend, Response, ResponseOptions
} from '@angular/http';
import { HttpClientModule, HttpClient, HttpHandler } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Observable } from 'rxjs';
import { CommonService } from './CommonService';
import { Routes, RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

describe('CommonService mock service', () => {
    let service: CommonService;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, HttpClientModule, RouterTestingModule],
            providers: [CommonService]
        });

        // inject the service
        service = TestBed.get(CommonService);
    });
    it('should have a service instance', () => {
        expect(service).toBeDefined();
    });
    it('should return the true(json exits)', () => {

    });
});    