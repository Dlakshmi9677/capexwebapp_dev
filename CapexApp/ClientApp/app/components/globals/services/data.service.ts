import { Injectable, Inject } from "@angular/core";
import { DEFAULT_REQUEST_OPTIONS, RequestResult } from "../../../../webWorker/app-workers/http/httpRequest";
import { Observable, Observer, fromEvent } from "rxjs";
import { WORKER_TOPIC } from "../../../../webWorker/app-workers/shared/worker-topic.constants";
import { CommonJsMethods } from "../../common/library/CommonJavaScriptMethods";
import { WorkerMessage } from "../../../../webWorker/app-workers/shared/worker-message.model";
import { AppConfig } from "./app.config";
import { AppConsts } from "../../common/Constants/AppConsts";
///import { IApplicationLogger } from "../../loggers/IApplicationLogger";
import { HttpClient } from "@angular/common/http";
// import { getPackedSettings } from 'http2';
import 'rxjs/add/operator/map'
import { map, retry, catchError } from 'rxjs/operators';
import { IModelMessage, MessageModel, MessageDataTypes } from '../Model/Message';
//import { url } from 'inspector';
import { IPInfo } from '../Model/CommonModel';
//import { defaultApp } from 'process';

@Injectable()
export class DataService {

  private worker: Worker;
  worker$: Observable<Event>;
  httprequestid = 'Default';
  BASEURL: string = AppConfig.AppMainSettings.env.ConstructionApiUrl;
  FORMSCHEMAROUTE: string = "FormSchema";// visur/schema
  DGRAPH_ROUTE_PATH: string = "DataSync";
  CONSTRUCTION_BASE_PATH: string = 'Construction';
  AfterBaseUrlForData: string = "";// visur/data
  AfterBaseUrlFortables: string;// visur/tree-data

  private httpKeyValue: Map<string, Observer<Object>> = new Map<string, Observer<Object>>();

  constructor(private appConfig: AppConfig, private http: HttpClient) {
    this.workerInit();
  }


  workerInit(): void {
    try {
      if (!!this.worker === false) {
        this.worker = new Worker(AppConsts.apiWorkerPath);
        this.worker$ = fromEvent(this.worker, 'message');
        var workerResponse = this.worker$.pipe(map(response => WorkerMessage.getInstance((<MessageEvent>response).data)));
        this.worker$
          .subscribe((response: MessageEvent) => {
            try {
              var responcemsg: WorkerMessage = WorkerMessage.getInstance(response.data);
              switch (responcemsg.topic) {
                case WORKER_TOPIC.RESTHTTP:
                  var httpresponce: RequestResult = response.data.dataPayload;
                  if (httpresponce.status == 0) {// aborted and cancelled requests
                    this.abortRequestFromWorker(responcemsg);
                  }
                  else if (httpresponce.status != 504) {
                    response.data.dataPayload.data = JSON.parse(response.data.dataPayload.data);
                    var resobserver: Observer<Object> = this.httpKeyValue.get(responcemsg.restMethodName);
                    if (resobserver) {
                      if (CommonJsMethods.isvalidJson(httpresponce.data)) {//message json string with single quote
                        //this is the condition to stored inexeded data read
                        var msgPayload = JSON.parse(httpresponce.data).Payload;
                        if (msgPayload != null && CommonJsMethods.isvalidJson(msgPayload)) {
                          var parseData = JSON.parse(msgPayload);
                          if (parseData.hasOwnProperty("message_store_type")) {
                            var idbMessageValue = JSON.stringify(parseData.value);
                            var message = JSON.parse(httpresponce.data);
                            message.Payload = idbMessageValue;
                            httpresponce.data = JSON.stringify(message);
                          }

                          this.httpRequestResponseNext(resobserver, httpresponce, responcemsg);
                        }
                        else {
                          var msgPayload = JSON.parse(httpresponce.data);
                          var parseData = msgPayload;
                          if (parseData.hasOwnProperty("message_store_type")) {
                            var idbMessageValue = JSON.stringify(parseData.value);
                            var message = JSON.parse(httpresponce.data);
                            message.Payload = idbMessageValue;
                            httpresponce.data = JSON.stringify(message);
                          }

                          this.httpRequestResponseNext(resobserver, httpresponce, responcemsg);

                        }
                      }
                      else {
                        this.httpRequestResponseNext(resobserver, httpresponce, responcemsg);
                      }
                    }
                  }

                  break;
                case WORKER_TOPIC.AbortEvent://all aborted event at onces
                  this.abortRequestFromWorker(responcemsg);
                  break;
                default:
                  break;
              }

            } catch (ex) {
              //this.logger.Error(new Error(ex));
            }

          }, (error) => console.log(new Error('WORKER ERROR::' + error)));
      }
    } catch (e) {
      //this.logger.Error(e);
    }
  }




  /**
  * common method for index db and backend service operation
  *
  * @param DataType
  * @param operationType
  * @param method
  * @param url
  * @param queryParams
  * @param body
  * @param options
  */
  httpRestRequestsFromWebWorker(workerTopic: string, operationType: string, method: 'get' | 'post', url: string, queryParams: any = {},
    body: any = null, options = DEFAULT_REQUEST_OPTIONS): Observable<any> {
    var self = this;
    if (workerTopic == WORKER_TOPIC.RESTHTTP || workerTopic == WORKER_TOPIC.GisMap || WORKER_TOPIC.AbortEvent) {
      operationType = CommonJsMethods.newGuid() + "_" + this.httprequestid;
    }
    this.httprequestid = "Default";
    return Observable.create((observer: Observer<Object>) => {

      var workerMsg: WorkerMessage = new WorkerMessage(workerTopic, operationType,
        {
          "method": method, "url": url, "queryParams": queryParams,
          "body": body, "options": options,
        }, AppConfig.AppMainSettings)

      if (this.worker) {
        this.worker.postMessage(workerMsg);
      }
      self.httpKeyValue.set(workerMsg.restMethodName, observer);
    });

  }
  abortRequestFromWorker(workerMessage: WorkerMessage) {
    try {
      var resobserver: Observer<Object> = this.httpKeyValue.get(workerMessage.restMethodName);
      if (resobserver) {
        resobserver.complete();
        this.httpKeyValue.delete(workerMessage.restMethodName);
      }
    }
    catch (e) {
      //this.logger.Error(new Error(e));
    }
  }

  httpRequestResponseNext(resobserver: Observer<Object>, httpresponce: RequestResult, responcemsg: WorkerMessage): void {
    //resobserver.next();
  }

  sendResponseToNext(resobserver: Observer<Object>, httpresponce: RequestResult, responcemsg: WorkerMessage, backendFormName: string, currentActiveForm: string) {
    if (backendFormName == "Default" || backendFormName.toUpperCase() == currentActiveForm.toUpperCase()) {
      resobserver.next(httpresponce.data);
      resobserver.complete();
      this.httpKeyValue.delete(responcemsg.restMethodName);
    }
    else {
      resobserver.complete();
      this.httpKeyValue.delete(responcemsg.restMethodName);
    }
  }

  getPeopleSchema(formName: string): Observable<any> {
    const url = `wwwroot/test-schemas/${formName}.json`;
    return this.http.get(url);
  }

  getSchema(formName: string): Observable<any> {
    const url = `wwwroot/test-schemas/${formName}.json`;
    return this.http.get(url);
  }

  CreateOrUpdateData(message: IModelMessage) {
    const url = this.BASEURL + this.FORMSCHEMAROUTE;
    return this.http.post(url, message);
  }

  CreateOrUpdateOffline() {

  }

  Read(message: IModelMessage) {

  }


  public GetClientIP(): Observable<IPInfo> {
    const methodName = 'GetClientIP()';

    // try {
    //   console.log(methodName);

    //   return this.http
    //     .get<IPInfo>('https://api.ipify.org?format=json')
    //     .pipe(retry(3),
    //       catchError((err: HttpErrorResponse) => {
    //         const errorMessage: HttpErrorResponse | ErrorEvent = err.error instanceof ErrorEvent ? err.error : err;

    //         return throwError(errorMessage);
    //       }));
    // } catch (error) {
    //   console.error(error);
    //   throw error;
    // }
    return null;
  }

  getDataFromApi(options: HttpRequestOptions): Observable<any> {


    switch (options.HttpMethod) {
      case HttpMethod.get: {

        break;
      }
      case HttpMethod.put: {
        break;
      }
      case HttpMethod.post: {
        break;
      }
      case HttpMethod.delete: {
        break;
      }
    }

    return Observable.create((observer: Observer<boolean>) => {
      observer.next(null);
    })

  }


  getDataFromBackend(messageModel: IModelMessage) {
    switch (messageModel.DataType) {
      case "":
        {

          break;
        }
      default: {
        let url = this.BASEURL + '/' + this.FORMSCHEMAROUTE
        return this.http.post(url, messageModel);
        break;
      }
    }
  }

  postDataToBackend(messageModel: IModelMessage) {
    switch (messageModel.DataType) {
      case MessageDataTypes.GRAPHDBSYNC:
      case MessageDataTypes.SECURITYGROUPS:
        let url = this.BASEURL + '/' + this.DGRAPH_ROUTE_PATH
        return this.http.post(url, messageModel);
        break;
      default: {
        let url = this.BASEURL + '/' + this.FORMSCHEMAROUTE
        return this.http.post(url, messageModel);
        break;
      }
    }
  }

  createEntity(messageModel: IModelMessage) {
    let url = this.BASEURL + '/' + this.CONSTRUCTION_BASE_PATH;
    return this.http.post(url, messageModel);
  }

}


export interface HttpRequestOptions {

  /**
   * Payload needed for post method
   */
  body?: string;
  /**
   * Post,Delete,Put,Get
   */
  HttpMethod: HttpMethod;


}
export enum HttpMethod {
  "get", "post", "delete", "put"
}





