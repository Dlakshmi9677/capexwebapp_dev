import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { CookieService } from 'ngx-cookie';

//classes and services
import { UUIDGenarator } from "visur-angular-common";
import { CommonService } from '../CommonService'
import { AppConfig } from '../services/app.config';
import { WORKER_TOPIC } from '../../../../webWorker/app-workers/shared/worker-topic.constants';
import { WorkerMessage } from '../../../../webWorker/app-workers/shared/worker-message.model';
import { DEFAULT_REQUEST_OPTIONS } from '../../../../webWorker/app-workers/http/httpRequest';
import { CommonJsMethods } from '../../common/library/CommonJavaScriptMethods';
import { Datatypes } from '../Model/CommonModel';


@Injectable()
export class MessagingService {
  public baseUrl = AppConfig.BaseURL;
  public CustomerAppsettings = AppConfig.AppMainSettings;

  constructor(private http: HttpClient, private commonService: CommonService, public appconfig: AppConfig,private cookieService:CookieService) {
    var url = new URL(this.baseUrl);
    this.commonService.hostname = url.hostname;
    var protocol = url.protocol
    this.commonService.websocketProtocol = (protocol == 'http:') ? 'ws' : 'wss';
  }

  ngOnDestroy() {
    this.commonService.sessionSucess = false;
  }

  //******************Socket Initialize Message****** */

  public initializeWebSocketConnection(): Promise<any> {
     this.commonService.clientGuid = UUIDGenarator.generateUUID();
    if ((this.appconfig.token == "" || this.appconfig.token == null) && this.CustomerAppsettings.env.UseAsDesktop != true) {

      var url2 = AppConfig.BaseURL + 'Home/CallgetTokenAsync?companyName=' + this.appconfig.tenantName + '&username=' + this.appconfig.userName + '&password=' + AppConfig.AppMainSettings.env.password + '&subdomain=' + "." +AppConfig.AppMainSettings.env.domain +'&applicationName='+AppConfig.AppMainSettings.env.ApplicationName;//'.visur.io';
      return new Promise<void>((resolve, reject) => {

        this.http.get(url2, { "responseType": "text" }).toPromise().then((response: any) => {
          if (response != "" && response != null) {
            console.log("VSCODE GET TOKEN RESPONSE: " + response);
            var res = response.toString();
            this.appconfig.token = res;
            this.cookieService.put("token", res);
            ///////////////////////////////////////////
            //this.socket_URL = `${this.websocketProtocol}://${this.hostname}:32003/StockServiceWS/?t=` + this.appconfig.token + '&tname=' + this.appconfig.tenantName+'&appname='+AppConfig.AppMainSettings.env.ApplicationName;
            this.connectToWebSocket();
          }
          else {
            //throw new Exception("Exception while fetching company and user from token");
            console.error("********Invalid Grant provided,Please check User Credentials*****************");
          }
          resolve();
        }).catch((response: any) => {

        });
      });

    } else {
      this.connectToWebSocket();

    }

    return Promise.resolve();
  }
  public connectToWebSocket() {
    this.wsWorkerMessageOnOpen();
  }
  wsWorkerMessageOnOpen() {
    var messageForId = this.commonService.getMessage();
    messageForId.EntityID = null;
    messageForId.EntityType = null;
    messageForId.Payload = this.commonService.currentUserName;
    messageForId.DataType = "ClientID";
    messageForId.Routing = null;
    messageForId.MessageKind = null;

    var personPayload = { "EntityType": "Person", "Fabric": "People",DataType:Datatypes.ENTITYINFO };
    var messageForUserDetails = this.commonService.getMessage();
    messageForUserDetails.EntityID = this.commonService.currentUserId;
    messageForUserDetails.EntityType = "Person";
    messageForUserDetails.Payload = JSON.stringify(personPayload);
    messageForUserDetails.DataType = Datatypes.PEOPLE;
    messageForUserDetails.Routing = "OriginSession";
    messageForUserDetails.MessageKind = "READ";
    messageForUserDetails.Type = "Details";

    let body = {
      "ClientMessage": messageForId,
      "PersonMessage": messageForUserDetails
    };
    this.sendMessageToServerNext(body, "wsWorkerMessageOnOpen", "open");
  }

  //***#########Sending Message#############*********/
  sendMessageToServerNext(message, callerMethodName, methodType) {
    let socket_URL;
    let body = message;
    // if (this.commonService.isElectron) {
    //   socket_URL = this.commonService.socket_URL;
    // }
    // else {
      socket_URL = `${this.commonService.websocketProtocol}://${this.commonService.hostname}:32003/StockServiceWS/?t=` + this.appconfig.token + '&tname=' + this.appconfig.tenantName +'&appname='+AppConfig.AppMainSettings.env.ApplicationName;
    // }
    var workerMsg: WorkerMessage = new WorkerMessage(
      WORKER_TOPIC.WS,
      CommonJsMethods.newGuid(),
      {
        "method": methodType, "url": socket_URL, "queryParams": { "callerMethodName": callerMethodName },
        "body": body, "options": DEFAULT_REQUEST_OPTIONS,
      }
    );
    if (this.commonService.worker)
      this.commonService.worker.postMessage(workerMsg);
  }
}