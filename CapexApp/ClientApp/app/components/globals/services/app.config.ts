
import { CookieService } from 'ngx-cookie';
import { Injectable, Injector } from '@angular/core';
import { Subject, of } from 'rxjs';
import { HttpClient } from "@angular/common/http";
import { environment } from '../../../../environments/environment';
import { IAppConfig } from '../Model/app-config.model';
import { Router } from '@angular/router';
import * as localForage from 'localforage';
import { persistState } from '@datorama/akita';
import { EntityCommonStoreQuery } from '../../common/state/entity.state.query';
import { UsersStateQuery } from '../../../../webWorker/app-workers/commonstore/user/user.state.query';
import { UserStateConstantsKeys } from '../../../../webWorker/app-workers/commonstore/user/user.state.model';
import { CapabilityModel, CAPABILITY_NAME, FABRICS, TreeTypeNames } from '../Model/CommonModel';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { AuthCommonStoreQuery } from '../../common/state/Auth/auth.state.query';
import { switchMap, debounceTime, tap } from 'rxjs/operators';
import { CapabilityAuthGaurd } from '../../authguard/services/capability-authgaurd.service';

@Injectable()
export class AppConfig {

  static AppMainSettings: IAppConfig = null;
  static BaseURL = "";
  static BlobURL = "";
  static CapabilitiesJson: any = "";
  static FabricJson: any = "";
  public tenantName: any;
  public userName = "";
  public token = "";
  public applicationName="";
  public IsDocumentManagementOn: boolean = false;
  public reSetStore: any;
  public UserCapabilitiesSyncOffline = new Subject();
  public getUserpreFrenceData = new Subject();
  public ipc;
  //for routing check
  public AllHeaderRoutingListService = [];
  public AllHeaderRoutingList = [];
  loadHead = [];
  homeConfig = {
    'source': 'V3 Home',
    'sourceSub': 'wwwroot/images/V3 Icon Set part2/V3 Home.svg',
    'title': 'Home',
    'tooltip': 'HOME',
    'Fabric': 'Home',
    'routerLinkActive': false,
    'isFavIcon': false,
    'routerLink': '/Home',
    'id': 'Home',
    'baseRoute': 'Home',
    'class': 'icon21-21',
    'order': 20,
    'admin': true,
    'offlineStatus': false,
    'Show': true
  };
  //userDetails
  public UserDetails: any;
  AllHeaderRouting = [];
  capabilitiesDetails = [];

  setBaseUrl(url) {
    AppConfig.BaseURL = url
  }

  constructor(public usersStateQuery: UsersStateQuery, public entityCommonStoreQuery: EntityCommonStoreQuery, private _cookieService: CookieService, private injector: Injector, private http: HttpClient, public authCommonStoreQuery: AuthCommonStoreQuery) {
    this.token = this._cookieService.get("token");
  }

  public get router(): Router {
    return this.injector.get(Router);
  }

  load() {
    const jsonFile = `wwwroot/config/config.${environment.name}.json`;
    return new Promise<any>((resolve, reject) => {
      this.http.get(jsonFile).toPromise().then((response) => {
        AppConfig.AppMainSettings = <IAppConfig>response;
        this.CreateBaseURL();
        this.createBlobURL();
        this.storeLoad();
        resolve();
      }).catch((response: any) => {
        this.CreateBaseURL()
        reject(`Could not load file '${jsonFile}': ${JSON.stringify(response)}`);
      });
    }).then(() => {
      return this.GetCapabilities();
    });

  }

  storeLoad() {
    if ((AppConfig.AppMainSettings.env.name == "PROD" || AppConfig.AppMainSettings.env.name == "DEV")) {
      if (this.tenantName) {
        localForage.config({
          driver: localForage.INDEXEDDB,
          name: 'Visur',
          storeName: this.tenantName
        });

        persistState({ key: 'entities', include: ['entities'], storage: localForage });

        localForage.getItem("entities").then((res: any) => {
          if (res) {
            if (Object.keys(res.entities.entities).length != 0) {
              this.entityCommonStoreQuery.Set(res.entities.entities);
            }
          }
        })

        persistState({ key: 'users', include: ['users'], storage: localForage });

        localForage.getItem("users").then((res: any) => {
          if (res) {
            if (Object.keys(res.users).length != 0) {
              if (res.users.userPrefrence)
                this.usersStateQuery.add(UserStateConstantsKeys.userPrefrence, res.users.userPrefrence)

              if (res.users.modifiedTime)
                this.usersStateQuery.add(UserStateConstantsKeys.modifiedTime, res.users.modifiedTime)

              if (res.users.windowCloseTime)
                this.usersStateQuery.add(UserStateConstantsKeys.windowCloseTime, res.users.windowCloseTime)

            }
          }
        });
      }
    }
  }


  CreateBaseURL() {
    if (AppConfig.AppMainSettings == null) {
      AppConfig.BaseURL = window.location.origin + '/';
    }
    else if (AppConfig.AppMainSettings.env.name == "DEV") {
      if (AppConfig.AppMainSettings.env.UseAsVScode == true || AppConfig.AppMainSettings.env.UseAsDesktop == true) {
        AppConfig.BaseURL = AppConfig.AppMainSettings.env.BaseURL + '/';
        if (AppConfig.AppMainSettings.env.UseAsVScode) {
          this.tenantName = AppConfig.AppMainSettings.env.companyName;
          this.userName = AppConfig.AppMainSettings.env.username;
        }
        else {
          if (AppConfig.AppMainSettings.env.BaseURL == "") {
            AppConfig.BaseURL = window.location.origin + '/';
          }
          else {
            AppConfig.BaseURL = AppConfig.AppMainSettings.env.BaseURL + '/';
          }
        }
      }
      else {
        AppConfig.BaseURL = window.location.origin + '/'+"constructionapp/";
        this.userName = this._cookieService.get("u_cookie");
        this.tenantName = this._cookieService.get("c_cookie");
      }
    }
    else {
      AppConfig.BaseURL = window.location.origin +":9060" + '/';
      this.userName = this._cookieService.get("u_cookie");
      this.tenantName = this._cookieService.get("c_cookie");
    }

    if(this.applicationName==""){
      this.applicationName=AppConfig.AppMainSettings.env.ApplicationName;
    }

    /*if(!AppConfig.BaseURL.includes("9060")){
      let baseUrl = AppConfig.BaseURL;
      AppConfig.BaseURL=baseUrl.substring(baseUrl.length-1)=="/"?baseUrl.substring(0,baseUrl.length-1)+":9060/":baseUrl+":9060/"
    }*/
  }

  createBlobURL() {
    if (AppConfig.AppMainSettings == null) {
      console.error("Could Not Find Blob In Config");
    }
    else {
      AppConfig.BlobURL = AppConfig.AppMainSettings.althingsBlob.url;
    }
  }

  /**
    * Authentication
    */
  async GetCapabilities() {
    if (this.userName) {
      this.getUserDetails$().pipe(untilDestroyed(this, 'destroy')).subscribe((res: any) => {
      });
      return this.fabricStatusCheck();
    }
    if (this.token)
      return this.getUserNameAndCompanyName(this.token);
  }

  fabricStatusCheck() {
    if (this.tenantName) {
      var url = AppConfig.BaseURL + 'Home/UserDetailsAsync?tenantName=' + this.tenantName + '&userEmail=' + this.userName +'&applicationName='+this.applicationName;
      return new Promise<void>((resolve, reject) => {

        this.http.get(url, { "responseType": "text" }).toPromise().then((response: any) => {
          this.CapabilitiesAssigning(response);
          resolve();
        }).catch((response: any) => {
          this.CreateBaseURL();
          this.callToSendErrorToLoginPage("FAILED TO FETCH CAPABILITIES");
          this.router.navigate(["/"]).then(result => {
            window.location.href = window.location.origin;
          });
          window.location.href = window.location.origin;
          reject(`<<<PERMISSION>>> FAILED TO FETCH CAPABILITIES '${url}': ${JSON.stringify(response)}`);
        });
      });
    }
    else {
      console.error("TenantName is " + this.tenantName);
    }
  }

  callToSendErrorToLoginPage(errorMessage: string) {
    try {
      var url = AppConfig.BaseURL + 'Home/SendErrorToLoginPage?_errorMessage=' + errorMessage;
      this.http.get(url).pipe(untilDestroyed(this, 'destroy')).subscribe((res: any) => {
        console.log(res);
        this.logout();
      },
        error => {
          console.log(error)
        },
        () => { console.log("completed") });
    }
    catch (e) {
      console.error('Exception in search() of CommonService in globals/CommonService at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  logout() {
    try {
      if (this._cookieService.get("token")) {
        this.deleteCookies();
      }
    } catch (e) {
      console.error('Exception in logout() of CommonService.ts at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  deleteCookies() {
    try {
      localStorage.clear();
      sessionStorage.clear();
      this._cookieService.removeAll();

      if (this._cookieService.get("token"))
        this.http.get(AppConfig.BaseURL + 'Home/RemoveCustomCookies?deleteType=All').pipe(untilDestroyed(this, 'destroy')).subscribe();

      window.location.replace(window.location.origin);
    }
    catch (e) {
      console.error('Exception in deleteCookies() of CommonService.ts at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  async CapabilitiesAssigning(response) {
    try {
      response = JSON.parse(response);
      AppConfig.FabricJson = response;
      AppConfig.CapabilitiesJson = response;
      this.UserDetails = response;
      var capabilities = this.UserDetails["Capabilities"];
      var adminCapabilities = this.UserDetails["AdminCapabilities"];
      var securityGroups = this.UserDetails["SecurityGroups"];

      
      var domain = window.location.pathname.split('/')[1];

      if (domain != "Home" && domain != "" && domain != null && (domain == "Security" || domain == "bifabric" || domain == FABRICS.PEOPLEANDCOMPANIES)) {

        var capabilitiesName = "";
        var authUser = false;

        capabilities.filter(function (item, index, array) {
          if (!authUser) {
            try {
              capabilitiesName = item["Name"];
              switch (domain) {
                case FABRICS.PEOPLEANDCOMPANIES:
                  domain = "People & Companies";
                  break;
                case "bifabric":
                  domain = "Business Intelligence";
                  break;
                case "Security":
                  domain = "Security";
                  break;
              }
            }
            catch (e) {
              console.error(e);
            }

            if (capabilitiesName == domain) {
              authUser = true;
              return;
            }
          }
        });

        if (authUser == false) {
          this.router.navigate(['UnauthorizedPage']);
        }
      }

      AppConfig.FabricJson = capabilities;
      var url = AppConfig.BaseURL + 'Home/GetCapabilityDetails';
      this.http.get(url).pipe(untilDestroyed(this, 'destroy')).subscribe((res: any) => {
        if(res)
        this.checkCapabilityStatusForAllowing(capabilities, adminCapabilities,res);
      },
        error => {
          console.log(error)
        },
        () => { console.log("completed") });
    } catch (ex) {
      console.error('exception in OfflinefabricStatusCheck', ex);
    }
  }

  checkCapabilityStatusForAllowing(capabilities, adminCapabilities,capabilitiesDetails) {
    try {
      var self = this;

      self.pushMatchedCapabilities(capabilities, adminCapabilities,capabilitiesDetails);

      if (capabilities.length == 0) {
      let existcap = this.AllHeaderRoutingList.filter(d=>d.id==this.homeConfig.id)
      if(existcap.length == 0)
        this.AllHeaderRoutingList.push(this.homeConfig);
        this.sort(self.AllHeaderRoutingList);
        this.AllHeaderRoutingListService = self.AllHeaderRoutingList;
        this.router.navigate(['Home']);
      }
      this.getUserpreFrenceData.next();
    } catch (e) {
      console.error('Exception in checkCapabilityStatusForAllowing() of TemplateHeaderComponent at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }
  pushMatchedCapabilities(capabilities, adminCapabilities,capabilitiesDetails) {
    try {
      var self = this;
      this.capabilitiesDetails = capabilitiesDetails;
      let tempcapabilities = capabilities && capabilities.length ? capabilities : [];
      let tempadminCapabilities = adminCapabilities && adminCapabilities.length ? adminCapabilities : [];
      this.AllHeaderRouting = this.getCapabilities(tempcapabilities,tempadminCapabilities,capabilitiesDetails)
      
      if (this.AllHeaderRoutingList.length == 1) {
        let header = this.AllHeaderRoutingList[0];
        if (header.Fabric == CAPABILITY_NAME.ENTITYMANAGEMENT) {
          header.queryParameter = { 'leftnav': 'true' }
        }
      }
      this.AllHeaderRoutingList.push(this.homeConfig);
      this.sort(self.AllHeaderRoutingList);
      this.AllHeaderRoutingListService = self.AllHeaderRoutingList.reverse();

    } catch (e) {
      console.error('Exception in pushMatchedCapabilities() of TemplateHeaderComponent at time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }
  public sort(list) {
    try {
      (<Array<any>>list).sort((leftSide, rightSide) => {
        if (leftSide.order < rightSide.order)
          return -1;
        if (leftSide.order > rightSide.order)
          return 1;
        return 0;
      });
    } catch (e) {
    }
  }

  /**
   * Authentication
   * @param token
   */
  private getUserNameAndCompanyName(token: string) {

    var url = AppConfig.BaseURL + 'Home/GetAndValidateToken?token=' + this.token +'&applicationName='+this.applicationName;

    return this.http.get(url).toPromise().then((response: Response) => {
      if (response["_body"] != "") {
        var json = response;

        if (json["valid"]) {
          if (json["valid"].toString() == "False" && (json["error"].toString() == "Access token expired" || json["error"].toString() == "Invalid Access Token. Access token is not ACTIVE.")) {
            document.location.href = document.location.origin;
          }
          else if (json["error"].toString() == "") {
            this.userName = json["user"].toString();
            this.tenantName = json["tenant"].toString().split('.')[0];
            this.storeLoad();
            if (this.userName && this.tenantName) {
              this.getUserDetails$().pipe(untilDestroyed(this, 'destroy')).subscribe((res: any) => {
                console.log(res);
              });
            }
            return this.fabricStatusCheck();
          }
          else {
            console.log(json);
            if (json["error"].toString() != "") {
              document.location.href = document.location.origin;
            }
          }
        }
        else
          console.error('Respose is not coming properly from GetAndValidateToken. And the response is ' + response);
      }
    });
  }

  destroy() { }

  getUserDetails$() {
    var url = '';
    url = AppConfig.BaseURL + 'Home/UserDetailsAsync?tenantName=' + this.tenantName + '&userEmail=' + this.userName+'&applicationName='+this.applicationName;
    return this.authCommonStoreQuery.select(state => state).pipe(switchMap((hashCache) => {
      const apiCall = this.http.get(url, { "responseType": "text" }).pipe(tap(async res => {
        if (AppConfig.AppMainSettings.env.UseAsDesktop) {
          let fields = { Capabilities: 'capabilities', webResponse: res };
          this.UserCapabilitiesSyncOffline.next(fields);
        }
        await this.StoreUserDetails(res);
      }));
      let newState;
      if (hashCache.TenantId) {
        newState = JSON.parse(JSON.stringify(hashCache));
        delete newState["ZoomdataToken"];
      }

      return newState ? of(newState) : apiCall;
    })).pipe(debounceTime(150))
  }

  async StoreUserDetails(res) {
    let response = JSON.parse(res);
    response["UserName"] = this.userName;
    response["TenantName"] = this.tenantName;
    var capabilitiesArray = this.addFabricPropertyToState(response["Capabilities"]);
    var rolesArray = this.addRolesToState(response["Capabilities"]);
    var securityGroupsArray = this.addSecurityGroupsToState(response["SecurityGroups"]);
    this.authCommonStoreQuery.Add(state => ({
      UserName: this.userName,
      TenantName: this.tenantName,
      UserId: response["UserId"],
      TenantId: response["TenantId"],
      Capabilities: capabilitiesArray,
      AdminCapabilities: response["AdminCapabilities"],
      UserRoles: rolesArray,
      UserSecurityGroups:securityGroupsArray
    }));
  }

  addFabricPropertyToState(capabilities) {
    capabilities.forEach(cap => {
        cap["Fabric"] = cap.Name;
    })
    return capabilities;
  }

  addRolesToState(capabilities) {
    let roles = [];
    capabilities.forEach(cap => {
      let role = {
        RoleName: cap["role"] && cap["role"].length ? cap["role"][0]["EntityName"] : null,
        RoleId: cap["role"] && cap["role"].length ? cap["role"][0]["EntityId"] : null,
        CapabilityName: cap["Name"],
        CapabilityId: cap["CapabilityId"],
        Fabric: cap["Name"]
      }
      roles.push(role);
    })
    return roles;
  }
  addSecurityGroupsToState(securtiyGroups) {
    let securitygroups = [];
    securtiyGroups.forEach(sg => {
      let securitygroup = {
        SecurityGroupName: sg.EntityName,
        SecurityGroupId: sg.EntityId,
        CapabilityName: sg.Capability && sg.Capability.length ?  sg.Capability[0].CapabilityName : null,
        CapabilityId: sg.Capability && sg.Capability.length ? sg.Capability[0].CapabilityId : null,
        Fabric: sg.Capability && sg.Capability.length ?  sg.Capability[0].CapabilityName : null,
        SubType:sg.SubType
      }
      securitygroups.push(securitygroup);
    })
    return securitygroups;
  }

  getCapabilities(capabilities,adminCapabilities,capabilitiesDetails){
    var self = this;
    let capabiliyList = [];
    capabilities.forEach(cap => {
      let capabilityName = cap.Name;
      let capabilityDetails = capabilitiesDetails.find(item => item.CapabilityId == cap.CapabilityId);
      if(capabilityDetails){
      let capability : CapabilityModel = {
        source : capabilityName+' Icon',
        sourceSub : 'wwwroot/images/fabricIcons/'+capabilityName+'_icon.svg',
        title : capabilityName,
        tooltip : capabilityName.toUpperCase(),
        routerLinkActive : false,
        isFavIcon : false,
        routerLink : '/'+capabilityName,
        queryParameter : this.getQueryParam(capabilityDetails),
        id : capabilityName,
        baseRoute : capabilityName,
        class : 'icon21-21',
        order : capabilityDetails.Sequence,
        admin : adminCapabilities.find(obj => obj.CapabilityId === cap.CapabilityId) ? true : false,
        Fabric : capabilityName,
        entityId : cap.CapabilityId,
        offlineStatus : false,
        Show :true
      }
      switch (capability.Fabric) {
        case CAPABILITY_NAME.PEOPLEANDCOMPANIES:
          capability.Show = false;
          break;
        case CAPABILITY_NAME.SECURITY:
          capability.Show = adminCapabilities && adminCapabilities.length ? true : false;
          break;
      }
      self.loadHead.push(capability);
      capabiliyList.push(capability);
      self.AllHeaderRoutingList = Array.from(new Set(self.loadHead));    }});
    return capabiliyList;
  }

  getQueryParam(capabilityDetails){
    var queryParam = {'leftnav':false};
    if(capabilityDetails.EnableMode){
      queryParam['enabledmode'] = capabilityDetails.EnableMode;
    }
    if(capabilityDetails.LeftNav){
      queryParam['leftnav'] = capabilityDetails.LeftNav === 'true';
    }
    if(capabilityDetails.RightNav){
      queryParam['rightnav'] = capabilityDetails.RightNav === 'true';
    }
    if(capabilityDetails.RightTab){
      queryParam['righttab'] = capabilityDetails.RightTab;
    }
    if(capabilityDetails.LeftTab){
      queryParam['tab'] = capabilityDetails.LeftTab;
    }
    return queryParam;
  }
  
}
