import React from "react";

const defaultValue = {
  dispatch: null,
  state: null,
  isImparative: null,
  onNodeClick: (node) => {},
  folderCreation: (node) => {},
  fileCreating : (node) => {},
  deleteFolder:(data) => {},
  editFolderName:(data) => {},
  deleteFile:(data) => {},
  fileEdit :(data) => {}
};
const TreeContext = React.createContext(defaultValue);

const useTreeContext = () => React.useContext(TreeContext);

export { TreeContext, useTreeContext };
