// Copyright (c) 2020 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import React from 'react';
import document from 'global/document';
import {Provider} from 'react-redux';
import {browserHistory, Router, Route} from 'react-router';
//import {syncHistoryWithStore} from 'react-router-redux';
import {ReactDOM} from 'react-dom';
import store from './store';
import App from './app';
import {buildAppRoutes} from './utils/routes';
const initialState = {};
import renderRoot from './components/root';
import createAppStore from './store';


//const store = configureStore(initialState, browserHistory);

//const history = syncHistoryWithStore(browserHistory, store);
//var hashHistory = ReactRouter.hashHistory;

const map = (function initKeplerGl() {
  const id = 'keplergl-0';
  const store = createAppStore();

  const divElmt = document.createElement('div');
  divElmt.setAttribute(
    'style',
    'width: 100vw; height: 100vh; position: absolute'
  );
  document.body.appendChild(divElmt);

  return {
    render: () => {
      renderRoot({id, store, ele: divElmt});
    },
    store
  };
})();

map.render();