export function getUrlParams(){
    let search = window.location.search
    if(!search && search ==""){
      alert("Param is empty")
      return;
    }
    search  = search.substring(1, search.length)
    search=search.replace('EntityID','EntityId')
    let params =  search.split("&");
    let json = {}
    for(let param of params){
     let fields = param.split("=");
     if(fields.length ==2){
      json[fields[0]]=fields[1]
     }      
    }
    return json; 
   }
  export function getTreeData(data){
    let rootNode={id:data[0].split('/')[0],name:data[0].split('/')[0],files:[],type:'folder'}
    console.log(data.sort())
    let treeData=[]
    data.forEach(d=>{
      let elements = d.split('/');
      let lastele=elements[elements.length-1]
      let type=lastele.split('.').length==1?'folder':'file';
      console.log(type)
      if(type=='folder'){
        let node={
          id:d,
          type: type,
          name: lastele,
          checked:false,
          parent:d.slice(0, d.lastIndexOf('/'))
        }
        if(type=='folder'){
          node['files']=[]
        }
        console.log(node)
        treeData.push(node)
      }
    })
    console.log(treeData)
    getChildren(treeData,rootNode)
    console.log(rootNode)
    return [rootNode]
  }
 function getChildren(treeData,node){
    let children =treeData.filter(d=>d.parent==node.id)
    children.forEach(d=>{
      getChildren(treeData,d)
    })
    node.files=children
  }