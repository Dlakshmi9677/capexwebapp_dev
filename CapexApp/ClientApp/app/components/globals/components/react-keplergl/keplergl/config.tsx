import {environment} from '../../../../../../environments/environment'
const jsonFile = `wwwroot/config/config.${environment.name}.json`;

export function getConfig(){
    return new Promise( (resolve, reject) => {
        fetch(jsonFile).then(res => res.json()).then(data => {
            resolve(data);
        });
    });
}

