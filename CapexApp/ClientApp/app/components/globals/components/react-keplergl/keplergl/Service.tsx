import { Subject } from 'rxjs';

const subject = new Subject();

export const Service = {
    sendMessage: value => subject.next(value),
    getMessage: () => subject.asObservable()
};