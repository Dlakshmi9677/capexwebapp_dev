// Copyright (c) 2020 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import {LoadDataModalFactory, withState} from 'visur.kepler.gl/components';
import {LOADING_METHODS} from '../constants/default-settings';
import {FileUpload} from '../components/load-data-modal/file-uploader/file-upload'
import SampleMapGallery from '../components/load-data-modal/sample-data-viewer';
import LoadRemoteMap from '../components/load-data-modal/load-remote-map';
import LoadRemoteFiles from '../components/load-data-modal/load-remote-files'
import LoadRemotePublicFiles from '../components/load-data-modal/load-remote-public-files'
// import SampleMapsTab from '../components/load-data-modal/sample-maps-tab';

import {loadRemoteMap, loadSample, loadSampleConfigurations} from '../actions';
// import { from } from 'rxjs';

const CustomLoadDataModalFactory = (...deps) => {
  const LoadDataModal = LoadDataModalFactory(...deps);
  //const defaultLoadingMethods = LoadDataModal.defaultProps.loadingMethods;
  const additionalMethods = {
    fileUpload:{
      id: LOADING_METHODS.upload,
      label: 'modal.loadData.upload',
      elementType: FileUpload
    },
    remote: {
      id: LOADING_METHODS.remote,
      label: 'Load Visur Dataset',
      elementType: LoadRemoteMap
    },
    private:{
      id: LOADING_METHODS.private,
      label: 'Load Private Files',
      elementType: LoadRemoteFiles
    },
    public:{
      id: LOADING_METHODS.public,
      label: 'Load Public Files',
      elementType: LoadRemotePublicFiles
    }
    // sample: {
    //   id: LOADING_METHODS.sample,
    //   label: 'load visur sample dataset',
    //   elementType: SampleMapGallery,
    //   tabElementType: SampleMapsTab
    // }
  };

  // add more loading methods
  LoadDataModal.defaultProps = {
    ...LoadDataModal.defaultProps,
    loadingMethods: [
      //defaultLoadingMethods.find(lm => lm.id === 'upload'),
      additionalMethods.fileUpload,
      additionalMethods.remote,
      additionalMethods.private,
      additionalMethods.public
      // defaultLoadingMethods.find(lm => lm.id === 'storage'),
      // additionalMethods.sample
    ]
  };

  return withState([], state => ({}), {
    onLoadSample: loadSample,
    onLoadRemoteMap: loadRemoteMap,
    loadSampleConfigurations
  })(LoadDataModal);
};

CustomLoadDataModalFactory.deps = LoadDataModalFactory.deps;

export function replaceLoadDataModal() {
  return [LoadDataModalFactory, CustomLoadDataModalFactory];
}
