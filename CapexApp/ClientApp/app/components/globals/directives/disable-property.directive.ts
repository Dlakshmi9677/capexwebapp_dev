import { Directive, Input, OnInit } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[disableControl]'
})
export class DisableProperty implements OnInit {
  @Input() set disableControl(colfields: any) {
    let condition = !colfields.update || colfields.isCalculated || colfields.disable;
    const action = condition ? 'disable' : 'enable';
    var control = this.ngControl.control ? this.ngControl.control:undefined;
    if(control)
    this.ngControl.control[action]({ emitEvent: false });
  }
  constructor(private ngControl: NgControl) {
  }

  ngOnInit(): void {
  }
}