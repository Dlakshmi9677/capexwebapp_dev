
import {  ChangeDetectorRef,  Directive,  ElementRef,  Input,  NgZone,  OnDestroy,  OnInit,  Component,  Inject, HostListener } from '@angular/core';
import { fromEvent, Subject, of as observableOf } from 'rxjs';
import { map, switchMap, takeUntil } from 'rxjs/operators';


@Directive({
  selector: '[draggableDialog]',
})
export class DraggableDialogDirective implements OnInit, OnDestroy {
  // Element to be dragged
  private _target: HTMLElement;

  @Input() Draggable: HTMLElement;
  @Input() DraggableData: any;

  private isDragable: boolean = false;

  // dialog container element to resize
  private _container: HTMLElement;

  // Drag handle
  private _handle: HTMLElement;
  private _delta = { x: 0, y: 0 };
  private _offset = { x: 0, y: 0 };

  private _destroy$ = new Subject<void>();

  private innerWidth;
  private innerHeight;

  private elementWidth;
  private elementHeight;

  constructor( private _elementRef: ElementRef,  private _zone: NgZone,  private _cd: ChangeDetectorRef ) { }

  public ngOnInit(): void {
    this.innerWidth = window.innerWidth;
    this.innerHeight = window.innerHeight;

    this.elementWidth = this.Draggable.clientWidth;
    this.elementHeight = this.Draggable.clientHeight;

    this._elementRef.nativeElement.style.cursor = 'default';
    this._handle = this._elementRef.nativeElement.parentElement.parentElement.parentElement;
    this._target = this._elementRef.nativeElement.parentElement.parentElement.parentElement;
    this._container = this._elementRef.nativeElement.parentElement.parentElement;
    this._container.style.overflow = 'hidden';

    this._setupEvents();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    this.innerHeight = window.innerHeight;
  }

  public ngOnDestroy(): void {
    if (!!this._destroy$ && !this._destroy$.closed) {
      this._destroy$.next();
      this._destroy$.complete();
    }
  }

  private _setupEvents() {
    this._zone.runOutsideAngular(() => {
      const mousedown$ = fromEvent(this._handle, 'mousedown');
      const mousemove$ = fromEvent(document, 'mousemove');
      const mouseup$ = fromEvent(document, 'mouseup');

      const mousedrag$ = mousedown$.pipe(
        switchMap((event: any) => {
          const startX = event.clientX;
          const startY = event.clientY;
          const rectX = this._container.getBoundingClientRect();

          if (event.target.id == "dragHandler" )
            this.isDragable = true;
          else
            this.isDragable = false;

          if (
            // if the user is clicking on the bottom-right corner, he will resize the dialog
            startY > rectX.bottom - 15 &&
            startY <= rectX.bottom &&
            startX > rectX.right - 15 &&
            startX <= rectX.right
          ) {

            return observableOf(null);
          }

          return mousemove$.filter((res) => this.isDragable).pipe(
            map((innerEvent: MouseEvent) => {
              innerEvent.preventDefault();
              this._delta = {
                x: innerEvent.clientX - startX,
                y: innerEvent.clientY - startY,
              };
            }),
            takeUntil(mouseup$),
          );
        }),
        takeUntil(this._destroy$),
      );

      mousedrag$.subscribe(() => {
        if (this._delta.x === 0 && this._delta.y === 0) {
          return;
        }
        if (this.isDragable)
          this._translate();
      });

      mouseup$.pipe(takeUntil(this._destroy$)).filter((res) => this.isDragable).subscribe(() => {
        this._offset.x += this._delta.x;
        this._offset.y += this._delta.y;
        this._delta = { x: 0, y: 0 };
        this._cd.markForCheck();
      });
    });
  }

  private _translate() {
    requestAnimationFrame(() => {
      this._target.style.transform = ` translate(${this.calMaxWidth()}px,  ${this.calMaxHeight()}px) `;
    });
  }

  private calMaxWidth() {
    let calcWidth;
    // if(this.DraggableData && (this.DraggableData.Type == "TodayVolume" ||this.DraggableData.Type == "LoadFluid")){
    //   this.elementWidth = 540;
    // }else{
    //   if(this.DraggableData && (this.DraggableData.Type == "WellHeaderVolume")){
    //     this.elementWidth = 600;
    //   }
    // }
    this.elementWidth = this._elementRef.nativeElement.clientWidth;
    calcWidth = (this.innerWidth / 2) - (this.elementWidth / 2);
    let result = this._offset.x + this._delta.x;

    if (result > 0 && result > calcWidth) 
      return calcWidth;
    else if (result < 0 && result < -Math.abs(calcWidth))
      return -Math.abs(calcWidth);
    else
        return result;
  }

  private calMaxHeight() {
    let calcHeight;
    // if(this.DraggableData && (this.DraggableData == "attentionpopup") ){
    //   this.elementHeight = 188;
    // }
    // else if(this.DraggableData && (this.DraggableData.Type == "TodayVolume")){
    //   this.elementHeight = 245
    // }else{
    //   if(this.DraggableData && (this.DraggableData.Type == "WellHeaderVolume")){
    //     this.elementHeight = 290;
    //   }else{
    //     if(this.DraggableData && (this.DraggableData.Type == "LoadFluid")){
    //       this.elementHeight = 375;
    //     }
    //   }
    // }
    this.elementHeight = this._elementRef.nativeElement.clientHeight;
    calcHeight = (this.innerHeight / 2) - (this.elementHeight / 2);
    
    let result = this._offset.y + this._delta.y;

    if (result > 0 && result > calcHeight)
      return calcHeight;
    else if (result < 0 && result < -Math.abs(calcHeight))
      return -Math.abs(calcHeight);
    else
      return result;
  }

}
