import { Directive, ElementRef, HostListener, EventEmitter, Output, OnInit,  Input } from '@angular/core';
import { NgControl } from '@angular/forms';
import {CommonService} from '../CommonService';

@Directive({
    selector: '[isOutOfBound]',
    exportAs: 'outOfBound'
  })
  export class IsOutOfBoundDirective {
    constructor(private el: ElementRef, private commonService: CommonService) { }
    @Input('isOutOfBound') outOfBound = false;
    @HostListener('mouseenter',['$event']) onEnter(event: MouseEvent) {
        // console.log(event);
        const offWidth = this.el.nativeElement.offsetWidth;
        const scrollWidth = this.el.nativeElement.scrollWidth;
        // console.log(this.outOfBound);
        if(offWidth < scrollWidth)
        this.outOfBound = true;
        else
        return this.outOfBound = false;
    }
  }