import { ChangeDetectorRef, Directive, ElementRef, HostListener, Input, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NgSelectComponent } from '@ng-select/ng-select';
import { Subject, Subscription } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { CommonService } from '../CommonService';
export const PUMPTYPE="PumpType";
export const PUMPPLACEHOLDER="Pump Type"

@Directive({
    selector: '[ngSelectEditFocused]'
})
export class NgSelectEditFocusedDirective implements OnInit, OnDestroy {
    @Input() triggerCD:any;
   // @Input() Draggable: HTMLElement;
   // @Input() : any;
   @Input() myForm: FormGroup;
   @Input() EntityId;
   @Input() EntityType;
   @Input() colfieldsLabel;
   @Input() actualName;
   @Input() ConfigId;

   private subscriptions: Subscription[] = [];
  
   comboBoxSelector = "input[role='combobox']";
   className = 'ng-select-edit-focused';
   companies=["Licensee","Operator" ];
   //commonDonwstreamOptions=["nestedUnder","DownStreamLocationName" ];
   takeUntilDestroyObservables=new Subject();

    constructor(private ngSelect: NgSelectComponent, private render: Renderer2,private el: ElementRef,private cdref: ChangeDetectorRef,private commonservice:CommonService) {


        if (this.commonservice.getCurrentNgSelectUtil$) {
            this.commonservice.getCurrentNgSelectUtil$.pipe(filter((data: any) =>
            (data.EntityId == this.EntityId &&data.EntityType == this.EntityType &&data.PropertyName!=PUMPTYPE && this.ngSelect&&( (this.ngSelect.selectedItems.length!=0
            && ((this.ngSelect.selectedItems[0].value && this.ngSelect.selectedItems[0].value["EntityId"] 
            && (data.PropertyValue==this.ngSelect.selectedItems[0].value["EntityId"] || data.OldValue==this.ngSelect.selectedItems[0].value["EntityId"]))
            || (this.ngSelect.selectedItems[0].value && this.ngSelect.selectedItems[0].value["ConfigId"]&&  (data.PropertyValue==this.ngSelect.selectedItems[0].value["ConfigId"] || data.OldValue==this.ngSelect.selectedItems[0].value["ConfigId"]))
            || (data.OldValue==this.ngSelect.selectedItems[0].label || data.PropertyValue==this.ngSelect.selectedItems[0].label ))) || (data.PropertyValue==null &&this.ngSelect.selectedItems.length==0 ) )))).pipe(this.compUntilDestroyed()).subscribe((res: any) => {
              try {
            const comboBox = this.getComboBox();
            if (res && comboBox && this.ngSelect.selectedItems.length ) {
                if(this.companies.includes(res.PropertyName)  && !res.PropertyName.includes(this.ngSelect.placeholder)){
                    return;
                }
                // if(this.commonDonwstreamOptions.includes(res.PropertyName)  && !res.PropertyName.includes("nested Under")){
                //     return;
                // }
                if(this.ConfigId && res.configId && this.ConfigId != res.configId ){
                    return;
                }

                if(this.ngSelect.selectedItems[0].value && this.ngSelect.selectedItems[0].value["EntityId"] || this.ngSelect.selectedItems[0].value["ConfigId"]){
                    comboBox.value=res.actualname?res.actualname: (this.ngSelect.selectedItems[0].value  ["EntityName"]?this.ngSelect.selectedItems[0].value["EntityName"]:null);
                }else{
                    comboBox.value =res.PropertyValue;

                }
            }
            else if(res && this.ngSelect.selectedItems.length==0 && this.colfieldsLabel && this.colfieldsLabel==res.PropertyName){
                comboBox.value =res.PropertyValue;
            }

              }
              catch (e) {
                this.commonservice.appLogException(new Error('Exception in getCurrentNgSelectUtil$() of NgSelectEditFocusedDirective in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
              }
            });


                this.commonservice.getCurrentNgSelectUtil$.pipe(filter((data: any) =>
                (data.EntityId == this.EntityId &&data.EntityType == this.EntityType &&data.PropertyName==PUMPTYPE && this.ngSelect
                   && ((this.ngSelect.selectedItems.length!=0 &&
                (data.OldValue==this.ngSelect.selectedItems[0].label || data.PropertyValue==this.ngSelect.selectedItems[0].label )) 
                || (this.ngSelect.selectedItems.length==0 &&(this.ngSelect.placeholder==PUMPPLACEHOLDER || this.ngSelect.placeholder==PUMPTYPE) ))))).pipe(this.compUntilDestroyed()).subscribe((res: any) => {
                  try {
                
                    const comboBox = this.getComboBox();
                    if (res && comboBox && (this.ngSelect.selectedItems.length) ||(this.ngSelect.placeholder==PUMPPLACEHOLDER || this.ngSelect.placeholder==PUMPTYPE)) {
                            comboBox.value =res.OldValue;                        
                    }
                }
    
                  
                  catch (e) {
                    this.commonservice.appLogException(new Error('Exception in getCurrentNgSelectUtil$() secondSubscription of NgSelectEditFocusedDirective in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
                  }
                });
          }


    }

    ngOnInit() {
        try{
            this.render.addClass(this.ngSelect.element,this.className);
              this.subscriptions = [
                this.ngSelect.blurEvent.subscribe(evt => this.onBlur(evt)),
                this.ngSelect.changeEvent.subscribe(evt => this.onChange(evt)),
                this.ngSelect.focusEvent.subscribe(evt => this.onFocus(evt)),
                this.ngSelect.clearEvent.subscribe(evt => this.onClear(evt))
              ];

        }catch(e){
            console.error(e);
        }

    }
    ngOnDestroy() {
        try{
            this.render.removeClass(
                this.ngSelect.element,
                this.className
              );
              this.subscriptions.forEach(sub => sub.unsubscribe());

            this.takeUntilDestroyObservables.next();
            this.takeUntilDestroyObservables.complete();
        }catch(e){
            console.error(e);
        }

    }
  
    getComboBox(): HTMLInputElement {
        try{
            return this.ngSelect.element.querySelector(
                this.comboBoxSelector
              );
        }catch(e){
            console.error(e);
        }

    }
  
    onFocus($event) {
        try{
            const comboBox = this.getComboBox();
            if (comboBox && this.ngSelect.selectedItems.length) {
                if(this.ngSelect.bindValue==undefined && this.ngSelect.selectedItems[0].value!=undefined && this.ngSelect.selectedItems[0].value!=""){
                    if(this.ngSelect.selectedItems[0].value["EntityName"]){
                        this.ngSelect.selectedItems[0].label=this.ngSelect.selectedItems[0].value["EntityName"];
                    }else if(this.ngSelect.selectedItems[0].value["text"]){
                        this.ngSelect.selectedItems[0].label=this.ngSelect.selectedItems[0].value["text"];
                    }
                    else{
                        let value:any=this.ngSelect.selectedItems[0].value
                        this.ngSelect.selectedItems[0].label=value;
                    }
                }
              comboBox.value = this.ngSelect.selectedItems[0].label;
            }
        }catch(e){
            console.error(e);
        }

    }
  
    onChange($event) {
        try{
            const comboBox = this.getComboBox();
            if (comboBox && this.ngSelect.selectedItems.length) {
              setTimeout(() => {
                  if($event){
                    if(this.ngSelect.bindValue==undefined && this.ngSelect.selectedItems[0].value!=undefined && this.ngSelect.selectedItems[0].value!=""){
                        if(this.ngSelect.selectedItems[0].value["EntityName"]){
                            this.ngSelect.selectedItems[0].label=this.ngSelect.selectedItems[0].value["EntityName"];
                        }else if(this.ngSelect.selectedItems[0].value["text"]){
                            this.ngSelect.selectedItems[0].label=this.ngSelect.selectedItems[0].value["text"];
                        }
                        else{
                            let value:any=this.ngSelect.selectedItems[0].value
                            this.ngSelect.selectedItems[0].label=value;
                        }
                    }
                }
                if (this.ngSelect.selectedItems[0]) {
                  comboBox.value = this.ngSelect.selectedItems[0].label;
                }
              }, 0);
            }
        }catch(e){
            console.error(e);
        }

    }
  
    onClear($event) {
        try{
            const comboBox = this.getComboBox();
            if (comboBox) {
              comboBox.value = '';
              comboBox.blur();
             
            }

        }catch(e){
            console.error(e);
        }

    }
  
    onBlur($event) {
        try{
            const comboBox = this.getComboBox();
            if (comboBox) {
              comboBox.value = '';
            }
        }catch(e){
            console.error(e);
        }

    }

    @HostListener('keyup', ['$event'])
    keyEventUp(event: KeyboardEvent) {
        if (event.keyCode === 13 && event.key == "Enter") {
            if (this.EntityType == "EMGlobalAdmin" &&
                (this.ngSelect.placeholder == "Time Zone" || this.ngSelect.placeholder == "TimeZone") && this.colfieldsLabel == "TimeZone" && this.myForm.controls.TimeZone) {
                console.log("***********************key UP*******************************");
               setTimeout(() => {
                const comboBox = this.getComboBox();
                if (comboBox) {
                    comboBox.blur();
                }
               }, 10);
             
            }
            //   event.preventDefault();
            //   event.stopPropagation();
            //   return false;
        }
    }

    
compUntilDestroyed():any {
    return takeUntil(this.takeUntilDestroyObservables);
    }
    
}
