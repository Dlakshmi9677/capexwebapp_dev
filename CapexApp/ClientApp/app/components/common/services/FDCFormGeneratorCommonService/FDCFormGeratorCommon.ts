//external imports
import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { EntityStateConstantsKeys } from 'ClientApp/app/components/common/state/entity.state.model';
import { FABRICS, FabricsNames, MessageKind } from 'ClientApp/app/components/globals/Model/CommonModel';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

//internal services imports
//App consts,model classes
import { WORKER_TOPIC } from 'ClientApp/webWorker/app-workers/shared/worker-topic.constants';
import { AppConfig } from 'ClientApp/app/components/globals/services/app.config';
import { CommonService } from 'ClientApp/app/components/globals/CommonService';
import { Mesurements, EntityTypesReverseKVPair, IconNameJson, ReverseMesurements } from '../../Constants/JsonSchemas';
import { EntityTypes } from 'ClientApp/app/components/globals/Model/EntityTypes';
import { MessageModel, Routing } from 'ClientApp/app/components/globals/Model/Message';



@Injectable()
export class FDCFormGeratorCommon {
  UnitsOfMeasureMents: any = {};
  GasUnitsOfMeasureMentsEntityTypes: any = [];
  public CustomerAppsettings = AppConfig.AppMainSettings;

  constructor(public commonService: CommonService, private formBuilder: FormBuilder) {
  }

  /**
   * MultiGridRowSpan;
   */
  MultiGridRowSpan = 1;

  /**
   * gridLoop is used for iterating datas if the type is gridType.this method is calling from CommonIterationData() method.
   * @param col contains the total  no of columns.here we are assigning different parameters to each column data.
   *  @example example of gridLoop() method
   *  gridLoop()
   *  {
   *     //todo
   *  }
   */
  public gridLoop(col?, properties?, EntityType?, METER_JSON?,AccordionName?) {
    try {
      let colsArray = col.grid.cols;
      let colsArrayLength = colsArray.length;
      if (colsArrayLength > 0) {
        let maxIndexCol = colsArray[colsArrayLength - 1].col;
        var extraColumns = maxIndexCol - colsArrayLength;
        for (let i = 0; i < colsArrayLength + extraColumns; i++) {
          let item1 = colsArray.find(j => j.col === i + 1);
          if (item1 && item1.type != "none") {
            let propertyLabel = colsArray[i].label;
            this.updatePropertiesGrid(properties, propertyLabel,AccordionName);
            if (METER_JSON)
              METER_JSON["Controls"].push(properties[propertyLabel].actualname);
            if (properties[propertyLabel].colspan) {
              colsArray[i].colspan = properties[propertyLabel].colspan;
            } else {
              colsArray[i].colspan = 1;
              properties[propertyLabel].colspan = 1;
            }

          } else {
            if (item1 == undefined) {
              colsArray.push({ type: "none", col: i + 1, colspan: 1 });
            }
          }
          this.sort(colsArray);
        }
        this.sort(colsArray);
  
      }
    } catch (e) {
      this.commonService.appLogException(
        new Error(
          "Exception in gridLoop() of FdcFormGeneratorComponent in FDCFormTemplate at time " +
          new Date().toString() +
          ". Exception is : " +
          e
        )
      );
    }
  }

  /**
   * objectLoop is used for iterating datas if the type is objectType.this method is calling from CommonIterationData() method.
   * @param col contains the total  no of columns.here we are assigning different parameters to each column data.
   *  @example example of objectLoop() method
   *  objectLoop()
   *  {
   *     //todo
   *  }
   */
  public objectLoop(col?, properties?, METER_JSON?) {
    try {
      for (let i = 0; i < col.elements.length; i++) {
        if (i != col.elements[i].col - 1) {
          col.elements.splice(i, 0, { type: "none", colspan: 1 });
        } else {
          const propertyLabel = properties[col.elements[i].label] ? properties[col.elements[i].label].actualname : null;
          if (propertyLabel != null) {
            this.updatePropertiesObject(properties, propertyLabel);
            if (METER_JSON)
              METER_JSON["Controls"].push(propertyLabel);
            this.updateColsPan(col, properties, i);
          }
        }
      }
    } catch (e) {
      this.commonService.appLogException(
        new Error(
          "Exception in objectLoop() of FdcFormGeneratorComponent in FDCFormTemplate at time " +
          new Date().toString() +
          ". Exception is : " +
          e
        )
      );
    }
  }

  initializeCommonFields(entityData, properties?, bindhierarchy?: boolean,entityName?) {
    // if (this.fDCService.selectParent && (bindhierarchy || (entityData.EntityType && !Locations.includes(entityData.EntityType) && !this.fDCService.IsFdcAdmin))) {
    //   entityData.District = this.fDCService.District;
    //   entityData.DistrictId = this.fDCService.DistrictId;
    //   entityData.Area = this.fDCService.Area;
    //   entityData.AreaId = this.fDCService.AreaId;
    //   entityData.Field = this.fDCService.FieldId;//need to send FieldId
    //   entityData.FieldId = this.fDCService.FieldId;
		// }
		// let entity: any = this.commonService.getEntitiesById(this.fDCService.LocationId);
    // entityData.LocationId = this.fDCService.LocationId;
    // entityData.LocationType = entity ? entity.EntityType : null;
    // entityData.LocationName = entity ? entity.EntityName : null;
    // entityData.TenantName = this.commonService.tenantName;
    // entityData.TenantId = this.commonService.tenantID;
    // entityData.ModifiedBy = this.commonService.CurrentUserEntityName;
    // entityData.ModifiedDateTime = new Date().toISOString().slice(0, 19).replace("T", " ");
    // if (!entityData.CreatedDateTime)
    //   entityData.CreatedDateTime = entityData.ModifiedDateTime;
    // if (!entityData.CreatedBy) {
    //   entityData.CreatedBy = this.commonService.currentUserName;
    // }
    // if (properties) {
    //     entityData = this.toggleDataModification(entityData, properties, entityName);
    // }
    return entityData;
  }

  toggleDataModification(entityData, properties1,entityName?) {
    let properties = JSON.parse(JSON.stringify(properties1));
    for (var key in entityData) {
      if (properties[key]) {
        if (
          properties[key].type == "boolean" ||
          properties[key].type == "checkbox" ||
          properties[key].type == "toggle"
        ) {
          if (entityData[key] == null || entityData[key] == undefined) {
            entityData[key] = false;
          }
          if (ReverseMesurements[properties[key].checkboxvalue.true]) {
            properties[key].checkboxvalue.true =
              ReverseMesurements[properties[key].checkboxvalue.true];
          }
          if (ReverseMesurements[properties[key].checkboxvalue.false]) {
            properties[key].checkboxvalue.false =
              ReverseMesurements[properties[key].checkboxvalue.false];
          }
          // if (entityData && entityData.EntityType && properties[key] && properties[key].checkboxvalue
          //   && entityData.EntityType == EntityTypes.TRUCKTICKET && entityData.EntityType == EntityTypes.FFV) {
            // if (entityName && entityName=="OrificeMeterInfo" && properties[key].checkboxvalue.hasOwnProperty(entityData[key])) {
            //   entityData[key] = properties[key]["checkboxvalue"][entityData[key]];
            // }
          //}
          // else if (entityData && !entityData.EntityType) {
          //   if(entityName == "FuelFlareVent" || entityName =="TruckTicket" || entityName =="Truck Tickets"){ //ffv global admin
          //     if(entityData[key] == true  || entityData[key] == false)
          //       entityData[key] = properties[key]["checkboxvalue"][entityData[key]];
          //   }          
          // }
        }
        else if (
          properties[key].type == "multiselect" ||
          properties[key].type == "list"
        ) {
          if (
            entityData[key] == null ||
            entityData[key] == "" ||
            entityData[key] == undefined
          ) {
            entityData[key] = [];
          }
        }
      }
    }
    return entityData;
  }

  tableLoop(datas, properties) {
    let data = datas.cols;
    let lengthCol = datas.numberOfCols;
    try {
      for (let i = 0; i < data.length; i++) {
        if (lengthCol >= i) {
          if (i != data[i].col - 1) {
            data.splice(i, 0, { type: "none", col: i });
          } else {
            data[i].displayLabel = properties[data[i].label].label;
            data[i].placeholder = properties[data[i].label].placeholder
              ? properties[data[i].label].placeholder
              : data[i].displayLabel;
            data[i].measurement = properties[data[i].label].measures_left;
            data[i].type = properties[data[i].label].type;
            data[i].actualname = properties[data[i].label].actualname;
            data[i].options = properties[data[i].label].options;
            data[i].isCalculated = properties[data[i].label].isCalculated;
            data[i].originalname = properties[data[i].label].actualname;
            data[i].className = properties[data[i].label].className;
            data[i].disable = properties[data[i].label].disable;
            data[i].min = properties[data[i].label].min;
            data[i].overDue = properties[data[i].label].overDue;
            data[i].flexCount = properties[data[i].label].flexCount
              ? properties[data[i].label].flexCount
              : 20;
            data[i].minWidthcontain = properties[data[i].label].minWidthcontain ? true: false;
          
            data[i].DefaultValue = properties[data[i].label].hasOwnProperty("DefaultValue") ? properties[data[i].label].DefaultValue :"";
            if (properties[data[i].label].hasOwnProperty("isDynamicWidth")) {
              data[i].isDynamicWidth = properties[data[i].label].isDynamicWidth;
            } else {
              data[i].isDynamicWidth = true;
            }
            if (data[i].type == "img") data[i].flexCount = 0;
            //if (data[i].type == "imgIcon") data[i].flexCount = 4;
            
            if (properties[data[i].label].colspan) {
              data[i].colspan = properties[data[i].label].colspan;
            } else {
              data[i].colspan = 1;
            }
            data[i].conditional = properties[data[i].label].conditional;
            if(!this.commonService.isCapbilityAdmin()){
              data[i].update = properties[data[i].label].update != undefined ? properties[data[i].label].update : false;
              data[i].scope = properties[data[i].label].scope != undefined ? properties[data[i].label].scope : 'None';
            }
            else{
              data[i].update = true;
              data[i].scope = "Update";
            }
            data[i].allowDuplicate = properties[data[i].label].hasOwnProperty("allowDuplicate") ? properties[data[i].label].allowDuplicate : true;
            if (properties[data[i].label].hasOwnProperty("validation")) {
              data[i].validation = properties[data[i].label].validation;
            }

            if (properties[data[i].label].checkboxvalue) {
              data[i].checkboxvalue = properties[data[i].label].checkboxvalue;
            }
            if (properties[data[i].label].decimalPrecision) {
              data[i].decimalPrecision =
                properties[data[i].label].decimalPrecision;
            }
          }
        }

        if (data[i].label == "TankSizeInMeterlabel") {
          var FirstIndexOfvalue = data[i].displayLabel.indexOf("(");
          var lastIndexOfvalue = data[i].displayLabel.indexOf(")");
          var defultunitValue = data[i].displayLabel.substring(
            FirstIndexOfvalue + 1,
            lastIndexOfvalue
          );
          data[i].displayLabel = data[i].displayLabel.replace(
            defultunitValue,
            Mesurements[defultunitValue]
          );
        }
      }
    } catch (e) {
      this.commonService.appLogException(
        new Error(
          "Exception in tableLoop() of FdcFormGeneratorComponent in FDCFormTemplate at time " +
          new Date().toString() +
          ". Exception is : " +
          e
        )
      );
    }
  }

  /**
   * sort method is used for sorting the columns in a accending order.
   * @param cols  ontains the total  no of columns.here we are assigning different parameters to each column data.
   *  @example example of sort() method
   *  sort()
   *  {
   *     //todo
   *  }
   */
  public sort(cols) {
    try {
      (<Array<any>>cols).sort((leftSide, rightSide) => {
        if (leftSide.col < rightSide.col) return -1;
        if (leftSide.col > rightSide.col) return 1;
        return 0;
      });
    } catch (e) {
      this.commonService.appLogException(
        new Error(
          "Exception in sort() of FdcFormGeneratorComponent in FDCFormTemplate at time " +
          new Date().toString() +
          ". Exception is : " +
          e
        )
      );
    }
  }
  public CommonCreateGroup(propertyName, properties, EntityType) {
    try {
      const generalproperties = properties[propertyName];
      let group = this.formBuilder.control({});
      if (generalproperties) {
        var defaultvalue: any;
        defaultvalue = properties[propertyName]["DefaultValue"];
        if (Mesurements[defaultvalue]) {
          defaultvalue = Mesurements[defaultvalue];
        }
        var requiredstatus =
          properties[propertyName]["validation"] != null
            ? properties[propertyName]["validation"]["required"]
            : false;
        var pattern =
          properties[propertyName]["validation"] != null
            ? properties[propertyName]["validation"]["pattern"]
            : false;
        var currentDate = new Date();
        // if (this.fDCService.calenderProductionDate) {
        //   currentDate = new Date(this.fDCService.calenderProductionDate);
        //   currentDate.setHours(8, 0, 0, 0);
        // }
        switch (generalproperties.actualname) {
          case "EntityName":
            defaultvalue = this.commonService.NameOfEntity;
            break;
          case "EntityType":
            defaultvalue = EntityType;
            break;
          case "OnProdDate":
          case "OnProductionDate":
            if(this.commonService.lastOpenedFabric.toUpperCase()==FABRICS.ENTITYMANAGEMENT.toUpperCase() && this.commonService.EMProductionDate){
              currentDate = new Date(this.commonService.EMProductionDate);
              currentDate.setHours(8, 0, 0, 0);
            }
            defaultvalue = currentDate;
            break;
          case "LoadProductionDay":
          case "UnloadProductionDay":
            defaultvalue = currentDate;
            break;
          case "VisurStartDate":
              let newdate = new Date(new Date().getFullYear(), new Date().getMonth(), 0, 8, 0, 0, 0);
              defaultvalue = currentDate <= newdate ? currentDate : newdate ;
            break;
          case "InstallDate":
            defaultvalue = new Date(this.commonService.EMProductionDate);
            defaultvalue.setHours(8, 0, 0, 0);
            break;
          default:
            if (generalproperties.type == "datetime") {
              if(EntityType == "Recalculate"){
                defaultvalue = this.commonService.maxDate;
              }else {
                defaultvalue = currentDate;
              }
            }
            break;
        }
        if (generalproperties && generalproperties.type == "select" && defaultvalue == "") {
          defaultvalue = null;
        }
        if (generalproperties && generalproperties.type == "double") {
          var decimalLength =
            generalproperties["decimalPrecision"] != undefined
              ? generalproperties["decimalPrecision"]["maxLength"]
              : "2";
          defaultvalue = Number(defaultvalue).toFixed(Number(decimalLength));
        }
        if (
          generalproperties.type == "toggle" ||
          generalproperties.type == "checkbox" ||
          generalproperties.type == "boolean"
        ) {
                 if(defaultvalue==""){
                     defaultvalue=false;
                   }
          group = this.formBuilder.control(defaultvalue, {
            updateOn: "change"
          });
        } else if (requiredstatus) {
          if (
            (generalproperties.actualname == "JointsToFluid" ||
              generalproperties.actualname == "UnLoadTotalVolume" ||
              generalproperties.actualname == "LoadTotalVolume" ||
              generalproperties.actualname == "LoadGrossVolume" ||
              generalproperties.actualname == "LoadTareVolume"||
              generalproperties.actualname == "UnLoadGrossVolume" ||
              generalproperties.actualname == "UnLoadTareVolume" ||
              generalproperties.actualname == "PumpDisplacement" ||
              generalproperties.actualname == "DesignedRateDay") &&
              generalproperties.type == "double"
          ) {
            var minValue = 0.01;
            group = this.formBuilder.control(defaultvalue, {
              validators: [
                Validators.required,
                Validators.pattern(pattern),
                Validators.min(minValue)
              ]
            });
          }
          else if (generalproperties.actualname == "PumpDiameter"  &&
            generalproperties.type == "double") {
            var minValue = 0.0001;
            group = this.formBuilder.control(defaultvalue, {
              validators: [
                Validators.required,
                Validators.pattern(pattern),
                Validators.min(minValue)
              ]
            });
          } else if(generalproperties.actualname == "TankSize" && generalproperties.type == "popovertable"){
              let minValue = 0.00001;
              group = this.formBuilder.control(defaultvalue, {
                validators: [Validators.required, Validators.pattern(pattern), Validators.min(minValue)], updateOn: "blur"
              });
          }
          // else if (generalproperties.actualname=="EntityName" && this.commonService.lastOpenedFabric.toUpperCase()==FABRICS.ENTITYMANAGEMENT.toUpperCase()){
          //   group = this.formBuilder.control(defaultvalue, {
          //     validators: [Validators.required, Validators.pattern(pattern)],updateOn:"change"
          //   });
          // }
           else {
              group = this.formBuilder.control(defaultvalue, {
                validators: [Validators.required, Validators.pattern(pattern)], updateOn: "blur"
              });
          }
        } else if (!requiredstatus && pattern) {
          group = this.formBuilder.control(defaultvalue, {
            validators: [Validators.pattern(pattern)]
          });
        } else {
          group = this.formBuilder.control(defaultvalue);
        }
      }
      return group;
    } catch (e) {
      this.commonService.appLogException(
        new Error(
          "Exception in CommonCreateGroup() of FDCFormGeratorCommon in FDCFormTemplate at time " +
          new Date().toString() +
          ". Exception is : " +
          e
        )
      );
    }
  }

  public MakeDetectChanges(cdRef) {
    try {
      if (cdRef) {
        cdRef.markForCheck();
      }
    } catch (e) {
      this.commonService.appLogException(
        new Error(
          "Exception in MakeDetectChanges() of FDCFormGeratorCommon in FDCFormTemplate at time " +
          new Date().toString() +
          ". Exception is : " +
          e
        )
      );
    }
  }

  //public GetDataIntegrationIcon(name: string, active:boolean) {
  //  if (name == "ProdView" || !active) {
  //    return "wwwroot/images/AlThing UI Icon-Button Set v1/FDC_GrayStatus_Icon_Idle.svg";
  //  }
  //  return "wwwroot/images/AlThing UI Icon-Button Set v1/FDC_HealthyStatus_Icon_Idle.svg";
  //}

  public setFormControlValueforList(propertyName, propertyVal, group) {
    try {
      group.controls[propertyName].setValue(Number(propertyVal).toFixed(2), {
        emitEvent: false
      });
    } catch (e) {
      this.commonService.appLogException(
        new Error(
          "Exception in setFormControlValueforList() of FdcFormGeneratorComponent in FDCFormTemplate at time " +
          new Date().toString() +
          ". Exception is : " +
          e
        )
      );
    }
  }

  public FindIndex(list, obj) {
    try {
      return list.findIndex(function (cur) {
        return Object.keys(obj).every(function (key) {
          return obj[key] === cur[key];
        });
      });
    } catch (e) {
      this.commonService.appLogException(
        new Error(
          "Exception in FindIndex() of FdcFormGeneratorComponent in FDCFormTemplate at time " +
          new Date().toString() +
          ". Exception is : " +
          e
        )
      );
    }
  }

  public SetUnitsOfMeasurementsFOrEquip(jsonData) {
    try {
      let measureMent = Mesurements;
      if (jsonData != null && jsonData.hasOwnProperty("GasUnitsOfMeasurements") && measureMent[jsonData["GasUnitsOfMeasurements"]]) {
        jsonData["GasUnitsOfMeasurements"] = measureMent[jsonData["GasUnitsOfMeasurements"]];
      }
      if (jsonData != null && jsonData.hasOwnProperty("Units") && measureMent[jsonData["Units"]]) {
        jsonData["Units"] = measureMent[jsonData["Units"]];
      }
      if (jsonData != null && jsonData.hasOwnProperty("LiquidUnitsOfMeasurements") && measureMent[jsonData["LiquidUnitsOfMeasurements"]]) {
        jsonData["LiquidUnitsOfMeasurements"] = measureMent[jsonData["LiquidUnitsOfMeasurements"]];
      }
      if (jsonData != null && jsonData.hasOwnProperty("GaugeUom") && measureMent[jsonData["GaugeUom"]] ) {
        jsonData["GaugeUom"] = measureMent[jsonData["GaugeUom"]];
      }
    } catch (e) {
      this.commonService.appLogException(
        new Error(
          "Exception in SetUnitsOfMeasurementsFOrEquip() of FdcFormGeneratorComponent in FDCFormTemplate at time " +
          new Date().toString() +
          ". Exception is : " +
          e
        )
      );
    }
  }
  /**
   * adddata is used for add the Ticket(attached form data) to the Control which is calling from listsDataCategorizeAndSetting
   *  @example example of adddata() method
   *  adddata()
   *  {
   *     //todo
   *  }
   */
  public adddGroupData(group, properties, propertyName) {
    try {
      var generalProperties = properties[propertyName];

      if (generalProperties) {
        var defaultvalue = properties[propertyName]["DefaultValue"];
        if (generalProperties && generalProperties.type == "double") {
          var decimalLength =
            generalProperties["decimalPrecision"] != undefined
              ? generalProperties["decimalPrecision"]["maxLength"]
              : "2";
          defaultvalue = Number(defaultvalue).toFixed(Number(decimalLength));
        }
        var requiredstatus =
          properties[propertyName]["validation"] != null
            ? properties[propertyName]["validation"]["required"]
            : false;
        var pattern =
          properties[propertyName]["validation"] != null
            ? properties[propertyName]["validation"]["pattern"]
            : false;
        var maxValue = properties[propertyName]["validation"]
          ? properties[propertyName]["validation"]["maxValue"]
          : false;
        var minValue = properties[propertyName]["validation"]
          ? properties[propertyName]["validation"]["minValue"]
          : false;
        var currentDate = new Date();
        // if (this.fDCService.calenderProductionDate) {
        //   currentDate = new Date(this.fDCService.calenderProductionDate);
        // }
        switch (generalProperties.actualname) {
          case "EntityName":
            defaultvalue = this.commonService.NameOfEntity;
            break;
          case "OnProdDate":
          case "OnProductionDate":
          case "FromDate":
            defaultvalue = currentDate;
            break;
        }
        if (
          generalProperties.type == "toggle" ||
          generalProperties.type == "checkbox" ||
          generalProperties.type == "boolean"
        ) {
          group.addControl(
            propertyName,
            this.formBuilder.control(defaultvalue, {
              updateOn: "change",
              validators: []
            })
          );
        } else if (requiredstatus) {
          group.addControl(
            propertyName,
            this.formBuilder.control(defaultvalue, [
              Validators.required,
              Validators.pattern(pattern),
              Validators.max(maxValue),
              Validators.min(minValue)
            ])
          );
        } else if (!requiredstatus && pattern) {
          group.addControl(
            propertyName,
            this.formBuilder.control(defaultvalue, [
              Validators.pattern(pattern)
            ])
          );
        } else {
          group.addControl(
            propertyName,
            this.formBuilder.control(defaultvalue)
          );
        }
      }
      return group;
    } catch (e) {
      this.commonService.appLogException(
        new Error(
          "Exception in adddGroupData() of FdcFormGeneratorComponent in FDCFormTemplate at time " +
          new Date().toString() +
          ". Exception is : " +
          e
        )
      );
    }
  }
  ObservablesOfFormSubscription = [];

  public CreateControlForAddedArray(element, list, properties) {
    try {
      let group = this.formBuilder.control({});
      var generalProperties = properties[element];
      if (generalProperties) {
        var requiredstatus =
          properties[element]["validation"] != null
            ? properties[element]["validation"]["required"]
            : false;
        var pattern =
          properties[element]["validation"] != null
            ? properties[element]["validation"]["pattern"]
            : false;
        if (requiredstatus) {
          if (
            properties[element] &&
            properties[element].hasOwnProperty("decimalPrecision")
          ) {
            let data = properties[element]["decimalPrecision"];
            group = this.formBuilder.control(
              Number(list[element]).toFixed(data["maxLength"])
            );
          } else {
            group = this.formBuilder.control(list[element], [
              Validators.required,
              Validators.pattern(pattern)
            ]);
          }
        } else {
          if (
            properties[element] &&
            properties[element].hasOwnProperty("decimalPrecision")
          ) {
            let data = properties[element]["decimalPrecision"];
            group = this.formBuilder.control(
              Number(list[element]).toFixed(data["maxLength"])
            );
          } else if (
            properties[element]["type"] == "boolean" ||
            properties[element]["type"] == "checkbox" ||
            properties[element]["type"] == "toggle"
          ) {
            var measures = list[element];
            if (properties[element] && properties[element]["checkboxvalue"]) {
              var checktrue = properties[element]["checkboxvalue"].true;
              if (
                measures == checktrue ||
                [true, "True", "On", "true", "on"].includes(measures)
              ) {
                group = this.formBuilder.control(true, { updateOn: "change" });
              } else {
                group = this.formBuilder.control(false, { updateOn: "change" });
              }
            } else {
              group = this.formBuilder.control(list[element], {
                updateOn: "change"
              });
            }
          } else {
            group = this.formBuilder.control(list[element]);
          }
        }
      } else {
        group = this.formBuilder.control(list[element]);
      }
      return group;
    } catch (e) {
      this.commonService.appLogException(
        new Error(
          "Exception in CreateControlForAddedArray() of FdcFormGeneratorComponent in FDCFormTemplate at time " +
          new Date().toString() +
          ". Exception is : " +
          e
        )
      );
    }
  }
  public CreateControldataForAddedArray(element, list, properties) {
    try {
      let bindingdata;
      var generalProperties = properties[element];
      if (generalProperties) {
        if (list[element] && Mesurements[list[element]]) {
          list[element] = Mesurements[list[element]];
        }
        var requiredstatus =
          properties[element]["validation"] != null
            ? properties[element]["validation"]["required"]
            : false;
        var pattern =
          properties[element]["validation"] != null
            ? properties[element]["validation"]["pattern"]
            : false;
        if (requiredstatus) {
          if (
            properties[element] &&
            properties[element].hasOwnProperty("decimalPrecision")
          ) {
            let data = properties[element]["decimalPrecision"];
            bindingdata = Number(list[element]).toFixed(
              Number(data["maxLength"])
            );
          } else {
            (bindingdata = list[element]),
              [Validators.required, Validators.pattern(pattern)];
          }
        } else {
          if (
            properties[element] &&
            properties[element].hasOwnProperty("decimalPrecision")
          ) {
            let data = properties[element]["decimalPrecision"];
            bindingdata = Number(list[element]).toFixed(
              Number(data["maxLength"])
            );
          } else if (
            list[element]["Type"] == "boolean" ||
            list[element]["Type"] == "checkbox"
          ) {
            var measures = list[element];
            if (properties[element] && properties[element]["checkboxvalue"]) {
              var checktrue = properties[element]["checkboxvalue"].true;
              if (
                measures == checktrue ||
                measures == true ||
                measures == "True"
              ) {
                bindingdata = true;
              } else {
                bindingdata = false;
              }
            } else {
              bindingdata = list[element];
            }
          } else {
            bindingdata = list[element];
          }
        }
      }
      return bindingdata;
    } catch (e) {
      this.commonService.appLogException(
        new Error(
          "Exception in CreateControldataForAddedArray() of FdcFormGeneratorComponent in FDCFormTemplate at time " +
          new Date().toString() +
          ". Exception is : " +
          e
        )
      );
    }
  }
  public UpdateMeasurementsForProperties(
    Properties,
    GasVolume,
    LiquidVolume,
    EntityType?,
    data?
  ) {
    try {
      if (
        LiquidVolume &&
        GasVolume &&
        LiquidVolume != "false" &&
        GasVolume != "false"
      ) {
        for (var key in Properties) {
          if (
            (key.toString().indexOf("Percent") == -1 && key.toString().indexOf("NGLType") == -1 &&
              (key.toString().indexOf("Condy") != -1 ||
                key.toString().indexOf("Oil") != -1 ||
                key.toString().indexOf("Fluid") != -1 ||
                key.toString().indexOf("Sand") != -1 ||
                key.toString().indexOf("Water") != -1 ||
                key.toString().indexOf("NGL") != -1 ||
                key.toString().indexOf("Volume") != -1 ||
                key.toString().indexOf("RateOrHour") != -1 ||
                key.toString().indexOf("Rate") != -1 ||
                key.toString().indexOf("TotalFlare") != -1 ||
                key.toString().indexOf("TotalFuel") != -1 ||
                key.toString().indexOf("TotalVent") != -1 ||
                key.toString().indexOf("Change") != -1)) ||
            key.toString().indexOf("Gas") != -1 ||
            (key.toString().indexOf("Units") != -1 &&
              (Properties["Units"] && Properties["Units"]["type"] == "select"))
          ) {
            if (Properties[key.toString()]) {
              let field = Properties[key.toString()]["actualname"];
              if (
                field == "Change" ||
                field == "Volume" ||
                field == "TodayVolume" ||
                field == "YesterdayVolume" || 
                field == "RateOrHour" ||
                field == "Rate" ||
                field == "TotalVent" ||
                field == "TotalFuel" ||
                field == "TotalFlare"
              ) {
                switch (EntityType) {
                  case "PDMeter":
                  case "Orifice":
                  case "RotaMeter":
                  case "Ultrasonic":
                  case "FFV":
                  case "FuelFlareVent":
                    DecimalVolume = GasVolume;
                    break;
                  default:
                    var DecimalVolume = LiquidVolume;
                    break;
                }
              }
              else if (EntityType == "TruckTicket") {
                switch (field) {
                  case "LoadGrossVolume":
                  case "LoadNetVolume":
                  case "LoadTareVolume":
                  case "UnLoadGrossVolume":
                  case "UnLoadNetVolume":
                  case "UnLoadTareVolume":
                    DecimalVolume = Properties[key.toString()].measures_left.true
                    break;
                  default:
                    DecimalVolume = LiquidVolume;
                    break;
                }
                break;
              }
              else {
                var DecimalVolume = LiquidVolume;
              }
              if (
                key.toString().indexOf("Gas") != -1 ||
                key.toString() == "STVFactor") {
                DecimalVolume = GasVolume;
              }
              if (Mesurements[DecimalVolume]) {
                DecimalVolume = Mesurements[DecimalVolume];
              }
              if (
                EntityType == "Tank" &&
                Properties["Units"] &&
                Properties["Units"]["actualname"] == "Units"
              ) {
                Properties[key.toString()].DefaultValue = DecimalVolume;
              }
              if(Properties && Properties[key].measurement_append){
                DecimalVolume = DecimalVolume + Properties[key].measurement_append
              }
              // if (EntityType == "Allowables") {
              //   DecimalVolume = DecimalVolume + "/Day";
              // } else 
              if (
                (field == "Volume" || field == "RateOrHour" || field == "Rate" || field == "TotalFlare" || field == "TotalFuel" || field == "TotalVent") &&
                (EntityType == "FFV" || EntityType == "FDCGlobalAdmin")
              ) {
                if (Mesurements[GasVolume]) {
                  DecimalVolume = Mesurements[GasVolume];
                } else {
                  DecimalVolume = GasVolume
                }
              }
              Properties[key.toString()].measures_left = {
                true: DecimalVolume,
                false: DecimalVolume
              };
            }
          } else if (EntityType == "Tank" && key == "STVFactor") {
            if(GasVolume != "mcf"){
              Properties[key].measures_left["true"] = Mesurements[Properties[key].measures_left["true"]]
              Properties[key].measures_left["false"] = Mesurements[Properties[key].measures_left["false"]]
            } else{
              Properties[key].measures_left = {
                true: GasVolume,
                false: GasVolume
              };
            }
          }
        }
      }
    } catch (e) {
      this.commonService.appLogException(
        new Error(
          "Exception in CreateControldataForAddedArray() of FdcFormGeneratorComponent in FDCFormTemplate at time " +
          new Date().toString() +
          ". Exception is : " +
          e
        )
      );
    }
  }

  //Right Mesaurements For Volume
  public VolumeMeasurementsForEquipments(dataa: any, EntityType, Properties) {
    try {
      switch (EntityType) {
        case EntityTypes.ORIFICE:
        case EntityTypes.PDMETER:
        case EntityTypes.NONMETERED:
        case EntityTypes.ULTRASONIC:
        case EntityTypes.FFV:
        case EntityTypes.TURBINE:
        case EntityTypes.CORIOLIS_METER:
          this.UpdateMeasurementsForProperties(
            Properties,
            dataa["GasUOM"],
            dataa["LiquidUOM"],
            EntityType
          );
          break;
        case EntityTypes.TANK:
          this.UpdateMeasurementsForProperties(
            Properties,
            dataa["GasUOM"],
            dataa["LiquidUOM"],
            EntityType,
            dataa
          );
          break;
        //case EntityTypes.TURBINE:
        //case EntityTypes.CORIOLIS_METER:
        //  this.UpdateMeasurementsForProperties(
        //    Properties,
        //    dataa["GasUOM"],
        //    dataa["LiquidUOM"],
        //    EntityType
        //  );
        default:
          break;
      }
    } catch (e) {
      this.commonService.appLogException(
        new Error(
          "Exception in VolumeMeasurementsForEquipments() of FdcFormGeneratorComponent in FDCFormTemplate at time " +
          new Date().toString() +
          ". Exception is : " +
          e
        )
      );
    }
  }

  //Right Mesaurements For Volume
  public UpdateMeasurementsForEquipmentsBasedOnConfig(
    dataa: any,
    EntityType,
    Properties,
    LocationUOM?
  ) {
    try {
      
      switch (EntityType) {
        case EntityTypes.ROTAMETER:
          if (Properties && Properties["Rate"]) {
            if (
              this.CustomerAppsettings.env.name == "DEV" &&
              this.CustomerAppsettings.env.UseAsDesktop == true &&
              this.commonService.onlineOffline == false &&
              dataa["RateType"] == null
            ) {
              dataa["RateType"] = "";
            }

            if (dataa["Units"] != undefined && dataa["RateType"] != undefined) {
              let rate = dataa["RateType"];
              if (dataa["RateType"] == "Per Day") {
                rate =  '/Day';
              }
              else if (dataa["RateType"] == "Per Hour") {
                rate =  '/Hour';
              }
              var units = dataa["Units"] + rate;
            } else {
              units = "10³m³/Day";
            }
            Properties["Rate"].measures_left = { true: units, false: units };
          }
          if (Properties && Properties["Volume"] && LocationUOM && LocationUOM.length && Object.keys(LocationUOM[0]).length) {
              var units = LocationUOM[0]["GasUOM"];
              Properties["Volume"].measures_left = {
                true: units,
                false: units
              };
          }
          //this.UpdateMeasurementsForProperties(Properties, dataa["GasUOM"], dataa["LiquidUOM"], true);
          break;
        case EntityTypes.ULTRASONIC:
          if (Properties && Properties["Volume"]) {
            if (dataa["EFM"] && (dataa["EFM"] != "On" ||dataa["EFM"] == true)) {
              Properties["Volume"].isCalculated = true;
            }else{
              Properties["Volume"].isCalculated = false;
            }
          }
          break;
        case EntityTypes.TANK:
          if (Properties && Properties["TankSize"]) {
            if (dataa["Units"] && dataa["Units"]) {
              var units = dataa["Units"];
              Properties["TankSize"].measures_left = {
                true: units,
                false: units
              };
              //Properties["Volume"].isCalculated = false;
            }
          }
          break;
        case EntityTypes.CORIOLIS_METER:
          if (Properties && Properties["GasVolume"]) {
            if (dataa["EFM"] && (dataa["EFM"] == "On"||dataa["EFM"] == true)) {
              Properties["GasVolume"].isCalculated = true;
            }else{
              Properties["GasVolume"].isCalculated = false;
            }
          }
          break;
      }
    } catch (e) {
      this.commonService.appLogException(
        new Error(
          "Exception in UpdateMeasurementsForEquipmentsBasedOnConfig() of FdcFormGeneratorComponent in FDCFormTemplate at time " +
          new Date().toString() +
          ". Exception is : " +
          e
        )
      );
    }
  }

  updateFieldPermission(actionmenu, createDeletePermission) {
    for (let col of actionmenu.cols) {
      let permissionsIconArray = [];
      if (col.elements) {
        for (let i = 0; i < col.elements.length; i++) {
          // if (
          //   col.elements[i].label == "Coll" &&
          //  // this.fDCService.showActivityCenterInFdcForms
          // ) {
          //   permissionsIconArray.push(col.elements[i]);
          // } else 
          if (
            (col.elements[i].label == "addTicket" || col.elements[i].label == "ffvaddIcon" || col.elements[i].label == "addIcon") &&
            createDeletePermission && createDeletePermission.Create
          ) {
            permissionsIconArray.push(col.elements[i]);
          } else if (
            col.elements[i].label == "deleteIcon" &&
            createDeletePermission && createDeletePermission.Delete
          ) {
            permissionsIconArray.push(col.elements[i]);
          }
          else if (
            col.elements[i].label == "Name"
          ) {
            permissionsIconArray.push(col.elements[i]);
          }
        }
        col.elements = permissionsIconArray;
      }
    }
  }

  updateUserFieldsScope(userFields: any[], properties: any, createDeletePermission?, isNewEntity?) {
    for (var j = 0; j < userFields.length; j++) {
      let label = userFields[j]["Name"];
      if (properties[label]) {
        switch (userFields[j]["field|scope"]) {
          case "None":
              properties[label].update = false;
              properties[label].scope = userFields[j]["field|scope"];
              break;
          case "Read":
            properties[label].update = false;
            properties[label].scope = userFields[j]["field|scope"];
            if (properties[label].type == 'datetime') {
              properties[label].isCalculated = true;
            }
            if (label == "deleteIcon" && (createDeletePermission && !createDeletePermission.Delete)) {
              properties[label].update = false;
              properties[label].scope = "None";
            }
            break;
          case "Update":
            properties[label].update = true;
            properties[label].scope = userFields[j]["field|scope"];
            if (label == "deleteIcon" && (createDeletePermission && !createDeletePermission.Delete)) {
              properties[label].update = false;
              properties[label].scope = "None";
            }
            break;
        }
        if( (createDeletePermission && !createDeletePermission.Update) && isNewEntity){
          properties[label].update = false;
          properties[label].scope = "None";
        }
      }
    }
  }

  updateAllFieldsScope(properties: any) {
    for (let propertyLabel in properties) {
      // if ( PopupForms.includes(propertyLabel) && this.fDCService.PopupFormsForPermissions && this.fDCService.PopupFormsForPermissions.length != 0) {
      //   var isPopupFormsExist = false;
      //   this.fDCService.PopupFormsForPermissions.forEach(PopupForms => {
      //     if (PopupForms.Name.toLowerCase() == propertyLabel.toLowerCase()) {
      //       isPopupFormsExist = true;
      //     } 
      //   });
      //   if (isPopupFormsExist) {
      //     properties[propertyLabel].update = true;
      //     properties[propertyLabel].scope = "Update";
      //   } else {
      //     properties[propertyLabel].update = false;
      //     properties[propertyLabel].scope = "None";
      //   }
      // }

      // if ((this.fDCService.createdeletepermissionForPopup &&!this.fDCService.createdeletepermissionForPopup["Delete"]) &&this.fDCService.isPopupForm &&propertyLabel == "deleteIcon") {
      //   properties[propertyLabel].update = false;
      //   properties[propertyLabel].scope = "None";
      // }
    }

    // if (this.fDCService.isPopupForm && (properties["Save"] && properties["Save"].scope == "Read")) {
    //   properties["Save"] = false;
    //   properties["Save"] = "None";
    // }
  }

  setDefaultScope(properties: any, isFabricAdmin: boolean) {
    for (let propertyLabel in properties) {
      properties[propertyLabel].update = true;
      properties[propertyLabel].scope = "None";

    }
  }

  updateTankAndTurbineProperties(properties: any, EntityType: string, jsonAdminData: any) {
    for (let propertyLabel in properties) {
      if (jsonAdminData["GasUnitsOfMeasurements"] && properties[propertyLabel].measures_left && properties[propertyLabel].measures_left.true != "%" && properties[propertyLabel].measures_left.true != "oC" &&
        (propertyLabel == "TodayReading" ||
          propertyLabel == "YesterdayReading")
      ) {
        if (
          ((EntityType == EntityTypes.TURBINE ||
            EntityType == EntityTypes.TANK) &&
            properties[propertyLabel].measures_left.true != "103m3") ||
          EntityType == EntityTypes.PDMETER
        ) {
          if (Mesurements[jsonAdminData["GasUnitsOfMeasurements"]]) {
            properties[propertyLabel].measures_left = {
              true: Mesurements[jsonAdminData["GasUnitsOfMeasurements"]],
              false: Mesurements[jsonAdminData["GasUnitsOfMeasurements"]]
            };
          } else {
            properties[propertyLabel].measures_left = {
              true: jsonAdminData["GasUnitsOfMeasurements"],
              false: jsonAdminData["GasUnitsOfMeasurements"]
            };
          }
        }
      }

      if (jsonAdminData["LiquidUnitsOfMeasurements"] && properties[propertyLabel].measures_left && properties[propertyLabel].measures_left.true != "%" &&
        properties[propertyLabel].measures_left.true != "oC" &&
        (propertyLabel == "TodayReading" ||
          propertyLabel == "YesterdayReading")
      ) {
        if (Mesurements[jsonAdminData["LiquidUnitsOfMeasurements"]]) {
          properties[propertyLabel].measures_left = {
            true: Mesurements[jsonAdminData["LiquidUnitsOfMeasurements"]],
            false: Mesurements[jsonAdminData["LiquidUnitsOfMeasurements"]]
          };
        } else {
          properties[propertyLabel].measures_left = {
            true: jsonAdminData["LiquidUnitsOfMeasurements"],
            false: jsonAdminData["LiquidUnitsOfMeasurements"]
          };
        }
      }
      if (
        (jsonAdminData["GaugeUom"] || jsonAdminData["Units"]) &&
        (propertyLabel == "TodayReading" ||
          propertyLabel == "YesterdayReading" ||
          propertyLabel == "Gauge" ||
          propertyLabel == "YesterdayGauge")
      ) {
        if (
          properties[propertyLabel].actualname == "Gauge" ||
          properties[propertyLabel].actualname == "YesterdayGauge"
        ) {
          if (properties.Gauge.validation && jsonAdminData["GaugeUom"] == "%") {
            properties.Gauge.validation.custom = "per";
          }

          if (Mesurements[jsonAdminData["GaugeUom"]]) {
            if(propertyLabel == "Gauge" && jsonAdminData.GaugeMethodOps == "Strapping Table") {
              properties[propertyLabel].measures_left = {
                true: null,
                false: null
              };
            }
            else {
              properties[propertyLabel].measures_left = {
                true: Mesurements[jsonAdminData["GaugeUom"]],
                false: Mesurements[jsonAdminData["GaugeUom"]]
              };
            }
          } else {
            properties[propertyLabel].measures_left = {
              true: jsonAdminData["GaugeUom"],
              false: jsonAdminData["GaugeUom"]
            };
          }
        } else if (
          properties[propertyLabel].measures_left &&
          properties[propertyLabel].measures_left.true != "%" &&
          properties[propertyLabel].measures_left.true != "oC"
        ) {
          if (Mesurements[jsonAdminData["Units"]]) {
            properties[propertyLabel].measures_left = {
              true: Mesurements[jsonAdminData["Units"]],
              false: Mesurements[jsonAdminData["Units"]]
            };
          } else {
            properties[propertyLabel].measures_left = {
              true: jsonAdminData["Units"],
              false: jsonAdminData["Units"]
            };
          }
        } else {
          properties[propertyLabel].measurement =
            properties[propertyLabel].measures_left;
        }
      }

    }
  }
  updateSchemaBasedOnProperties(tab,properties,formCapabilities,adminSchema){
    try {
      let headerGrid=[];
      let contentGrid=[];
      let contentObject=[];
      for (const key in properties) {
        if (properties.hasOwnProperty(key)) {
          const element = properties[key];
          if(element.capability&&element.capability.hasOwnProperty([formCapabilities[tab]])){
            let obj=element.capability[formCapabilities[tab]]
            if(obj.type=='header')
            headerGrid.push({label:key,col:obj.col})
            else if(obj.type=='content'&&obj.subtype=='grid')
            contentGrid.push({label:key,col:obj.col})
            else 
            contentObject.push({label:key,col:obj.col})
          }
        }
      }
      adminSchema['accordion']['header']['grid']['cols'].forEach(d => {
        if((d.type=='grid')){
          this.sort(headerGrid)
          d.grid.cols=headerGrid;
        }
      });
      adminSchema['accordion']['content']['grid']['cols'].forEach((d) => {
        if((d.type=='grid')){
          this.sort(contentGrid)
          d.grid.cols=contentGrid;
          if(contentObject.length==0){
            d.grid.numberOfCols=adminSchema['accordion']['content']['grid']['numberOfCols']
            d.colspan=adminSchema['accordion']['content']['grid']['numberOfCols']
          }
        }})
      adminSchema['accordion']['content']['grid']['cols'].forEach((d,index) => {
        if(d.type=='object'&&contentObject.length==0){
          adminSchema['accordion']['content']['grid']['cols'].splice(index, 1);
        }
      });
    } catch (error) {
    }
  }
  updateOrificeProperties(properties: any, jsonAdminData: any) {
    for (let propertyLabel in properties) {
      if (
        jsonAdminData["StaticUom"] &&
        properties[propertyLabel].actualname == "StaticPressure"
      ) {
        properties[propertyLabel]["measurement"] = {
          true: jsonAdminData["StaticUom"],
          false: jsonAdminData["StaticUom"]
        };
      }

      if (
        jsonAdminData["DifferenceUom"] &&
        properties[propertyLabel].actualname == "DifferentialPressure"
      ) {
        if (Mesurements[jsonAdminData["DifferenceUom"]]) {
          properties[propertyLabel].measurement = {
            true: Mesurements[jsonAdminData["DifferenceUom"]],
            false: Mesurements[jsonAdminData["DifferenceUom"]]
          };
        } else {
          properties[propertyLabel].measurement = {
            true: jsonAdminData["DifferenceUom"],
            false: jsonAdminData["DifferenceUom"]
          };
        }
      }

      if (
        jsonAdminData["TemperatureUom"] &&
        properties[propertyLabel].actualname == "Temperature"
      ) {
        if (Mesurements[jsonAdminData["TemperatureUom"]]) {
          properties[propertyLabel].measurement = {
            true: Mesurements[jsonAdminData["TemperatureUom"]],
            false: Mesurements[jsonAdminData["TemperatureUom"]]
          };
        } else {
          properties[propertyLabel].measurement = {
            true: jsonAdminData["TemperatureUom"],
            false: jsonAdminData["TemperatureUom"]
          };
        }
      }

      if (
        jsonAdminData["PlateUom"] &&
        properties[propertyLabel].actualname == "Plate"
      ) {
        if (Mesurements[jsonAdminData["PlateUom"]]) {
          properties[propertyLabel].measurement = {
            true: Mesurements[jsonAdminData["PlateUom"]],
            false: Mesurements[jsonAdminData["PlateUom"]]
          };
        } else {
          properties[propertyLabel].measurement = {
            true: jsonAdminData["PlateUom"],
            false: jsonAdminData["PlateUom"]
          };
        }
      }
    }
  }

  setMeasurement(properties, propertyLabel, type) {
    var FirstIndexOfvalue = this.UnitsOfMeasureMents[type];
    if (Mesurements[FirstIndexOfvalue]) {
      FirstIndexOfvalue = Mesurements[FirstIndexOfvalue];
    }
    FirstIndexOfvalue = FirstIndexOfvalue + "/Day";
    properties[propertyLabel].measures_left.true = FirstIndexOfvalue;
    properties[propertyLabel].measurement =
      properties[propertyLabel].measures_left;
  }
  setAllowablePopup(properties) {
    let gasProperties = ["GasDay", "SandDay"];
    for (var propertyLabel of gasProperties) {
      if (properties[propertyLabel]) {
        this.setMeasurement(properties, propertyLabel, "GasUOM");
      }
    }

    let liquidProperties = ["OilDay", "WaterDay", "CondyDay"];

    for (var propertyLabel of liquidProperties) {
      if (properties[propertyLabel]) {
        this.setMeasurement(properties, propertyLabel, "LiquidUOM");
      }
    }
  }


  // getFormSchema(entityType: string, formType: string, fabric: string, payload: any, entityId?: string, entityName?: string) {
  //   let key = fabric.toUpperCase() + "_" + entityType + "_" + formType;
  //   let formSchema$ = this.formSchemaSendBackEnd(entityType, formType, fabric, payload, entityId, entityName, key).map((res) => {
  //     let tempSchemaData: any = JSON.parse(JSON.stringify(res));
  //     let tempSchema = this.transformFormSchema(tempSchemaData, entityType, entityName);
  //     tempSchemaData['Schema'] = JSON.stringify(tempSchema);
  //     return tempSchemaData;
  //   });
  //   return formSchema$.shareReplay(1);
  // }


  // formSchemaSendBackEnd(entityType: string, formType: string, fabric: string, payload: any, entityId: string, entityName: string, key: string) {
  //   var message: MessageModel = this.commonService.getMessage();
  //   message.Payload = JSON.stringify(payload);
  //   message.DataType = formType;
  //   message.EntityID = entityId;
  //   message.EntityType = entityType;
  //   message.MessageKind = MessageKind.READ;
  //   message.Routing = Routing.OriginSession;
  //   message.Type = payload.Info;
  //   message.Fabric = fabric.toUpperCase();


  //   return this.fDCService.schemaEntityCommonStoreQuery
  //     .selectEntity(key)
  //     .pipe(switchMap((hashCache) => {
  //       const apiCall = this.commonService.httpRestRequestsFromWebWorker(WORKER_TOPIC.RESTHTTP, "", "post", this.commonService.baseUrl + 'api/AlThings/GetDataFromBackEnd', {}, message)
  //         .map((res: string) => {
  //           let msg = JSON.parse(res);
  //           let schema = JSON.parse(msg.Payload)["Schema"];
  //           this.commonService.upsertSchema(key, schema);
  //           return JSON.parse(schema);/*JSON.parse(schema);*/
  //         });

  //       let newState;
  //       if (hashCache) {
  //         newState = JSON.parse(JSON.stringify(hashCache));
  //         delete newState['id'];
  //       }

  //       return newState ? of(newState) : apiCall;
  //     })).debounceTime(150);
  // }

  transformFormSchema(formSchema: any, entityType: string, entityName: string): any {
    try {
      let METER_JSON: any;
      let properties: any;
      //let formSchema: any;
      let adminSchema: any;

      //let payload: any = JSON.parse(res['Payload']);
      //if (!payload.Childen && !payload.Locationdata) {
      //  formSchema = JSON.parse(payload["Schema"]);
      //}

      //formSchema = schema;
      if (Object.keys(formSchema).length != 0) {
        let data1 = formSchema.Schema;
        METER_JSON = JSON.parse(data1);
        properties = METER_JSON['Properties'];



 

        if (entityType == "WaterSourceWell" && properties["WellType"]) {
          properties["WellType"].options = ["Source"];
        }

        if (properties && properties['Icon']) {
          properties['Icon']['label'] = IconNameJson[entityType];
        }

        if (properties && properties['Name'] && entityName != "") {
          properties["Name"]["label"] = entityName;
        }

        if (entityName == "DivisionOfInterest" || entityName == "Cycles" || entityName == "Allowables" || entityName == "Targets") {
          adminSchema = METER_JSON;
        }
        else {
          adminSchema = METER_JSON['Schema'];
        }

        this.LocationButtonDisableEnableSchema(true, properties, adminSchema['accordion']['content'], entityType);
  
 
				let entity: any// = this.commonService.getEntitiesById(this.fDCService.LocationId);


        if (properties && properties["Name"]) {
          properties["Name"]["label"] = entityName;
        }

        if (adminSchema && adminSchema != {} && adminSchema != '{}' && Object.keys(adminSchema).length != 0) {
          this.CommonSchemaIteration(adminSchema, properties, entityType);
        }
      }
      //let tempData: any = { 'Schema': adminSchema };
      return adminSchema;
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in addNewEntityFormSchemaSubscription() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  CommonSchemaIteration(adminSchema: any, properties: any, entityType: string) {
    try {
      this.MeasurementConvertionFunctionSchema(properties);
      let header = adminSchema['accordion']['header'];
      let content = adminSchema['accordion']['content'];
      let count = 0;

      if (content) {
        if (content.type == 'table') {
          if (header.grid) {
            for (let data of [header.grid.cols]) {
              for (let col of data) {
                if (col.type == 'grid' || col.type == 'table') {
                  this.gridLoop(col, properties, entityType);
                }
                else {
                  this.objectLoop(col, properties);
                }
              }
            }
          }

          if (header.type == 'table')
            this.tableLoopIterationSchema(header, properties, entityType);
          if (content.type == 'table')
            this.tableLoopIterationSchema(content, properties, entityType);

          if (content.actionmenu && content.actionmenu.cols) {
            for (let col of content.actionmenu.cols) {
              this.objectLoop(col, properties);
            }
          }
        }
        else {
          if (content.type == 'list') {
            /**
             * for finding the rowspan count
             */
            for (let col of content.data.grid.cols) {
              if (col.type == 'object') {
                let elementlength = 0;
                col.elements.forEach(cols => {
                  if (cols.type == "button")
                    elementlength += 0.7;
                  else
                    elementlength += 1;
                });
                count = (count >= elementlength) ? count : elementlength;
              }
              else if (col.type == 'grid' || col.type == 'table') {
                let nestedRowSpanLength = Math.ceil(col.grid.cols.length / col.grid.numberOfCols)
                count = (count >= nestedRowSpanLength) ? count : nestedRowSpanLength;
                var cols = col.grid.cols;
                this.sort(cols)
              }
            }

            if (content.actionmenu && content.actionmenu.cols) {
              for (let col of content.actionmenu.cols) {
                this.objectLoop(col, properties);
              }
            }

            /**
             * for Iterating the loops to sort cols data
             */
            for (let data of [content.data.grid.cols, header.grid.cols]) {
              for (let col of data) {
                if (col.type == 'grid' || col.type == 'table') {
                  this.gridLoop(col, properties, entityType);
                }
                else {
                  this.objectLoop(col, properties);
                }
              }
            }
          }
          else if (content.type == 'section') {
            /**
             * Binding the Accordions data to particular accordion in NestedAccordions
             */
            /*
             *
             * For Filtering Header MenuItems
             */
            //this.nestedAccordionInitialization(content);

            if (header.actionmenu && header.actionmenu.cols) {
              for (let col of header.actionmenu.cols) {
                this.objectLoop(col, properties);
              }
            }

            /**
             * only headerdata filtering
             */
            for (let col of header.grid.cols) {
              if (col.type == 'grid') {
                this.gridLoop(col, properties, entityType);
              }
              else {
                this.objectLoop(col, properties);
              }
            }
          }
          else if (content.type == 'Multigrid') {
            /**
             * This loop for Sorting the data and adding required properties
             */
            for (let data of [content.data.grid.cols, header.grid.cols]) {
              for (let col of data) {
                if (col.type == 'grid' && col.grid.cols[0] && col.grid.cols[0].type == 'grid') {
                  this.MultiGridRowSpan = 1;
                  var cols = col.grid.cols;
                  this.sort(cols)
                  col.grid.cols.forEach(col => {
                    this.gridLoop(col, properties, entityType);
                  });
                } else if (col.type == 'grid' || col.type == 'table') {
                  var cols = col.grid.cols;
                  this.sort(cols)
                  this.gridLoop(col, properties, entityType);
                }
                else {
                  this.objectLoop(col, properties);
                }
              }
            }
          }
          else {
            /**
             * Default Normal accordions data looping
             */
            for (let data of [content.grid.cols, header.grid.cols]) {
              for (let col of data) {
                if (col.type == 'grid' || col.type == 'table') {
                  this.gridLoop(col, properties, entityType);
                }
                else {
                  this.objectLoop(col, properties);
                }
              }
            }

            /**
             * to Finding the RowSpan of the accordion
             */
            for (let data of [content.grid.cols]) {
              for (let col of data) {
                if (col.type == 'object') {
                  let elementlength = 0;
                  col.elements.forEach(cols => {
                    if (cols.type == "button")
                      elementlength += 0.7;
                    else
                      elementlength += 1;
                  });
                  count = (count >= elementlength) ? count : elementlength;
                } else if (col.type == 'grid' || col.type == 'table') {
                  let colslength = 0;
                  col.grid.cols.forEach(column => {
                    colslength += Number(column.colspan);
                  });
                  let nestedRowSpanLength = Math.ceil(colslength / col.grid.numberOfCols)
                  count = (count >= nestedRowSpanLength) ? count : nestedRowSpanLength;
                  var cols = col.grid.cols;
                  this.sort(cols)
                }
              }
            }
          }
        }
      }
    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in CommonIterationData() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  LocationButtonDisableEnableSchema(status: boolean, properties: any, content: any, entityType: string) {
    try {

    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in LocationButtonDisableEnable() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  DistrictAreaFieldOptionsBindingSchema(properties: any): any {
    try {

    }
    catch (e) {
      this.commonService.appLogException(new Error('Exception in DistrictAreaFieldOptionsBinding() of FdcFormGeneratorComponent in FDCFormTemplate at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }


  MeasurementConvertionFunctionSchema(properties: any) {
    if (properties) {
      for (let property in properties) {
        if (properties[property]['checkboxvalue']) {
          if (properties[property]['checkboxvalue'].true && Mesurements[properties[property]['checkboxvalue'].true]) {
            properties[property]['checkboxvalue'].true = Mesurements[properties[property]['checkboxvalue'].true];
          }
          if (properties[property]['checkboxvalue'].false && Mesurements[properties[property]['checkboxvalue'].false]) {
            properties[property]['checkboxvalue'].false = Mesurements[properties[property]['checkboxvalue'].false];
          }
        }
        if (properties[property]['measures_left']) {
          if (properties[property]['measures_left']['true'] && Mesurements[properties[property]['measures_left']['true']]) {
            properties[property]['measures_left']['true'] = Mesurements[properties[property]['measures_left']['true']];
          }
          if (properties[property]['measures_left']['false'] && Mesurements[properties[property]['measures_left']['false']]) {
            properties[property]['measures_left']['false'] = Mesurements[properties[property]['measures_left']['false']];
          }
        }
        if (properties[property]['options'] && properties[property].actualname != "EntityType") {
          for (let i = 0; i < properties[property]['options'].length; i++) {
            if (properties[property]['options'][i] && Mesurements[properties[property]['options'][i]]) {
              properties[property]['options'][i] = Mesurements[properties[property]['options'][i]];
            }
            if (properties[property] != "EntityType") {
              if (properties[property]['options'][i] && EntityTypesReverseKVPair[properties[property]['options'][i]]) {
                properties[property]['options'][i] = EntityTypesReverseKVPair[properties[property]['options'][i]];
              }
            }
          }
        }
      }
    }
  }

  tableLoopIterationSchema(content: any, properties: any, entityType: string) {
    if (content.tableHeader)
      this.tableLoop(content.tableHeader, properties);
    if (content.tableContent)
      this.tableLoop(content.tableContent, properties);
  }

  destroy() { }
  //***********Common Colspan update for grid and object
  updateColsPan(col, properties, i) {
    if (properties[col.elements[i].label].colspan) {
      col.elements[i].colspan = properties[col.elements[i].label].colspan;
    } else {
      properties[col.elements[i].label].colspan = 1;
      col.elements[i].colspan = 1;
    }
  }

  updatePropertiesObject(properties, propertyLabel) {
    if (!properties[propertyLabel].hasOwnProperty("isDynamicWidth")) {
      properties[propertyLabel].isDynamicWidth = true;
    }
  }

  updatePropertiesGrid(properties, propertyLabel,AccordionName?) {
    properties[propertyLabel].measurement = properties[propertyLabel].measures_left;
    if (!properties[propertyLabel].placeholder) {
      properties[propertyLabel].placeholder = properties[propertyLabel].label;
    }

    if(propertyLabel=='EntityType'){//to change entity type color green to normal
      properties[propertyLabel].EntityTypeTextColorNone=true;
    }

    let propertyName = properties[propertyLabel].actualname;
    if (propertyName == "FromLocation" || propertyName == "ToLocation") {
      properties[propertyLabel].options = [];
    }

    if (!properties[propertyLabel].hasOwnProperty("isDynamicWidth")) {
      properties[propertyLabel].isDynamicWidth = true;
    }

    if (Mesurements[propertyLabel]) {
      if ((propertyLabel == "bbl" && AccordionName == EntityStateConstantsKeys.GeneralSettings)) {
        return;
      } else {
        properties[propertyLabel].label = Mesurements[propertyLabel];

      }
    }

  }
}
