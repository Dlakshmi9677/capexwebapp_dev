import { Component, Output, OnInit, Input, ViewChild, EventEmitter, HostListener,ChangeDetectorRef } from '@angular/core';
import { CommonService } from '../../globals/CommonService';
import { MatMenuTrigger, MatMenu } from '@angular/material/menu';

@Component({
  selector: 'app-map-context-menu',
  templateUrl: './common-context-menu.component.html',
  styleUrls: ['./common-context-menu.component.scss']
})

export class CommonContextMenuComponent implements OnInit {
  @Input() items;
  activeStatus: boolean = false;
  @ViewChild('childMenu', {static: true}) public childMenu;
  @ViewChild('menu', {static: true}) menu: MatMenu;
  @ViewChild(MatMenuTrigger, {static: true}) matMenutrigger: MatMenuTrigger;

  activeWindow: boolean = true;
  _width: number = window.innerWidth;
  _ScreenWidht = screen.width;
  x1;
  x2;
  public menuLeft = 0;
  public menuTop = 0;
  isContextMenuOpen = false;
  @Output()
  ClickEvent: EventEmitter<any> = new EventEmitter();
  logger: any;
  constructor(public commonService: CommonService,private cdRef: ChangeDetectorRef ) { }

  ngOnInit() {
  }

  getClass(child) {
      this.activeStatus = false;
  }

  contextClick(MenuData) {
    this.ClickEvent.emit(MenuData);
    this.cdRef.detectChanges();
  }
}
