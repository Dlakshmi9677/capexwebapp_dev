import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { produce } from 'immer';
import { EntityCompanyTypesStateModel } from './companytype.state.model';
import { EntityCompanyTypesState, EntityCompanyTypesStore } from './companytype.state.store';

@Injectable({ providedIn: 'root' })
export class EntityCompanyTypesStoreQuery extends QueryEntity<EntityCompanyTypesState, EntityCompanyTypesStateModel> {
    constructor(protected store: EntityCompanyTypesStore) {
        super(store);
    }

    /**
    * intialize put into state
    *
    * @param entities state of the entities store
    */
    Set(entities: any[]) {
        // if(!Array.isArray(entities))
        if (entities && Object.keys(entities).length > 0)
            Object.keys(entities).forEach(key => { if (entities[key] === undefined) { delete entities[key]; } });

        this.store.set(entities);
    }

    /**
     * Add new Entitiy to the existing store
     */
    upsert(entityId: string, entity: any) {
        if (!entity || !entityId)
            throw new Error('Error :: Entity or EntityId is undefined!');

        this.store.upsert(entityId, entity);
    }

    AddAll(entities: any[]) {
        entities = entities.filter((entity: any) => entity != undefined);
        this.store.add(entities);
    }

    // Update Security group and Capability array in state
    UpdateArrayValue(entities, key, value) {
        try {
            this.store.update(entities, state => produce(state, draft => {
                let existingValues = <Array<any>>draft[key];
                if (existingValues && existingValues.indexOf(value) < 0) {
                    draft[key] = [...existingValues, value]
                }
                else {
                    draft[key] = [value]
                }
            }))
        }
        catch (e) {
            console.error(e);
        }
    }

    removeEntities(entityIds:Array<string>){
        this.store.remove(entityIds);
    }
}