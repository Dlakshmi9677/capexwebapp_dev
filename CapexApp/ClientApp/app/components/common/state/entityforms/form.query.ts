import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { produce } from 'immer';
import { Entity, FormStateModel } from './form.model';
import { CommonEntityState, FormStore } from './form.store';

@Injectable({providedIn: 'root'})
export class FormStateQuery extends QueryEntity<CommonEntityState, FormStateModel<Entity>> {
  constructor(protected store: FormStore) {
    super(store);
  }

  public Set(entity: any) {
    try {
      this.store.set(entity);
    } catch (e) {
      console.error(e);
    }
  }

  public upsertState(entityId: string, entity: any) {
    if (!entity || !entityId) {
      throw new Error('Error :: Entity or EntityId is undefined!');
    }

    this.store.upsert(entityId, entity[entityId]);
  }

  public upsertKey(entityId: string, value: any) {
    this.store.update(state => produce(state, draft => {
      let existingValue = draft['entities'][entityId];
      if (existingValue && Object.keys(existingValue).length) {
        this.updateStateKey(entityId, draft, value);
      }
      else {
        // call to insert
        this.upsertState(entityId, value);
      }
    }));
  }

  public upsertModel(entityId: string, value: any) {
    this.store.update(state => produce(state, draft => {
      let existingValue = draft['entities'][entityId];
      this.updateStateModel(entityId, existingValue, value);
    }));
  }

  private updateStateModel(entityId, existingValue, newValue) {
    //call to update
    let entity = newValue[entityId];
    let key = Object.keys(entity)[0];
    existingValue[key] = entity[key];
  }

  private updateStateKey(entityId, existingValue, newValue) {
    existingValue['entities'][entityId] = newValue[entityId];
  }

  public resetStore() {
    this.store.reset();
  }

  public upsertInnerKeyDataInState(entityId: string, ConfigKey: any, OriginalkeyInData: any, InnerKeyInData: any, InnerKeyvalue: any) {
    try {
      let existingData = this.getStateValueByEntityId(entityId);
      existingData[ConfigKey][OriginalkeyInData][InnerKeyInData] = InnerKeyvalue;
      this.store.update(state => produce(state, draft => {
        // let oldData = draft['entities'][entityId];
        this.updateStateBasedOnKey(entityId, draft, existingData);
      }));
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * Returns StateValue For All Entities in FormState
   */
  getStateValue() {
    let ExistingStateValue = this.getValue()['entities'];
    if (!ExistingStateValue) {
      return ExistingStateValue;
    }
    else {
      return JSON.parse(JSON.stringify(ExistingStateValue));
    }
  }

  /**
   * Returns StateValue Based On EntityId
   */
  getStateValueByEntityId(entityId) {
    let ExistingStateValue = this.getValue()['entities'][entityId];
    if (!ExistingStateValue) {
      return ExistingStateValue;
    }
    else {
      return JSON.parse(JSON.stringify(ExistingStateValue));
    }
  }

  updateStateBasedOnKey(entityId, existingValue, newValue) {
    try {
      existingValue['entities'][entityId] = newValue;
    } catch (e) {
      console.error(e);
    }
  }

  updateStateValueBasedOnType(entityId, type, propertyName, propertyValue) {
    try {
      let existingValue = this.getValue()['entities'][entityId];
      let keys: string[] = Object.keys(existingValue);
      let tempKeys: string[] = keys.filter((key: string) => key.includes(type));
      if (tempKeys && tempKeys.length > 0) {
        let recordType: string = tempKeys[0];
        let tempData = JSON.parse(JSON.stringify(existingValue[recordType]['data']));
        tempData[propertyName] = propertyValue;

        this.store.update(state => produce(state, draft => {
          draft['entities'][entityId][recordType] = tempData;
        }));
      }
    }
    catch (e) {
      console.error(e);
    }
  }
}
