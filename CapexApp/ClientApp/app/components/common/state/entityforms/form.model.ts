
//form state store model
export class Entity { }
export class FormStateModel<T extends Entity>{
}

//form state components and service usage models
export class EntityForm<T>{
  constructor(private key: string, private value: T) {
  }
  createInstance() {
    let key = this.value.constructor.name;
    return { [this.key]: { [key]: this.value } };
  }
}


export class FacilityConfig {
  data: any;
}
Object.defineProperty(FacilityConfig, "name", {
  value: "FacilityConfig"
});
export class FacilityDataEntry {
  data: any;
}
Object.defineProperty(FacilityDataEntry, "name", {
  value: "FacilityDataEntry"
});
export class DeliveredConfig {
  data: any;
}
Object.defineProperty(DeliveredConfig, "name", {
  value: "DeliveredConfig"
});
export class DeliveredDataEntry {
  data: any;
}
Object.defineProperty(DeliveredDataEntry, "name", {
  value: "DeliveredDataEntry"
});

/****
 * Equipments Model
 */
export class OrificeConfig {
  data: any;
}
Object.defineProperty(OrificeConfig, "name", {
  value: "OrificeConfig"
});

export class OrificeDataEntry {
  data: any;
}
Object.defineProperty(OrificeDataEntry, "name", {
  value: "OrificeDataEntry"
});

export class TurbineConfig {
  data: any;
}
Object.defineProperty(TurbineConfig, "name", {
  value: "TurbineConfig"
});
export class TurbineDataEntry {
  data: any;
}
Object.defineProperty(TurbineDataEntry, "name", {
  value: "TurbineDataEntry"
});
export class PDMeterConfig {
  data: any;
}
Object.defineProperty(PDMeterConfig, "name", {
  value: "PDMeterConfig"
});

export class PDMeterDataEntry {
  data: any;
}
Object.defineProperty(PDMeterDataEntry, "name", {
  value: "PDMeterDataEntry"
});

export class CoriolisMeterConfig {
  data: any;
}
Object.defineProperty(CoriolisMeterConfig, "name", {
  value: "CoriolisMeterConfig"
});
export class CoriolisMeterDataEntry {
  data: any;
}
Object.defineProperty(CoriolisMeterDataEntry, "name", {
  value: "CoriolisMeterDataEntry"
});

export class RotaMeterConfig {
  data: any;
}
Object.defineProperty(RotaMeterConfig, "name", {
  value: "RotaMeterConfig"
});
export class RotaMeterDataEntry {
  data: any;
}
Object.defineProperty(RotaMeterDataEntry, "name", {
  value: "RotaMeterDataEntry"
});
export class UltrasonicConfig {
  data: any;
}
Object.defineProperty(UltrasonicConfig, "name", {
  value: "UltrasonicConfig"
});
export class UltrasonicDataEntry {
  data: any;
}
Object.defineProperty(UltrasonicDataEntry, "name", {
  value: "UltrasonicDataEntry"
});
export class TankConfig {
  data: any;
}
Object.defineProperty(TankConfig, "name", {
  value: "TankConfig"
});
export class TankDataEntry {
  data: any;
}
Object.defineProperty(TankDataEntry, "name", {
  value: "TankDataEntry"
});
export class CompressorConfig {
  data: any;
}
Object.defineProperty(CompressorConfig, "name", {
  value: "CompressorConfig"
});
export class CompressorDataEntry {
  data: any;
}
Object.defineProperty(CompressorDataEntry, "name", {
  value: "CompressorDataEntry"
});
export class SeparatorConfig {
  data: any;
}
Object.defineProperty(SeparatorConfig, "name", {
  value: "SeparatorConfig"
});
export class SeparatorDataEntry {
  data: any;
}
Object.defineProperty(SeparatorDataEntry, "name", {
  value: "SeparatorDataEntry"
});
export class TreaterConfig {
  data: any;
}
Object.defineProperty(TreaterConfig, "name", {
  value: "TreaterConfig"
});
export class TreaterDataEntry {
  data: any;
}
Object.defineProperty(TreaterDataEntry, "name", {
  value: "TreaterDataEntry"
});
export class DEHYConfig {
  data: any;
}
Object.defineProperty(DEHYConfig, "name", {
  value: "DEHYConfig"
});
export class DEHYDataEntry {
  data: any;
}
Object.defineProperty(DEHYDataEntry, "name", {
  value: "DEHYDataEntry"
});
export class PumpConfig {
  data: any;
}
Object.defineProperty(PumpConfig, "name", {
  value: "PumpConfig"
});
export class PumpDataEntry {
  data: any;
}
Object.defineProperty(PumpDataEntry, "name", {
  value: "PumpDataEntry"
});
export class PumpjackConfig {
  data: any;
}
Object.defineProperty(PumpjackConfig, "name", {
  value: "PumpjackConfig"
});
export class PumpjackDataEntry {
  data: any;
}

Object.defineProperty(PumpjackDataEntry, "name", {
  value: "PumpjackDataEntry"
});
export class NonMeteredConfig {
  data: any;
}
Object.defineProperty(NonMeteredConfig, "name", {
  value: "NonMeteredConfig"
});
export class NonMeteredDataEntry {
  data: any;
}
Object.defineProperty(NonMeteredDataEntry, "name", {
  value: "NonMeteredDataEntry"
});
/****
 * Truckticket Model
 */
export class TruckTicketConfig {
  data: any;
}
Object.defineProperty(TruckTicketConfig, "name", {
  value: "TruckTicketConfig"
});
export class TruckTicketDataEntry {
  data: any;
}
Object.defineProperty(TruckTicketDataEntry, "name", {
  value: "TruckTicketDataEntry"
});
/****
 * FFV Model
 */
export class FFVConfig {
  data: any;
}
Object.defineProperty(FFVConfig, "name", {
  value: "FFVConfig"
});
export class FFVDataEntry {
  data: any;
}
Object.defineProperty(FFVDataEntry, "name", {
  value: "FFVDataEntry"
});
export class FormButtonsStatus {
}