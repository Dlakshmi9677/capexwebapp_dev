import { Query } from '@datorama/akita';
import { Injectable } from '@angular/core';
import { AuthCommonStore } from './auth.state.store';
import { AuthStateModel } from './auth.state.model';

@Injectable({ providedIn: 'root' })
export class AuthCommonStoreQuery extends Query<AuthStateModel> {

  constructor(protected store: AuthCommonStore) {
    super(store);
  }

  Add(entity: any) {
    try {
      this.store.update(entity);
    } catch (e) {
      console.error(e);
    }
  }

  Update(entity: any) {
    try {
      this.store.update(state => (entity));
    } catch (e) {
      console.error(e);
    }
  }

}
