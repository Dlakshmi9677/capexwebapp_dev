export interface AuthStateModel {

  UserId: string;
  UserName: string;
  TenantId: string;
  TenantName: string;
  ZoomdataToken: string;
  Capabilities: Array<any>;
  AdminCapabilities:Array<any>;
  UserRoles:Array<any>;
  UserSecurityGroups:Array<any>;
}
