import { StoreConfig, Store } from '@datorama/akita';
import { Injectable } from '@angular/core';
import { AuthStateModel } from './auth.state.model';

const createInitialState = (): AuthStateModel => {
  return {
    UserId : null ,
    UserName : null,
    TenantId : null,
    TenantName : null,
    ZoomdataToken : null,
    Capabilities : [],
    AdminCapabilities : [],
    UserRoles: [],
    UserSecurityGroups:[]
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'tokens', resettable: true })
export class AuthCommonStore extends Store<AuthStateModel> {
  constructor() {
    super(createInitialState());

  }
}
