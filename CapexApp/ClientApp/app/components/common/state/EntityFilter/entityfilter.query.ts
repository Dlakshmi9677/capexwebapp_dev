import { QueryEntity } from '@datorama/akita';
import { Injectable } from '@angular/core';
import { EntityFilterCommonStore,EntityFilterStateInterFace } from './entityfilter.store';
import { EntityFilterState } from './entityfilter.model';

@Injectable({ providedIn: 'root' })
export class EntityFilterStoreQuery extends QueryEntity<EntityFilterStateInterFace,EntityFilterState> {

  constructor(protected store: EntityFilterCommonStore) {
    super(store);
  }


  Set(entities: any) {
    try {
        this.store.set(entities);
    }
    catch (e) {
        console.error(e);
    }
}

/**
 * Add new Entitiy to the existing store
 */
Add(key, schema: any) {
  try {
    this.store.upsert(key, schema);
  } catch (e) {
    console.error(e);
  }
}
  /**
   * Delete filter from state
   *
   * @param {any[]} keys
   * @memberof EntityFilterStoreQuery
   */
  deleteNodeByIds(keys: any[]) {
    try {
      this.store.remove(keys);
    }
    catch (e) {
      console.error(e);
    }
  }

SetActive(id){
  this.store.setActive(id);
}

}
