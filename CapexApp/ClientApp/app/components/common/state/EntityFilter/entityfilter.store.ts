import { EntityFilterState } from './entityfilter.model'
import { StoreConfig, EntityStore,EntityState } from '@datorama/akita';
import { Injectable } from '@angular/core';
export interface EntityFilterStateInterFace extends EntityState<EntityFilterState> { }

const createInitialState=(json): EntityFilterState=>{
  return JSON.parse(json);}
const json ={
  schema: null,
  data: null,
  capability: null,
  treeName: null
}
@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'Filter', resettable: true })
export class EntityFilterCommonStore extends EntityStore<EntityFilterStateInterFace,EntityFilterState> {
  constructor() {
    super({ initialState: createInitialState(JSON.stringify(json)), loading: false });

  }
}
