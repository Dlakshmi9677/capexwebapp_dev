import { Query, arrayAdd, arrayUpdate,arrayRemove } from '@datorama/akita';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FDCState, FDCStateConstantsKeys } from './fdc.state.model';
import { FDCStore } from './fdc.state.store';
import { produce } from 'immer';

@Injectable({ providedIn: 'root' })
export class FDCStateQuery extends Query<FDCState> {
  constructor(protected store: FDCStore) {
    super(store);
  }

  /**
   * 
   * @param key "e.g. wells,facilities etc. etc"
   * @param value json object e.g {"EntityId":"123453",.......}  
   */
  entityStateAddItem(key, objectJsonString: string) {
    if (key) {
      if (!this.IsJsonString(objectJsonString)) {
        console.error("entityState given string is not a valid json");
        return;
      }
      var valueObj = JSON.parse(objectJsonString);
      this.store.update(state => produce(state, draft => {
        draft[FDCStateConstantsKeys.entityState][key].push(valueObj);
      }));
    }
    else {
      console.error("Path string could not be zero to add item into ");
    }
  }

  /**
   * 
   * @param key "e.g. wells,facilities etc etc"
   * @param EntityId Unique or primary key id
   * @param value json object
   */
  entityStateUpdateItem(key, EntityId, objectJsonString: string) {
    if (key) {
      if (!this.IsJsonString(objectJsonString)) {
        console.error("entityState given string is not a valid json");
        return;
      }
      var valueObj = JSON.parse(objectJsonString);
      this.store.update(state => produce(state, draft => {
        draft[FDCStateConstantsKeys.entityState][key][draft[FDCStateConstantsKeys.entityState][key].findIndex(obj => obj.EntityId === EntityId)] = valueObj;
      }))
    }
    else {
      console.error("Path string could not be zero to add item into ");
    }
  }

  /**
   * for example
   * ["downTimeReason"]
   * 
   * ["entityState"]
   * 
   * ["entityState","wells"]
   * 
   * ["downTimeReason",...]
   * 
   * @param path array of string path
   */
  getPathValue$(path: Array<string>) {
    var data$;
    if (path.length) {
      switch (path.length) {
        case 1:
          data$ = this.getLevel1$(path[0]);
          break;
        case 2:
          data$ = this.getLevel2$(path[0], path[1]);
          break;
        case 3:
          data$ = this.getLevel3$(path[0], path[1], path[2]);
          break;
        case 4:
          data$ = this.getLevel4$(path[0], path[1], path[2], path[3]);
          break;
        default: break;
      }
      return data$;
    }
    else {
      console.error("Unable to get UserStore with array path string length zero")
    }
  }

  /**
   * 
   * @param key e.g entityState,downTimeReasons and so on.
   */
  private getLevel1$(key: string): Observable<any> {
    return this.select(state => (
      state[key] != undefined) ? state[key] : null);
  }

  /**
   * 
   * @param key1 entityState,downTimeReasons etc.
   * @param key2 wells,facilities etc.
   */
  private getLevel2$(key1: string, key2: string): Observable<any> {
    return this.select(state => (
      state[key1] != undefined && state[key1][key2] != undefined) ? state[key1][key2] : null);
  }

  /**
   * 
   * @param key1 entityState,downTimeReasons etc etc.
   * @param key2 well,facilities etc
   * @param key3 
   */
  private getLevel3$(key1: string, key2: string, key3: string): Observable<any> {
    return this.select(state => (
      state[key1] != undefined &&
      state[key1][key2] != undefined &&
      state[key1][key2][key3] != undefined) ?
      state[key1][key2][key3] : null);
  }

  /**
   * 
   * @param key1 
   * @param key2 
   * @param key3 
   * @param key4 
   */
  private getLevel4$(key1: string, key2: string, key3: string, key4: string): Observable<any> {
    return this.select(state => (
      state[key1] != undefined &&
      state[key1][key2] != undefined &&
      state[key1][key3][key3] != undefined
      && state[key1][key2][key3][key4] != undefined) ?
      state[key1][key2][key3][key4] : null);
  }

  /**
   * To get full state object
   */
  getAll(): FDCState {
    return this.getValue();
  }

  /**
   * 
   * @param key key of the state e.g. entityState,setAsDefault and so on.
   */
  getStateKey(key) {
    return this.getValue()[key];
  }

  /**
   * 
   * To update value of the key item currently present in array
   * 
   * // example
    var key="dataAlertRules";
    var value = {value:"myNameName"};
    var innerId="EntityId";
    var innerValue="2326754dsfhgdfgdfgd3";
     for example 
     this.fdcStateQuery.updateArrayItemValue(StateConstantsKeys.downtimeReasons,{EntityType:"myEntityType"},"EntityId",data.EntityId)
    
   * 
   * @param key key of the State item e.g downtimereasons
   * @param value which is the key want to update of the item
   * @param innerId unique key of object
   * @param innerValue value of unique key
   */
  updateArrayItemValue(key, value, innerId, innerValue) {
    this.store.update(entity => ({
      [key]: arrayUpdate(entity[key], item => item[innerId] == innerValue, value)
    }));
  }

  /**
   * To add new item into state key
   * 
   * @param key e.g downtimereason
   * @param value new object which is adding into array
   */
  addNewItemIntoKeyArray(key, value) {
    this.store.update(entity => ({
      [key]: arrayAdd(entity[key], value)
    }));
  }

  /**
   * To fully replace key with value
   */
  add(key, value) {
    this.store.update(state => ({
      [key]: value
    }));    
  }

  addTimeZone(key, value){
    this.store.update(state =>produce(state,draft=>{
      draft[key]= value
    }));
  }
   /**
   * Add new Entitiy to the existing store
   */
  AddObject(key,objKey, value: any) {
   try{
    this.store.update(state=>produce(state,draft=>{
      draft[key][objKey]=value;
    }));
   }
   catch(e){
    console.error("AddObject: "+e)
   }
  }

  /**
   * example
   * 
   * this.fdcStateQuery.removeItemEntityState('headers',"EntityId",res.EntityId);
   * 
   * @param key state key e.g. entityState,downTimeReasons etc.
   * @param innerId unique or primarkey of an object e.g "EntityId"
   * @param InnerIdsValue  value of the unique key or primary key e.g "xyz"
   */
  removeItemEntityState(key,innerId,InnerIdsValue) {
   this.store.update(state => produce(state, draft => {
      draft[FDCStateConstantsKeys.entityState][key].splice([draft[FDCStateConstantsKeys.entityState][key].findIndex(obj => obj[innerId] === InnerIdsValue)],1)
    }))
  }

  /**
   * example
   * 
   * this.fdcStateQuery.removeItemEntityState('headers',"EntityId",res.EntityId);
   * 
   * @param key state key e.g. downTimeReasons etc.
   * @param innerId unique or primarkey of an object e.g "EntityId"
   * @param InnerIdsValue  value of the unique key or primary key e.g "xyz"
   */
  removeItemFromState(key,innerId,InnerIdsValue) {
    this.store.update(state => produce(state, draft => {
       draft[key].splice([draft[key].findIndex(obj => obj[innerId] === InnerIdsValue)],1)
     }))
   }

 private IsJsonString(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }


    /**
   * example
   * 
   * this.fdcStateQuery.removefdcStateItem('headers',"EntityId",res.EntityId);
   * 
   * @param key state key e.g. statuses,downTimeReasons,formations etc.
   * @param innerId unique or primarkey of an object e.g "EntityId"/"ConfigId"
   * @param InnerIdsValue  value of the unique key or primary key e.g "xyz"
   */
  removefdcStateItem(key,innerId,InnerIdsValue) {
    this.store.update(state => produce(state, draft => {
       draft[key].splice([draft[key].findIndex(obj => obj[innerId] === InnerIdsValue)],1)
     }))
   }

   setLoadingStore(flag:boolean){
     this.store.setLoading(flag);
   }
}