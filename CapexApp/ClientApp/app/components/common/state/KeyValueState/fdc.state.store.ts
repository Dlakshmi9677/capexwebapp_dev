import { Store, StoreConfig } from '@datorama/akita';
import { FDCState } from './fdc.state.model'
import { Injectable } from '@angular/core';


import * as localForage from 'localforage';
import { persistState } from '@datorama/akita';
import { AppConfig } from '../../../globals/services/app.config';


const createInitialState = (json): FDCState => {
    return JSON.parse(json);
}

const json = {
    id: '',
    DowntimeReasons: [] as Array<any>,
    DataAlertRules: [] as Array<any>,
    TankLibrary: [] as Array<any>,
    Statuses: [] as Array<any>,
    Formations: [] as Array<any>,
    FuelFlareVent: [] as Array<any>,
    VolumeOverrideReasons: [] as Array<any>,
    OrificeLibrary: [] as Array<any>,
    RecalculateHistory: [] as Array<any>,
    schema: {} as any,
    variables: {} as any,
    timeZone : '' as string,
    mytestData: '' as string
};


@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'fdc', resettable: true })
export class FDCStore extends Store<FDCState> {
    public CustomerAppsettings = AppConfig.AppMainSettings;

    constructor(private appConfig: AppConfig) {

        super(createInitialState(JSON.stringify(json)));

        if ((this.CustomerAppsettings.env.name == "PROD" || this.CustomerAppsettings.env.name == "DEV") && this.CustomerAppsettings.env.UseAsDesktop == false) {
            if (this.appConfig.tenantName) {
                localForage.config({
                    driver: localForage.INDEXEDDB,
                    name: 'Visur',
                   // version: 1.1,
                    storeName: this.appConfig.tenantName
                });

                persistState({ key: 'fdc', include: ["fdc"], storage: localForage });
            }
            else {
                console.error(this.storeName + " TenantName can not be undefinded");
            }
        }
    }
}

