import { FABRICS } from '../../globals/Model/CommonModel';

export interface EntityStateModel {
}

export class EntityStateConstantsKeys {
  public static hierarchy = ["Area", "Field", "District"];
  public static downtimeReasons = "DowntimeReasons";
  public static dataAlertRules = "DataAlertRules";
  public static tankLibrary = "TankLibrary";
  public static FacilityPipelineData = "PipelineLibrary";
  public static status = "Statuses";
  public static GeneralSettings = "GeneralSettings";
  public static formations = "Formations";
  public static ffv = "FuelFlareVent";
  public static volumeOverrideReasons = "VolumeOverrideReasons";
  public static orificeLibrary = "OrificeLibrary";
  public static recalculateHistory = "RecalculateHistory";
  public static schema = "schema";
  public static entities = "entities";
  public static setAsDefault = "setAsDefault";
  public static List = "List";
  public static Locations = "Locations";
  public static All = "All";
  public static Hierarchy = "Hierarchy";
  public static Licensee = "Licensee";
  public static FacilityCode = "FacilityCode";

  public static EntitiesPhysicalFlowPartialHierachy = "EntitiesPhysicalFlowPartialHierachy";

  public static userPrefrenceSetAsDefault = "SetAsDefault";

  public static entityStateAreas = "areas";

  public static entityStateDistrict = "district";

  public static entityStateEquipments = "equipments";
  public static entityStateFacilities = "facilities";
  public static entityStateFields = "fields";

  public static entityStateHeaders = "headers";
  public static entityStateWells = "wells";

  public static entitiesKVP = "entitiesKVP";

  public static entitiesCategorized = "entitiesCategorized";
}

export class HierarchyModel {
  EntityId: string;
  EntityType: string;
  EntityName: string;
  TimeZone: string;
  Parent: Array<any>;
  Capability: Array<any>;
  Capabilities: Array<any>;
  TypeOf: string;

  public getdefultData() {
    var defultData = {
      EntityId: "",
      EntityType: "",
      EntityName: "",
      TimeZone: null,
      Parent: [],
      Capabilities: [],
      Capability: [],
      TypeOf: ""
    }
    return defultData;
  }
}

export class WellsModel {
  EntityId: string;
  EntityType: string;
  EntityName: string;
  TimeZone: string;
  TypeOf: string;
  WellType: string;
  FacilityCode:string;
  HcType: string;
  OperatedStatus:string;
  Properties: Array<any>;
  Parent: Array<any>;
  Status: string;
  Capability: Array<any>;
  Capabilities: Array<string>;
  typeOfField: string;
  Downstream: Array<string>;
  Upstream: Array<string>;


  public getdefultData() {
    var defultData = {
      EntityId: "",
      EntityType: "",
      EntityName: "",
      FacilityCode:null,
      TimeZone: null,
      TypeOf: "",
      WellType: "",
      HcType:"",
      OperatedStatus:"",
      Properties: [],
      Parent: [],
      Status: "",
      Capabilities: [],
      Capability: [],
      typeOfField: "",
      Downstream: [],
      Upstream:[]
    }
    return defultData;
  }
}

export class EquipmentsModels {
  EntityId: string;
  EntityType: string;
  EntityName: string;
  FacilityCode:string;
  TimeZone: string;
  TypeOf: string;
  Capability: Array<any>;
  Capabilities: Array<string>;
  Properties: Array<any>;
  Parent: Array<any>;
  Active: string;
  typeOfField: string;
  Downstream: Array<string>;
  Upstream: Array<string>;
  VesselLink: Array<any>;
  Separation: string;

  public getdefultData() {
    var defultData = {
      EntityId: "",
      EntityType: "",
      EntityName: "",
      FacilityCode:null,
      TimeZone: null,
      TypeOf: "",
      Properties: [],
      Capability: [],
      Capabilities: [],
      Parent: [],
      Status: "",
      Active: "",
      typeOfField: "",
      Downstream: [],
      VesselLink: [],
      Separation: ""
    }
    return defultData;
  }
}

export class FacilityModels {
  EntityId: string;
  EntityType: string;
  EntityName: string;
  FacilityCode:string;
  TimeZone: string;
  TypeOf: string;
  OperatedStatus:string;
  Properties: Array<any>;
  Parent: Array<any>;
  Status: string;
  Capabilities: Array<string>;
  Capability: Array<any>;
  typeOfField: string;
  Downstream: Array<string>;


  public getdefultData() {
    var defultData = {
      EntityId: "",
      EntityType: "",
      EntityName: "",
      FacilityCode:null,
      TimeZone: null,
      TypeOf: "",
      OperatedStatus:"",
      Properties: [],
      Parent: [],
      Status: "",
      Capabilities: [],
      Capability: [],
      typeOfField: "",
      Downstream: []

    }
    return defultData;
  }
}
export class FacilityCodeModels {
  EntityId: string;
  EntityType: string;
  EntityName: string;
  TimeZone: string;
  TypeOf: string;
  FacilityCode: string;
  Properties: Array<any>;
  Parent: Array<any>;
  Capabilities: Array<string>;
  Capability: Array<any>;
  typeOfField: string;
  NestedUnder: Array<string>;
  FacilityOperationalStatus:string;

  public getdefultData() {
    var defultData = {
      EntityId: "",
      EntityType: "",
      EntityName: "",
      TimeZone: null,
      TypeOf: "",
      FacilityCode:"",
      Properties: [],
      Parent: [],
      Capabilities: [],
      Capability: [],
      typeOfField: "",
      NestedUnder: [],
      FacilityOperationalStatus:""
    }
    return defultData;
  }
}

export class DeliveredModels {
  EntityId: string;
  EntityType: string;
  EntityName: string;
  FacilityCode:string;
  TimeZone: string;
  TypeOf: string;
  OperatedStatus:string;
  Properties: Array<any>;
  Parent: Array<any>;
  Status: string;
  Capabilities: Array<string>;
  Capability: Array<any>;
  typeOfField: string;
  Downstream: Array<string>;


  public getdefultData() {
    var defultData = {
      EntityId: "",
      EntityType: "",
      EntityName: "",
      FacilityCode: null,
      TimeZone: null,
      TypeOf: "",
      OperatedStatus:"",
      Properties: [],
      Parent: [],
      Status: "",
      Capabilities: [],
      Capability: [],
      typeOfField: "",
      Downstream: []

    }
    return defultData;
  }
}

export class vesselsModel {
  EntityId: string;
  EntityType: string;
  EntityName: string;
  FacilityCode:string;
  TimeZone: string;
  TypeOf: string;
  Capability: Array<any>;
  Capabilities: Array<string>;
  Properties: Array<any>;
  Parent: Array<any>;
  Active: string;
  typeOfField: string;
  Downstream: Array<string>;
  TestingVessel: boolean;
  Separation: string;

  public getdefultData() {
    var defultData = {
      EntityId: "",
      EntityType: "",
      EntityName: "",
      FacilityCode: null,
      TimeZone: null,
      TypeOf: "",
      Properties: [],
      Capability: [],
      Capabilities: [],
      Parent: [],
      Status: "",
      Active: "",
      typeOfField: "",
      Downstream: [],
      TestingVessel: "",
      Separation: ""

    }
    return defultData;
  }  
}


/**************************People Fabric **************************/
export class PersonsModel{
  EntityId: string;
  EntityType: string;
  EntityName: string;
  Parent:Array<string>;
  SG:Array<string>;
  ModifiedDateTime:string;

  public getdefultData() {   
    let defaultPerson = {
     EntityId:"",
     EntityType:"",
     EntityName:"",
     Parent:[],
     SG:[FABRICS.PEOPLEANDCOMPANIES],
     ModifiedDateTime: new Date().toISOString()
    };
    return defaultPerson;
  }

}

export class OfficeModel{
  EntityId: string;
  EntityType: string;
  EntityName: string;
  Parent:Array<string>;
  SG:Array<string>;
  ModifiedDateTime:string;

  public getdefultData() {    
    let defaultPerson = {
     EntityId:"",
     EntityType:"",
     EntityName:"",
     Parent:[],
     SG:[FABRICS.PEOPLEANDCOMPANIES],
     ModifiedDateTime: new Date().toISOString()
    };
    return defaultPerson;
  }
}

export class CompanyModel {
  EntityId: string;
  EntityType: string;
  EntityName: string;
  CompanyType: Array<string>;
  SG:Array<string>;
  ModifiedDateTime:string;
  public getdefultData() {
    let defaultData ={
      EntityId:"",
      EntityType:"",
      EntityName:"",
      CompanyType:[],
      SG:[FABRICS.PEOPLEANDCOMPANIES],
     ModifiedDateTime: new Date().toISOString()
    };
    return defaultData;
  }
}

export class PeopleParentModel{
  EntityId:string;
  EntityType:string;
}




