import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { SchemaEntityStateModel } from './schema.entity.state.model';
import { SchemaCommonEntityState, SchemaEntityCommonStore } from './schema.entity.state.store';

@Injectable({ providedIn: 'root' })
export class SchemaEntityCommonStoreQuery extends QueryEntity<SchemaCommonEntityState, SchemaEntityStateModel> {
  constructor(protected store: SchemaEntityCommonStore) {
      super(store);
  }

  Set(entities: any) {
      try {
          this.store.set(entities);
      }
      catch (e) {
          console.error(e);
      }
  }

  /**
   * Add new Entitiy to the existing store
   */
  Add(key, schema: any) {
    try {
      this.store.upsert(key, schema);
    } catch (e) {
      console.error(e);
    }
  }

  SetActive(id){
    this.store.setActive(id);
  }
  resetStore(){
    this.store.reset();
  }
}