import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { SchemaEntityStateModel } from './schema.entity.state.model';
export interface SchemaCommonEntityState extends EntityState<SchemaEntityStateModel> { }

const createInitialState = (json): SchemaEntityStateModel => {
    return JSON.parse(json);
}
const json = {};

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'schemas', resettable: true })
export class SchemaEntityCommonStore extends EntityStore<SchemaCommonEntityState, SchemaEntityStateModel> {
    constructor() {
        super({ initialState: createInitialState(JSON.stringify(json)), loading: false });

        // localForage.config({
        //     driver: localForage.INDEXEDDB,
        //     name: 'Visur',
        //     storeName: 'v2rdemo'
        // });
        // persistState({ key: this.storeName, include: [this.storeName], storage: localForage });
    }
}