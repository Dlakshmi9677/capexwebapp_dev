import { Injectable } from "@angular/core";
import { UsersStateQuery } from "../../../../webWorker/app-workers/commonstore/user/user.state.query";
import { EntityTypes } from "../../globals/Model/EntityTypes";
import { UserStateConstantsKeys } from "../../../../webWorker/app-workers/commonstore/user/user.state.model";
import { resetStores } from "@datorama/akita";
import { MessageModel } from "../../globals/Model/Message";
@Injectable({ providedIn: 'root' })
export class StateRecords {

    constructor(private userStoreQuery: UsersStateQuery) { }

    isStateObjectExists(message: MessageModel) {

        switch (message.EntityType) {
            case EntityTypes.UserPreference:
                var data = this.userStoreQuery.getStateKey(UserStateConstantsKeys.userPrefrence);
                if (data != null && data != {}) {
                    message.Payload = JSON.stringify(data);
                    return message;
                }
                break;
            default: break;
        }
        return false;
    }
    resetAll() {
        try {
            resetStores({});
        }
        catch (e) {
            console.error("reSetStore " + e);
        }
    }
    reSetStore() {
        try {
            resetStores({ exclude: ['users'] });
        }
        catch (e) {
            console.error("reSetStore " + e);
        }
    }
}
