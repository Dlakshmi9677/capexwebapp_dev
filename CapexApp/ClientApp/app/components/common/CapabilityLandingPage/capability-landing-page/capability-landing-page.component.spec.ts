import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CapabilityLandingPageComponent } from './capability-landing-page.component';

describe('CapabilityLandingPageComponent', () => {
  let component: CapabilityLandingPageComponent;
  let fixture: ComponentFixture<CapabilityLandingPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CapabilityLandingPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CapabilityLandingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
