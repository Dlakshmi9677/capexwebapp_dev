import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-capability-landing-page',
  templateUrl: './capability-landing-page.component.html',
  styleUrls: ['./capability-landing-page.component.scss']
})
export class CapabilityLandingPageComponent implements OnInit {
@Input() capability
  constructor() { }

  ngOnInit(): void {
  }

}
