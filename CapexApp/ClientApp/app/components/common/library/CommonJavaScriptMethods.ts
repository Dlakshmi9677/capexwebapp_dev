export class CommonJsMethods {
    public static isvalidJson(json: string) {
        try {
            JSON.parse(json);
        } catch (e) {
            return false;
        }
        return true;
    }

    public static isEmpty(jsonObject) {
        for (var key in jsonObject) {
            if (jsonObject.hasOwnProperty(key))
                return true;
        }
        return false;
    }

    public static newGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}