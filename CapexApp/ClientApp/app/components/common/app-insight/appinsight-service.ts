import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApplicationInsights } from '@microsoft/applicationinsights-web';
import { AppConfig } from '../../globals/services/app.config';

@Injectable()
export class AppInsightsService {
  public instrumentationKey;
  public config;
  public baseUrl = AppConfig.BaseURL;
  public appInsights;
  constructor(private http: HttpClient) {
    this.getInstrumentationKey();
  }

  getInstrumentationKey() {
    var Appset = AppConfig.AppMainSettings;
    if (Appset.env.name != "DEV" && Appset.env.UseAsVScode == false) {
      let url = this.baseUrl + 'Home/GetInstrumentationKey';
      this.http.get(url).subscribe(res => {
        this.instrumentationKey = res;
        var init = new ApplicationInsights({ config: {instrumentationKey: this.instrumentationKey} });
        this.appInsights = init.loadAppInsights();
      });
    }
  }

  /**
   * @param {?=} name
   * @param {?=} url
   * @param {?=} properties
   * @param {?=} measurements
   * @param {?=} duration
   * @return {?}
   */
  logPageView(name?: string, url?: string, properties?: any, measurements?: any, duration?: number) {
    this.appInsights.trackPageView({name : name, url : url, properties : properties, measurements : measurements, duration : duration});
  }
  /**
   * @param {?} name
   * @param {?=} properties
   * @param {?=} measurements
   * @return {?}
   */
  logEvent(name: string, properties?: any) {
    if(this.appInsights){
    this.appInsights.trackEvent(name, properties);
    }
  }
  /**
   * @param {?} exception
   * @param {?=} handledAt
   * @param {?=} properties
   * @param {?=} measurements
   * @return {?}
   */

  logException(exception: Error) {
    if(this.appInsights){
    this.appInsights.trackException(exception);
    }
  }
  /**
   * @param {?} message
   * @param {?=} properties
   * @param {?=} severityLevel
   * @return {?}
   */
  logTrace(message: string, properties?: any, severityLevel?: any) {
    if(this.appInsights){
    this.appInsights.trackTrace(message, properties);
    }
  }
}
