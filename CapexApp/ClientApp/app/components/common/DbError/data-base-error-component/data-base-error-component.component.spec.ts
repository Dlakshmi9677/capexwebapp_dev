import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataBaseErrorComponentComponent } from './data-base-error-component.component';

describe('DataBaseErrorComponentComponent', () => {
  let component: DataBaseErrorComponentComponent;
  let fixture: ComponentFixture<DataBaseErrorComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataBaseErrorComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataBaseErrorComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
