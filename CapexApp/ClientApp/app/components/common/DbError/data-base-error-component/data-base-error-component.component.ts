import { Component, OnInit } from '@angular/core';
import { CommonService } from './../../../globals/CommonService';
@Component({
  selector: 'app-data-base-error-component',
  templateUrl: './data-base-error-component.component.html',
  styleUrls: ['./data-base-error-component.component.scss']
})
export class DataBaseErrorComponentComponent implements OnInit {

  constructor(public commonservice:CommonService) { }

  ngOnInit() {
  }
   Cancel(){
      this.commonservice.isDBError = false;
   }
}
