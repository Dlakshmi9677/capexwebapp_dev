import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppConfig } from '../../globals/services/app.config';
import { CommonService } from '../../globals/CommonService';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { FabricRouter } from '../../globals/Model/FabricRouter';

@Component({
  selector: 'invalid',
  templateUrl: 'invalid.component.html',
  styleUrls: ['./invalid.component.scss']
})

export class InvalidPageComponent {
  public isUnauthorizedPage: boolean = false;

  constructor(private router: Router, public dialog: MatDialog) {
    this.CheckisUnauthorizedPage();
  }

  CheckisUnauthorizedPage() {
    if (this.router.url == '/UnauthorizedPage')
      this.openDialog();
    else
      console.error("=============Invalid Page==============" + this.router.url);
  }

  openDialog() {
    const dialogConfig = {
      height: '100%',
      width: '100%',
      maxWidth: '100%',
      disableClose: true,
      panelClass: "dialogOpacity"
    };

    const dialogRef = this.dialog.open(DialogContent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}

@Component({
  selector: 'dialog-content',
  templateUrl: 'dialog-content.html',
  styleUrls: ['./invalid.component.scss']
})

export class DialogContent {
  public UserCanAccessCapabilitiesList = AppConfig.CapabilitiesJson.Capabilities;
  public isUnauthorizedPage: boolean = true;

  constructor(private router: Router, private appConfig: AppConfig, public dialog: MatDialog, private commonService: CommonService, public dialogRef: MatDialogRef<DialogContent>) {
    this.router.navigate([FabricRouter.HOME_FABRIC]);
  }

  onClick(data) {
    try {
      this.commonService.menuFavIconsHide.next();
      this.commonService.routeToFabrics(data)
      this.commonService.isSubHeaderClick = true;

      if (data.id && data.id == 'menu')
        this.commonService.isSubHeaderEnable = !this.commonService.isSubHeaderEnable;

      setTimeout(() => {
        this.commonService.isSubHeaderEnable = false;
        this.commonService.subHeaderDisable = false;
      }, 0);

      this.commonService.popupformclose = false;
      this.dialogRef.close();
    }
    catch (e) {
      console.error('Exception in onClick() of sub-header.component  at sub-Header time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

}
