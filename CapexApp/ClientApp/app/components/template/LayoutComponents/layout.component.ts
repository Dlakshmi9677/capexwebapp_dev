import { ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ResizeEvent } from 'angular-resizable-element';
import { Subject } from 'rxjs';
import { takeUntil } from "rxjs/operators";
import { CommonService } from '../../globals/CommonService';
import { FABRICS } from '../../globals/Model/CommonModel';
import { AppConfig } from '../../globals/services/app.config';
import { FdcHeaderPopupTsComponent } from '../LayoutComponents/headers/fdc-header-popup/fdc-header-popup.component';

declare let $: any;
@Component({
  selector: 'layout',
  templateUrl: 'layout.component.html',
  styleUrls: ['./layout.component.styles.scss'],
})

export class LayoutComponent implements OnInit, OnDestroy {
  private themeWrapper = document.querySelector('body');
  @ViewChild('sideHeaderElement',{static:false}) sideHeaderElement:ElementRef;
  clientHeight = 500;
  leftTreeWidth = 234;
  minWidth = 234;
  maxWidth = 362;
  ComponentName = 'LeftSideBar';
  ListQueryParmas: any;
  public CustomerAppsettings = AppConfig.AppMainSettings;
  public layoutClass = 'layout-web';
  fabricClass = 'full-fabric-view';
  takeUntilDestroyObservables=new Subject();

  constructor(private route: ActivatedRoute, public commonService: CommonService, private router: Router,  private renderer: Renderer2, private matBar: MatSnackBar,private cdRef: ChangeDetectorRef) {
    try {
      for (var key in commonService.darkTheme) {
        if (commonService.darkTheme.hasOwnProperty(key)) {
          var val = commonService.darkTheme[key];
          this.themeWrapper.style.setProperty(key, val);
        }
      }
      this.commonService.zoomApplicationForTabletMode$.pipe(this.compUntilDestroyed()).subscribe((res:any)=>{
        this.zoomApplicationForTabletMode(res);
      })
      this.commonService.subheadertoggleMode$.pipe(this.compUntilDestroyed()).subscribe((res:any)=>{
        this.subheaderZoom();
      })      
    }
    catch (e) {
      console.error('Exception in constructor of layout.component in LayoutComponents at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
      this.commonService.appLogException(new Error('Exception in constructor of layout.component in LayoutComponents at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  /**
   * For Zooming The Application in TabletMode
   * @param data 
   */
  subheaderZoom(){
  var subheader = document.getElementById('sub-header');
  var headerPage = document.getElementById('main-header');

    if (subheader && subheader.style && headerPage && headerPage.style) {
         subheader.style.zoom = headerPage.style.zoom ;
    }
  }
  zoomApplicationForTabletMode(data?:any){
  var headerPage = document.getElementById('main-header');
  var  containerPage = document.getElementById('full-app-container');
  var zoomValueToBeUpdate:any=0;
  let IncrementOrDecrement: string = parseInt(this.commonService.ZoomPercentage) === 100 ? "Default" : parseInt(this.commonService.ZoomPercentage) > 100 ? "Increment" : "Decrement";
  if(data.Bydefault){
    var ByDefault = data.Bydefault;
  }  
  if (data) {
      switch (data.ZoomType) {
        case "Increment":
          zoomValueToBeUpdate += 10;
          break;
        case "Decrement":
          zoomValueToBeUpdate -= 10;
          break;
        default:
          if (this.commonService.tabletMode) {    
              if(data.Value==true){
                this.commonService.zoomApplicationForTabletMode$.next({ "ZoomType": IncrementOrDecrement, "Value": parseInt(this.commonService.ZoomPercentage) + '%' });
              }
      } else {
            zoomValueToBeUpdate = 0;
            if (headerPage.style) {
              headerPage.style.zoom = 100 + '%';
            } if (containerPage.style) {
              containerPage.style.zoom = 100 + '%';
            }
            return false;
          }
          break;
      }
    }

    if (data.ZoomType == "Decrement") {
      if (headerPage.style.zoom == "10%") {
        headerPage.style.zoom = "15%"
      }
      if (containerPage.style.zoom == "10%") {
        containerPage.style.zoom = "15%"
      }
    }

    if (headerPage.style) {
      if (headerPage.style.zoom==""){
        headerPage.style.zoom=100+'%';
        if(zoomValueToBeUpdate==undefined){
         headerPage.style.zoom = 100 + '%';
       }
      }
      else{
        if(zoomValueToBeUpdate==undefined){
          headerPage.style.zoom = 100 + '%';
        } else {
          if (headerPage.style.zoom == "5%") {
            headerPage.style.zoom = "0%"
          }
          if(!ByDefault){
            headerPage.style.zoom = parseInt(headerPage.style.zoom) + zoomValueToBeUpdate + '%';
            }
            else{
            headerPage.style.zoom = this.commonService.ZoomPercentage;
            }
        }
      }
    }
    if (containerPage.style) {
      if (containerPage.style.zoom==""){
        containerPage.style.zoom=100+'%';
        if(zoomValueToBeUpdate==undefined){
          containerPage.style.zoom = 100 + '%';
       }
      }
      else{
        if(zoomValueToBeUpdate==undefined){
          containerPage.style.zoom = 100 + '%';
        } else {
          if (containerPage.style.zoom == "5%") {
            containerPage.style.zoom = "0%"
          }
          if(!ByDefault){
            containerPage.style.zoom = parseInt(containerPage.style.zoom)+ zoomValueToBeUpdate + '%';
            }
            else{
            containerPage.style.zoom = this.commonService.ZoomPercentage;
            }
        }
      }
    }
   return false;
  }

  closesubheader() {
    try {
      this.commonService.isUserMenuHeader = false;
      this.commonService.isSubHeaderEnable = false;
      this.commonService.isContextMenuHeader = false;
      this.commonService.subHeaderDisable =false;
    }
    catch (e) {
      console.error('Exception in closesubheader() of layout.component in LayoutComponents at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
      this.commonService.appLogException(new Error('Exception in closesubheader() of layout.component in LayoutComponents at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  ngOnInit() {
    try {
      this.route.queryParams.pipe(this.compUntilDestroyed()).subscribe(params => {
        this.commonService.isSubHeaderEnable = false;
        this.ListQueryParmas = JSON.parse(JSON.stringify(params));
        if (this.ListQueryParmas.leftnav != this.commonService.checkTreeActive.toString()) {
          if (this.ListQueryParmas.leftnav == 'true')
            this.collapseChange("LeftSideBar");
        }
        this.cdRef.detectChanges();
      });
      if (this.commonService.isclickoncreatelist) {
        setTimeout(() => {
          this.renderer.selectRootElement('#TextBoxForList').focus()

        }, 0);
      }
    } catch (e) {
      console.error('Exception in ngOnInit() of layout.component in LayoutComponents at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
      this.commonService.appLogException(new Error('Exception in ngOnInit() of layout.component in LayoutComponents at time ' + new Date().toString() + '. Exception is : ' + e));
    }

  }

  ngAfterViewChecked(){
    this.commonService.sideHeaderElement = this.sideHeaderElement;
  }

  IntialCount = 0;
  collapseChange(event) {
    try {
      // if (this.commonService.lastOpenedFabric.toUpperCase() == "FDC" || this.commonService.lastOpenedFabric == "OPERATIONALFORM") {
      //   this.commonService.renderedMapOnCollapse.next("RenderderedMap");
      // }
      if (this.IntialCount == 0) {
        this.IntialCount = this.IntialCount + 1;
      }
      if (event == 'All' || event == 'Lists' || event == 'Search') {
        this.commonService.checkTreeActive = true;
        // this.commonService.tabIndexIdVar = event.toLowerCase();
      }

      if (event === this.ComponentName) {
        this.commonService.checkTreeActive = true;
      }

      this.ListQueryParmas['leftnav'] = this.commonService.checkTreeActive;
      this.router.navigate([], { relativeTo: this.route, queryParams: this.ListQueryParmas });

    } catch (e) {
      console.error('Exception in collapseChange() of LayoutComponent  at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in collapseChange() of LayoutComponent  at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
 
leftHeaderToggle() {
    try {
      setTimeout(() => {
        this.commonService.layoutJSON.LeftHeader = !this.commonService.layoutJSON.LeftHeader;
        this.commonService.updateLayout.next();  
      }, 0);
    } 
    catch (e) {
      console.error('Exception in leftTreeToggle() of LayoutComponent  at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in leftTreeToggle() of LayoutComponent  at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }
  
  rightHeaderToggle() {
    try {
      if(!this.router.url.includes("EntityID") && this.commonService.lastOpenedFabric == FABRICS.ENTITYMANAGEMENT){}
      else{
        if(this.commonService.layoutJSON.RightHeader){
          this.commonService.rightreeCollapse = true;
        }
        else{
          this.commonService.rightreeCollapse = false;
        }
        this.commonService.layoutJSON.RightHeader = !this.commonService.layoutJSON.RightHeader;
        this.commonService.updateLayout.next();
      }
    }
    catch (e) {
      console.error('Exception in leftTreeToggle() of LayoutComponent  at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in leftTreeToggle() of LayoutComponent  at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  openSnackBar(messageToShow) {
    try {
      this.matBar.openFromComponent(FdcHeaderPopupTsComponent, {
        data: { message: messageToShow },
        verticalPosition: 'top',
        horizontalPosition: 'center',
        panelClass: ['snackBarPopUp'],
      })
      this.commonService.sBar = this.matBar;
    }
    catch (e) {
      console.error('Exception in openSnackBar() of LayoutComponent  at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in openSnackBar() of LayoutComponent  at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  
  onResizeEnd( ev : ResizeEvent ){
    if(this.commonService.isUserSettingForm){
      if( ev.rectangle.right <= this.maxWidth && ev.rectangle.right >= (this.minWidth + 42) )
      this.leftTreeWidth = ev.rectangle.right - 42;
    }
  }
  
  headerClick(ev){
    if(this.commonService.isSubHeaderEnable && this.commonService.subHeaderDisable)
    {
      this.commonService.isSubHeaderEnable = false;
      this.commonService.subHeaderDisable =false;
    }
      else if(this.commonService.isSubHeaderEnable)
      this.commonService.subHeaderDisable =true;
  }

  compUntilDestroyed():any {
    return takeUntil(this.takeUntilDestroyObservables);
    }

  ngOnDestroy() {
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }


}