import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, of } from 'rxjs';
import { takeUntil } from "rxjs/operators";
import { ObservableStoreService } from '../../../../common/state/observables/observables.entity.state.query';
import { CommonService } from '../../../../globals/CommonService';
import { AngularHierarchyTreeComponent } from '../../../../globals/components/angular-hierarchy-tree/angular-hierarchy-tree.component';
import { AngularTreeEventMessageModel, AngularTreeMessageModel, FABRICS, FabricsNames, TreeOperations, TreeTypeNames } from '../../../../globals/Model/CommonModel';
import { EntityTypes } from '../../../../globals/Model/EntityTypes';

@Component({
  selector: 'capability-tree',
  templateUrl: './capability.tree.component.html',
  styleUrls: ['./capability.tree.component.styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class CapabilityTreeComponent implements OnInit {

  @Output() EmmitTreeData: EventEmitter<any> = new EventEmitter<any>();
  @Input() HierarchyTreeName = '';
  @ViewChild('hierarchyTree', { static: false }) hierarchyTree: AngularHierarchyTreeComponent;
  takeUntilDestroyObservables = new Subject();
  constructor(public observableStateService: ObservableStoreService, public commonService: CommonService, private router: Router,
    public cdref: ChangeDetectorRef) {

    // msg.treeName==="MultiOperation"?true:
    this.commonService.sendDataToAngularTree
      .filter(msg => msg.treeName.indexOf(this.HierarchyTreeName) > -1)
      .pipe(this.compUntilDestroyed())
      .subscribe((data: AngularTreeMessageModel) => {
        try {
          // if (data.fabric != FABRICS.FDC.toLocaleUpperCase()) {
            this.hierarchyTree.receiveMessageFromFabrics(data);
          // }
          // else if ((data.treeOperationType != TreeOperations.createFullTree || data.treeName.indexOf(TreeTypeNames.LIST) > -1 || data.treeName.indexOf(TreeTypeNames.GroupsAndPeopleInShare) > -1 || data.treeName.indexOf(TreeTypeNames.SharedPersons) > -1) && data.fabric == FABRICS.FDC.toLocaleUpperCase()) {
          //   this.hierarchyTree.receiveMessageFromFabrics(data);
          // }
          cdref.detectChanges();
          cdref.markForCheck();
        }
        catch (e) {
          console.error('Exception in constructor of sendDataToAngularTree  subscriber of capability.tree.component  at CapabilityTree. time ' + new Date().toString() + '. Exception is : ' + e);
        }

      });

    this.commonService.getAngularTreeEvent$.filter((data: AngularTreeEventMessageModel) => data.treeName.indexOf(this.HierarchyTreeName) > -1).pipe(this.compUntilDestroyed()).subscribe((data: AngularTreeEventMessageModel) => {
      this.hierarchyTree.getAngularTreeEvent(data);
    });


  }
  filterFunc(info) {
    if (info['tab'] == this.HierarchyTreeName && info['fabirc'] == FabricsNames.SECURITY) {
      return true;
    }
    return false;
  }
  ngOnInit(): void {
    this.commonService.permissionEntitiesTab$.filter(info => this.filterFunc(info)).pipe(this.compUntilDestroyed()).subscribe((data) => {
      this.enablePermissionsEntitiesTree();
    });

    this.commonService.physicalInstanes$
      .filter(d => d && d.treeName == this.HierarchyTreeName)
      .subscribe((d) => {
        this.hierarchyTree.nodes = [];
        if (d.type != TreeTypeNames.Construction) {
          const data = JSON.parse(JSON.stringify(d.data));
          this.hierarchyTree.reinitializeTreeSubscription(of(data));
        }
        else {
          this.enablePermissionsEntitiesTree();
        }
      });
  }

  ngAfterViewInit() {
    let fabricName = this.commonService.getFabricNameByUrl(this.router.url);
    this.observableStateService.getState$(fabricName, this.HierarchyTreeName).first()
      .pipe(this.compUntilDestroyed())
      .subscribe((obs :any)=> {
        this.hierarchyTree.GenerateTree(obs);
      });
  }

  enablePermissionsEntitiesTree() {
    let fabricName = this.commonService.getFabricNameByUrl(this.router.url);
    this.observableStateService.getState$(fabricName, this.HierarchyTreeName).first()
      .pipe(this.compUntilDestroyed())
      .subscribe((obs: any) => {
        this.hierarchyTree.GenerateTree(obs);
      });

  }

  getTreeData() {
    return this.hierarchyTree.tree;
  }

  sendDataToFabric(msg: AngularTreeMessageModel) {
    this.commonService.sendDataFromAngularTreeToFabric.next(msg);
  }

  compUntilDestroyed():any {
    return takeUntil(this.takeUntilDestroyObservables);
    }
  ngOnDestroy() {
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }
}
