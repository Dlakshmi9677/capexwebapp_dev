import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatMenuTrigger } from '@angular/material/menu';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { EntityFilterStoreQuery } from '../../../../common/state/EntityFilter/entityfilter.query';
import { CommonService } from '../../../../globals/CommonService';
import { AngularTreeMessageModel, FABRICS, TreeOperations, TreeTypeNames } from '../../../../globals/Model/CommonModel';
import { AppConfig } from '../../../../globals/services/app.config';
export const OPERATIONALSTATUS = "OperationalStatus";
export const STATUS="Status";
export const FILTERENTITYTYPES="EntityTypes";
@Component({
  selector: 'common-entity-filter',
  templateUrl: './common-entity-filter.component.html',
  styleUrls: ['./common-entity-filter.component.styles.scss']
})

export class CommonEntityFilter implements OnInit, OnDestroy  {
  public CustomerAppsettings = AppConfig.AppMainSettings;
  // @ViewChild(MatMenuTrigger, {static: false}) trigger: MatMenuTrigger;
  @Input() TreeName=''
  myForm:FormGroup
  RouteParam
  SelectAllStatus : boolean=false
  isStatusInitialize:boolean=false;
  filterApplied=false
  EntityTypes=[]
  // menuOpen: boolean;
  filterData={};
  SelectedEntityType={};
  payload = [];
  customSort = [];
  sortbycapability;
 @Output() entityFilterEmmitter = new EventEmitter(); 
  // sortbyentitytype;
  // fabric;
  filteredEntity = "";
  FilterStatusForTabs = [];
  filterOptionsExpand = false;
  openfilter = false;
  openEntityTypeSelection=false
  leftTab;
  filterValue;
  jsonSchema
  expandCollapse=true
  matIcon
  ExpandSingle_Icon_Idle = "V3 ToggleDown";
  CollapsSingle_Icon_Idle = "ExpandAccordion";
  @ViewChild('filterformHeader',{static:false}) filterformHeader:ElementRef;
  @ViewChild('filterformBody',{static:false}) filterformBody:ElementRef;
  @ViewChild('filterheader',{static:false}) filterheader:ElementRef;
  @ViewChild('filterEntities',{static:false}) filterEntities:ElementRef;
  currentItemForApplyFilter;
  selectedStatus={};
  selectedOperatedStatus={};
  AllStatuses : boolean=false;
  AllOperationalStatuses:boolean=false;
  selectedFacilityOperationalStatus={};
  currentObjectKey:any;
  commonTabBasedOnCapability=[];
  takeUntilDestroyObservables=new Subject();
  constructor(private el:ElementRef,public router: Router,public route: ActivatedRoute, public commonService: CommonService, public entityFilterStoreQuery: EntityFilterStoreQuery ) {
    this.commonService.sendDataToAngularTree
    .filter(msg => msg.treeOperationType == TreeOperations.filteroptionjson && msg.treeName[0] == this.TreeName)
      .pipe(this.compUntilDestroyed())
    .subscribe((data: AngularTreeMessageModel) => {
        this.receiveMessageFromFabrics(data);
    });
    this.commonService.sendDataFromAngularTreeToFabric
      .pipe(filter(message => message.treeOperationType == TreeOperations.FullTreeOnCreation))
      .pipe(this.compUntilDestroyed())
      .subscribe((msg: AngularTreeMessageModel) => {
        try {
          if(this.route.queryParams && msg.treeName[0]==this.route.queryParams['_value']['tab'] && msg.treeName[0]==this.TreeName)
          this.checkFilterStatus(this.route.queryParams['_value']['tab']);
        }
        catch (e) {
          console.error('Exception in sendDataFromAngularTreeToFabric subscriber of FDCService in  FDC/service at time ' + new Date().toString() + '. Exception is : ' + e);
        }

      })
  }

  ngOnInit() {
    try {
      this.initializeCommonTabBasedOnCapability()
      // this.fabric = this.commonService.getFabricNameByUrl(this.router.url);
      this.route.queryParams.pipe(this.compUntilDestroyed()).subscribe((params :any)=> {
        if(this.RouteParam && params.tab && params.tab!=this.RouteParam.tab && (this.TreeName==params.tab )){
          this.checkFilterStatus(params.tab); // || this.TreeName == this.commonService.treeIndexLabel
        }
        if(this.router.url.includes("EntityID")|| this.router.url.includes("EntityId")){
          this.closeOptions();
        }
        this.RouteParam = JSON.parse(JSON.stringify(params));
    });
    this.router.events.filter((evt:any)=>evt && evt.snapshot && evt.snapshot.queryParams  && evt.snapshot.queryParams.tab && evt.snapshot.queryParams.tab==this.TreeName).subscribe((event:any) => {
      if((event && event.url && (event.url.includes("EntityID") ||  event.url.includes("EntityId")))|| (this.router && this.router.url && this.routerCheckForCloseFilterForm())){
        this.closeOptions();
      }
    });
 
    }
    catch (e) {
      console.error('Exception in ngOnInit() of entity-filter.component  at EntityFilter. time ' + new Date().toString() + '. Exception is : ' + e);
    }
    
  }
  initializeCommonTabBasedOnCapability(){
    let key1=''
    switch (this.commonService.getFabricNameByUrl(this.router.url)) {
      case FABRICS.ENTITYMANAGEMENT:
        this.commonTabBasedOnCapability=[]
        break;
      case FABRICS.BUSINESSINTELLIGENCE:
        this.commonTabBasedOnCapability = [TreeTypeNames.PHYSICALFLOW, TreeTypeNames.LIST, TreeTypeNames.ALL];
      default:
        break;
    }
  }
  //dynamic position for the filterform while expanding the left tree
  ngAfterViewChecked(){
    if(this.filterformHeader && this.filterformBody && this.commonService.sideHeaderElement){
      this.filterformHeader.nativeElement.style.left=(this.commonService.sideHeaderElement.nativeElement["offsetWidth"] + 42) + "px";
      this.filterformBody.nativeElement.style.left=(this.commonService.sideHeaderElement.nativeElement["offsetWidth"] + 42) + "px";
      if(this.filterheader && this.filterEntities){
        this.filterheader.nativeElement.style.left=(this.filterformBody.nativeElement.offsetWidth + this.filterformBody.nativeElement.offsetLeft)+"px";
        this.filterEntities.nativeElement.style.left=(this.filterformBody.nativeElement.offsetWidth +   this.filterformBody.nativeElement.offsetLeft)+"px";
      }
    }
  }
  routerCheckForCloseFilterForm(){
    if(this.router.url.includes("EntityID")|| this.router.url.includes("EntityId")||this.router.url.includes('fdcadminform')||this.router.url.includes('EMGlobalAdmin'))
    {
      return true;
    }
    return false;
  }
  checkFilterStatus(tab){
    let key =this.commonService.capabilityFabricId+"_"+tab
      this.entityFilterStoreQuery
        .selectEntity(key).first()
        .subscribe(res=>{
 
          if (res && res.treeName ){
          this.removeOldFilterForm();

          this.filterData=res.data;
          this.jsonSchema=res.schema
          this.applyFilter(tab)
         }
         else{
          this.leftTab=undefined;
          this.filterApplied=false
          this.entityFilterEmmitter.emit(this.filterApplied);
         // this.removeFilter(tab);
         // this.removeFilterByTab(tab);
        }
        }
        )
  }
  
  removeOldFilterForm(){
    this.filterData=null;
    this.filterApplied=false;
    this.commonService.isFilterIconActive = false;
    this.openfilter = false;
    this.openEntityTypeSelection=false
  }
  removeFilterByTab(tab){
    // if(this.commonService.isFilterIconActive){
    //   this.fdcservice.onRefreshUrl = false;
    // }
    this.commonService.isFilterIconActive = false;
    this.filterApplied=false;
    this.filterData=null
    this.entityFilterEmmitter.emit(this.filterApplied);
    this.closeOptions();
    this.entityFilterStoreQuery.deleteNodeByIds([this.commonService.capabilityFabricId+'_'+tab])
    var message: AngularTreeMessageModel = {
      "fabric": this.commonService.getFabricNameByUrl(this.router.url),
      "treeName": [tab],
      "treeOperationType": TreeOperations.RemoveFilter,
      "treePayload": TreeTypeNames.ALL
    };
   this.commonService.sendDataFromAngularTreeToFabric.next(message);
  }
  expandPannel(obj){
    try {
      obj.expand=!obj.expand
      obj.expandIcon = obj.expand ? this.ExpandSingle_Icon_Idle : this.CollapsSingle_Icon_Idle;
    } catch (error) {
      console.error()
    }
  }
  receiveMessageFromFabrics(message: AngularTreeMessageModel) {
    try {
      this.SelectAllStatus=true;
      this.AllStatuses =true;
      this.AllOperationalStatuses=true;
      this.jsonSchema=message.treePayload.schema
      this.filterData=message.treePayload.data
      if(this.filterData && this.filterData['EntityTypes'] && this.filterData['EntityTypes'].hasOwnProperty("Plant")){
      this.filterData['EntityTypes']["Gas Plant"] = this.filterData['EntityTypes']["Plant"];
      delete this.filterData['EntityTypes']["Plant"];
      }
      this.SelectedEntityType={}
      this.selectedOperatedStatus={};
      this.selectedStatus={};
      this.selectedFacilityOperationalStatus={};
      for( var key in this.filterData['EntityTypes']) {
        if (this.filterData['EntityTypes'].hasOwnProperty(key)) {
          const element = this.filterData['EntityTypes'][key];
          if(element){
          this.SelectedEntityType[key]=element
          }
          else{
            this.SelectAllStatus=false;
          }

        }
      }
      if(this.SelectedEntityType.hasOwnProperty("Plant"))
      delete this.SelectedEntityType["Plant"];
      this.facilityCodeOperationalArrangement(message);

    }
    catch (e) {
      console.error('Exception in receiveMessageFromFabrics() of entity-filter.component  at EntityFilter. time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }
  filterStatusAndOperatedStatus(objectKey){
    try{
      if(this.filterData.hasOwnProperty(objectKey) && Object.keys(this.filterData[objectKey]).length!=0){
        var referencedData=JSON.parse(JSON.stringify(this.filterData));
       Object.keys(referencedData[objectKey]).forEach(element=>{
         if(objectKey==OPERATIONALSTATUS)  
          this.selectedOperatedStatus[element]=referencedData[objectKey][element];
          else
          this.selectedStatus[element]=referencedData[objectKey][element] ;
       })
      }
    }catch(e){
      console.error('Exception in filterStatusAndOperatedStatus() of entity-filter.component  at EntityFilter. time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }
  arrangeDefaultStatusAndOperatedStatus(){
    try{
      if(Object.keys(this.selectedStatus).length!=0){
        Object.keys(this.selectedStatus).forEach(element=>{
         if(Object.keys(this.selectedStatus[element]).length!=0){
          Object.keys(this.selectedStatus[element]).forEach((innerelment)=>{
            if(this.selectedStatus[element][innerelment]){
              const value=this.selectedStatus[element][innerelment].value;
              if(!value){
                delete this.selectedStatus[element][innerelment];
                this.AllStatuses=false;
              }  
            }
           })
         }

        });
      }
  
      if(Object.keys(this.selectedOperatedStatus).length!=0){
        Object.keys(this.selectedOperatedStatus).forEach(element=>{
          if(Object.keys(this.selectedOperatedStatus[element]).length!=0){
          Object.keys(this.selectedOperatedStatus[element]).forEach((innerelment)=>{
            if(this.selectedOperatedStatus[element][innerelment]){
              const value=this.selectedOperatedStatus[element][innerelment].value;
              if(!value){
                delete this.selectedOperatedStatus[element][innerelment];
                this.AllOperationalStatuses=false;
              } 
            }
          })
        }
        });
      }
      this.isStatusInitialize=true;
    }catch(e){
      console.error('Exception in arrangeDefaultStatusAndOperatedStatus() of entity-filter.component  at EntityFilter. time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }
  facilityCodeOperationalArrangement(message: AngularTreeMessageModel){

  }
  removeEntityType(item){

      delete this.SelectedEntityType[item]
      this.filterData['EntityTypes'][item]=false
      this.SelectAllStatus=false
    

  }
  clickEvent(type,item,event){
    switch (type) {
      case 'Save':
        //this.StoreDataIntoState();
        if(this.filterData && this.filterData['EntityTypes'] && this.filterData['EntityTypes'].hasOwnProperty("Gas Plant")){
        this.filterData['EntityTypes']["Plant"] = this.filterData['EntityTypes']["Gas Plant"];
        delete this.filterData['EntityTypes']["Gas Plant"];
        }
        this.saveIntoStateAmongallTABS();
        this.applyFilter(this.commonService.treeIndexLabel)
        break;
      case 'Close':
        this.closeOptions(true);
        break;
      case 'Remove':
        this.removeEntityType(item)
        break;
      case 'RemoveFilter':
        this.removeFilter(this.commonService.treeIndexLabel)
        break;
      case 'Modify':
        this.Enablefilter()
        break;
      case 'selectAll':
        this.selectAll(event.checked)
        break;
      case 'toggle':

        this.entityTypeSelectioen(event.checked,item)
        this.getStatus()
      

        break;
      default:
        break;
    }
  }
  saveIntoStateAmongallTABS(){
    
    if(this.commonTabBasedOnCapability.includes(this.commonService.treeIndexLabel)){
      this.commonTabBasedOnCapability.forEach(tabkey=>{
        this.StoreDataIntoState(tabkey);
      })
    }
    else
      this.StoreDataIntoState(this.commonService.treeIndexLabel)
  }
  StoreDataIntoState(tab){
    // let key=this.commonService.capabilityFabricId+"_"+this.commonService.treeIndexLabel;
    let key=this.commonService.capabilityFabricId+"_"+tab;
    this.entityFilterStoreQuery.Add(key,{
      schema: this.jsonSchema,
      data: JSON.parse(JSON.stringify(this.filterData)),
      capability: this.commonService.capabilityFabricId,
      treeName: tab
    });
  }
  entityTypeSelectioen(event,item){
    if(event){
      this.SelectedEntityType[item]=event
    }
    else{
      delete this.SelectedEntityType[item]
    }
  }
  selectAll(event){
    if(this.currentItemForApplyFilter==STATUS){
      this.ShowstatusBasedOnSelectAll(event,this.currentItemForApplyFilter);
    }else if(this.currentItemForApplyFilter==OPERATIONALSTATUS){

        this.ShowOperationalstatusBasedOnSelectAll(event,this.currentItemForApplyFilter);

      
    }else{
      for (const key in this.filterData['EntityTypes']) {
        if (this.filterData['EntityTypes'].hasOwnProperty(key)) {
          // const element = this.filterData['EntityTypes'][key];
          this.filterData['EntityTypes'][key]=event
          this.entityTypeSelectioen(event,key)
        }
      }
    }

  }
  getStatus(){
    this.SelectAllStatus=true;
    for (const key in this.filterData['EntityTypes']) {
      if (this.filterData['EntityTypes'].hasOwnProperty(key)) {
        const element = this.filterData['EntityTypes'][key];
        if(!element){
          this.SelectAllStatus=false;
        }
      }
    }
    return true
  }
  getActiveNode(entity){
    if(this.filteredEntity == entity)
      return "#318fd78a";
  }
  getKeys(obj){
   return Object.keys(obj).sort()
  }
  applyFilter(tab){
    this.filterApplied=true;
    this.closeOptions()
    this.commonService.isFilterIconActive = true;
    this.entityFilterEmmitter.emit(this.filterApplied);
    // var message: AngularTreeMessageModel = {
    //   "fabric": this.commonService.lastOpenedFabric,
    //   "treeName": [tab],
    //   "treeOperationType": TreeOperations.selectedfilteroption,
    //   "treePayload": this.filterData
    // };
    // this.commonService.sendDataFromAngularTreeToFabric.next(message);
    var message: AngularTreeMessageModel;
    if(this.commonTabBasedOnCapability.includes(tab)) {
      this.commonTabBasedOnCapability.forEach(treetab => {
        message = {
          "fabric": this.commonService.lastOpenedFabric,
          "treeName": [treetab],
          "treeOperationType": TreeOperations.selectedfilteroption,
          "treePayload": this.filterData
        };
        this.commonService.sendDataFromAngularTreeToFabric.next(message);
      })
    }else{
      message = {
        "fabric": this.commonService.lastOpenedFabric,
        "treeName": [tab],
        "treeOperationType": TreeOperations.selectedfilteroption,
        "treePayload": this.filterData
      };
      this.commonService.sendDataFromAngularTreeToFabric.next(message);
    }
    this.filterData=null
  }
  closeOptions(fromCloseButton?){
    this.commonService.isFilterIconActive = false;
    this.openfilter = false;
    if(fromCloseButton)
    this.filterData=null
    this.openEntityTypeSelection=false
    this.commonService.updateEntityFilterStatus(false);
    // this.entityFilterEmmitter.emit(this.openfilter);
  }
  getCropedValue(value){
   return value.length>9?value.substring(0,9)+'...':value
  }
  removeFilter(tab){
    // if(this.commonService.isFilterIconActive){
    //   this.fdcservice.onRefreshUrl = false;
    // }
    this.commonService.isFilterIconActive = false;
    this.filterApplied=false;
    this.filterData=null
    this.entityFilterEmmitter.emit(this.filterApplied);
    this.closeOptions();
    if(this.commonTabBasedOnCapability.includes(tab)){
      this.commonTabBasedOnCapability.forEach(tabkey=>{
        this.entityFilterStoreQuery.deleteNodeByIds([this.commonService.capabilityFabricId+'_'+tabkey])
        var message: AngularTreeMessageModel = {
          "fabric": this.commonService.getFabricNameByUrl(this.router.url),
          "treeName": [tabkey],
          "treeOperationType": TreeOperations.RemoveFilter,
          "treePayload": tabkey
        };
       this.commonService.sendDataFromAngularTreeToFabric.next(message);
      })
    }
    else
    this.entityFilterStoreQuery.deleteNodeByIds([this.commonService.capabilityFabricId+'_'+tab])
    var message: AngularTreeMessageModel = {
      "fabric": this.commonService.getFabricNameByUrl(this.router.url),
      "treeName": [tab],
      "treeOperationType": TreeOperations.RemoveFilter,
      "treePayload": TreeTypeNames.ALL
    };
   this.commonService.sendDataFromAngularTreeToFabric.next(message);
  }

  removeFilterFromDependentTabs(tab){
    var message: AngularTreeMessageModel = {
      "fabric": this.commonService.getFabricNameByUrl(this.router.url),
      "treeName": [tab],
      "treeOperationType": TreeOperations.RemoveFilter,
      "treePayload": TreeTypeNames.ALL
    };
   this.commonService.sendDataFromAngularTreeToFabric.next(message);
  }
  collapseExpand(){
    this.filterOptionsExpand = !this.filterOptionsExpand;
  }


  Enablefilter(eve?) {
    try {
      this.leftTab = this.commonService.treeIndexLabel;
      this.openfilter = true;
      this.commonService.updateEntityFilterStatus(true);
      //this.entityFilterEmmitter.emit(this.openfilter);
      if(this.el.nativeElement.firstElementChild != null && this.el.nativeElement.lastElementChild !=null){
        this.filterOptionsExpand = false;
      } else{
        this.filterOptionsExpand = true;
      }
      var message: AngularTreeMessageModel = {
        "fabric": this.commonService.getFabricNameByUrl(this.router.url),
        "treeName": [this.commonService.treeIndexLabel],
        "treeOperationType": TreeOperations.filteroptionjson,
        "treePayload": ""
      };
      if (this.openfilter) {
        this.payload = [];
        this.commonService.sendDataFromAngularTreeToFabric.next(message);
      }
    }
    catch (e) {
      console.error('Exception in Enablefilter() of entity-filter.component  at EntityFilter. time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }
  getHeaderTop(){
    if(this.CustomerAppsettings.env.UseAsDesktop){
      return "73px"
    } else{
      return "42px"
    }
   }
   getBodyTop(){
     if(this.CustomerAppsettings.env.UseAsDesktop){
       return "100px";
     } else{
       return "75px";
     }
   }
   getCurrentFilterType(type){
     try{

      this.currentObjectKey=FILTERENTITYTYPES;
       
      this.openEntityTypeSelection=true;
       if (type && type.actualName){
        this.currentItemForApplyFilter=type.actualName;
       
       }
     }catch(e){
      console.error('Exception in getCurrentFilterType() of entity-filter.component  at EntityFilter. time ' + new Date().toString() + '. Exception is : ' + e);
     }

   }
   removeFromFilterOptions(type,accordionKey,Innerkey?,objectKey?,Value?){
     try{
      var objectSample={};
      objectSample[Value]=objectKey;
     switch(type){
      case "Remove":
      if(accordionKey==STATUS){
       this.removeObjectFromStatusList(accordionKey,Innerkey,objectSample);
      }else if(accordionKey==OPERATIONALSTATUS){
       this.removeObjectFromOperationalStatusList(accordionKey,Innerkey,objectSample);
      }   
      break;
     //  case "save":
     //  break;
     //  case 'toggle':
        // break;
    default:break;
     }
     }catch(e){
      console.error('Exception in removeFromFilterOptions() of entity-filter.component  at EntityFilter. time ' + new Date().toString() + '. Exception is : ' + e);

     }

   }

  removeObjectFromStatusList(accordionKey,Innerkey,objectSample){  
    try{
      if( this.selectedStatus[Innerkey] && Object.keys(this.selectedStatus[Innerkey]).length!=0){
        Object.keys(this.selectedStatus[Innerkey]).forEach(element=>{
           if(objectSample.hasOwnProperty(element) && JSON.stringify(objectSample[element]) === JSON.stringify(this.selectedStatus[Innerkey][element])){
                 delete this.selectedStatus[Innerkey][element];
                 this.filterData[accordionKey][Innerkey][element].value=false;
                 this.AllStatuses=false;
           }
        })
      }
    } catch(e){
      console.error('Exception in removeObjectFromStatusList() of entity-filter.component  at EntityFilter. time ' + new Date().toString() + '. Exception is : ' + e);

    }   

  }
  removeObjectFromOperationalStatusList(accordionKey,Innerkey,objectSample){  
    try{
      if( this.selectedOperatedStatus[Innerkey] && Object.keys(this.selectedOperatedStatus[Innerkey]).length!=0){
        Object.keys(this.selectedOperatedStatus[Innerkey]).forEach(element=>{
           if(objectSample.hasOwnProperty(element) && JSON.stringify(objectSample[element]) === JSON.stringify(this.selectedOperatedStatus[Innerkey][element])){
                 delete this.selectedOperatedStatus[Innerkey][element];
                 this.filterData[accordionKey][Innerkey][element].value=false;
                 this.AllOperationalStatuses=false;
                 
           }
        })
      }
    } catch(e){
      console.error('Exception in removeObjectFromOperationalStatusList() of entity-filter.component  at EntityFilter. time ' + new Date().toString() + '. Exception is : ' + e);

    }   

  }
  toggleFilterForStatuses(eventType,AccordionKey,InnerKey,Item,ItemValue,event){
    try{
      if(AccordionKey==STATUS){
        this.statusSelection(InnerKey,Item,ItemValue,event);
     }else{
      this.operationalStatusSelection(InnerKey,Item,ItemValue,event);
     }
    }catch(e){
      console.error('Exception in toggleFilterForStatuses() of entity-filter.component  at EntityFilter. time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }
  statusSelection(InnerKey,Item,ItemValue,event){
    try{
      if(event && event.checked){
        this.selectedStatus[InnerKey][Item]=ItemValue[Item];
        this.selectedStatus[InnerKey][Item]["value"]=event.checked;
      }
      else{
        this.AllStatuses =false;
        delete this.selectedStatus[InnerKey][Item];
      }
    }catch(e){
      console.error('Exception in operationalStatusSelection() of entity-filter.component  at EntityFilter. time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  operationalStatusSelection(InnerKey,Item,ItemValue,event){
    try{
      if(event && event.checked){
        this.selectedOperatedStatus[InnerKey][Item]=ItemValue[Item];
        this.selectedOperatedStatus[InnerKey][Item]["value"]=event.checked;
      }
      else{
        this.AllOperationalStatuses=false;
        delete this.selectedOperatedStatus[InnerKey][Item];
      }
    }catch(e){
      console.error('Exception in operationalStatusSelection() of entity-filter.component  at EntityFilter. time ' + new Date().toString() + '. Exception is : ' + e);

}  
  }

  ShowstatusBasedOnSelectAll(event,AccordionKey) {
    try{
      Object.keys(this.filterData[AccordionKey]).forEach(key => {
        if (this.filterData[AccordionKey].hasOwnProperty(key)) {
          Object.keys(this.filterData[AccordionKey][key]).forEach((element) => {
            if (this.filterData[AccordionKey][key].hasOwnProperty(element)) {
              this.filterData[AccordionKey][key][element].value = event;
            }
            if(event){
              this.selectedStatus[key][element]=this.filterData[AccordionKey][key][element];
              this.selectedStatus[key][element]["value"]=event;
            }else{
              this.AllStatuses =false; 
              delete this.selectedStatus[key][element];
            }
          })
        }
      })
    }catch(e){
      console.error('Exception in ShowstatusBasedOnSelectAll() of entity-filter.component  at EntityFilter. time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }
  ShowOperationalstatusBasedOnSelectAll(event,AccordionKey) {
    try{
      Object.keys(this.filterData[AccordionKey]).forEach(key => {
        if (this.filterData[AccordionKey].hasOwnProperty(key)) {
          Object.keys(this.filterData[AccordionKey][key]).forEach((element) => {
            if (this.filterData[AccordionKey][key].hasOwnProperty(element)) {
              this.filterData[AccordionKey][key][element].value = event;
            }
            if(event){
              this.selectedOperatedStatus[key][element]=this.filterData[AccordionKey][key][element];
              this.selectedOperatedStatus[key][element]["value"]=event;
            }else{
              this.AllOperationalStatuses=false;
              delete this.selectedOperatedStatus[key][element];
            }
          })
        }
      })
    }catch(e){
      console.error('Exception in ShowOperationalstatusBasedOnSelectAll() of entity-filter.component  at EntityFilter. time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }
  getAddToFilterKeys(obj){
    return Object.keys(obj);
   }
   facilityCodeOperationalSelection(event,item){
    if(event){
      this.selectedFacilityOperationalStatus[item]=event
    }
    else{
      delete this.selectedFacilityOperationalStatus[item]
    }
  }
  getOperationalStatus(){
    this.SelectAllStatus=true;
    for (const key in this.filterData[OPERATIONALSTATUS]) {
      if (this.filterData[OPERATIONALSTATUS].hasOwnProperty(key)) {
        const element = this.filterData[OPERATIONALSTATUS][key];
        if(!element){
          this.SelectAllStatus=false;
        }
      }
    }
    return true
  }

  facilityCodeOperationalSelectAll(event){
      for (const key in this.filterData[OPERATIONALSTATUS]) {
        if (this.filterData[OPERATIONALSTATUS].hasOwnProperty(key)) {
          const element = this.filterData[OPERATIONALSTATUS][key];
          this.filterData[OPERATIONALSTATUS][key]=event;
          this.facilityCodeOperationalSelection(event,key)
        }
      }
  }
  
compUntilDestroyed():any {
  return takeUntil(this.takeUntilDestroyObservables);
  }
  ngOnDestroy() {
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }

}

