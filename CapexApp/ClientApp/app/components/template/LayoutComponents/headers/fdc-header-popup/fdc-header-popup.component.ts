import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { CommonService } from '../../../../globals/CommonService';
/** FDCGlobalPopup component*/

@Component({
    selector: 'app-fdc-header-popup',
    templateUrl: './fdc-header-popup.component.html',
    styleUrls: ['./fdc-header-popup.component.scss']
})


export class FdcHeaderPopupTsComponent {
  open: boolean = false;
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any, public CommonService: CommonService) {
    //this.CommonService.status$$.subscribe((open: boolean) => this.open = open);
  }

  close(): void {
    this.CommonService.loadingBarAndSnackbarStatus("","");
    this.CommonService.sBar.dismiss();
  }

}
