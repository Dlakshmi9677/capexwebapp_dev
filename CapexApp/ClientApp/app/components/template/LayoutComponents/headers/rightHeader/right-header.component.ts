import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from "rxjs/operators";
// import { GisMapSliderComponent } from '../../../../common/GisMapComponent/components/slider/gis-slider.component';
import { CommonService } from '../../../../globals/CommonService';
import { FabricsNames } from '../../../../globals/Model/CommonModel';
@Component({
  selector: 'right-side-header',
  templateUrl: './right-header.component.html',
  styleUrls: ['./right-header.component.styles.scss']
  // ,
  // providers: [GisMapSliderComponent]
})
export class SideRightHeaderComponent implements OnInit, OnDestroy {
   

  isSubHeaderEnable = false;
  queryParams: any;
  /**
   * dialogRef
   */
  dialogRef : any;
  takeUntilDestroyObservables=new Subject();
  constructor(public commonService: CommonService, public dialog: MatDialog, private route: ActivatedRoute, private router: Router) {
    this.commonService.enablerightTreeTab.pipe(this.compUntilDestroyed()).subscribe((headNode) => {
      this.headerClickEvent(headNode);
    });
  }

  ngOnInit(): void {
    this.route.queryParams.pipe(this.compUntilDestroyed()).subscribe((params) => {
      this.queryParams = JSON.parse(JSON.stringify(params));
    });
  }
  
  headerClickEvent(head: any) {
    try{
      if (head && head != null) {
        let headObj = this.commonService.getRightClickEventModelObject(head);
        if (this.commonService.getFabricNameByUrl(this.router.url) == FabricsNames.SECURITY) {
          headObj.capability = FabricsNames.SECURITY;
          let currenttab = this.route && this.route.snapshot ? this.route.snapshot.queryParamMap.get('tab') :null;
          if (!currenttab) {
            return;
          }
        }
        this.commonService.rightTreeClick.next(headObj);
      }
      
      // this._gisMapSliderComponent.resetSlider();
    }
    catch (e) {
      console.error('Exception in onClick() of side-header.component  at side-Header time ' + new Date().toString() + '. Exception is : ' + e);
    }
      
  }
  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables);
  }
  ngOnDestroy() {
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }
 
}
