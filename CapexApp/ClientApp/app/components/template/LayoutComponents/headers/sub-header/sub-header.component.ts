import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatMenuTrigger } from '@angular/material/menu';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { PopUpAlertComponent } from 'ClientApp/app/components/common/PopUpAlert/popup-alert.component';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { PopupOperation } from '../../../../../components/globals/Model/AlertConfig';
import { CANT_ACCESS_CAPABILITY } from '../../../../common/Constants/AttentionPopupMessage';
import { CommonService } from '../../../../globals/CommonService';
import { FABRICS } from '../../../../globals/Model/CommonModel';
@Component({
  selector: 'sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.scss']
})

export class SubHeaderComponent implements OnInit, OnDestroy {
  dialogRef: any;
  isdesktop: boolean = true;
  contextMenuPosition = { x: '0px', y: '0px' };
  pinAndUnpin: string = 'Pin To Favorites';
  @ViewChild(MatMenuTrigger, { static: false }) contextMenu: MatMenuTrigger;
    takeUntilDestroyObservables = new Subject();
  constructor(public route: ActivatedRoute, public router: Router, public dialog: MatDialog, public commonService: CommonService) {
    if (this.commonService.CustomerAppsettings.env.UseAsDesktop) {
      //to make open in new tab disable
      this.isdesktop = false;
    }
  }

  ngOnInit(): void {
      this.router.events.pipe(filter(event => event instanceof NavigationEnd)).pipe(this.compUntilDestroyed()).subscribe((event:any) => {
      try {
          let url = event.url;
          let fabricName = this.commonService.getFabricNameByUrl(url);
          this.commonService.changeFavIcon(fabricName);
          let data = this.commonService.appConfig.AllHeaderRoutingList.filter((list) => { if (list.Fabric == fabricName) return list });
          if (data.length == 1 && this.commonService.isSubHeaderClick)
            this.onRouterchange(data[0]);
      }
      catch (e) {
        console.error(e);
      }
    })
  }

  onContextMenu(event: any, list) {
    if(list&& list.offlineStatus && this.commonService.CustomerAppsettings.env.UseAsDesktop && !this.commonService.onlineOffline){
      return;
    }
    let isFavList = false;
    this.commonService.faviconList.forEach((items) => { if (items.Fabric == list.Fabric) isFavList = true; });
    if (isFavList)
      this.pinAndUnpin = 'Unpin From Favorites';
    else
      this.pinAndUnpin = 'Pin To Favorites';
    event.preventDefault();
    this.contextMenuPosition.x = event.currentTarget.offsetLeft + 'px';
    if (this.commonService.CustomerAppsettings.env.UseAsDesktop) {
      this.contextMenuPosition.y = 110 + 'px';
    } else {
      this.contextMenuPosition.y = 84 + 'px';
    }

    this.contextMenu.menuData = list;
    this.contextMenu.openMenu();
  }

  onContextMenuPin() {
    this.setFavHeader(this.contextMenu.menuData);
  }

  onContextMenuOpenNew() {
    let list = this.contextMenu.menuData;
    if (list['routerLink'] && list["Fabric"] == 'Security') {
      let link = list['routerLink'].substr(1, list['routerLink'].length) + '?leftnav=' + list.queryParameter.leftnav
      window.open(this.commonService.baseUrl + link, '_blank');
    }
    else if (list['routerLink'] && list["Fabric"] != 'Home') {
      let link = list['routerLink'].substr(1, list['routerLink'].length) + '?tab=' + list.queryParameter.tab + '&leftnav=' + list.queryParameter.leftnav
      window.open(this.commonService.baseUrl + link, '_blank');
    }
    else {
      let link = list['routerLink'].substr(1, list['routerLink'].length);
      window.open(this.commonService.baseUrl + link, '_blank');
    }
  }

  /**
   * Method Used to assign the  current active capability id
   * @param data
   */
  assignCapabilityId(data) {
    this.commonService.capabilityFabricId = data.entityId;
  }

  onRouterchange(data) {
    try {
      //update userprefrence in indexeddb
      console.log("onRouterchange,sub-header.component," + data.id);
      this.commonService.updateIndexDBUserPrefrence(data.id);
      // this.commonService.formLoader = false;
      this.commonService.isSubHeaderClick = false;
      // this.commonService.isListReadRequest = false;
      if (data.id && data.id == 'home')
        this.commonService.showtoggle = false;
      else
        this.commonService.showtoggle = true;
      // this.commonService.clearText = '';
      //this.commonService.isLeftHeaderActive = true;
      //this.authorizationService.securityGroupsData = [];
      this.commonService.isContextMenuHeader = false;
      this.commonService.capabilityFabricId = data.entityId;
      //this.titleService.setTitle(data.title);
      //this.authorizationService.securityAdminEnabledFabrics = [];

      this.commonService.appConfig.AllHeaderRoutingList.forEach((head: any) => {
        if (head.id == data.id) {
          head.routerLinkActive = true;
        }
        else
          head.routerLinkActive = false;
        this.commonService.isclickoncreatelist = false;
      });
      this.commonService.currentRoutingSource = data.sourceSub;
    } catch (ex) {
      console.error('Exception in onRouterchange() of sub-header.component  at sub-Header time ' + new Date().toString() + '. Exception is : ' + ex);
    }
  }

  onClick(data) {
    try {
      var switchTab = true;
      
   
        data = this.commonService.ResetheaderData(data);
        this.commonService.switchButton = data.Fabric;
        if (switchTab) {
          this.commonService.menuFavIconsHide.next();
          this.commonService.routeToFabrics(data)
          this.commonService.isSubHeaderClick = true;
          if (data.id && data.id == 'menu') {
            this.commonService.isSubHeaderEnable = !this.commonService.isSubHeaderEnable;
          }
          setTimeout(() => {
            this.commonService.isSubHeaderEnable = false;
            this.commonService.subHeaderDisable = false;
            // if(data.id!='home')
            //this.commonService.isCreateDestroyLeftTree=true;
          }, 0);
          this.commonService.popupformclose = false;
        }
    }
    catch (e) {
      console.error('Exception in onClick() of sub-header.component  at sub-Header time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }

  setFavHeader(data: any) {
    try {
      if (data.isFavIcon == false) {
        let isExistFav = false;
        this.commonService.faviconList.forEach((list: any) => {
          if (list.Fabric == data.Fabric)
            isExistFav = true;
        });
        if (!isExistFav) {
          data.isFavIcon = true;
          var favIconObj = {"Fabric":data.Fabric,"entityId":data.entityId};
          //this.commonService.faviconList.push(favIconObj);
          this.commonService.faviconList = Object.assign([], this.commonService.faviconList);
          this.commonService.faviconList.push(favIconObj);
          let faviconList = this.commonService.faviconList;
          this.commonService.updateIndexDBUserPrefrence(null, null, faviconList);
        }
      } else { this.unSetFavHeaderIcon(data); }
    }
    catch (e) {
      console.error('Exception in setFavHeader() of sub-header.component  at sub-Header time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  routeToFabrics(data) {
    let mutualFabric = [];

    this.assignCapabilityId(data);
    //this.authorizationService.securityGroupsData = [];
    this.commonService.isContextMenuHeader = false;
   // this.authorizationService.securityAdminEnabledFabrics = [];
    this.commonService.capabilityFabricId = data.entityId;

    let url: string = this.commonService.getPreviousUrl();
    let entityId;
    url.split(';').forEach(data => {
      if (data.includes('EntityID')) {
        entityId = data.split('=')[1];
      }
    })
    if (url.includes('EntityID') && mutualFabric.includes(data.baseRoute)) {
      this.RouteBasedOnPreviousUrl(url, data, entityId)
    }
    else {
      this.router.navigate([data.routerLink], { queryParams: data.queryParameter })
    }
  }

  RouteBasedOnPreviousUrl(url, data, entityId) {

    let Hirerchy = ['District', 'Area', 'Field']
    let message = this.commonService.getMessage();
    message.Payload = JSON.stringify({ CapabilityId: this.commonService.capabilityFabricId, LocationId: entityId });
    message.EntityID = "EntityData";
    message.MessageKind = "READ";
    this.commonService.getEntityDataFromAuthorization(JSON.stringify(message)).pipe(this.compUntilDestroyed()).subscribe(res => {
      let EntityType;
      if (res != null && res['EntityDetails'] && res['EntityDetails'].length > 0 && res['EntityDetails'][0]['EntityType']) {
        EntityType = res['EntityDetails'][0]['EntityType'];
      }
      if (!Hirerchy.includes(EntityType) && EntityType) {
        var fabricname = url.split('/')[1];
        url = url.replace('&tab=List&', '&tab=All&');

        url = url.replace(fabricname, data.baseRoute);
        this.router.navigateByUrl(url)
      }
      else {
        this.router.navigate([data.routerLink], { queryParams: data.queryParameter })
      }
    })
  }



  CloseSubHeader() {
    this.commonService.isSubHeaderEnable = false;
    this.commonService.subHeaderDisable = false;
  }


  unSetFavHeaderIcon(data) {
    try {
      let lists = [];
      this.commonService.faviconList.forEach((list: any) => {
        if (list.Fabric != data.Fabric)
          lists.push(list);
      });
      this.commonService.faviconList = lists;
      let faviconList = this.commonService.faviconList;
      this.commonService.updateIndexDBUserPrefrence(null, null, faviconList);
      this.commonService.appConfig.AllHeaderRoutingList.forEach((list) => {
        if (data.Fabric == list.Fabric) {
          list.isFavIcon = false;
        }
      });
    }
    catch (e) {
      console.error('Exception in unSetFavHeaderIcon() of sub-header.component  at sub-Header time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }
  openPopUpDialog(messageToshow, isSave, callingFrom?): void {
    try {
      let config = {
        header: 'Attention!',
        isSubmit: isSave,
        content: [messageToshow],
        subContent: [],
        operation: PopupOperation.PermissionAlertConfirm,
        callingFrom: callingFrom
      };
          let matConfig = new MatDialogConfig();
      matConfig.data = config;
      matConfig.width = '650px';
      matConfig.disableClose = true;
      this.dialogRef = this.dialog.open(PopUpAlertComponent, matConfig);
    }
    catch (e) {
      console.error("Error in openPopUpDialog of sub-header component",e);
    }
  }
  compUntilDestroyed():any {
    return takeUntil(this.takeUntilDestroyObservables);
    }

  ngOnDestroy() {
    console.log("SubHeaderComponent Destroyed.")
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }
}
