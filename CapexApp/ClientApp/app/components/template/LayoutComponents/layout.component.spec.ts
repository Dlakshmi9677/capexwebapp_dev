﻿import {
    async, inject, TestBed, ComponentFixture, fakeAsync, tick
} from '@angular/core/testing';
import {
    MockBackend,
    MockConnection
} from '@angular/http/testing';

import {
    XHRBackend, Response, ResponseOptions
} from '@angular/http';
import { HttpClientModule, HttpClient, HttpHandler } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';
import { CommonService } from '../../globals/CommonService';
import { LayoutComponent } from './layout.component';
import { Routes, RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { expand } from 'rxjs/operator/expand';
import { By } from '@angular/platform-browser';
import { MessagingService } from '../../globals/services/MessagingService';
import { TaskFabricService} from '../../fabrics/taskfabric/services/taskfabric.Service';
//mport { CollaborationService } from '../../fabrics/CollaborationFabric/services/CollaborationService';
import {ActivityCenterService  } from '../../fabrics/activity-center/services/activity-center.service';
import { AssetsService } from '../../fabrics/AssetsFabric/services/assets.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';

/**
 * please dont use expect(component).toBeFalsy();
 * because it wont allow to load our component in browser
 */

describe('LayoutComponent', () => {

    let component: LayoutComponent;
    let fixture: ComponentFixture<LayoutComponent>;
    let commonservice;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule, RouterTestingModule, FormsModule
            ],
            declarations: [LayoutComponent],
            providers: [
                CommonService, MessagingService, TaskFabricService, ActivityCenterService, AssetsService,
                { provide: XHRBackend, useClass: MockBackend }, { provide: 'BASE_URL', useValue: 'http://localhost' }
            ],
            schemas: [NO_ERRORS_SCHEMA]
        });
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LayoutComponent);
        component = fixture.componentInstance;
    });
    it('should create LayoutComponent', () => {
        expect(component).toBeTruthy();
    });
    it('checking Logout Method functionality', () => {
        component.LogOut();
    });

});
