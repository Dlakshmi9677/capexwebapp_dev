import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
// import { GisEventMessage } from '../../../../common/GisMapComponent/model/map.models';
// import { MapService } from '../../../../common/GisMapComponent/services/map.service';
import { CommonService } from '../../../../globals/CommonService';
import { Subject} from 'rxjs';
import {  takeUntil } from "rxjs/operators";

@Component({
  selector: 'form-gen',
  templateUrl: 'form-gen.component.html',
  styleUrls: ['./form-gen.component.styles.scss']
})

export class FormGenComponent implements OnInit, OnDestroy {

  @Input() colsdata: any;
  @Input() myForm: FormGroup;
  @Input() title: string;
  @Input() datas: any; // any = {"MAP PROPERTIES":{"Base":true},"LOCATIONS":{"GasWells":true,"Gas/OilWells":true,"GasDisposals":true,"GasInjections":true,"CO2Injections":true,"OilWells":true,"BitumenWells":true,"HeavyOilWells":true,"StreamInjections":true,"WaterSources":true,"WaterDisposals":true,"WaterInjections":true,"GasPlants":true,"GasBatteries":true,"TruckTerminals":true,"TreatingFacilities":true,"WaterFacilities":true,"OilBattries":true,"Pads":true,"Satelities":true,"Headers":true,"Raisers":true,"MeterSts":true,"BoosterSts":true,"CompressorSts":true,"InventorySts":true},"ALLOCATION PATHS":{"DeliveredFwd":true,"AllocatedBkwd":null},"MARKER STYLE":{"Color":null,"Size":null},"VISIBLE SCALE":{"Min":null,"Max":null,"Opacity":null}};
  @Output() getentitydata = new EventEmitter<String>();
  initilize: boolean = false;
  autoFilter: any;
  filteredOptions: any;
  takeUntilDestroyObservables = new Subject();

  constructor(public commonService: CommonService, public router: Router, public route: ActivatedRoute, private formBuilder: FormBuilder) {
  }

  setGroupControl() {
    try {
      this.colsdata.forEach(propertyName => {
        var group = this.formBuilder.control(null
        );
        // subscribing to all the properties for value change event
        group.valueChanges.pipe(this.compUntilDestroyed()).subscribe((value: any) => {
          try {
            var rightSideBarTitleArray = ["LOCATIONS","MAP PROPERTIES","GRID LAYOUT","PRODUCTION"];
            /**
            * Layer Event for Gis Map Component
            */
           var toggleProperty = {
            EntityType: propertyName.entityName,
            Value: value,
            title: this.title
          };
           switch(propertyName.entityName){
             case "ToggleAll":
               Object.keys(this.myForm.controls).forEach(propertyName =>{
                  if(propertyName != toggleProperty.EntityType){
                    this.myForm.controls[propertyName].setValue(toggleProperty.Value);
                  }
               });
               break;
           }
            // if (rightSideBarTitleArray.includes(this.title)) {

            //   var toggleProperty = {
            //     EntityType: propertyName.entityName,
            //     Value: value,
            //     title: this.title
            //   };
            //   var msgData: GisEventMessage = {
            //     messageType: "RightSidebarMapMessage",
            //     payload: {
            //       EntityType: propertyName.entityName,
            //       visible: value,
            //       visibility: 'visibility',
            //       title: this.title
            //     }
            //   }
            //   if (this.title == "MAP PROPERTIES") {
            //     if (propertyName.option.indexOf(value) != -1) {
            //       setTimeout(() => {
            //         this.mapService.sendDataToGisMap.next(msgData);
            //       }, 1000);
            //     }
            //   }
            //   else {
            //     setTimeout(() => {
            //       this.mapService.sendDataToGisMap.next(msgData);
            //     }, 1000);
            //   }
            // }
          }
          catch (e) {
            console.error('Exception in setGroupControl() of valueChanges subscriber of form-gen.component  at list-form-generate time ' + new Date().toString() + '. Exception is : ' + e);
          }

        })

        if (this.datas && this.datas[propertyName.entityName])
          group.setValue(this.datas[propertyName.entityName]);
        return this.myForm.addControl(propertyName.entityName, group);

      });
    }
    catch (e) {
      console.error('Exception in setGroupControl() of form-gen.component  at list-form-generate time ' + new Date().toString() + '. Exception is : ' + e);
    }

  }


  ngOnInit() {
    try {
      this.setGroupControl();
      this.initilize = true;
    }
    catch (e) {
      console.error('Exception in ngOnInit() of form-gen.component  at list-form-generate time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  keyPress(event: any) {
    try {
      const pattern = /[0-9\+\-\ ]/;

      let inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode != 8 && !pattern.test(inputChar)) {
        event.preventDefault();
      }
    }
    catch (e) {
      console.error('Exception in keyPress() of form-gen.component  at list-form-generate time ' + new Date().toString() + '. Exception is : ' + e);
    }
  }

  compUntilDestroyed():any {
    return takeUntil(this.takeUntilDestroyObservables);
    }
  ngOnDestroy(): void {
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }

  getselectedentitydata(data, event) {
    this.getentitydata.emit(data);
  }

  /**
   * NG-Select Options select and Highlight
   * @param $event 
   */
  onOptionClick($event) {
    if ($event && $event.target && $event.target.select) {
      $event.target.select();
    }
  }
  // filter(filterValue: any, lists: any, name: string) {
  //   try {
  //     var temp = lists.filter(option => option.toLowerCase().includes(filterValue.toLowerCase()));
  //     if (temp.length == 0) {
  //       filterValue = filterValue.slice(0, filterValue.length - 1);
  //       this.myForm.controls[name].setValue(filterValue);
  //     }
  //     this.autoFilter = temp;
  //     return temp;
  //   }
  //   catch (e) {
  //     console.error('Exception in filter() of form-gen.component  at list-form-generate time ' + new Date().toString() + '. Exception is : ' + e);
  //   }

  // }

  // autoCompleteChange(label, index) {
  //   try {
  //     let values = this.myForm.controls[label].value;
  //     let list = this.colsdata[index]['option'];
  //     if (list)
  //       this.filteredOptions = this.myForm.controls[label].valueChanges.pipe(startWith(''), map(val => { if (val == '') { val = values }; return this.filter(val, list, label) }));
  //   }
  //   catch (e) {
  //     console.error('Exception in autoCompleteChange() of form-gen.component  at list-form-generate time ' + new Date().toString() + '. Exception is : ' + e);
  //   }

  // }

  // autoCompleteFocusOut(label, index) {
  //   try {
  //     let values = this.myForm.controls[label].value;
  //     let list = this.colsdata[index]['option'];
  //     if (list && list.indexOf(values) == -1)
  //       this.myForm.controls[label].setValue('');
  //     this.filteredOptions = new Observable<string[]>();
  //   }
  //   catch (e) {
  //     console.error('Exception in autoCompleteFocusOut() of form-gen.component  at list-form-generate time ' + new Date().toString() + '. Exception is : ' + e);
  //   }

  // }
}
