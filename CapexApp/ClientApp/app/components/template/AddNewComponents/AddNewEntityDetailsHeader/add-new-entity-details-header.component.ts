// AoT compilation doesn't support 'require'.
//external imports
import { ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { UsersStateQuery } from '../../../../../webWorker/app-workers/commonstore/user/user.state.query';
import { AppConsts } from '../../../common/Constants/AppConsts';
import { PeopleActivitiesService } from '../../../fabrics/PeopleFabric/services/people-activities-service';
import { CommonService } from '../../../globals/CommonService';
//internal imports
import { FABRICS, FabricsNames } from '../../../globals/Model/CommonModel';



@Component({
  selector: 'add-new-entity-details-header',
  templateUrl: './add-new-entity-details-header.component.html',
  styleUrls: ['./add-new-entity-details-header.component.styles.scss'],
})

export class AddNewEntityDetailsHeaderComponent implements OnDestroy, OnInit {

  /*Global Variables Declaration Section */
  @Output() commonEmitter = new EventEmitter();
  @Input() myForm: FormGroup;
  @Input() schema: any;
  @Input() data: any;
  @Input() properties: any;
  @Input() EntityType;

  NewProperties = {};
  keys = [];
  Lists = [];
  EntityTypesOptions = [];
  formGroupControl;
  listOfValidateEntityType: any = [];
  fabricName;
  roleSchema: any;
  takeUntilDestroyObservables = new Subject();

  constructor(public usersStateQuery: UsersStateQuery, public commonService: CommonService, public router: Router, public formBuilder: FormBuilder,
    public PeopleService: PeopleActivitiesService, public cdRef: ChangeDetectorRef, public dialog: MatDialog, public peopleservice: PeopleActivitiesService,
    public route: ActivatedRoute) {
  }

  ngOnInit() {
    try {
      this.commonService.layoutJSON.RightHeader = false;
      this.commonService.isCreateNewEntity = true;
      this.router.events.filter((event: any) => event && event instanceof NavigationEnd).pipe(this.compUntilDestroyed()).subscribe((event: any) => {
        try {
          let url = event.url;
          this.fabricName = this.commonService.getFabricNameByUrl(url);
          if (this.fabricName == FabricsNames.ENTITYMANAGEMENT) {
            this.fabricName = this.commonService.getFabricNameByTab(this.route.queryParams['_value']['tab']);
          }
        }
        catch (e) {
          this.commonService.appLogException(new Error('Exception in  ngOnInit() of router.events.subscriber of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e));
        }
      });

      this.route.queryParams.pipe(this.compUntilDestroyed()).subscribe((res: any) => {
        try {
          let url = this.router.url;
          this.fabricName = this.commonService.getFabricNameByUrl(url);
          if (this.fabricName == FabricsNames.ENTITYMANAGEMENT) {
            this.fabricName = this.commonService.getFabricNameByTab(res.tab);
          }
        }
        catch (e) {
          this.commonService.appLogException(new Error('Exception in  ngOnInit() of router.events.subscriber of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e));
        }
      });

      this.formGroupControl = this.formBuilder.group({});

      this.commonService.readAddNewSchema(this.fabricName).pipe(this.compUntilDestroyed()).filter((d: any) => d).subscribe(res => {
        this.addNewEntityReceiveBackend(res);
      })
    }
    catch (e) {
      console.error('Exception in ngOnInit() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
      this.commonService.appLogException(new Error('Exception in  ngOnInit() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  addNewEntityReceiveBackend(res) {
    try {
      this.controlsEnablesAndReset();
      this.addNewEntitySchemaSubscription(res);
    } catch (e) { }
  }

  controlsEnablesAndReset() {
    try {
      if (this.keys) {
        this.keys.forEach(propertyName => {
          this.myForm.controls[propertyName].enable({ onlySelf: false, emitEvent: false })
          this.myForm.reset();
        });
      }
    } catch (e) {
      console.error('Exception in controlsEnablesAndReset() of create-entity.component at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  addNewEntitySchemaSubscription(Schema) {
    try {
      this.keys = [];
      this.Lists = [];
      let RoleSchema;
      if (Schema["RoleSchema"]) {
        RoleSchema = JSON.parse(Schema["RoleSchema"]);
        this.listOfValidateEntityType = [];
      }
      if (this.fabricName == FABRICS.PEOPLEANDCOMPANIES) {
        this.NewProperties = Schema;
      } else {
        this.NewProperties = JSON.parse(Schema["Schema"]);
      }
      this.formGroupControl = this.formBuilder.group({});


      if (this.fabricName == FABRICS.PEOPLEANDCOMPANIES) {
         this.NewProperties['EntityType']['options'].forEach((element,index) => {
           if (element.Value == 'Group') {
        //     element.Value = 'Office'
        //     element.EntityName = 'Office'
           this.NewProperties['EntityType']['options'].splice(index, 1);
           }
         });
        this.EntityTypesOptions = this.NewProperties['EntityType']['options'];
        delete this.NewProperties["Parent"];
        // if (this.PeopleService.contextmenuFlag) {
        //   if (this.commonService.PeopleParentBinding.type == "Company")
        //     this.NewProperties['EntityType']['options'] = this.EntityTypesOptions.filter(option => option.Value != this.commonService.PeopleParentBinding.type);
        //   if (this.commonService.PeopleParentBinding.type == "Group")
        //     this.NewProperties['EntityType']['options'] = this.EntityTypesOptions.filter(option => option.Value != this.commonService.PeopleParentBinding.type && option.Value != "Company");
        // }
      }
      delete this.NewProperties["EntityName"];
       delete this.NewProperties["roles"];
      this.keys = Object.keys(this.NewProperties);
      this.Lists = [];
      this.keys.forEach((res) => { this.Lists.push(this.NewProperties[res]); });
      // if (this.commonService.createbyContextmenu == true) {
      //   this.fDCService.contextmenuFlag = true;
      // }
      this.createFormGroups();
      this.cdRef.detectChanges();
    } catch (e) {
      console.error('Exception in addNewEntitySchemaSubscription() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
      this.commonService.appLogException(new Error('Exception in  addNewEntitySchemaSubscription() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  createFormGroups() {
    try {
      this.keys.forEach(propertyName => {
        var value = null;
        var control = this.myForm.get(propertyName);
        if (control) {
          value = control.value;
        }
        if (this.fabricName == FABRICS.PEOPLEANDCOMPANIES) {
          var group = this.formBuilder.control(value, Validators.required);
        }
        else {
          if (propertyName != "Parent")
            var group = this.formBuilder.control(value, Validators.required);
          else
            var group = this.formBuilder.control(value, null);
        }
        // subscribing to all the properties for value change event     
        if (this.myForm.get(propertyName)) {
          this.myForm.setControl(propertyName, group);
        } else {
          this.myForm.addControl(propertyName, group);
        }
        this.valueChangesSubsCription(group, propertyName)
      });

    }
    catch (e) {
      console.error('Exception in createFormGroups() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
      this.commonService.appLogException(new Error('Exception in  createFormGroups() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  valueChangesSubsCription(group, propertyName) {
    group.valueChanges.pipe(debounceTime(500)).pipe(distinctUntilChanged()).subscribe(propertyVal => {
      try {
        if (propertyVal) {
          this.createControlAndChangesSubscription(propertyName, propertyVal);
          if (this.myForm.valid) {
            this.commonEmitter.emit();
          } else {
            this.commonEmitter.emit("RemoveChild");
          }
          this.cdRef.detectChanges();
        } else if (propertyName == "CategoryType" && propertyVal == null) {
          if (this.myForm && this.myForm.controls && this.myForm.controls["Parent"]) {
            this.myForm.controls["Parent"].clearValidators();
            this.myForm.controls["Parent"].updateValueAndValidity({ emitEvent: false });
          }

        }
      }
      catch (e) {
        console.error('Exception in createFormGroups() of ValuesChanges subscriber of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
        this.commonService.appLogException(new Error('Exception in createFormGroups() of ValuesChanges subscriber of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e));
      }
    })
  }

  createControlAndChangesSubscription(name, value) {
    try {
      // if (this.contextmenuData)
      //   this.fDCService.selectParent = true;
      // else
      //   this.fDCService.selectParent = false;
      switch (this.fabricName) {
        case FABRICS.PEOPLEANDCOMPANIES:
          if (name == 'EntityType') {
          }
          break;
      }
      console.log(name + "    value : ", value);
    }
    catch (e) {
      console.error('Exception in createControlAndChangesSubscription() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
      this.commonService.appLogException(new Error('Exception in  createControlAndChangesSubscription() of create-entity.component in create-entity-dialog at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  CloseForm() {
    try {
      this.commonService.switchButton = undefined;
      this.router.navigate(['../../../'], { relativeTo: this.route, queryParams: { 'tab': this.commonService.treeIndexLabel, 'leftnav': this.commonService.isLeftTreeEnable } });
    }
    catch (e) {
      console.error('Exception in CloseForm() of AddNewComponent at time ' + new Date().toString() + '. Exception is : ' + e);
      this.commonService.appLogException(new Error('Exception in CloseForm() of AdddNewComponent at time ' + new Date().toString() + '. Exception is : ' + e));
    }
  }

  ngOnDestroy() {
    this.commonService.enableTableModeButton();
    if (this.commonService.getFromFDCStateByKey("TabletModebuttonStatus")) {
      this.commonService.onTabletMode();
    }
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }

  onKeyUp(event, properties) {
    if (event.keyCode == 8) {
      if (properties.actualname == "CategoryType" && this.myForm.controls[properties.actualname].value == null) {
        this.myForm.controls['EntityType'].setValue(null, { emitEvent: true });
        this.myForm.controls['Parent'].setValue(null, { emitEvent: false });
        this.NewProperties['EntityType'].options = [];
        this.NewProperties['Parent'].options = [];
        // this.myForm.removeControl('Parent');
      }
      else if (properties.actualname == "EntityType" && this.myForm.controls[properties.actualname].value == null && this.myForm.controls['Parent']) {
        if (!this.commonService.createbyContextmenu) {
          this.myForm.controls['Parent'].setValue(null, { emitEvent: false });
          this.NewProperties['Parent'].options = [];
        }
      }
      // else if (properties.actualname == "Parent" && this.myForm.controls[properties.actualname].value == null) {
      //   if (this.myForm.controls['CategoryType'] && this.myForm.controls['CategoryType'].value.Value == "Non-Wells" || this.myForm.controls['CategoryType'] && this.myForm.controls['CategoryType'].value.Value == "Wells") {
      //     var hirerchydata = {
      //       "District": this.myForm.controls['Parent'].value,
      //       "DistrictId": this.myForm.controls['Parent'].value,
      //       "Area": this.myForm.controls['Parent'].value,
      //       "AreaId": this.myForm.controls['Parent'].value,
      //       "Field": this.myForm.controls['Parent'].value,
      //       "FieldId": this.myForm.controls['Parent'].value
      //     }
      //     this.fDCService.selectParent = false;
      //     var model = new FDCFormOperationModel()
      //     model.OperationType = 'ParentChangefor2ndForm'
      //     model.PayLoad = hirerchydata
      //     this.fDCService.FDCFormOperations.next(model)
      //   }
      // }
      if (!this.myForm.valid) {
        this.commonEmitter.emit("RemoveChild");
      }
    }
  }

  onOptionClick($event) {
    try {
      if ($event && $event.target && $event.target.select) {
        $event.target.select();
      }
    } catch (e) {
      console.error(e);
    }

  }

  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables);
  }
}
