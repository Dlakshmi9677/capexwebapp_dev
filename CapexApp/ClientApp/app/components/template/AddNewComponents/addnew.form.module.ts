import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AngularMaterialModule } from '../../../app.module';
import { SharedModule1 } from '../../shared/shared1.module';
import { AddnewRouting } from './addnew.form.routing';
import { AddNewEntityDeactivateGaurdService } from './services/add-new-entity-can-deactivate-guard.service';
@NgModule({
  imports: [CommonModule, AddnewRouting, SharedModule1,  AngularMaterialModule],
    declarations: [],
    providers: [AddNewEntityDeactivateGaurdService],
  exports: []
})

export class AddNewFormModule {
}
