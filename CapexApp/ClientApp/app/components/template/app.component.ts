import { Component, HostListener, OnDestroy } from '@angular/core';
// import { HttpHeaders, HttpParams } from "@angular/common/http";
import { MatIconRegistry } from "@angular/material/icon";
import { MatSnackBarConfig } from '@angular/material/snack-bar';
import { DomSanitizer } from '@angular/platform-browser';
import { CookieService } from 'ngx-cookie';
import { Observable } from 'rxjs';
import { takeUntil } from "rxjs/operators";
import { Subject } from 'rxjs';

import { UserStateConstantsKeys } from '../../../webWorker/app-workers/commonstore/user/user.state.model';
import { UsersStateQuery } from '../../../webWorker/app-workers/commonstore/user/user.state.query';
// import { BIFabricService } from '../fabrics/bifabric/services/BIFabricService';
import { CommonService } from '../globals/CommonService';
import { GlobalIcons } from '../globals/icons';
import { FABRICS } from '../globals/Model/CommonModel';
import { EntityTypes } from '../globals/Model/EntityTypes';
import { MessageType, Routing } from '../globals/Model/Message';
import { MessagingService } from '../globals/services/MessagingService';
import { AppConfig } from './../globals/services/app.config';
import { ThemeService } from './../globals/services/theme.service';


@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnDestroy {
  canIntialize = false;
  isDarkTheme: Observable<boolean>;
  AppsettingConfig = null;
  public baseUrl = AppConfig.BaseURL;
  public CustomerAppsettings = AppConfig.AppMainSettings;
  takeUntilDestroyObservables = new Subject();

  constructor(public appConfig: AppConfig, public usersStateQuery: UsersStateQuery, public commonService: CommonService, public messagingService: MessagingService, private matIconRegistry: MatIconRegistry, private themeService: ThemeService,public sanitizer: DomSanitizer, private cookieService: CookieService) {
    try {
      this.commonService.loadRouting();
      this.appConstructorInit();
      this.appConfig.getUserpreFrenceData.pipe(this.compUntilDestroyed()).subscribe(res => {
        this.getUserPrefrence();
      })
    }
    catch (e) {
      console.error(e);
    }
  }

  ngOnInit() {
    this.appNgOnInit();
  }

  ngAfterViewInit() {
    this.appNgAfterViewInit();
    this.registerIcon();
  }

  ngOnDestroy() {
    this.takeUntilDestroyObservables.next();
    this.takeUntilDestroyObservables.complete();
  }

  compUntilDestroyed(): any {
    return takeUntil(this.takeUntilDestroyObservables);
  }


  @HostListener("window:visibilitychange", ["$event"])
  onVisibilityChange($event) {
    const isVisible = $event.target.visibilityState === 'visible';
    if (isVisible) {
      this.commonService.LogoutFromAllOpenTabs();
    }
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHander(event) {
    try {
      var utc = Math.floor((new Date()).getTime() / 1000);
      let isLogOut = sessionStorage.getItem("logout");
      if (!isLogOut) {
        this.usersStateQuery.add(UserStateConstantsKeys.windowCloseTime, utc);
        if (this.commonService.lastOpenedFabric)
          this.setLastOpenedFabricInState();
      }

      var dateTime = new Date().toISOString().slice(0, 19).replace('T', ' ');
      let payload = {
        "Fabric": this.commonService.lastOpenedFabric,
        "UserPinnedTabs": this.commonService.faviconList,
        "Theme": this.commonService.themeDark,
        "Capabilities": this.appConfig.AllHeaderRouting,
        [UserStateConstantsKeys.windowCloseTime]: utc
      };
      var userPayload = { "email": this.commonService.username, "ModifiedDateTime": dateTime, "cacheName": "TenantDetails", "UpdatePref": payload };
      this.commonService.sendMessageToServer(JSON.stringify(userPayload), EntityTypes.UserPreference, this.commonService.currentUserName, EntityTypes.UserPreference, MessageType.UPDATE, Routing.AllFrontEndButOrigin, '', FABRICS.COMMON.toUpperCase())

      sessionStorage.clear();
      localStorage.clear();
    }
    catch (e) {
      console.error(e);
    }
  }

  setLastOpenedFabricInState() {
    var userPrefPayload = this.usersStateQuery.getStateKey(UserStateConstantsKeys.userPrefrence);
    var payload = {};
    Object.keys(userPrefPayload).forEach(key => {
      if (key == "Fabric")
        payload[key] = this.commonService.lastOpenedFabric;
      else
        payload[key] = userPrefPayload[key];
    })
    this.usersStateQuery.add(UserStateConstantsKeys.userPrefrence, payload);
  }

  appConstructorInit() {
    try {
      this.canIntialize = this.commonService.appcanIntialize;
      var devicename = null;
      var username = null;
      var password = null;
      this.commonService.currentUserId = this.appConfig.UserDetails["UserId"];
      this.commonService.tenantID = this.appConfig.UserDetails["TenantId"];
      this.commonService.tenantName = this.appConfig.tenantName;
      username = this.appConfig.userName;
      this.commonService.CurrentUserEntityName = username.split('@')[0];
      
      if (this.commonService.CustomerAppsettings != null) {
        var Appset = this.commonService.CustomerAppsettings;
        if (Appset.env.name === "DEV" && Appset.env.UseAsVScode == true) {

          password = Appset.env.password;
          devicename = Appset.env.devicename;

          alert("VSCode mode is ON." +
            "\ntenantName = " + AppConfig.AppMainSettings.env.companyName +
            "\nuserName = " + AppConfig.AppMainSettings.env.username +
            "\npassword = " + AppConfig.AppMainSettings.env.password);
        }
        else if (Appset.env.name === "DEV" && Appset.env.UseAsDesktop == true) {
          password = Appset.env.password;
          devicename = Appset.env.devicename;
        }
        else {
          devicename = this.cookieService.get("dname_cookie");
          password = this.cookieService.get("p_cookie");
        }
      }
      else {
        devicename = this.cookieService.get("dname_cookie");
        password = this.cookieService.get("p_cookie");
      }

      if (devicename == "InActive" || username == null) {
        document.getElementById("myProgress").style.display = "none";
        this.LogOut();
      }
      // this.commonService.password = password;
      if (devicename == "InActive" || username == null) {
        document.getElementById("myProgress").style.display = "none";
        this.LogOut();
      }

      // let headers = new HttpHeaders();
      // const requestOptions = {
      //   params: new HttpParams()
      // };
      // headers.append("Authorization", "Basic " + btoa(username + ":" + password));
      // this.BIFabricService.Reqoptions = requestOptions;//new RequestOptions();
      // this.BIFabricService.Reqoptions.headers = headers;

      this.isDarkTheme = this.themeService.isDarkTheme;

      this.matIconRegistry.addSvgIcon(`search`, `/images/AlThing UI Icon-Button Set v1/Header_Search_Icon_Grey_Idle.svg`);
      this.matIconRegistry.addSvgIcon(`funnelIcon`, `/images/Funnel-Sort/Funnel_Icon_Idle.svg`);
      this.matIconRegistry.addSvgIcon(`sortIcon`, `/images/Funnel-Sort/Sort_Icon_Idle.svg`);
      this.commonService.IsUserPreferrence = true;
      if (username) {
        this.commonService.username = username;
      } else {

      }
      this.commonService.currentUserName = username.split('@')[0];
      this.intializeApplication();
    }
    catch (e) {
      console.error(e);
      if (this.AppsettingConfig.env.name === "DEV" && this.AppsettingConfig.env.UseAsVScode == true) {
        alert("Please update config.dev.json file.");
      }
    }
  }

  appNgOnInit() {
    try {
      this.isDarkTheme = this.themeService.isDarkTheme;
    }
    catch (e) {
      console.error(e);
    }
  }

  appNgAfterViewInit() {
    try {
      let config = new MatSnackBarConfig();
      config.duration = 3000;
      config.verticalPosition = 'top';
      config.panelClass = ['mySnackbar'];
      if (document.getElementById("myProgress")) {
        document.getElementById("myProgress").style.display = "none";
      }
    }
    catch (e) {
      console.error(e);
    }
  }
 
  registerIcon() {
    try {
      var icons = GlobalIcons
      for (let element in icons) {
        this.matIconRegistry.addSvgIcon(
          element,
          this.sanitizer.bypassSecurityTrustResourceUrl(icons[element]));
      }
    }
    catch (e) {
      console.error('Exception in registerIcon() of app.component in template at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  intializeApplication() {
    try {
      this.commonService.appcanIntialize = true;
      
        this.messagingService.initializeWebSocketConnection().then(res => {
          setTimeout(() => {
            this.getUserPrefrence();
          }, 1000);
        }).catch(e => {
          console.error(e);
        });

      if (this.commonService.tenantID != undefined) {
        this.commonService.getUrl("ZoomData").pipe(this.compUntilDestroyed()).subscribe((url: any) => {
          this.commonService.zoomdataURl = url;
        });
      }
    }
    catch (e) {
      console.error('Exception in intializeApplication() of app.component in template at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  getUserPrefrence() {
    try {
      let userpreferanceCapability = {
        "Fabric": null,
        "UserPinnedTabs": null,
        "Theme": null,
        "Capabilities": null,
        "ZoomPercentage": this.commonService.ZoomPercentage
      };
      this.appConfig.AllHeaderRoutingListService.forEach((element: any) => {
        if (element.title)
          userpreferanceCapability[element.title] = null;
      })
      // this.commonService.CapabilityPermissionFabric = userpreferanceCapability;
      var userPayload = { "email": this.commonService.username, "cacheName": "TenantDetails", "Permission": userpreferanceCapability };
      this.commonService.sendMessageToServer(JSON.stringify(userPayload), EntityTypes.UserPreference, this.commonService.currentUserName, EntityTypes.UserPreference, MessageType.READ, 'OriginSession', '', FABRICS.COMMON.toUpperCase());
    }
    catch (e) {
      console.error('Exception in appConstructorInit() of app.component in template at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }

  LogOut() {
    try {
      this.cookieService.removeAll();
      localStorage.clear();
      sessionStorage.clear();
      if (window.location.origin) {
        window.location.replace(window.location.origin);
      }
    }
    catch (e) {
      console.error('Exception in LogOut() of app.component in template at time ' + new Date().toString() + '. Exception is : ' + e + new Error().stack);
    }
  }
}
