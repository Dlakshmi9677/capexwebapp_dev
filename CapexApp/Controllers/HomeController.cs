using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CapexApp.Models;
using CapexApp.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using CapexApp.commonconfig;
using Newtonsoft.Json;
using ICommonInterfaces;
using ICommonInterfaces.Model;
using Microsoft.ServiceFabric.Services.Remoting.Client;
using System.Fabric;
using Microsoft.ApplicationInsights;
using Microsoft.Azure.Management.DataLake.Store;
using Microsoft.Rest.Azure.Authentication;
using System.Text.RegularExpressions;
using Models.Ignite;
using CapexApp.Ignite;
using Microsoft.Extensions.Primitives;
using System.Collections.Generic;
using CapexApp.SqlUtil;
using ICommonInterfaces.KeyvaultService;

namespace CapexApp.Controllers
{
  public class HomeController : Controller
  {
    TelemetryClient tc = new TelemetryClient();
    private readonly IgniteClient igniteClient = new IgniteClient();

    private readonly string otpCacheName = "LoginSessions";

    private string token;
    public readonly StatelessServiceContext context;
    public readonly IAuthGuard remoteservice;
    public KeyvaultServiceConnectionStringApi keyvaultServiceConnectionStringApi;

    public static int MIN_LENGTH = 8;
    public static int MAX_LENGTH = 15;

    // ViewPages start
    public static string IndexViewPage = "Index";
    public static string UsernameViewPage = "UsernameAuthentication";
    public static string PasswordViewPage = "PasswordAuthentication";
    public static string MFAViewPage = "OTPAuthentication";
    public static string PasswordResetViewPage = "ChangePassword";
    public static string ResetpasswordMFAViewPage = "ChangePasswordOTPAuth";
    public static string HomeViewPage = "Home";
    public static string ApplicationSwitcherViewPage = "ApplicationSwitcher";
    // ViewPages end

    //@ViewBag.message start
    public static string unauthorized = "Unauthorized: Access is denied.";
    public static string fluctuationInNetwork = "Network change detected or fluctuation in the network. Please check your network.";
    public static string invalidLink = "Invalid Link.";
    public static string invalidAuthentication = "Invalid Authentication.";
    public static string subscriptionExpired = "Company subscription has expired.";
    public static string sessionExpired = "Your session has expired. Please, log-in again.";
    public static string userNotExist = "Could not find your Visur Account! \nIf you do not have a Visur Account, please contact your Administrator.";
    public static string passwordLink = "A password link was sent to your e-mail.";
    public static string passwordReseted = "Your password reset was successful, please login with your new password.";
    public static string mismatchPasswords = "Both passwords must match.";
    public static string lastPassword = "You can't reuse your last password.";
    public static string enterCompanyName = "Please enter the company name.";
    public static string enterPassword = "Please enter the password.";
    public static string enterVerificationCode = "Please enter the Verification Code.";
    public static string verificationCodeExpired = "Your Verification Code has been expired.";
    public static string verificationCodeSent = "Verification code has been sent.";
    public static string enterValidCompanyName = "Please enter a valid company name.";
    public static string enterValidUsername = "Please enter a valid email address.";
    public static string enterValidPassword = "Please enter a valid password.";
    public static string enterValidVerificationCode = "Please enter a valid Verification Code.";
    public static string somethingWrong = "Something went wrong. Please re-try again.";
    public static string loginFailed = "Login failed. Please try again.";
    public static string technicalIssue = "Some technical issue is there. Please try after a few minutes.";
    public static string containLowerCase = "Password should contain At least one lower case letter.";
    public static string containUpperCase = "Password should contain At least one upper case letter.";
    public static string passwordLength = "Password should not be less than " + MIN_LENGTH + " or greater than " + MAX_LENGTH + " characters.";
    public static string passwordValidationMessage = "Password must contain " + MIN_LENGTH + " characters minimum and " + MAX_LENGTH + " characters maximum." +
      "\nPassword must contain, one number, one special character and minimum one lower and upper case letter.";
    public static string containNumericValue = "Password should contain At least one numeric value.";
    public static string containSpecialChar = "Password should contain At least one special case characters.";
    public static string timeoutException = "Operation Timeout.";
    public static string nonSelectApplicationError = " Please select desired application";
    //@ViewBag.message End

    public static string ErrorMessage = string.Empty;
    private readonly static string ApplicationName = "ConstructionApplication ~ HomeController";

    public HomeController(StatelessServiceContext context)
    {
      this.context = context;
      try
      {
        remoteservice = ServiceProxy.Create<IAuthGuard>(new Uri("fabric:/AuthguardApp/AuthguardApi"), listenerName: "Authguard");
        keyvaultServiceConnectionStringApi = new KeyvaultServiceConnectionStringApi();
      }
      catch (Exception ex)
      {
        TcException(ex, "HomeController", " remoteservice not getting ServiceProxy value.");
      }
    }

    [HttpGet, Route("ApplicationStatus")]
    public async Task<String> ApplicationStatus()
    {
      return ApplicationName + "Running";
    }

    public async Task<IActionResult> Index()
    {
      string methodName = "Index()";

      try
      {
        //if (Request.Cookies["UnauthorizedPage"] == "404")
        //{
        //  RemoveCustomCookies("All");
        //  @ViewBag.message = unauthorized;
        //  return View(UsernameViewPage);
        //}

        //if (Request.Cookies["SessionExpired"] == "440")
        //{
        //  RemoveCustomCookies("All");
        //  @ViewBag.message = fluctuationInNetwork;
        //  return View(UsernameViewPage);
        //}
        //=================================================

        if (Request.Path != "/" &&
          Request.Path != "/Home/TenantAuthentication" &&
          Request.Path != "/Home/UsernameAuthentication" &&
          Request.Path != "/Home/PasswordAuthentication" &&
          Request.Path != "/Home/ApplicationSwitcher" &&
          Request.Path != "/Home/OTPAuthentication")
        {
          ViewData["path_cookie"] = string.Empty;
          HttpContext.Response.Cookies.Append("path_coockie", Request.Path);
        }

        string host = await GetTenantUserNamePwdValue("url");
        string subdomain = await GetTenantUserNamePwdValue("domainName");
        tc.TrackTrace("Method Index() host: " + host + "subdomain: " + subdomain);

        string devicename = Request.Cookies["dname_cookie"];
        string token = Request.Cookies["token"];

        if (!await IsNullOrEmptyOrWhiteSpace(token) && devicename != null && devicename!="InActive" && devicename.Contains("Active"))
        {
          //var appTenantName = await GetTenantUserNamePwdValue("tenantName");
          //var applications = await GetTenantApplicationsListAsync(appTenantName);
          if (Request.Path == "/")
          {
            var tenantName = await GetTenantUserNamePwdValue("tenantName");

            var redirectApp = devicename.Split('_')[1];
            var environemnt = CommonConfiguration.GetConfig(context, "environment");
            var redirectUrl = string.Empty;
            if (environemnt == "dev")
            {
              redirectUrl = "http://localhost:9060/" + $"{redirectApp}/Home";
            }
            else
            {
              redirectUrl = "https://" + tenantName + CommonConfiguration.GetConfig(context, "Extension") + "/" + $"{redirectApp}/Home";
            }

            return Redirect(redirectUrl);
          }
          else
          {
            return Home();
          }
        }
        else if (host != subdomain && host != "localhost")
        {
          tc.TrackTrace("Method Index() host:" + host + " Not equal subdomain: " + subdomain);

          RemoveCustomCookies("All");
          var tenantName = await GetTenantUserNamePwdValue("tenantName");
          var applications = await GetTenantApplicationsListAsync(tenantName);
          if (applications.Count == 0)//Error
          {
            @ViewBag.message = somethingWrong;
            return View();
          }
          else if (applications.Count == 1)//Redirect to User Page
          {
            var redirectionApp = (applications.ElementAt(0).ApplicationId + CommonConfiguration.GetConfig(context, "ApplicationPostFix")).ToLower();
            var redirectUrl = "https://" + tenantName + CommonConfiguration.GetConfig(context, "Extension") + "/" + $"{redirectionApp}/Home/UserNameAuthentication";
            return Redirect(redirectUrl);
          }
          else//To Show Application Switch Page
          {
            var loginModel = new LoginModel()
            {
              tenantName = tenantName,
              ApplicationNames = applications
            };
            return View(ApplicationSwitcherViewPage, loginModel);
          }
        }
        else
        {
          @ViewBag.message = ErrorMessage;
          ErrorMessage = string.Empty;
          RemoveCustomCookies("All");
          return View();
        }
      }
      catch (Exception ex)
      {
        TcException(ex, methodName);
        return View();
      }
    }

    [HttpGet, Route("login/ApplicationSwitcher")]
    public async Task<IActionResult> Index(LoginModel loginModel)
    {
      string methodName = "Index()";

      try
      {
        string token = Request.Cookies["token"];
        var applicationName = await GetApplicationName();
        if (!await IsNullOrEmptyOrWhiteSpace(token) && Request.Cookies["dname_cookie"] == "Active_" + applicationName)
        {
          if (JObject.Parse(await GetAndValidateToken(token,applicationName))["valid"].ToString() == "True")
            return await Task.FromResult(Home());
          else
          {
            TcTrack(methodName, " Response is coming false from GetAndValidateToken(). tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username + ", token:" + token);
            @ViewBag.message = sessionExpired;
            RemoveCustomCookies("All");
            return await Task.FromResult(View(ApplicationSwitcherViewPage, loginModel));
          }
        }
      }
      catch (Exception ex)
      {
        TcException(ex, methodName, "User click on browser history and try to get login. tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username + ", token:" + token);
      }

      try
      {
        loginModel.url = await GetTenantUserNamePwdValue("tenantName");

        if (await IsNullOrEmptyOrWhiteSpace(loginModel.tenantName))
        {
          @ViewBag.message = enterCompanyName;
          return await Task.FromResult(View(IndexViewPage));
        }
        else
          loginModel.tenantName = loginModel.tenantName.ToLower().Trim();

        loginModel.ApplicationNames = await GetTenantApplicationsListAsync(loginModel.tenantName);

        TcTrack(methodName, "Tracing " + methodName + " in " + ApplicationName + " Started. \ntenantName:" + loginModel.tenantName + " & URL: " + loginModel.url);

        if (loginModel.url == loginModel.tenantName || loginModel.url == "visur" || loginModel.url == "localhost") // 1~IFS
        {
          if (await remoteservice.TenantAuthentication(loginModel.tenantName)) // 2~IFS
          {
            TcTrack(methodName, "From authguard TenantAuthentication() response is coming true. tenantName: " + loginModel.tenantName);
            if (await GetSubscriptionState(loginModel.tenantName)) // 3~IFS
            {
              TcTrack(methodName, "from getSubscriptionState() response is coming true. tenantName:" + loginModel.tenantName);
              HttpContext.Response.Cookies.Append("c_cookie", loginModel.tenantName);

              if (loginModel.url == "localhost")
                return await Task.FromResult(View(ApplicationSwitcherViewPage, loginModel));

              return Redirect("https://" + loginModel.tenantName + CommonConfiguration.GetConfig(context, "Extension") + "/");
            }
            else // 3~IFS (getSubscriptionState)
            {
              @ViewBag.message = subscriptionExpired;
              TcTrack(methodName, "Tracing " + methodName + " in " + ApplicationName + ". \n from getSubscriptionState() method response is coming false for tenantName: " + loginModel.tenantName);
              return await Task.FromResult(View(IndexViewPage));
            }
          }
          else // 2~IFE (Authguard)
          {
            @ViewBag.message = enterValidCompanyName;
            TcTrack(methodName, "Tracing " + methodName + " in " + ApplicationName + ". \n from authguard remoteservice." + methodName + " method response is coming false for tenantName:" + loginModel.tenantName);
            return await Task.FromResult(View(IndexViewPage));
          }
        }
        else // 1~IFE (if condition)
        {
          @ViewBag.message = enterValidCompanyName;
          TcTrack(methodName, "Tracing " + methodName + " in " + ApplicationName + ". (If) condition is not satisfying. \ntenantName: " + loginModel.tenantName + " & URL: " + loginModel.url);
          return await Task.FromResult(View(IndexViewPage));
        }
      }
      catch (Exception ex)
      {
        TcException(ex, methodName, " tenantName: " + loginModel.tenantName);

        @ViewBag.message = somethingWrong;
        return await Task.FromResult(View(IndexViewPage));
      }
    }

    public IActionResult ApplicationSwitcher()
    {
      return View();
    }
    public IActionResult UsernameAuthentication()
    {
      StringValues tenantName;
      HttpContext.Request.Query.TryGetValue("tenantName", out tenantName);
      if (tenantName.Count > 0)
      {
        var model = new LoginModel();
        model.tenantName = HttpContext.Request.Query["tenantName"].ToString();
        return View(model);
      }
      else
      {
        return View();
      }
    }
    public IActionResult PasswordAuthentication()
    {
      return View();
    }
    public IActionResult OTPAuthentication()
    {
      return View();
    }
    public IActionResult Home()
    {
      return View(HomeViewPage);
    }
    public IActionResult InvalidPage()
    {
      return View();
    }

    [HttpGet]
    public async Task<IActionResult> ApplicationSwitcher(LoginModel loginModel)
    {
      string methodName = "ApplicationSwitcher()";
      TcTrack(methodName + "started");

      //var applicationName = CommonConfiguration.GetConfig(context, "ApplicationName");
      var applicationPostFix = CommonConfiguration.GetConfig(context, "ApplicationPostFix");

      //User Selected Value
      var userSelectApplicationName = loginModel.ApplicationId != null ? loginModel.ApplicationId.Split('_')[1] : "";
      TcTrack(methodName + " userSelectApplicationName:" + userSelectApplicationName);

      if (await IsNullOrEmptyOrWhiteSpace(userSelectApplicationName))
      {
        @ViewBag.message = nonSelectApplicationError;
        loginModel.ApplicationNames = await GetTenantApplicationsListAsync(loginModel.tenantName);
        return View(loginModel);
      }

      var redirectionApp = (userSelectApplicationName + applicationPostFix).ToLower();
      var environemnt = CommonConfiguration.GetConfig(context, "environment");
      var redirectUrl = string.Empty;
      if (environemnt == "dev")
      {
        redirectUrl = "http://localhost:9060/" + $"{redirectionApp}/Home/UserNameAuthentication?tenantName=" + loginModel.tenantName;
      }
      else
      {
       redirectUrl = "https://" + loginModel.tenantName + CommonConfiguration.GetConfig(context, "Extension") + "/" + $"{redirectionApp}/Home/UserNameAuthentication";
      }
      TcTrack(methodName + " redirectUrl:" + redirectUrl);

      return Redirect(redirectUrl);
    }

    [HttpGet, Route("login/PasswordAuthentication")]
    public async Task<IActionResult> UsernameAuthentication(LoginModel loginModel)
    {
      string methodName = "UsernameAuthentication()";

      try
      {
        string token = Request.Cookies["token"];
        var applicationName = await GetApplicationName();
        if (!await IsNullOrEmptyOrWhiteSpace(token) && Request.Cookies["dname_cookie"] == "Active_" + applicationName)
        {
          TcTrack(methodName, " try to get login. tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username + ", token:" + token);

          if (JObject.Parse(await GetAndValidateToken(token,applicationName))["valid"].ToString() == "True")
            return await Task.FromResult(Home());
          else
          {
            TcTrack(methodName, " Response is coming false from GetAndValidateToken(). tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username + ", token:" + token);
            @ViewBag.message = sessionExpired;
            RemoveCustomCookies("All");
            return await Task.FromResult(View(UsernameViewPage));
          }
        }
      }
      catch (Exception ex)
      {
        TcException(ex, methodName, "User click on browser history and try to get login. tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username + ", token:" + token);
      }

      try
      {
        if (await IsNullOrEmptyOrWhiteSpace(loginModel.tenantName))
          loginModel.tenantName = await GetTenantUserNamePwdValue("tenantName");
        else
          loginModel.tenantName = loginModel.tenantName.ToLower().Trim();

        if (await IsNullOrEmptyOrWhiteSpace(loginModel.username) || !loginModel.username.Contains('@'))
        {
          @ViewBag.message = enterValidUsername;
          return await Task.FromResult(View(UsernameViewPage));
        }
        else
          loginModel.username = loginModel.username.ToLower().Trim();

        HttpContext.Response.Cookies.Append("c_cookie", loginModel.tenantName);
        TcTrack(methodName, "Method " + methodName + ", tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username);

        if (await remoteservice.UsernameAuthentication(loginModel.tenantName, loginModel.username))
        {
          TcTrack(methodName, " Response is coming true from authguard UsernameAuthentication(). tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username);

          HttpContext.Response.Cookies.Append("u_cookie", loginModel.username);
          return await Task.FromResult(View(PasswordViewPage));
        }
        else
        {
          TcTrack(methodName, " Response is coming false from authguard UsernameAuthentication(). tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username);

          @ViewBag.message = enterValidUsername;
          return await Task.FromResult(View(UsernameViewPage));
        }
      }
      catch (Exception ex)
      {
        TcException(ex, methodName, " tenantName: " + loginModel.tenantName + " username: " + loginModel.username);

        @ViewBag.message = somethingWrong;
        return await Task.FromResult(View(UsernameViewPage));
      }
    }

    [HttpPost, Route("login/auth")]
    public async Task<IActionResult> PasswordAuthentication(LoginModel loginModel)
    {
      string methodName = "PasswordAuthentication()";

      try
      {
        string token = Request.Cookies["token"];
        var applicationName = await GetApplicationName();
        if (!await IsNullOrEmptyOrWhiteSpace(token) && Request.Cookies["dname_cookie"] == "Active_" + applicationName)
        {
          if (JObject.Parse(await GetAndValidateToken(token,applicationName))["valid"].ToString() == "True")
            return await Task.FromResult(Home());
          else
          {
            TcTrack(methodName, " Response is coming false from GetAndValidateToken(). tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username + ", token:" + token);
            @ViewBag.message = sessionExpired;
            RemoveCustomCookies("All");
            return await Task.FromResult(View(UsernameViewPage));
          }
        }
      }
      catch (Exception ex)
      {
        TcException(ex, methodName, "User click on browser history and try to get login. tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username + ", token:" + token);
      }

      try
      {
        if (await IsNullOrEmptyOrWhiteSpace(loginModel.tenantName))
          loginModel.tenantName = await GetTenantUserNamePwdValue("tenantName");
        else
          loginModel.tenantName = loginModel.tenantName.ToLower().Trim();

        if (await IsNullOrEmptyOrWhiteSpace(loginModel.username))
          loginModel.username = await GetTenantUserNamePwdValue("username");
        else
          loginModel.username = loginModel.username.ToLower().Trim();

        if (await IsNullOrEmptyOrWhiteSpace(loginModel.password))
        {
          @ViewBag.message = enterPassword;
          return await Task.FromResult(View(PasswordViewPage));
        }
        else
          loginModel.password = loginModel.password.Trim();

        if (await IsNullOrEmptyOrWhiteSpace(loginModel.deviceDetail))
        {
          @ViewBag.message = technicalIssue;
          var exception = new Exception("deviceDetail is coming null or undefined: " + loginModel.deviceDetail);
          TcException(exception, methodName, " tenantName: " + loginModel.tenantName + " username: " + loginModel.username);
          return await Task.FromResult(View(PasswordViewPage));
        }

        loginModel.ApplicationId = await GetApplicationName();

        CookieOptions options = new CookieOptions { Expires = DateTime.Now.AddDays(1) };
        TcTrack(methodName, "Method " + methodName + ", tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username);

        string result = await remoteservice.PasswordAuthentication(loginModel.tenantName, loginModel.username, loginModel.password, loginModel.deviceDetail,loginModel.ApplicationId);
        TcTrack(methodName, "Method " + methodName + ", response is coming " + result + " from authguard PasswordAuthentication(). tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username);
        if (result == "true")
        {
          if (await getToken(loginModel.tenantName, loginModel.username, loginModel.password, CommonConfiguration.GetConfig(context, "Extension"),loginModel.ApplicationId))
          {
            TcTrack(methodName, " Response is coming true from getToken(). tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username);

            RemoveCustomCookies("ComapnyUserPasswordCookie");
            var applicationName = await GetApplicationName();
            HttpContext.Response.Cookies.Append("dname_cookie", "Active_" + applicationName, options);

            //get the enter path
            var pathUrl = Request.Cookies["path_coockie"];
            if (pathUrl != null && pathUrl != "")
            {
              HttpContext.Response.Cookies.Delete("path_coockie");
              return Redirect(pathUrl);
            }
            else
              return await Task.FromResult(Home());
          }
          else
            throw new Exception(" From getToken() response is coming false, might be some services is down. tenantName: " + loginModel.tenantName + ", username: " + loginModel.username + ", deviceDetail: " + loginModel.deviceDetail);
        }
        else if (result == "MFA")
        {
          HttpContext.Response.Cookies.Append("p_cookie", loginModel.password, options);
          HttpContext.Response.Cookies.Append("dname_cookie", "InActive", options);

          return await Task.FromResult(View(MFAViewPage));
        }
        else
        {
          HttpContext.Response.Cookies.Append("dname_cookie", "InActive", options);

          @ViewBag.message = enterValidPassword;
          return await Task.FromResult(View(PasswordViewPage));
        }
      }
      catch (Exception ex)
      {
        TcException(ex, methodName, " tenantName: " + loginModel.tenantName + " username: " + loginModel.username);

        /*if (ex.Message == "This can happen if message is dropped when service is busy or its long running operation and taking more time than configured Operation Timeout.")
        {
          @ViewBag.message = timeoutException;
          return await Task.FromResult(View(PasswordViewPage));
        }*/

        @ViewBag.message = loginFailed;
        return await Task.FromResult(View(PasswordViewPage));
      }
    }

    [HttpPost, Route("login/auth2")]
    public async Task<IActionResult> OTPAuthentication(LoginModel loginModel)
    {
      string methodName = "OTPAuthentication()";

      try
      {
        if (await IsNullOrEmptyOrWhiteSpace(loginModel.tenantName))
          loginModel.tenantName = await GetTenantUserNamePwdValue("tenantName");
        else
          loginModel.tenantName = loginModel.tenantName.ToLower().Trim();

        if (await IsNullOrEmptyOrWhiteSpace(loginModel.username))
          loginModel.username = await GetTenantUserNamePwdValue("username");
        else
          loginModel.username = loginModel.username.ToLower().Trim();

        if (await IsNullOrEmptyOrWhiteSpace(loginModel.password))
          loginModel.password = await GetTenantUserNamePwdValue("password");
        else
          loginModel.password = loginModel.password.Trim();

        if (await IsNullOrEmptyOrWhiteSpace(loginModel.otpCode))
        {
          @ViewBag.message = enterVerificationCode;
          return await Task.FromResult(View(MFAViewPage));
        }
        else
          loginModel.otpCode = loginModel.otpCode.Trim();

        CookieOptions options = new CookieOptions { Expires = DateTime.Now.AddDays(1) };
        TcTrack(methodName, "tenantName: " + loginModel.tenantName + ", username: " + loginModel.username + ", otpCode: " + loginModel.otpCode + ", deviceDetail: " + loginModel.deviceDetail + ", url: " + loginModel.url + ", isChangePassword: " + loginModel.isChangePassword);
        loginModel.ApplicationId = await GetApplicationName();
        if (await remoteservice.OTPAuthentication(loginModel.tenantName, loginModel.username, loginModel.password, loginModel.otpCode, loginModel.deviceDetail, loginModel.url,loginModel.ApplicationId, loginModel.isChangePassword))
        {
          TcTrack(methodName, " Response is coming true from authguard OTPAuthentication(). tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username);

          this.RemoveCustomCookies("ComapnyUserPasswordCookie");

          if (await this.getToken(loginModel.tenantName, loginModel.username, loginModel.password, CommonConfiguration.GetConfig(context, "Extension"),loginModel.ApplicationId))
          {
            TcTrack(methodName, " Response is coming true from getToken(). tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username);

            var applicationName = await GetApplicationName();
            HttpContext.Response.Cookies.Append("dname_cookie", "Active_" + applicationName, options);
            return await Task.FromResult(Home());
          }
          else
          {
            TcTrack(methodName, " Response is coming false from getToken(). tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username);
            HttpContext.Response.Cookies.Append("dname_cookie", "InActive", options);

            @ViewBag.message = invalidAuthentication;
            return await Task.FromResult(View(MFAViewPage));
          }
        }
        else if (await isOTPExpired(loginModel.tenantName, loginModel.username, loginModel.url))
        {
          TcTrack(methodName, " Response is coming true from GetOTPFromIgnite(). tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username);
          HttpContext.Response.Cookies.Append("dname_cookie", "InActive", options);

          @ViewBag.message = verificationCodeExpired;
          return await Task.FromResult(View(MFAViewPage));
        }
        else
        {
          TcTrack(methodName, " Response is coming false from authguard OTPAuthentication(). tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username);
          HttpContext.Response.Cookies.Append("dname_cookie", "InActive", options);

          @ViewBag.message = enterValidVerificationCode;
          return await Task.FromResult(View(MFAViewPage));
        }
      }
      catch (Exception ex)
      {
        TcException(ex, methodName, " tenantName: " + loginModel.tenantName + ", username: " + loginModel.username + ", otpCode: " + loginModel.otpCode + ", deviceDetail: " + loginModel.deviceDetail + ", url: " + loginModel.url + ", isChangePassword: " + loginModel.isChangePassword);

        @ViewBag.message = loginFailed;
        return await Task.FromResult(View(MFAViewPage));
      }
    }

    public async Task<IActionResult> ChangePassword()
    {
      string methodName = "ChangePassword()";

      try
      {
        //if (Request.Cookies["dname_cookie"] == "Active" && !await IsNullOrEmptyOrWhiteSpace(Request.Cookies["token"]))
        //{
        //  var pathUrl = Request.Cookies["path_coockie"];
        //  if (pathUrl != null && pathUrl != "")
        //  {
        //    HttpContext.Response.Cookies.Delete("path_coockie");
        //    return Redirect(pathUrl);
        //  }
        //  else
        //    return Redirect("/homefabric");
        //}
        //else
        //{
        RemoveCustomCookies("All");
        if (await CheckUserExist(HttpContext.Request.QueryString.Value.Split('?')[1]))
          return View();
        else
          return await Task.FromResult(View(UsernameViewPage));
        //}
      }
      catch (Exception ex)
      {
        @ViewBag.message = invalidLink;
        TcException(ex, methodName);
        return await Task.FromResult(View(UsernameViewPage));
      }
    }

    /*
     * url: required
     * createPassword: required
     * confirmPassword: required
     */
    [HttpPost]
    public async Task<IActionResult> ChangePassword(LoginModel loginModel)
    {
      string methodName = "ChangePassword()";
      TcTrack(methodName, " url:" + loginModel.url);

      try
      {
        if (!await IsNullOrEmptyOrWhiteSpace(loginModel.url))
        {
          TcTrack(methodName, " Request going to validate password   createPassword .:" + loginModel.createPassword + ", confirmPassword:" + loginModel.confirmPassword + ", url:" + loginModel.url);

          string isPasswordValidated = await ValidatePassword(loginModel.createPassword, loginModel.confirmPassword);

          TcTrack(methodName, " Response coming from ValidatePassword(). result:" + isPasswordValidated + ", url:" + loginModel.url);

          if (isPasswordValidated != "true")
          {
            @ViewBag.message = isPasswordValidated;
            @ViewBag.passwordValidationMessage = passwordValidationMessage;
            return await Task.FromResult(View(PasswordResetViewPage));
          }

          TcTrack(methodName, " Request going to authguard.   createPassword .:" + loginModel.createPassword + ", confirmPassword:" + loginModel.confirmPassword + ", url:" + loginModel.url);
          loginModel.ApplicationId = await GetApplicationName();
          string result = await remoteservice.ChangePassword(loginModel.createPassword, loginModel.confirmPassword, loginModel.url,loginModel.ApplicationId);
          TcTrack(methodName, " Response coming from authguard. result:" + result + ", url:" + loginModel.url);
          switch (result)
          {
            case "true":
              return await Task.FromResult(View(ResetpasswordMFAViewPage));

            case "false":
              throw new Exception("response coming false from authguard ChangePassword()");

            case "enterPassword":
              @ViewBag.message = enterPassword;
              return await Task.FromResult(View(PasswordResetViewPage));

            case "passwordsNotSame":
              @ViewBag.message = mismatchPasswords;
              return await Task.FromResult(View(PasswordResetViewPage));

            case "lastPassword":
              @ViewBag.message = lastPassword;
              return await Task.FromResult(View(PasswordResetViewPage));

            case "invalidLink":
              @ViewBag.message = invalidLink;
              return await Task.FromResult(View(PasswordResetViewPage));

            default:
              throw new Exception(result);
          }
        }
        else
        {
          string exceptionMessage = "url IsNullOrEmptyOrWhiteSpace. url: " + loginModel.url;
          throw new Exception(exceptionMessage);
        }
      }
      catch (Exception ex)
      {
        @ViewBag.message = technicalIssue;
        TcException(ex, methodName, ", url: " + loginModel.url);
        return await Task.FromResult(View(PasswordResetViewPage));
      }
    }

    public IActionResult ChangePasswordOTPAuth()
    {
      return View();
    }

    [HttpPost]
    public async Task<IActionResult> ChangePasswordOTPAuth(LoginModel loginModel)
    {
      string methodName = "ChangePasswordOTPAuth()";

      try
      {
        if ((!await IsNullOrEmptyOrWhiteSpace(loginModel.url)) &&
          (!await IsNullOrEmptyOrWhiteSpace(loginModel.createPassword)) &&
         (!await IsNullOrEmptyOrWhiteSpace(loginModel.confirmPassword)))
        {
          TcTrack(methodName, " createPassword: " + loginModel.createPassword + ", confirmPassword:" + loginModel.confirmPassword + ", url:" + loginModel.url);
          if (await IsNullOrEmptyOrWhiteSpace(loginModel.otpCode))
          {
            @ViewBag.message = enterVerificationCode;
            return await Task.FromResult(View(ResetpasswordMFAViewPage));
          }
          else
          {
            TcTrack(methodName, " createPassword: " + loginModel.createPassword + ", confirmPassword:" + loginModel.confirmPassword + ", url:" + loginModel.url + ", otpCode: " + loginModel.otpCode + ", deviceDetail: " + loginModel.deviceDetail);
            loginModel.ApplicationId = await GetApplicationName();
            if (await remoteservice.OTPAuthentication(loginModel.tenantName, loginModel.username, loginModel.createPassword, loginModel.otpCode, loginModel.deviceDetail, loginModel.url,loginModel.ApplicationId, "true"))
            {
              @ViewBag.correctMsg = passwordReseted;
              return await Task.FromResult(View(UsernameViewPage));
            }
            else
            {
              @ViewBag.message = enterValidVerificationCode;
              return await Task.FromResult(View(ResetpasswordMFAViewPage));
            }
          }
        }
        else
          throw new Exception("url, createPassword, confirmPassword IsNullOrEmptyOrWhiteSpace. url: " + loginModel.url);
      }
      catch (Exception ex)
      {
        TcException(ex, methodName, " tenantName: " + loginModel.tenantName + " username: " + loginModel.username);

        @ViewBag.message = technicalIssue;
        return await Task.FromResult(View(UsernameViewPage));
      }
    }

    [HttpPost]
    public async Task<bool> isOTPExpired(string tenantName, string username, string url)
    {
      string methodName = "isOTPExpired()";
      tc.TrackTrace(methodName + " started.");

      bool result = false;
      try
      {
        if (await GetOTPFromIgnite(tenantName, username, url) == "expire")
          result = true;
      }
      catch (Exception ex)
      {
        TcException(ex, "GetOTPFromIgnite()", " When ignite service is down, then trying to read OTP from the DB.");

        try
        {
          var applicationName = await GetApplicationName();
          GetUserDetailModel response = await remoteservice.GetUserDetail(tenantName, username,applicationName);
          tc.TrackTrace(methodName + ", response from DB after getting exception from ignite service, trying to read from db, GetUserDetail(): " + response);

          if (response.OTPCode == "expire")
            result = true;
        }
        catch (Exception e)
        {
          TcException(e, "isOTPExpired()" + ", GetUserDetail() method");
        }
      }

      return await Task.FromResult(result);
    }

    private async Task<bool> getToken(string tenantName, string username, string password, string subdomain,string applicationName)
    {
      string methodName = "getToken()";
      TcTrack(methodName, "Method " + methodName + ", tenantName:" + tenantName + ", userName:" + username);

      bool result = false;
      try
      {
        var response = await remoteservice.GetToken(username + "@" + tenantName + CommonConfiguration.GetConfig(context, "Extension"), password,applicationName);
        TcTrack(methodName, "Method " + methodName + ", tenantName:" + tenantName + ", userName:" + username + ", Response from Authguard GetToken(): " + response);

        if (response != "" && response != "{}")
        {
          token = JObject.Parse(response)["access_token"].ToString();
          string expiryIn = JObject.Parse(response)["expires_in"].ToString();

          CookieOptions options = new CookieOptions { Expires = DateTime.Now.AddDays(1) };
          HttpContext.Response.Cookies.Append("expires_in", expiryIn, options);
          HttpContext.Response.Cookies.Append("token", token, options);

          result = true;
        }
      }
      catch (Exception ex)
      {
        TcException(ex, methodName, " tenantName: " + tenantName + " username: " + username);
      }

      return result;
    }

    [HttpGet]
    public async Task<string> CallgetTokenAsync(string companyName, string username, string password, string subdomain,string applicationName)
    {
      await getToken(companyName, username, password, subdomain,applicationName);
      string temp = token;
      token = "";
      return temp;
    }

    [HttpGet]
    public async Task<string> UserEmailValidationinLdap(string tenantName, string email)
    {
      try
      {
        string baseurl = CommonConfiguration.GetConfig(context, "Wso2URL");
        string wso2Url = baseurl + "dc/validateUserMail";
        string contentType = CommonConfiguration.GetConfig(context, "ContentType");

        var postData = "domainComponent=" + tenantName;
        postData += "&mail=" + email;
        var response = JObject.Parse(await PostClient(wso2Url, postData, contentType));

        return await Task.FromResult(response["entity"].ToString());
      }
      catch (Exception ex)
      {
        string innerExceptionMessage = null;
        if (ex.InnerException != null)
        {
          innerExceptionMessage = ex.InnerException.Message + "\t" + ex.InnerException.Data + "\t" + ex.InnerException.StackTrace;
        }
        string msgInfo = "{\n\"MethodName\": \"UserEmailValidationinLdap\",\n\"Message\":" + ex.Message + ",\n\"InnerException\":" + innerExceptionMessage + ",\n\"StackTrace\":" + ex.StackTrace + "\n}";
        tc.TrackException(new Exception("\n" + msgInfo));
        return await Task.FromResult(msgInfo);
      }
    }

    [HttpPost]
    public async Task<IActionResult> ResendOTP(LoginModel loginModel)
    {
      string methodName = "ResendOTP()";
      tc.TrackTrace(methodName + " started.");

      try
      {
        string token = Request.Cookies["token"];
        var applicationName = await GetApplicationName();
        if (!await IsNullOrEmptyOrWhiteSpace(token) && Request.Cookies["dname_cookie"] == "Active_" + applicationName)
        {
          if (JObject.Parse(await GetAndValidateToken(token,applicationName))["valid"].ToString() == "True")
            return await Task.FromResult(Home());
          else
          {
            TcTrack(methodName, " Response is coming false from GetAndValidateToken(). tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username + ", token:" + token);
            @ViewBag.message = sessionExpired;
            RemoveCustomCookies("All");
            return await Task.FromResult(View(UsernameViewPage));
          }
        }
      }
      catch (Exception ex)
      {
        TcException(ex, methodName, "User click on browser history and try to get login. tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username + ", token:" + token);
      }

      try
      {
        if (await IsNullOrEmptyOrWhiteSpace(loginModel.tenantName))
          loginModel.tenantName = await GetTenantUserNamePwdValue("tenantName");

        if (await IsNullOrEmptyOrWhiteSpace(loginModel.username))
          loginModel.username = await GetTenantUserNamePwdValue("username");
        loginModel.ApplicationId = await GetApplicationName();
        TcTrack(methodName, " requesting going to ResendOTP(). tenantName:" + loginModel.tenantName + ", userName:" + loginModel.username + ", url:" + loginModel.url);
        if (loginModel.isChangePassword == "true" && (!await IsNullOrEmptyOrWhiteSpace(loginModel.url)) &&
          await remoteservice.ResendOTP(loginModel.tenantName, loginModel.username, loginModel.deviceDetail, loginModel.url, loginModel.isChangePassword,loginModel.ApplicationId))
        {
          @ViewBag.correctMsg = verificationCodeSent;
          return await Task.FromResult((View(ResetpasswordMFAViewPage)));
        }
        else if ((!await IsNullOrEmptyOrWhiteSpace(loginModel.deviceDetail)) &&
          (loginModel.isChangePassword != "true") &&
          (await remoteservice.ResendOTP(loginModel.tenantName, loginModel.username, loginModel.deviceDetail, loginModel.url, loginModel.isChangePassword,loginModel.ApplicationId)))
        {
          @ViewBag.correctMsg = verificationCodeSent;
          return await Task.FromResult(View(MFAViewPage));
        }
        else
          throw new Exception("tenantName, username, device_details IsNullOrEmptyOrWhiteSpace or response coming false from authguard ResendOTP() method. tenantName: " + loginModel.tenantName + ", username: " + loginModel.username + ", deviceDetail: " + loginModel.deviceDetail);
      }
      catch (Exception ex)
      {
        TcException(ex, methodName);

        @ViewBag.message = technicalIssue;
        return await Task.FromResult(View(UsernameViewPage));
      }
    }

    public async Task<string> GetOTPFromIgnite(string tenantName, string username, string url)
    {
      string methodName = "GetOTPFromIgnite()";
      string exceptionMsg = methodName + ": tenantname: " + tenantName + ", userName: " + username;
      TcTrack(methodName, exceptionMsg);

      try
      {
        if ((await IsNullOrEmptyOrWhiteSpace(tenantName)) &&
            (await IsNullOrEmptyOrWhiteSpace(username)) &&
            (!await IsNullOrEmptyOrWhiteSpace(url)))
        {
          string decryptURL = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(url)).Trim();
          tenantName = decryptURL.Split('@')[2].Split('.')[0];
          username = decryptURL.Split('@')[0] + '@' + decryptURL.Split('@')[1];

          TcTrack(methodName, exceptionMsg);
        }

        //return JsonConvert.DeserializeObject<dynamic>(await GetClient(CommonConfiguration.GetConfig(context, "Ignite") + "/Tenantservice/userlogin/sessions?cacheName=LoginSessions&key=" + username + '@' + tenantName + CommonConfiguration.GetConfig(context, "Extension")))["otp"].ToString();
        string key = username + '@' + tenantName + CommonConfiguration.GetConfig(context, "Extension");
        IgniteThinClientService _igniteThinClientService = new IgniteThinClientService(CommonConfiguration.GetConfig(context, "endpoint"));
        LoginSessionCache _loginSessionCache = await _igniteThinClientService.ReadRecordAsync(otpCacheName, key);

        return _loginSessionCache.otp;
      }
      catch (Exception ex)
      {
        TcException(ex, methodName, exceptionMsg);
        throw new Exception(exceptionMsg);
      }
    }

    [HttpGet]
    public async Task<String> getUrl(string key)
    {
      String Url = CommonConfiguration.GetConfig(context, key);
      return await Task.FromResult(Url);
    }

    [HttpPost]
    public async Task<IActionResult> SendEmailPage(LoginModel loginModel)
    {
      string methodName = "SendEmailPage()";
      TcTrack(methodName, loginModel.tenantName + ", userName:" + loginModel.username);

      try
      {
        if (await IsNullOrEmptyOrWhiteSpace(loginModel.tenantName))
          loginModel.tenantName = await GetTenantUserNamePwdValue("tenantName");

        if (await IsNullOrEmptyOrWhiteSpace(loginModel.username))
          loginModel.tenantName = await GetTenantUserNamePwdValue("username");

        if (await IsNullOrEmptyOrWhiteSpace(loginModel.ApplicationId))
          loginModel.ApplicationId = await GetApplicationName();

        if (await IsNullOrEmptyOrWhiteSpace(loginModel.isForgotPassword))
          loginModel.isForgotPassword = "ResetPassword";

        string authRouteMessage = await GetAuthOnRouteMessageString(loginModel, AuthConstants.CreateEmail, loginModel.ApplicationId, string.Empty);

        if (await remoteservice.AuthOnRouteMessageaAsync(authRouteMessage))
        {
          @ViewBag.correctMsg = passwordLink;
          return await Task.FromResult(View(PasswordViewPage));
        }
        else
          throw new Exception("userName value coming null. or SendEmailPage response coming false from authguard.");
      }
      catch (Exception ex)
      {
        TcException(ex, methodName, " tenantName: " + loginModel.tenantName + " username: " + loginModel.username);

        @ViewBag.message = somethingWrong;
        return await Task.FromResult(View(PasswordViewPage));
      }
    }


    [HttpPost]
    public async Task<string> ValidatePassword(string createPassword, string confirmPassword)
    {
      string methodName = "ValidatePassword()";
      string result = string.Empty;
      try
      {
        if (string.IsNullOrWhiteSpace(createPassword) || string.IsNullOrWhiteSpace(confirmPassword))
          return await Task.FromResult(enterPassword);
        else if (createPassword != confirmPassword)
          return await Task.FromResult(mismatchPasswords);

        string password = (createPassword == confirmPassword) ? createPassword : string.Empty;

        var hasNumber = new Regex(@"[0-9]+");
        var hasUpperChar = new Regex(@"[A-Z]+");
        var hasLowerChar = new Regex(@"[a-z]+");
        var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");

        if (!((password.Length >= MIN_LENGTH) && (password.Length <= MAX_LENGTH)))
          result = "You have entered " + password.Length + " characters long password. " + passwordLength;
        else if (!hasLowerChar.IsMatch(password))
          result = containLowerCase;
        else if (!hasUpperChar.IsMatch(password))
          result = containUpperCase;
        else if (!hasSymbols.IsMatch(password))
          result = containSpecialChar;
        else if (!hasNumber.IsMatch(password))
          result = containNumericValue;
        else
          result = "true";
      }
      catch (Exception ex)
      {
        @ViewBag.message = technicalIssue;
        TcException(ex, methodName);
      }

      return await Task.FromResult(result);
    }

    private async Task<String> Postwso2login(string url, string username, string password)
    {
      Boolean result = false;
      try
      {
        var request = (HttpWebRequest)WebRequest.Create(url);

        var postData = "userName=" + username;
        postData += "&password=" + password;
        postData += "&url=" + CommonConfiguration.GetConfig(context, "Wso2Ip");

        var data = Encoding.ASCII.GetBytes(postData);

        request.Method = "POST";
        request.ContentType = "application/x-www-form-urlencoded";
        request.ContentLength = data.Length;

        using (var stream = request.GetRequestStream())
        {
          stream.Write(data, 0, data.Length);
        }
        var response = (HttpWebResponse)request.GetResponse();
        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
        if (responseString != null)
        {
          result = true;
          return await Task.FromResult(result.ToString());
        }
        return await Task.FromResult(result.ToString());
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message);
        ServiceEventSource.Current.Message("Exception Occurred At Postwso2login() in HomeController :  " + ex.Message);
        tc.TrackTrace("Tracing Postwso2login() HomeController");


        string innerExceptionMessage = null;
        if (ex.InnerException != null)
        {
          innerExceptionMessage = ex.InnerException.Message + "\t" + ex.InnerException.Data + "\t" + ex.InnerException.StackTrace;
        }
        string msgInfo = "{\n\"MethodName\": \"Postwso2login\",\n\"URL\":" + url + ",\n\"Message\":" + ex.Message + ",\n\"InnerException\":" + innerExceptionMessage + ",\n\"StackTrace\":" + ex.StackTrace + "\n}";
        tc.Context.Device.OperatingSystem = Environment.OSVersion.ToString();
        tc.TrackException(new Exception("\n" + msgInfo));
        throw new Exception("\n" + msgInfo);
      }
    }

    // for https issue
    public bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
    {
      return true;
    }

    private async Task<String> PostClient(string url, string postData, string contenttype)
    {
      try
      {
        // for https
        //ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);

        var request = (HttpWebRequest)WebRequest.Create(url);
        var data = Encoding.ASCII.GetBytes(postData);
        request.Method = "POST";
        request.ContentType = contenttype;
        request.ContentLength = data.Length;
        using (var stream = request.GetRequestStream())
        {
          stream.Write(data, 0, data.Length);
        }
        var response = (HttpWebResponse)request.GetResponse();

        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

        return await Task.FromResult(responseString);
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message);
        ServiceEventSource.Current.Message("Exception Occurred At PostClient() in HomeController :  " + ex.Message);
        tc.TrackTrace("Tracing PostClient() HomeController");


        string innerExceptionMessage = null;
        if (ex.InnerException != null)
        {
          innerExceptionMessage = ex.InnerException.Message + "\t" + ex.InnerException.Data + "\t" + ex.InnerException.StackTrace;
        }
        string msgInfo = "{\n\"MethodName\": \"PostClient\",\n\"URL\":" + url + ",\n\"ContentType\":" + contenttype + ",\n\"Message\":" + ex.Message + ",\n\"InnerException\":" + innerExceptionMessage + ",\n\"StackTrace\":" + ex.StackTrace + "\n}";
        tc.Context.Device.OperatingSystem = Environment.OSVersion.ToString();
        tc.TrackException(new Exception("\n" + msgInfo));
        throw new Exception("\n" + msgInfo);
      }
    }

    /**
     * Authentication Stub
     */
    [HttpGet]
    public async Task<string> GetAndValidateToken(string token,string applicationName)
    {
      tc.TrackTrace("GetAndValidateToken() Called");

      var response = string.Empty;
      try
      {
        response = await remoteservice.ValidateToken(token, string.Empty,applicationName);
        tc.TrackTrace("GetAndValidateToken() Response from Authguard: " + response);

        if (!await IsNullOrEmptyOrWhiteSpace(response) && IsValidJson(response))
        {
          if (JObject.Parse(response)["error"].ToString() == "An error occurred while sending the request. The server name or address could not be resolved")
          {
            response = await remoteservice.ValidateToken(token, string.Empty,applicationName); // re-try once again.
            if (JObject.Parse(response)["error"].ToString() == "An error occurred while sending the request. The server name or address could not be resolved")
            {
              SendErrorToLoginPage(fluctuationInNetwork);
              return response;
            }
          }

          if (JObject.Parse(response)["valid"].ToString() == "True")
          {
            string tokenValue = JObject.Parse(response)["token"].ToString();
            if (!await IsNullOrEmptyOrWhiteSpace(tokenValue) && tokenValue != token)
              HttpContext.Response.Cookies.Append("token", tokenValue);
          }
          else
            SendErrorToLoginPage(JObject.Parse(response)["error"].ToString());
        }
        else response = "exception";
      }
      catch (Exception ex)
      {
        tc.TrackException(ex);
      }

      tc.TrackTrace("GetAndValidateToken() Response from Authguard: " + response);
      return response;
    }

    private static bool IsValidJson(string strInput)
    {
      strInput = strInput.Trim();
      if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
          (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
      {
        try
        {
          var obj = JToken.Parse(strInput);
          return true;
        }
        catch (JsonReaderException jex)
        {
          //Exception in parsing json
          Console.WriteLine(jex.Message);
          return false;
        }
        catch (Exception ex) //some other exception
        {
          Console.WriteLine(ex.ToString());
          return false;
        }
      }
      else
      {
        return false;
      }
    }

    [HttpGet]
    public async Task<string> SubDomain()
    {
      string methodName = "SubDomain()";
      string subDomin = null;
      try
      {
        subDomin = CommonConfiguration.GetConfig(context, "Extension");
      }
      catch (Exception ex)
      {
        tc.TrackException(new Exception("\n" + ex + " " + methodName));
      }
      return (subDomin);
    }

    [HttpGet]
    public async Task<String> UserDetails(string tenantName, string userEmail,string applicationName)
    {
      try
      {
        tc.TrackTrace("HomeController UserDetails() Called");

        string result = await new IgniteClient().GetUserDetails(tenantName, userEmail,applicationName);

        tc.TrackTrace("Result from  HomeController UserDetails() " + result);

        return result;
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message);
        ServiceEventSource.Current.Message("Exception Occurred At FabricList() in HomeController :  " + ex.Message);
        tc.TrackTrace("Tracing FabricList() HomeController");


        string innerExceptionMessage = null;

        if (ex.InnerException != null)
        {
          innerExceptionMessage = ex.InnerException.Message + "\t" + ex.InnerException.Data + "\t" + ex.InnerException.StackTrace;
        }
        string msgInfo = "{\n\"MethodName\": \"FabricList\",\n\"TenantId\":" + tenantName + ",\n\"Message\":" + ex.Message + ",\n\"InnerException\":" + innerExceptionMessage + ",\n\"StackTrace\":" + ex.StackTrace + "\n}";
        tc.Context.Device.OperatingSystem = Environment.OSVersion.ToString();
        tc.TrackException(new Exception("\n" + msgInfo));
        throw new Exception("\n" + msgInfo);

      }
    }

    [HttpGet]
    public async Task<string> UserDetailsAsync(string tenantName, string userEmail,string applicationName)
    {
      string methodName = "UserDetailsAsync()";

      try
      {
        string result = string.Empty;

        if (!await IsNullOrEmptyOrWhiteSpace(tenantName) && !await IsNullOrEmptyOrWhiteSpace(userEmail))
          result = await new IgniteClient().GetUserDetailsAsync(tenantName, userEmail,applicationName);
        else
        {
          string token = Request.Cookies["token"];

          if (await IsNullOrEmptyOrWhiteSpace(token))
          {
            tc.TrackException(new NullReferenceException("token must not be null\n requested TenantName:" + tenantName));
            SendErrorToLoginPage(somethingWrong);
            return result;
          }
          JObject response = JObject.Parse(await GetAndValidateToken(token,applicationName));
          tenantName = response["tenant"].ToString().Split('.').First();
          userEmail = response["user"].ToString();

          if (await IsNullOrEmptyOrWhiteSpace(tenantName) && await IsNullOrEmptyOrWhiteSpace(userEmail))
          {
            tc.TrackException(new NullReferenceException("UserEmail or TenantName must not be null\n requested TenantName:" + tenantName));
            SendErrorToLoginPage(somethingWrong);
            return result;
          }

          result = await new IgniteClient().GetUserDetailsAsync(tenantName, userEmail,applicationName);
        }

        return result;
      }
      catch (Exception ex)
      {
        tc.TrackException(new Exception("\n" + ex + " " + methodName));
        throw new Exception(ex.Message);
      }
    }

    private async Task<string> GetClient(string url)
    {
      try
      {
        string result = null;

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        request.AutomaticDecompression = DecompressionMethods.GZip;

        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        using (Stream stream = response.GetResponseStream())
        using (StreamReader reader = new StreamReader(stream))
        {
          result = reader.ReadToEnd();
        }
        Console.WriteLine(result);
        return await Task.FromResult(result);
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message);
        ServiceEventSource.Current.Message("Exception Occurred At GetClient() in HomeController :  " + ex.Message);
        tc.TrackTrace("Tracing GetClient() HomeController");


        string innerExceptionMessage = null;
        if (ex.InnerException != null)
        {
          innerExceptionMessage = ex.InnerException.Message + "\t" + ex.InnerException.Data + "\t" + ex.InnerException.StackTrace;
        }
        string msgInfo = "{\n\"MethodName\": \"GetClient\",\n\"URL\":" + url + ",\n\"Message\":" + ex.Message + ",\n\"InnerException\":" + innerExceptionMessage + ",\n\"StackTrace\":" + ex.StackTrace + "\n}";
        tc.Context.Device.OperatingSystem = Environment.OSVersion.ToString();
        tc.TrackException(new Exception("\n" + msgInfo));
        return await Task.FromResult("false");
      }
    }

    [HttpGet]
    public async Task<String> GetInstrumentationKey()
    {
      string instrKey = "";
      try
      {
        instrKey = CommonConfiguration.GetConfiguration("InstrumentationKey");
      }
      catch (Exception ex)
      {
        tc.TrackException(new Exception("Exception in GetInstrumentationKey. Exception Is : " + ex));

      }
      var key = JsonConvert.SerializeObject(instrKey);
      return await Task.FromResult(key);
    }

    public async Task<bool> GetSubscriptionState(string tenantName)
    {
      string methodName = "GetSubscriptionState()";
      TcTrack(methodName, "tenantName: " + tenantName);

      bool result = false;
      try
      {
        string wso2Url = CommonConfiguration.GetConfig(context, "Wso2URL") + "tenant/subscriptionState";
        string postData = "tenantName=" + tenantName + CommonConfiguration.GetConfig(context, "Extension");
        string response = JObject.Parse(await PostClient(wso2Url, postData, "application/x-www-form-urlencoded"))["entity"].ToString();

        if (response == "true")
          result = true;

        TcTrack(methodName, "tenantName: " + tenantName + ", response from wso2Url, result: " + result);
      }
      catch (Exception ex)
      {
        TcException(ex, methodName, "tenantName: " + tenantName);
      }

      return await Task.FromResult(result);
    }

    [HttpGet]
    public async Task<bool> IsServerConnected(string servername, string database, string username, string password)
    {
      var cb = new SqlConnectionStringBuilder
      {
        DataSource = servername,
        UserID = username,
        Password = password
      };

      using (SqlConnection connection = new SqlConnection(cb.ConnectionString))
      {
        try
        {
          connection.Open();
          return await Task.FromResult(true);
        }
        catch (SqlException)
        {
          return await Task.FromResult(false);
        }
      }
    }

    [HttpGet]
    public async Task<bool> IsAzureADLConnected(string subId, string secretKey, string appId, string domainname, string resourceGroupName, string accountname)
    {
      try
      {
        DataLakeStoreAccountManagementClient _adlsClient;
        var creds = ApplicationTokenProvider.LoginSilentAsync(domainname, appId, secretKey).Result;
        _adlsClient = new DataLakeStoreAccountManagementClient(creds) { SubscriptionId = subId };

        var res = _adlsClient.Accounts.Exists(resourceGroupName, accountname);
        Console.WriteLine(res);
        return await Task.FromResult(true);

      }
      catch (Exception ex)
      {
        return await Task.FromResult(false);
        throw new Exception(ex.Message);
      }
    }

    [HttpGet]
    public async Task<string> GetAdminCredetial(string tenantID, string tenanName)
    {
      var admincredntial = await igniteClient.GetAdminCredetial(tenantID, tenanName);

      return await Task.FromResult(admincredntial);
    }


    public void RemoveCustomCookies(string deleteType)
    {
      try
      {
        switch (deleteType)
        {
          case "All":
            HttpContext.Response.Cookies.Delete("c_cookie");
            HttpContext.Response.Cookies.Delete("u_cookie");
            HttpContext.Response.Cookies.Delete("p_cookie");
            HttpContext.Response.Cookies.Delete("dname_cookie");
            HttpContext.Response.Cookies.Delete("token");
            HttpContext.Response.Cookies.Delete("expires_in");
            HttpContext.Response.Cookies.Delete("deviceDetail");
            HttpContext.Response.Cookies.Delete("path_coockie");
            HttpContext.Response.Cookies.Delete("UnauthorizedPage");
            HttpContext.Response.Cookies.Delete("SessionExpired");
            break;

          case "ComapnyUserPasswordCookie":
            HttpContext.Response.Cookies.Delete("c_cookie");
            HttpContext.Response.Cookies.Delete("u_cookie");
            HttpContext.Response.Cookies.Delete("p_cookie");
            HttpContext.Response.Cookies.Delete("expires_in");
            HttpContext.Response.Cookies.Delete("deviceDetail");
            HttpContext.Response.Cookies.Delete("UnauthorizedPage");
            HttpContext.Response.Cookies.Delete("SessionExpired");
            break;

          default: break;
        }
      }
      catch (Exception ex)
      {
        tc.TrackException(ex);
      }
    }

    [HttpPost]
    public IActionResult InvalidTokenAuthRouting([FromBody] string loginModel)
    {
      HttpContext.Response.Cookies.Append("UnauthorizedPage", "404");
      @ViewBag.message = fluctuationInNetwork;
      return View(UsernameViewPage);
    }

    [HttpPost]
    public IActionResult ReLoginPage(string loginModel)
    {
      RemoveCustomCookies("All");
      return View(UsernameViewPage);
    }

    [HttpPost]
    public async Task<bool> CheckUserExist(string urlQuery)
    {
      string methodName = "CheckUserExist()";

      bool result = false;
      try
      {
        string encryptedURL = System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(urlQuery)).Trim().ToLower();
        string tenantName = encryptedURL.Split('@')[2].Split('.')[0];
        string username = encryptedURL.Split('@')[0] + '@' + encryptedURL.Split('@')[1];

        TcTrack(methodName, " request sending UsernameAuthentication() in authguard. tenantName:" + tenantName + ", userName:" + username + ", encryptedURL:" + encryptedURL);
        result = await remoteservice.UsernameAuthentication(tenantName, username);
        if (!result)
        {
          string connectionStringTenantId = CommonConfiguration.GetConfig(context, "ConnectionStringTenantId");
          string applicationName = await GetApplicationName();
          string connectionStringType = CommonConfiguration.GetConfig(context, "ConnectionStringType");

          string connectionString = await keyvaultServiceConnectionStringApi.GetConnectionStringFromKeyVault(connectionStringTenantId, applicationName, connectionStringType);
          result = await new SqlUtilConfiguration().checkUserInDB(tenantName, username, connectionString, tc);
        }
        if (!result) // case ~ if false thn show invalid page.
          @ViewBag.message = userNotExist;
      }
      catch (Exception ex)
      {
        tc.TrackException(new Exception("\n" + ex + " " + methodName));
        @ViewBag.message = technicalIssue;
      }

      return result;
    }

    public bool SendErrorToLoginPage(string _errorMessage)
    {
      string methodName = "SendErrorToLoginPage()";

      bool result = false;
      try
      {
        switch (_errorMessage)
        {
          case "FAILED TO FETCH CAPABILITIES":
            ErrorMessage = somethingWrong;
            break;

          default:
            ErrorMessage = _errorMessage;
            break;
        }

        RemoveCustomCookies("All");
        result = true;
      }
      catch (Exception ex)
      {
        tc.TrackException(new Exception("\n" + ex + " " + methodName));
        @ViewBag.message = _errorMessage;
      }

      return result;
    }

    private void TcTrack(string methodNameTC, string tracktrace = "CustomTelemetry")
    {
      tc.TrackRequest("Track " + methodNameTC + " " + ApplicationName + " Request started", DateTimeOffset.UtcNow, new TimeSpan(0, 0, 3), "200", true);
      tc.TrackMetric(methodNameTC + " " + ApplicationName + " Metric", 100);
      tc.TrackEvent("Tracked Event " + methodNameTC + " " + ApplicationName);
      tc.TrackTrace(tracktrace);
    }

    private void TcException(Exception ex, string methodNameTC, string exceptionMessage = "exceptionMessage")
    {
      var datetime = new DateTime();
      string innerExceptionMessage = GetInnerException(ex);
      string msgInfo = "{\n\"MethodName\": " + methodNameTC + "," + "\n\"datetime\":" + datetime + ",\n\"Message\":" + ex.Message +
          ",\n\"InnerException\":" + innerExceptionMessage + ",\n\"StackTrace\":" + ex.StackTrace + ",\n\"exceptionMessage\":" + exceptionMessage + "}";

      tc.TrackException(new Exception("\n" + msgInfo));
      tc.Context.Device.OperatingSystem = Environment.OSVersion.ToString();
    }

    private static string GetInnerException(Exception ex)
    {
      if (ex.InnerException != null)
      {
        return ex.InnerException.Message + "\t" + ex.InnerException.Data + "\t" + ex.InnerException.StackTrace;
      }
      else
      {
        return null;
      }
    }

    private async Task<string> GetTenantUserNamePwdValue(string value = "tenantName")
    {
      string methodName = "GetTenantUserNamePwdValue()";

      string response = null;
      try
      {
        switch (value)
        {
          case "domainName":
            response = CommonConfiguration.GetConfig(context, "Extension").Split('.')[1] + "." + CommonConfiguration.GetConfig(context, "Extension").Split('.')[2];
            break;
          case "url":
            response = HttpContext.Request.Host.Host.ToString();
            //response = "visur.io";
            //response = "v2rdemo.visur.io";
            break;
          case "tenantName":
            response = HttpContext.Request.Host.Host.Split('.')[0];
            //response = "v2rdemo";
            break;
          case "username":
            response = Request.Cookies["u_cookie"];
            break;
          case "password":
            response = Request.Cookies["p_cookie"];
            break;
        }

        /*
         * It will work only for local environment.
         */
        //if ((value == "tenantName" || value == "url") && response == "localhost")
        //  response = "v2rdemo";

        if (!await IsNullOrEmptyOrWhiteSpace(response) && value != "password")
          response = response.ToLower().Trim();
      }
      catch (Exception ex)
      {
        TcException(ex, methodName);
      }

      return await Task.FromResult(response);
    }

    private async Task<bool> IsNullOrEmptyOrWhiteSpace(string value)
    {
      string methodName = "IsNullOrEmptyOrWhiteSpace()";

      bool result = false;
      try
      {
        if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
          result = true;
      }
      catch (Exception ex)
      {
        TcException(ex, methodName);
      }

      return await Task.FromResult(result);
    }
    private async Task<List<TenantApplicationNames>> GetTenantApplicationsListAsync(string tenantName)
    {
      var appNameList = new List<TenantApplicationNames>();
      var connectionStringTenantId = CommonConfiguration.GetConfig(context, "ConnectionStringTenantId");
      var connectionStringTenantName = CommonConfiguration.GetConfig(context, "ConnectionStringTenantName");
      var connectionStringType = CommonConfiguration.GetConfig(context, "ConnectionStringType");
      var domain = CommonConfiguration.GetConfig(context, "Domain");
      var environemnt = CommonConfiguration.GetConfig(context, "environment");

      var applicationName = string.Empty;
      if (environemnt == "dev")
      {
        applicationName = "energyapp";
      }
      else
      {
        applicationName = await GetApplicationName();
      }

      var connString = await keyvaultServiceConnectionStringApi.GetConnectionStringFromKeyVault(connectionStringTenantId, applicationName, connectionStringType);
      var appQuery = "select * from ApplicationRoutes where tenantName='" + tenantName + "' and isDeleted='false' and domain='" + domain + "' for json path";

      var result = await new SqlUtilConfiguration().SQLExceuteNonQuery(appQuery, connString, tc);

      if (!string.IsNullOrEmpty(result) && !string.IsNullOrWhiteSpace(result))
      {
        List<JObject> tableResult = JArray.Parse(result).ToObject<List<JObject>>();

        foreach (var jsonObject in tableResult)
        {
          var appId = jsonObject["ApplicationId"].ToString();
          var tenantId = jsonObject["TenantId"].ToString();
          var appTenantName = jsonObject["TenantName"].ToString();

          var applicationNames = new TenantApplicationNames
          {
            TenantId = tenantId + "_" + appId,
            TenantName = appTenantName,
            ApplicationId = appId
          };
          appNameList.Add(applicationNames);
        }
      }
      else
      {
        TcTrack("GetTenantApplicationsListAsync()" + tenantName + " Empty result");
      }
      return appNameList;
    }

    private async Task<string> GetApplicationName()
    {
      var applicationName = CommonConfiguration.GetConfig(context, "ApplicationName");
      var appPostFix = CommonConfiguration.GetConfig(context, "ApplicationPostFix");
      var appFullName = (applicationName + appPostFix).ToLower();
      return appFullName;
    }

    private async Task<string> GetAuthOnRouteMessageString(LoginModel loginModel, string messageType, string applicationName, string payload)
    {
      AuthOnRouteMessageInfo authMessage = new AuthOnRouteMessageInfo()
      {
        Application = applicationName,
        LoginModel = loginModel,
        MessageType = messageType,
        Payload = payload
      };

      string msg = JsonConvert.SerializeObject(authMessage);
      return msg;
    }

    [HttpPost, Route("TestConnectionString")]
    public async Task<string> TestConnectionString(string tenantId, string app, string conType, string tenantName, string options, string env)
    {
      var str = await keyvaultServiceConnectionStringApi.GetConnectionStringFromKeyVault(tenantId, app, conType, tenantName, options, env);
      return str;
    }

    [HttpGet]
    public async Task<string> GetCapabilityDetails()
    {
      string result = null;
      tc.TrackTrace("CreateUpdatePeopleEntities()");
      var message = new MessageWrapper();
      message.DataType = "CapabilityDetails";
      message.MessageKind = "READ";
      message.ApplicationName = await GetApplicationName();
      if (message != null)
      {
        var wrapper = JsonConvert.SerializeObject(message);
        var client = ServiceProxy.Create<ICommunication>(new Uri("fabric:/ConstructionApp/ConstructionApi"), listenerName: "Construction");
        result = await client.OnRouteMessageaAsync(wrapper);
        tc.TrackTrace("ReadCapabilityDetails() result:" + result);

      }
      return result;
    }
  }
}
