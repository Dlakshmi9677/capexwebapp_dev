using Apache.Ignite.Core.Client;
using CapexApp.Models;
using ICommonInterfaces;
using ICommonInterfaces.KeyvaultService;
using Microsoft.ApplicationInsights;
using Microsoft.ServiceFabric.Services.Remoting.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CapexApp.SqlUtil
{
  public class SqlUtilConfiguration
  {
    public KeyvaultServiceConnectionStringApi keyvaultServiceConnectionStringApi;
    public SqlUtilConfiguration()
    {
      keyvaultServiceConnectionStringApi = new KeyvaultServiceConnectionStringApi();
    }
    public async Task<bool> checkUserInDB(string tenantName, string username, string connString, TelemetryClient telemetryClient)
    {
      var response = "";
      bool result = false;
      string methodName = "checkUserInDB()";
      try
      {
        if (!string.IsNullOrEmpty(connString) || !string.IsNullOrWhiteSpace(connString))
        {
          using (var connection = new SqlConnection(connString))
          {
            connection.Open();
            string tableQuery = "SELECT [userStatus] FROM  [dbo].[" + tenantName + "_User_Table]  where [email] = '" + username + "' and isDeleted='False'";
            using (var command = new SqlCommand(tableQuery, connection))
            {
              command.CommandTimeout = 60;
              SqlDataReader sqlReader = command.ExecuteReader();
              while (sqlReader.Read())
              {
                response = (string)sqlReader["userStatus"];
                result = response == "newUser" ? true : false;
              }
            }
            if (connection != null)
              connection.Close();
          }
        }
        else
        {
          telemetryClient.TrackTrace(methodName + " connectionString can not be null or empty");
          result = true;
        }
      }
      catch (Exception ex)
      {
        telemetryClient.TrackException(new Exception("\n" + ex + " " + methodName));
      }
      return await Task.FromResult(result);
    }
    public async Task<string> SQLExceuteNonQuery(string dbQuery, string connectionString, TelemetryClient telementryClient)
    {
      var result = "";
      string methodName = "SQLExceuteQuery()";
      try
      {
        if (!string.IsNullOrEmpty(connectionString) || !string.IsNullOrWhiteSpace(connectionString))
        {
          using (var connection = new SqlConnection(connectionString))
          {
            connection.Open();
            var command = connection.CreateCommand();
            command.CommandText = dbQuery;
            command.ExecuteNonQuery();
            using (SqlDataReader reader = command.ExecuteReader())
            {
              while (reader.Read())
              {
                result += reader.GetString(0);
              }
            }
            if (connection != null)
              connection.Close();
          }
        }
        else
        {
          telementryClient.TrackTrace(methodName + " connectionString can not be null or empty");
        }
      }
      catch (Exception ex)
      {
        telementryClient.TrackException(new Exception("\n" + ex + " " + methodName));
      }
      return await Task.FromResult(result);
    }
    private Dictionary<string, object> SqlReaderDictObject(SqlDataReader reader)
    {
      return Enumerable.Range(0, reader.FieldCount).ToDictionary(reader.GetName, reader.GetValue);
    }

    public List<JObject> ExecuteDbQuery(string connectionString, string dbQuery, List<SqlParameter> parametersList = null)
    {
      List<JObject> objectList = new List<JObject>();
      using (var connection = new SqlConnection(connectionString))
      {
        connection.Open();
        try
        {
          using (SqlCommand command = new SqlCommand(dbQuery, connection))
          {
            if (parametersList != null && parametersList.Count > 0)
            {
              command.Parameters.AddRange(parametersList.ToArray());
            }
            using (SqlDataReader reader = command.ExecuteReader())
            {
              while (reader.Read())
              {
                var dictObject = SqlReaderDictObject(reader);
                var tempData = JsonConvert.SerializeObject(dictObject);
                var jsonData = JObject.Parse(tempData);
                objectList.Add(jsonData);
              }
              if (!reader.IsClosed)
              {
                reader.Close();
              }
            }
          }
        }
        catch (Exception ex)
        {
          throw;
        }
        finally
        {
          if ((System.Data.ConnectionState.Open.Equals(connection.State)))
          {
            connection.Close();
          }
        }
      }
      return objectList;
    }


    public async Task<string> GetConnectionStringFromKeyVault(string tenantId, string applicationName, string connectionStringType)
    {

      try
      {
        string connectionString = await keyvaultServiceConnectionStringApi.GetConnectionStringFromKeyVault(tenantId, applicationName, connectionStringType);
        return connectionString;
      }
#pragma warning disable
      catch (Exception ex)
#pragma warning restore
      {
        throw;
      }

    }
  }
}
