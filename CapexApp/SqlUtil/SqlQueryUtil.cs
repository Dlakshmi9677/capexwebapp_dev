using CapexApp.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapexApp.SqlUtil
{
  public class SqlQueryUtil
  {
    public string readSqlQueryBasedOnDataType(string dataType, string entityType, string entityName, string schemaName)
    {
      string query = "";
      switch (dataType)
      {
        case Constants.SCHEMA_PAGE:
          if (Constants.FIXED_MENU != entityType)
          {
            query = "select schemapage from LeftNav where entitytype = '" + entityType + "'";
          }
          else
          {
            query = "select schemapage from FixedMenus where entityname = '" + entityName + "'";
          }
          break;

        case Constants.SCHEMA:
          query = "select \"schema\" from Commonformsschema  where schemaname='" + schemaName + "' order by [Version] desc";
          //query = "select schema from Commonformschema  where schemaname='" + entityType + "' COLLATE NOCASE order by version desc limit 1;";
          break;
        case Constants.FILTER:
          query = "select * from filter where schemapage='" + schemaName + "' ";
         //query = "select * from filter where schemapage='" + entityType + "' COLLATE NOCASE;";

          break;




      }
      return query;
    }

    public string generateReadRecordQuery(string tableName, string dataType, string key, string entityId, string filterBy, bool isPrimary, string parentEntityId, string type)
    {
      string query = null;
      try
      {
        if (!string.IsNullOrEmpty(key))
        {
          query = "select " + SQLiteCommonColumns.Payload + ", " + SQLiteCommonColumns.DataPointer + " from " + tableName + " where " + SQLiteCommonColumns.IsDefault + "= 'true' and datapointer = '" + key + "';";
        }
        else if (string.IsNullOrEmpty(filterBy) && string.IsNullOrEmpty(entityId))
        {
          query = "select " + SQLiteCommonColumns.Parent + "," + SQLiteCommonColumns.Payload + "from " + tableName + "  where " + SQLiteCommonColumns.Parent + " = '" + entityId + "' and JSON_VALUE(" + SQLiteCommonColumns.Payload + ", '$." + filterBy + ") = false;";
        }
        else if (tableName.ToLower() == TableNames.UserSettings.ToLower())
        {
          if (dataType == Constants.USER_DETAIL_TABLE)
          {
            query = "select " + SQLiteCommonColumns.UserName + "," + SQLiteCommonColumns.EmailId + " as email," + SQLiteCommonColumns.LogInTime + " as lastlogin," + SQLiteCommonColumns.UserId + " as uuid from " + TableNames.UserTable + " where " + SQLiteCommonColumns.UserId + " = '" + entityId + "';";
          }
          else
          {
            query = "select " + SQLiteCommonColumns.Parent + ", " + SQLiteCommonColumns.UUID + "," + SQLiteCommonColumns.Payload + " from " + tableName + "  where " + SQLiteCommonColumns.UUID + " = '" + entityId + "';";
          }
        }
        else if (string.IsNullOrEmpty(entityId))
        {
          query = "select " + SQLiteCommonColumns.Parent + "," + SQLiteCommonColumns.Payload + " from " + tableName + ";";
        }
        else if (isPrimary && string.IsNullOrEmpty(parentEntityId))
        {
          query = "select " + SQLiteCommonColumns.Parent + "," + SQLiteCommonColumns.Payload + " from " + tableName + " where " + SQLiteCommonColumns.UUID + " = '" + entityId + "';";
        }
        //else if (tableName == UserSettings)
        //{
        //  query = "select " + SQLiteCommonColumns.UserName + "," + SQLiteCommonColumns.EmailId + "," + SQLiteCommonColumns.LogInTime + " from " + UserTable + ";";
        //}
        //else if (tableName == UserSettingsSync)
        //{
        //  query = " select " + LastSyncColumns.SyncStartTime + "," + LastSyncColumns.SyncEndTime + " from " + LastSyncTable + " order by syncstarttime desc" + ";";
        //}
        else
        {
          if (!string.IsNullOrEmpty(parentEntityId))
          {
            if (isPrimary)
            {
              if (type == "array")
              {
                query = "select " + SQLiteCommonColumns.Parent + "," + SQLiteCommonColumns.Payload + " from " + tableName + " where " + SQLiteCommonColumns.ParentEntityId + " = '" + entityId + "' and (" + SQLiteCommonColumns.IsDeleted + " = false or " + SQLiteCommonColumns.IsDeleted + " = 'false' or " + SQLiteCommonColumns.IsDeleted + " is null);";
              }
              else
              {
                query = "select " + SQLiteCommonColumns.Parent + "," + SQLiteCommonColumns.Payload + " from " + tableName + " where " + SQLiteCommonColumns.UUID + " = '" + entityId + "' and (" + SQLiteCommonColumns.IsDeleted + " = false or " + SQLiteCommonColumns.IsDeleted + " = 'false' or " + SQLiteCommonColumns.IsDeleted + " is null);";
              }
            }
            else
            {
              query = "select " + SQLiteCommonColumns.Parent + "," + SQLiteCommonColumns.UUID + "," + SQLiteCommonColumns.Payload + " from " + tableName + " where " + SQLiteCommonColumns.Parent + " = '" + entityId + "' AND " + SQLiteCommonColumns.ParentEntityId + " = '" + parentEntityId + "' AND (" + SQLiteCommonColumns.IsDeleted + " = false or " + SQLiteCommonColumns.IsDeleted + " = 'false' or " + SQLiteCommonColumns.IsDeleted + " is null);";
            }
          }
          else
          {
            query = "select " + SQLiteCommonColumns.Parent + "," + SQLiteCommonColumns.Payload + " from " + tableName + " where " + SQLiteCommonColumns.Parent + " = '" + entityId + "' AND " + SQLiteCommonColumns.ParentEntityId + " is null AND (" + SQLiteCommonColumns.IsDeleted + " = false or " + SQLiteCommonColumns.IsDeleted + " = 'false' or " + SQLiteCommonColumns.IsDeleted + " is null);";
          }
        }
        if (tableName == TableNames.IMAGES)
        {
          query = "select " + SQLiteCommonColumns.UUID + ", " + SQLiteCommonColumns.Parent + ", " + SQLiteCommonColumns.Payload + ", " + SQLiteCommonColumns.Thumbnail + ", " + SQLiteCommonColumns.Altitude + ", " + SQLiteCommonColumns.Longitude + ", " + SQLiteCommonColumns.Latitude + " from " + tableName + " where " + SQLiteCommonColumns.Parent + " = '" + entityId + "' AND " + SQLiteCommonColumns.ParentEntityId + " = '" + parentEntityId + "'  AND(" + SQLiteCommonColumns.IsDeleted + " = 'false' OR " + SQLiteCommonColumns.IsDeleted + " = false OR " + SQLiteCommonColumns.IsDeleted + " is null);";
        }
      }
      catch (Exception ex)
      {
        //this.logger.error('Exception in generateReadRecordQuery() of QueryBuildOperation.ts Exception is :: ', error);
      }
      return query;

    }


    public string generatePrimaryTableQuery(Object fieldMapping, string primaryTable, string entityid)
    {
      //var fieldkeys = fieldMapping != null ? fieldMapping. Object.keys(fieldMapping) : new JArray();//{ "Number":"DailyReport.Header:WDSNumber"} => ["Number"]
      string queryFieldForSingleTable = "";
      string queryFieldForMultipleTable = "";
      string queryPayload = "";
      string queryTables = "";
      string queryConditions = "";
      //string tableNameArray = [];
      string join = "left";
      string primary_Table = "";
      string flattenColumn = "";
      //      if (!isNullOrUndefined(primaryTable))
      //      {
      //        primary_Table = primaryTable.replace(":", "_").replace(".", "_");
      //      }
      //      for (let key of fieldkeys)
      //      {
      //        let field = fieldMapping[key];
      //        let formFields = (< string > field).split(":");  //DailyReport.Header:WDSNumber

      //        if (formFields && formFields.length == 2)
      //        {
      //          let column = formFields[1]; //WDSNumber
      //          let tableName = formFields[0].replace(".", "_");//DailyReport_Header


      //          if (!tableNameArray.includes(tableName))
      //          {
      //            // check alternative way of doing.. because uuid need to take only from primary table
      //            if (tableName == primary_Table)
      //            {
      //              queryPayload += ` ${ tableName}.${ SQLiteCommonColumns.ParentEntityId}, ${ tableName}.${ SQLiteCommonColumns.Parent},`;
      //            }
      //            tableNameArray.push(tableName);
      //            queryPayload += ` ${ tableName}.${ SQLiteCommonColumns.Payload}
      //            AS ${ tableName}
      //            _payload,`;
      //            queryTables += `${ tableName} ${ join}
      //            join `;
      //          }
      //          if (SQLiteCommonColumns.DefaultColumnNames.includes(column.toLowerCase()))
      //          {
      //            queryPayload += ` ${ tableName}.${ column} as ${ column},`;
      //            flattenColumn += `, ${ column} as ${ key}`;
      //          }
      //          else
      //          {
      //            queryFieldForSingleTable += `case json_type(payload, '$.${column}') is null when 1 then 'NULL' else json_extract(payload, '$.${column}') end ${ key}, `;
      //            queryFieldForMultipleTable += `case json_type(${ tableName}
      //            _payload,'$.${column}') is NULL when 1 then 'NULL' else json_extract(${ tableName}
      //      _payload,'$.${column}') end ${ key}, `;
      //    }
      //  }
      //}
      //let query = ``;
      //if (tableNameArray.length == 1)
      //{
      //  queryFieldForSingleTable = queryFieldForSingleTable.substring(0, queryFieldForSingleTable.lastIndexOf(','));
      //  query = `select ${ SQLiteCommonColumns.UUID}, ${ SQLiteCommonColumns.UUID}  as entityid, ${ SQLiteCommonColumns.ParentEntityId}  ,  ${ queryFieldForSingleTable} ${ flattenColumn}
      //  from  ${ tableNameArray[0]}`;
      //}
      //else
      //{
      //  for (let i = 0; i < tableNameArray.length - 1; i++)
      //  {
      //    queryConditions += `${ tableNameArray[i]}.${ SQLiteCommonColumns.Parent} = ${ tableNameArray[i + 1]}.${ SQLiteCommonColumns.Parent}
      //    and `;
      //  }
      //  queryFieldForMultipleTable = queryFieldForMultipleTable.substring(0, queryFieldForMultipleTable.lastIndexOf(','));
      //  queryPayload = queryPayload.substring(0, queryPayload.lastIndexOf(','));
      //  queryTables = queryTables.substring(0, queryTables.lastIndexOf(`${ join}`));
      //  queryConditions = queryConditions.substring(0, queryConditions.lastIndexOf('and'));
      //  // Parent will be uuid of primary table and in all child forms parent is refering to uuid/parent of primary table
      //  query = `select ${ SQLiteCommonColumns.Parent} as ${ SQLiteCommonColumns.UUID}, ${ SQLiteCommonColumns.Parent}  as ${ SQLiteCommonColumns.EntityId}, ${ SQLiteCommonColumns.ParentEntityId}  as ${ SQLiteCommonColumns.ParentEntityId},  ${ queryFieldForMultipleTable} ${ flattenColumn}
      //  from(select ${ queryPayload}
      //  from ${ queryTables}
      //  on ${ queryConditions}) as temp `;

      //}
      return "";
    }



  }

}
