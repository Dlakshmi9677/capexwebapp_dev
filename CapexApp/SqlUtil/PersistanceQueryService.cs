using CapexApp.commonconfig;
using CapexApp.keyvault;
using CapexApp.Models;
using ICommonInterfaces.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Fabric;
using System.Linq;
using System.Threading.Tasks;


namespace CapexApp.SqlUtil
{
  public class PersistanceQueryService
  {

    private readonly ServiceContext context;
    private SqlUtilConfiguration sqlutilInstance;
    private SqlQueryUtil queryUtilInstance;
    public ConnectionStringFromKeyVault connectionStringFromKeyVault;
    public PersistanceQueryService(ServiceContext context)
    {
      this.context = context;
      this.sqlutilInstance = new SqlUtilConfiguration();
      this.queryUtilInstance = new SqlQueryUtil();
      connectionStringFromKeyVault = new ConnectionStringFromKeyVault(context);
    }


    public async Task<string> readSchemaPageFromSql(MessageWrapper message)
    {
      string result = null;
      try
      {
        JObject payloadJson = JsonConvert.DeserializeObject<JObject>(message.Payload);
        string connectionString = await connectionStringFromKeyVault.GetConnectionStringFromKeyVaultServer(message.ApplicationName);
        string query = queryUtilInstance.readSqlQueryBasedOnDataType(payloadJson["dataType"].ToString(),payloadJson["entityType"].ToString(), payloadJson["entityName"].ToString(), payloadJson["schema"].ToString());
        var response = sqlutilInstance.ExecuteDbQuery(connectionString, query);
        result = JsonConvert.SerializeObject(response);
      }
      catch (Exception ex)
      {
        throw;
      }
      return result;
    }
    public async Task<string> readDataFromSql(MessageWrapper message)
    {
      string result = null;
      try
      {
        ParsePayload payloadJson = JsonConvert.DeserializeObject<ParsePayload>(message.Payload);
        string tableName = payloadJson.tableName;
        string dataType = payloadJson.dataType;
        string entityId = payloadJson.entityId;
        string filterBy = payloadJson.filterBy;
        bool primary = payloadJson.primary;
        string key = payloadJson.key;
        string parentEntityId = payloadJson.parentEntityId;
        string type = payloadJson.type;
        string query = payloadJson.query;
        if (string.IsNullOrEmpty(query))
        {
          if (string.IsNullOrEmpty(key))
          {
            query = queryUtilInstance.generateReadRecordQuery(tableName, dataType, key, entityId, filterBy, primary, parentEntityId, type);
          }
          else
          {
            query = queryUtilInstance.generateReadRecordQuery(tableName, dataType, key, entityId, filterBy, primary, parentEntityId, type);
          }
        }
         
        string connectionString = await connectionStringFromKeyVault.GetConnectionStringFromKeyVaultTenant(message.TenantID,message.ApplicationName);
        var response = sqlutilInstance.ExecuteDbQuery(connectionString, query);
        result = JsonConvert.SerializeObject(response);
      }
      catch (Exception ex)
      {
        throw;
      }
      return result;
    }
    public async Task<string> readTableDataFromSql(MessageWrapper message)
    {
      string result = null;
      try
      {
        JObject payloadJson = JsonConvert.DeserializeObject<JObject>(message.Payload);
        string tableName = payloadJson["tableName"].ToString();
        string entityId = payloadJson["entityId"].ToString();
        string type = payloadJson["type"].ToString();
        string sourceInfo = payloadJson["sourceInfo"].ToString();
        string query = payloadJson["query"].ToString();
        string queryParam = payloadJson["queryParam"].ToString();
        string filterData = payloadJson["filterData"].ToString();
        string key = payloadJson["key"].ToString();




        //string query = null;
        if (string.IsNullOrEmpty(key))
        {
          //query = queryUtilInstance.generateReadRecordQuery(tableName, dataType, key, entityId, filterBy, primary, parentEntityId, type);
        }
        else
        {
          //query = queryUtilInstance.generateReadRecordQuery(tableName, dataType, key, entityId, filterBy, primary, parentEntityId, type);
        }

        string connectionString = await connectionStringFromKeyVault.GetConnectionStringFromKeyVaultTenant(message.TenantID,message.ApplicationName);
        var response = sqlutilInstance.ExecuteDbQuery(connectionString, query);
        result = JsonConvert.SerializeObject(response);
      }
      catch (Exception ex)
      {
        throw;
      }
      return result;
    }

  }
}
